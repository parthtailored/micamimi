<?php
/**
 * @package     Joomla.Administrator
 * @subpackage  com_mica
 *
 * @copyright   Copyright (C) 2005 - 2017 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

require_once( JPATH_COMPONENT.DIRECTORY_SEPARATOR.'helpers/Workspace.php' );
require_once( JPATH_COMPONENT.DIRECTORY_SEPARATOR.'helpers/css.php' );

/**
 * MICA SEIShowresults model.
 *
 * @since  1.6
 */
class MicaModelSEIShowresults extends JModelLegacy
{

	var $_data                  = null;
	var $_total                 = null;
	var $_pagination            = null;
	var $townsearchvariable     = "OGR_FID";
	var $urbansearchvariable    = "OGR_FID";
	var $districtsearchvariable = "OGR_FID";
	var $statesearchvariable    = "OGR_FID";
	var $ruralprefix            = "Rural_";
	var $urnbanprefix           = "Urban_";
	var $totalprefix            = "Total_";

	function __construct(){
		parent::__construct();

		$app  = JFactory::getApplication();
		$user = JFactory::getUser();
		if($user->id == 0){
			$app->redirect("index.php");exit;
		}
		$this->_table_prefix = '#__mica_';
		$limit               = $app->input->get('limit','limit', 25, 0);
		//$limitstart        = $mainframe->getUserStateFromRequest($context.'limitstart','limitstart',0);
		//$limitstart        = ($limit != 0 ? (floor($limitstart / $limit) * $limit) : 0);
		$jlimitstart         = $app->input->get('limitstart');
		if(!$jlimitstart)	$limitstart=0;

		$this->setState('limit',1);
		$page = ($app->input->get("limitstart") != "") ? $app->input->get("limitstart") : 0;
		$statingpage = null;
		$this->setState('limitstart', $page);
		//$this->setState('limitstart',$limitstart);
	}

	/**
	 * A function called from view to get states.
	 */
	public function getStateItems(){
		$res = null;
		$db    = $this->getDBO();
		$query = "SELECT id, name FROM ".$db->quoteName('rail_state')." GROUP BY ".$db->quoteName('name');
		$db->setQuery($query);
		try {
			$res = $db->loadObjectList();
		} catch (Exception $e) {

		}
		return $res;
	}

	/**
	 *
	 */
	public function getAttributeCheck($db_table, $attributes, $m_type = null){
		$app     = JFactory::getApplication();
		$session =JFactory::getSession();
		$db      = $this->getDBO();

		$query = "DESCRIBE ".$db_table;
		$db->setQuery($query);
		$fields = $db->loadObjectList();

		//$m_types=$session->get('m_type');
		foreach($fields as $eachfield){
			$tablefield[] = $eachfield->Field;
		}

		if(!is_array($attributes)){
			$attributes=explode(",",$attributes);
		}
		$attrstr=array();

		//foreach($m_types as $m_type)
		{
			foreach($attributes as $eachattr)
			{
				$status=0;
				if($m_type=="Total" && $eachattr!="MPI" && $eachattr!="Score")
				{
					if(in_array($this->totalprefix.$eachattr,$tablefield))
					{
						$attrstr[]=$this->totalprefix.$eachattr;
						$status=1;
					}

				}
				if($m_type=="Urban" && $eachattr!="MPI"  && $eachattr!="Score")
				{
					if(in_array($this->urnbanprefix.$eachattr,$tablefield))
					{
						$attrstr[]=$this->urnbanprefix.$eachattr;
						$status=1;
					}
				}
				if($m_type=="Rural" && $eachattr!="MPI"  && $eachattr!="Score")
				{
					if(in_array($this->ruralprefix.$eachattr,$tablefield))
					{
						$attrstr[]=$this->ruralprefix.$eachattr;
						$status=1;
					}
				}
				if($eachattr=="MPI")
				{
					$m_type_rating = $app->input->get("m_type_rating",array("Total"), 'raw');
					foreach($m_type_rating as $eachprefix)
					{
						if(in_array($eachprefix."_".$eachattr,$tablefield))
						{
							$attrstr[]=$eachprefix."_".$eachattr;
							$status=1;
						}
					}

				}

				if($status==0)
				{
					if(in_array($eachattr,$tablefield))
					{
					$attrstr[]=$eachattr;
					}
				}
			}
		}

		//echo implode(",",$attrstr);exit;
		return implode(",",$attrstr);
	}

	/**
	 * A function called from view.
	 */
	public function getCustomData($infotype = null, $pagefromcontroller = null, $ajaxcall = false, $pdistrict ='', $chartSelectedVariable = null, $initialGraph = 0 ){

		$session = JFactory::getSession();
		$app     = JFactory::getApplication();
		$db      = $this->getDBO();

		$states       = $session->get('state');
		$mystates     = explode(",",$states);
		$mystates     = array_filter($mystates);
		$statesinsert = implode("','",$mystates);

		$district     = $session->get('district');
		//echo "<pre/>";print_r($district);exit;

		if($ajaxcall==true  && $pdistrict!="")
		{
			$district= $pdistrict;
		}

		$town         = $session->get('town');
		$urban        = $session->get('urban');
		$m_types      = $session->get('m_type');

		$attributes   = $session->get('attributes');
		$attributes   = explode(",",$attributes);

		//if(substr_count($district,"362")) { $district=$district.",363"; }
		$mytmp = array_filter($attributes);
		if($pagefromcontroller != ""){
			$page        = 13;
			$statingpage = 0;
		}else{
			$page        = ($app->input->get("limitstart") != "" ) ? $app->input->get("limitstart") : 0;
			$statingpage = null;
			//$this->setState('limitstart',$page); $this->setState('limit',13);
		}

		$db_table   = "";
		$attributes = array();
		$attribute  = "";
		if($town == "" && $urban == ""){
			//if($district!="")
			$db_table = "india_information";
		 	// else
		 	// 	$db_table="rail_state";

			foreach($m_types as $m_type){
				foreach($mytmp as $eachvar){
					if($m_type == "Total" ){
						if(strpos($eachvar, $this->urnbanprefix)=== false && strpos($eachvar, $this->ruralprefix)=== false &&  strpos($eachvar, $this->totalprefix)=== false){
							$attributes[] = $this->totalprefix.$eachvar;
						}else{
							$attributes[] = $eachvar;
						}
					}else if($m_type == "Rural"){
				 		//  $db_table="india_information";
						if(strpos($eachvar, $this->urnbanprefix)=== false && strpos($eachvar, $this->ruralprefix)=== false && strpos($eachvar, $this->totalprefix)=== false){
				 	  		$attributes[] = $this->ruralprefix.$eachvar;
						}else{
							$attributes[] = $eachvar;
						}
				 	}else if($m_type == "Urban"){
						//$db_table     = "india_information";
						//$attributes[] = $this->urbanprefix.$eachvar;
						if(strpos($eachvar, $this->urnbanprefix)=== false && strpos($eachvar, $this->ruralprefix)=== false &&  strpos($eachvar, $this->totalprefix)=== false){
				 	  		$attributes[] = $this->urnbanprefix.$eachvar;
						}else{
						 	$attributes[] = $eachvar;
						}
				 	}else{
						//$attributes[] = implode(",",$mytmp);
				 	}
				}
			}
		}

		$attributes = implode(",",array_unique($attributes));
		$session->set('themeticattribute',$attributes);

		if($ajaxcall == true  && $chartSelectedVariable != "")
		{
			$attributes = $chartSelectedVariable;
		}

		// Initial 5 attribute load in graph
		$initialGraph = $app->input->get("initialGraph");
		if ($initialGraph == 1) {
			$attributes = array_slice(explode(",",$attributes), 0, 5, true);
			$attributes = implode(",",$attributes);
		}

		//echo $attributes=$session->get('themeticattribute');exit;
		if(trim($district) == null || trim($district) == ","){
			if($states != "all," && $states != ""  && $states != "all"){

				$mystates = explode(",",$states);
				$mystates = array_filter($mystates);
				$mystates = implode("','",$mystates);
				$where    = "WHERE ".$db->quoteName('name')." IN ('".$mystates."') ";
				if($session->get('state_orgfid') != ""){
					$state_orgfid = $session->get('state_orgfid');
					//condition will be true when geoserver served ORG_FID, because of many shapes in db requested thes query
					$mystates1    = explode(",", $state_orgfid);
					$mystates1    = array_filter($mystates1);
					$mystates1    = implode("','", $mystates1);
					$where       .= " AND ".$this->statesearchvariable." IN ('".$mystates1."')";
				}
			}else{
				$mystates = "1=1";
				$where    = "";
				if($session->get('state_orgfid') != ""){
					$state_orgfid = $session->get('state_orgfid');
					//condition will be true when geoserver served ORG_FID, because of many shapes in db requested thes query
					$mystates1    = explode(",", $state_orgfid);
					$mystates1    = array_filter($mystates1);
					$mystates1    = implode("','", $mystates1);
					$where       .= " WHERE ".$this->statesearchvariable." IN ('".$mystates1."') ";
				}
			}

			$query = "SELECT ".$attributes." , ".$this->statesearchvariable." , name
				FROM ".$db->quoteName('rail_state')."
				".$where."
				GROUP BY ".$db->quoteName('name')."
				ORDER BY ".$db->quoteName('name');
			$db->setQuery($query);

			$session->set('activetable',"rail_state");
			$session->set('activenamevariable',"name");
			$session->set('activesearchvariable',$this->statesearchvariable);
			if($infotype != ""){
				//$session->set('popupactivedata',$db->loadObjectList());
			}else{
				//$session->set('activedata',$db->loadObjectList());
			}

			try {
				$this->_data = $db->loadObjectList();
			} catch (Exception $e) {
				$this->_data = NULL;
			}
			return $this->_data;

		}else{

			//echo $attributes=implode(",",$attributes);exit;
			if($district != "all," && $district != "all"){
				$states   = explode(",",$states);
				$states   = array_filter($states);
				$states   = implode("','",$states);

				$district = explode(",",$district);
				$district = array_filter($district);
				$district = implode("','",$district);

				$where    = "WHERE ".$this->districtsearchvariable." IN ('".$district."') AND ".$db->quoteName('state')." IN ('".$states."') ";
				if($session->get('district_orgfid') != ""){
					$state_orgfid = $session->get('district_orgfid');
					//condition will be true when geoserver served ORG_FID, because of many shapes in db requested thes query
					$mystates1    = explode(",", $state_orgfid);
					$mystates1    = array_filter($mystates1);
					$mystates1    = implode("','", $mystates1);
					$where       .= " AND ".$this->districtsearchvariable." IN ('".$mystates1."') ";
				}
			}else{

				$states = explode(",",$states);
				$states = array_filter($states);
				$states = implode("','",$states);
				$where  = "WHERE 1 = 1 AND ".$db->quoteName('state')." IN ( '".$states."')";

				if($session->get('district_orgfid') != ""){
					$state_orgfid = $session->get('district_orgfid');
					//condition will be true when geoserver served ORG_FID, because of many shapes in db requested thes query
					$mystates1    = explode(",",$state_orgfid);
					$mystates1    = array_filter($mystates1);
					$mystates1    = implode("','",$mystates1);
					$where       .= " AND ".$this->districtsearchvariable." IN ('".$mystates1."')";
				}
			}

			//  NOTE :: Remove pagination Temp - 25-03-2019
			$query = "SELECT ".$attributes." ,".$this->districtsearchvariable.", concat(distshp,'-',state) as name, state, distshp
				FROM ".$db->quoteName('india_information')."
				".$where."
					AND ".$db->quoteName('distshp')." <> ''
				ORDER BY ".$db->quoteName('state')." ASC
				";


			/*$query = "SELECT ".$attributes." ,".$this->districtsearchvariable.", concat(distshp,'-',state) as name, state, distshp
				FROM ".$db->quoteName('india_information')."
				".$where."
					AND ".$db->quoteName('distshp')." <> ''
				ORDER BY ".$db->quoteName('state')." ASC
				LIMIT ".(($statingpage === null) ? (($page)*50) : $statingpage).",".(($statingpage === null) ? 50 : 650);*/



			$db->setQuery($query);

			$session->set('activetable', "india_information");
			$session->set('activenamevariable', "distshp");
			$session->set('activesearchvariable',$this->districtsearchvariable);
			if($infotype != ""){
				//$session->set('popupactivedata',$db->loadObjectList());
			}else{
				//$session->set('activedata',$db->loadObjectList());
			}

			try {
				$this->_data = $db->loadObjectList();
			} catch (Exception $e) {
				$this->_data = NULL;
			}

			return $this->_data;
		}

	}


	/**
	 * function called from controller.
	 */
	public function getMaxForMatrix(){
		$session = JFactory::getSession();
		$db      = JFactory::getDBO();

		$table         = $session->get('activetable');
		$variable      = $session->get('themeticattribute');
		$district      = $session->get('district');
		$variable      = explode(",",$variable);
		$maxValsSelect = "";
		foreach($variable as $eachvariable){
			$maxValsSelect[] = "MAX(Convert(" . $eachvariable . ",SIGNED)) AS '" . $eachvariable . "'";
		}
		$maxValsSelect = implode(" , ",$maxValsSelect);

		//Complete the query
 		$maxValsQuery = "SELECT $maxValsSelect FROM $table WHERE OGR_FID IN (".$district.") LIMIT 0,1";
		$db->setQuery($maxValsQuery);
		return $db->loadAssocList();
	}

	/**
	 *
	 */
	public function getGraph(){
		$session = JFactory::getSession();

		$states     = $session->get('state');
		$district   = $session->get('district');
		$town       = $session->get('town');
		$urban      = $session->get('urban');
		$attributes = $session->get('attributes');

		$attributes = explode(",",$attributes);
		$attributes = array_filter($attributes);
		$attr       = array();
		$i          = 0;

		foreach($this->getCustomData(1,1) as $key=>$val){
			foreach($val as $ek=>$ev){
				if($ev == ""){
					$ev="0";
				}

				foreach($attributes as $eachattr){
					if($ek == $eachattr){
						$attr[$ek][]=$ev;
					}

					if($ek == $this->urnbanprefix.$eachattr){
						$attr[JTEXT::_($this->urnbanprefix.$eachattr)][]=$ev;
					}

					if($ek == $this->totalprefix.$eachattr){
						$attr[JTEXT::_($this->totalprefix.$eachattr)][]=$ev;
					}

					if($ek == $this->ruralprefix.$eachattr){
						$attr[JTEXT::_($this->ruralprefix.$eachattr)][]=$ev;
					}

					if($ek == "name"){
						$attrs['name'][$i]=$ev;
					}
				}
				if($i == 5){break;}
			}
			if($i == 5){break;}
			$i++;
		}

		$customattribute = $session->get('customattribute');
		$eachcustomattr  = array();
		$customattribute = explode(",",$customattribute);
		$customattribute = array_filter($customattribute);
		$z=0;

		foreach($attrs['name'] as $eachname){
			foreach($customattribute as $eachcustom){
				$eachcustom = explode(":",$eachcustom);
				$eachcustomattr[$eachcustom[0]][$z] = $this->getCustomAttributeValue($eachcustom[1],$this->_data,$eachname);
				if($z==10){break;}
			}
			if($z==5){break;}
			$z++;
		}

		$attr          = array_merge($attr,$eachcustomattr);
		$attr          = array_slice($attr,0,10);
		$arraytoreturn = "";
		$lastkey       = count(($attr));
		$z             = 1;
		foreach($attr as $key => $val){
			if($z !== $lastkey){
				$arraytoreturn .=JTEXT::_($key).";".implode(";",$val)."sln";
			}else{
				$arraytoreturn .=JTEXT::_($key).";".implode(";",$val);
			}
			$z++;
		}
		return $arraytoreturn;
	}

	/**
	 *
	 */
/*	public function getGraph_v2(){
		$session = JFactory::getSession();

		$states     = $session->get('state');
		$district   = $session->get('district');
		$town       = $session->get('town');
		$urban      = $session->get('urban');
		$attributes = $session->get('attributes');

		$attributes = explode(",",$attributes);
		$attributes = array_filter($attributes);
		$attr       = array();
		$i          = 0;

		foreach($this->getCustomData(1,1) as $key=>$val){
			foreach($val as $ek=>$ev){
				if($ev == ""){
					$ev="0";
				}

				foreach($attributes as $eachattr){
					if($ek == $eachattr){
						$attr[$ek][]=$ev;
					}

					if($ek == $this->urnbanprefix.$eachattr){
						$attr[JTEXT::_($this->urnbanprefix.$eachattr)][]=$ev;
					}

					if($ek == $this->totalprefix.$eachattr){
						$attr[JTEXT::_($this->totalprefix.$eachattr)][]=$ev;
					}

					if($ek == $this->ruralprefix.$eachattr){
						$attr[JTEXT::_($this->ruralprefix.$eachattr)][]=$ev;
					}

					if($ek == "name"){
						$attrs['name'][$i]=$ev;
					}
				}
				if($i == 5){break;}
			}
			if($i == 5){break;}
			$i++;
		}

		$customattribute = $session->get('customattribute');
		$eachcustomattr  = array();
		$customattribute = explode(",",$customattribute);
		$customattribute = array_filter($customattribute);
		$z=0;

		foreach($attrs['name'] as $eachname){
			foreach($customattribute as $eachcustom){
				$eachcustom = explode(":",$eachcustom);
				$eachcustomattr[$eachcustom[0]][$z] = $this->getCustomAttributeValue($eachcustom[1],$this->_data,$eachname);
				if($z==10){break;}
			}
			if($z==5){break;}
			$z++;
		}

		$attr          = array_merge($attr,$eachcustomattr);
		$attr          = array_slice($attr,0,10);
		$arraytoreturn = "";
		$lastkey       = count(($attr));
		$z             = 1;
		foreach($attr as $key => $val){
			if($z !== $lastkey){
				$arraytoreturn .=JTEXT::_($key).";".implode(";",$val)."sln";
			}else{
				$arraytoreturn .=JTEXT::_($key).";".implode(";",$val);
			}
			$z++;
		}

		return $arraytoreturn;
	}*/

	/*
		Get Graph Data
	*/
	public function getGraph_v2()
	{
		$session    = JFactory::getSession();
		$states     = $session->get('state');
		$district   = $session->get('district');
		$town       = $session->get('town');
		$urban      = $session->get('urban');
		$attributes = $session->get('attributes');

		$attributes = explode(",",$attributes);
		$attributes = array_filter($attributes);

		$attr       = array();
		$i          = 0;
		$datasets   = []; //dataset of all cities

		$app = JFactory::getApplication();
		$chartSelectedDistrict = $app->input->getVar("selectedDistrict");
		$chartSelectedVariable = $app->input->getVar("selectedVariable");
		$chartType             = $app->input->getVar("chartType");
		$initialGraph          = $app->input->getVar("initialGraph");

		if ($chartSelectedDistrict != "null" && $chartSelectedVariable != "null") {
			$getCustomData = $this->getCustomData(0, 0, 1, $chartSelectedDistrict, $chartSelectedVariable);
		}
		else{
			$getCustomData = $this->getCustomData(1,1);
		}

		foreach($getCustomData as $key=>$val){

			$cityname=$val->name ."~~".$val->OGR_FID;

			foreach($val as $ek=>$ev){
				if($ev == "N/a" || $ev == ""){
					$ev="0";
				}

				foreach($attributes as $eachattr){
					if($ek == $eachattr){
						//$attr[$ek][]=$ev;
						$datasets[JTEXT::_($ek)][$cityname]=$ev;
					}

					if($ek == $this->urnbanprefix.$eachattr){
						//$attr[JTEXT::_($this->urnbanprefix.$eachattr)][]=$ev;
						$datasets[JTEXT::_($this->urnbanprefix.$eachattr)][$cityname]=$ev;
					}

					if($ek == $this->totalprefix.$eachattr){
						//$attr[JTEXT::_($this->totalprefix.$eachattr)][]=$ev;
						$datasets[JTEXT::_($this->totalprefix.$eachattr)][$cityname]=$ev;
					}

					if($ek == $this->ruralprefix.$eachattr){
						//$attr[JTEXT::_($this->ruralprefix.$eachattr)][]=$ev;
						$datasets[JTEXT::_($this->ruralprefix.$eachattr)][$cityname]=$ev;
					}

					if($ek == "name"){
						$attrs['name'][$i]=$cityname;
					}
				}
			}
			$i++;
		}

		$customattribute = $session->get('customattribute');
		$eachcustomattr  = array();
		$customattribute = explode(",",$customattribute);
		$customattribute = array_filter($customattribute);

		// Adding custom varibles
		foreach($attrs['name'] as $eachnameData){
			$eachname = explode("~~",$eachnameData);
			foreach($customattribute as $eachcustom){
				$eachcustom = explode(":",$eachcustom);
				$eachcustomattr[$eachcustom[0]][$eachnameData] = $this->getCustomAttributeValue($eachcustom[1],$this->_data,$eachname[0]);
			}
		}

		$datasets = array_merge($eachcustomattr, $datasets);
		if ($chartType != "bar") {
			$datasets = array_slice($datasets,0,5);
		}

		if ($initialGraph == "1") {
			$datasets = array_slice($datasets,0,5);
		}

		$secondlevelDatasets = array ();
		$lableKey = 0;
		foreach ($datasets as $key => $dataset) {
			$secondlevelDatasets[$lableKey]["label"] = $key;
			$secondlevelDatasets[$lableKey]["data"] = array_values($dataset);

			$lableKey++;
		}

		$datasets["secondlevelDatasets"] = $secondlevelDatasets;

		return $datasets;
	}

	public function getMeterData()
	{
		$session    = JFactory::getSession();
		$states     = $session->get('state');
		$district   = $session->get('district');
		$town       = $session->get('town');
		$urban      = $session->get('urban');
		$attributes = $session->get('attributes');

		$attributes = explode(",",$attributes);
		$attributes = array_filter($attributes);
		$attr       = array();
		$i          = 0;
		$datasets   = []; //dataset of all cities

		foreach($this->getCustomData(1,1) as $key=>$val){
			$cityname = $val->name."~~".$val->OGR_FID;
			foreach($val as $ek=>$ev){ //$ek:-var name $ev:-value
				if($ev == ""){
					$ev="0";
				}

				foreach($attributes as $eachattr){
					if($ek == $this->urnbanprefix.$eachattr){
						$datasets[JTEXT::_($this->urnbanprefix.$eachattr)][$cityname]=$ev;
					}

					if($ek == $this->totalprefix.$eachattr){
						$datasets[JTEXT::_($this->totalprefix.$eachattr)][$cityname]=$ev;
					}

					if($ek == $this->ruralprefix.$eachattr){
						$datasets[JTEXT::_($this->ruralprefix.$eachattr)][$cityname]=$ev;
					}
				}
			}
		}
		return $datasets;
	}

	/*
		Get Selected Variable List for Custom Varible
	*/
	public function getSelectedVarListForCustom()
	{
		$session  = JFactory::getSession();
		$attr     = $session->get('themeticattribute');//themeticattribute

		$htmlData = '<select name="avail_attribute" id="avail_attribute" class="avial_select">
					<optgroup label="Default Variable" class="defaultvariable">';

		$eachattr = explode(",",$attr);

		foreach($eachattr as $eachattrs){
			$htmlData .= '<option value='.$eachattrs.'>'.JTEXT::_($eachattrs).'</option>';
		}

		$htmlData .= '</optgroup></select>';

		return $htmlData;
	}

	/*
		Get Selected Variable List for Speedo Meter Varible
	*/
	public function getSelectedVarListForSpeedoMeter()
	{
		$session  = JFactory::getSession();
		$attr     = $session->get('themeticattribute');//themeticattribute

		$htmlData = '<div class="row"><div class="col-sm-6"><div id="speed_variable"><div class="popup-heading"><input type="checkbox" name="metervariables_checkall" class="allcheck" id="metervariables_allcheck" /><label>Select All Variable</label></div><ul id ="variablesForMetre" class="list1 checkboxlist-popup">';

		$eachattr = explode(",",$attr);

		foreach($eachattr as $eachattrs){
			$htmlData .= '<li><input type="checkbox" class="metervariables_checkbox" id="metervariables_'.$eachattrs.'" value="'.$eachattrs.'" name="speed_variable[]" /><label for="metervariables_' . $eachattrs . '">'.JTEXT::_($eachattrs).'</label></li>';
		}

		$htmlData .= '</ul></div></div>';

		$district = $session->get('customdata');//Get Selected District

		$htmlData .= '<div class="col-sm-6"><div id="speed_region"><div class="popup-heading"><input type="checkbox" name="meterdistrict_checkall" class="allcheck" id="meterdistrict_allcheck" /><label>Select Distrct</label></div><ul id ="DistrictForMetre" class="list1 checkboxlist-popup">';

		foreach($district as $eachdata){
			$htmlData .= '<li><input type="checkbox" class="meterdistrict_checkbox" id="meterdistrict_'.$eachdata->name.'" value="'.str_replace(" ","%20",$eachdata->name).'" name="speed_region[]" /><label for="meterdistrict_' . $eachdata->name . '">'.$eachdata->name.'</label></li>';
		}

		$htmlData .= '</ul></div></div></div>';

		return $htmlData;
	}

	/*
		Get Selected District List for Graph
	*/
	public function getSelectedDistListForGraph()
	{
		$session  = JFactory::getSession();
		$district = $session->get('customdata');//Get Selected District

		$jinput   = JFactory::getApplication()->input;
		$checkedDistValue = $jinput->getVar('checkedDistValue', '');

		$selectedDistrictArray = explode(",",$checkedDistValue);

		$htmlData = '<ul id="statesForGraph" class="list1">';

		// Array sort by name
		array_multisort(array_column($district, 'name'), SORT_ASC, $district);

		foreach($district as $eachdata){
			if(in_array($eachdata->OGR_FID, $selectedDistrictArray)){
				$checked = "checked";
			}
			else{
				$checked = "";
			}
			$htmlData .= '<li><input type="checkbox" class="chartstates_checkbox"' .$checked. ' id="chartstates_'.$eachdata->name.'" value="'.$eachdata->OGR_FID.'" data-districtName="'.$eachdata->name.'" /><label for="chartstates_' . $eachdata->name . '">'.$eachdata->name.'</label></li>';
		}

		$htmlData .= '</ul>';

		return $htmlData;
	}

	/*
		Get Selected Variable List for Graph
	*/
	public function getSelectedVarListForGraph()
	{
		$session  = JFactory::getSession();
		$attr     = $session->get('themeticattribute');//themeticattribute

		$jinput   = JFactory::getApplication()->input;
		$checkedVarValue = $jinput->getVar('checkedVarValue', '');

		$selectedVarArray = explode(",",$checkedVarValue);

		$htmlData = '<ul id="variablesForGraph" class="list1">';

		$eachattr = explode(",",$attr);

		// Array sort by name
		array_multisort(array_values($eachattr), SORT_ASC, $eachattr);

		foreach($eachattr as $eachattrs){
			if(in_array($eachattrs, $selectedVarArray)){
				$checked = "checked";
			}
			else{
				$checked = "";
			}
			$htmlData .= '<li><input type="checkbox" class="chartvariables_checkbox"' .$checked. ' id="chartvariables_'.$eachattrs.'" value="'.$eachattrs.'" /><label for="chartvariables_' . $eachattrs . '">'.JTEXT::_($eachattrs).'</label></li>';
		}

		$htmlData .= '</ul>';

		return $htmlData;
	}

	/**
	 *
	 */
	public function getGraphSettings(){
		$session = JFactory::getSession();

		$states        = $session->get('state');
		$district      = $session->get('district');
		$town          = $session->get('town');
		$urban         = $session->get('urban');
		//$attributes  = $session->get('attributes');
		$data_settings = "";
		/*
			if($town!=""){
				$i=0;
				foreach($this->_data as $eachdata){
					$data_settings .= "<graph gid='".$i."'>";
					$data_settings .= "<title>".JTEXT::_($eachdata->name)."</title>";
					$data_settings .= "</graph>";
					$i++;
				}
			}else if($urban!=""){
				$i = 0;
				foreach($this->_data as $eachdata)
				{
					$data_settings .= "<graph gid='".$i."'>";
					$data_settings .= "<title>".JTEXT::_($eachdata->name)."</title>";
					$data_settings .= "</graph>";
					$i++;
				}
			}
		*/

		if(trim($district) == ","  || strlen($district) == 0){
			$i = 0;
			foreach($this->_data as $eachdata){
				$data_settings .= "<graph gid='".$i."'>";
				$data_settings .= "<title><![CDATA[".JTEXT::_($eachdata->name)."]]></title>";
				$data_settings .= "</graph>";
				$i++;
				if($i == 6){
					break;
				}
			}
		}else{
			$i = 0;
			foreach($this->_data as $eachdata){
				$data_settings .= "<graph gid='".$i."'>";
				$data_settings .= "<title><![CDATA[".JTEXT::_($eachdata->name)."]]></title>";
				$data_settings .= "</graph>";
				$i++;
				if($i == 6){
					break;
				}
			}
		}

		/*$session = JFactory::getSession();
		$attrnamesession=$session->get('customattribute');
		if(strlen($attrnamesession)>0)
		{
			$attrnamesession=explode(",",$attrnamesession);
			$attrnamesession=array_filter($attrnamesession);
			foreach($attrnamesession as $eachattr)
			{
				$name=explode(":",$eachattr);
				$data_settings .= "<graph gid='".$i."'>";
				$data_settings .= "<title><![CDATA[".$name[0]."]]></title>";
				$data_settings .= "</graph>";
				$i++;

			}

		}*/
		return $data_settings;
	}

	function scorevariablename($srprefix,$key,$type){
		foreach($srprefix as $eachprefix){
			if(strpos($key,$eachprefix) !== false){
				if($type == 1){
					return  str_replace($eachprefix,"",$key);
				}else{
					return str_replace("_","",$eachprefix);
				}
			}
		}
	}

	public function getDisplayGroup(){
		$session = JFactory::getSession();
		$db      = JFactory::getDBO();

		$urban           = $session->get('urban');
		$town            = $session->get('town');
		$district        = $session->get('district');
		$state           = $session->get('state');

		$attributes      = $session->get('attributes');
		$attributes      = explode(",",$attributes);
							$time_elapsed_secs = microtime(true) - $start;

		$customattribute = $session->get('customattribute');
		$customattribute = explode(",",$customattribute);
		$eachcustomattr  = array();
		foreach($customattribute as $eachcustom){
			$eachcustom                     = explode(":",$eachcustom);
			$eachcustomattr[$eachcustom[0]] = $eachcustom[1];
		}
		$eachcustomattr = array_filter($eachcustomattr);

		if($district != ""){
			$db_table = "district";
			$session->set('dataof',"District");
		}else if($town != ""){
			$db_table = "town";
			$session->set('dataof',"Town");
		}else if($urban != ""){
			$db_table = "urban";
			$session->set('dataof',"UA");
		}else{
			$db_table = "state";
		}

		$query ="SELECT * FROM ".$db->quoteName('#__mica_group_field')." AS allfields
				INNER JOIN ".$db->quoteName('#__mica_group')." AS grp ON ".$db->quoteName('grp.id')." = ".$db->quoteName('allfields.groupid')."
			WHERE ".$db->quoteName('allfields.table')." LIKE ".$db->quote($db_table)."
				AND ".$db->quoteName('grp.publish')." = ".$db->quote(1)."
			ORDER BY ".$db->quoteName('grp.ordering').", ".$db->quoteName('allfields.field')." ASC ";
		$db->setQuery($query);
		$list = $db->loadAssocList();


		$change_group_name = 1;
		$addedarray        = array();
		foreach($list as $each){

			// add condition by salim... Due to $this->_data coming emptry while getdata via ajax...
			if(count($this->_data) == 0)
			{
				$this->_data = $this->getCustomData();
			}
			

			foreach($this->_data as $togrp){
				//Edited by Dave, condition to check if its Diu, then ommit from the text display list.
				//if($togrp->OGR_FID == 363)
					//continue;

				foreach($togrp as $key => $val){
					$statename = $togrp->name;

					if(($each['field'] == $key || $this->urnbanprefix.$each['field'] == $key || $this->totalprefix.$each['field'] == $key || $this->ruralprefix.$each['field'] == $key) || ($each['group'] == "Score" && $each['field'] == $key) ){
						$i = 0;
						foreach($attributes as $eachsessionattr){
							if(trim($each['group']) == "Score" && $eachsessionattr == $key){
								$srprefix     = "Rural_";
								$suprefix     = "Urban_";
								$stprefix     = "Total_";
								$sprefixarray = array("Rural_","Urban_","Total_");

								if((strpos($key,$srprefix)==0) || (strpos($key,$suprefix)==0) || (strpos($key,$stprefix)==0)){
									$todisplay[$statename][JTEXT::_($each['group'])][JTEXT::_($this->scorevariablename($sprefixarray,$eachsessionattr,1))][$this->scorevariablename($sprefixarray,$eachsessionattr,0)] = array($this->scorevariablename($sprefixarray,$eachsessionattr,0) => $val,'key'=>$eachsessionattr);
									$icon[$each['group']] = $each['icon'];//$addedarray[]=$this->scorevariablename($sprefixarray,$eachsessionattr,1);
								}
							}else if(strstr($each['field'], $eachsessionattr) && trim($each['group']) != "Score"){
								$segment = explode($eachsessionattr,$key);

								if($segment[1] == "" && $segment[0].$each['field'] == $key){
									$todisplay[$statename][JTEXT::_($each['group'])][JTEXT::_($eachsessionattr)][] = array(str_replace("_","",$segment[0])=>$val,'key'=>$eachsessionattr);
									$icon[$each['group']] = $each['icon'];
									$addedarray[]         = $segment[0];
								}
							}

							if(count($eachcustomattr) != 0){
								foreach($eachcustomattr as $keys => $vals){
									//if(stristr($vals,$eachsessionattr))
									{
										$ev = $this->getCustomAttributeValue($vals, $this->_data, $statename);
										$todisplay[$statename]['Custom Variable'][$keys][$vals] = array($keys => $ev,'key'=>$eachsessionattr);
									}
								}
							}
							//ksort($todisplay[$statename][$each['group']][$eachsessionattr],SORT_STRING);
						}
					}
				}
				//$todisplay[$statename]=array_reverse($todisplay[$statename]);
				//break;
			}
		}
		//echo "<pre>";print_r($todisplay);exit;
		//echo "<pre>";print_r($todisplay);exit;
		//$session->set('Groupwisedata',$todisplay);

		return array($todisplay,$icon);
	}

	function countkeys($data){
		$i=0;
		foreach($data as $eachdata){
			$i = $i + count($eachdata);
		}
		return $i;
	}

	function getNewAttributeTable_cont()
	{
		$gpdata=$this->getDisplayGroup();
		$groupwisedata=$gpdata[0];
		$icon=$gpdata[1];
		$segmentrow=0;
		foreach($icon as $ikey=>$ival)
		{
			foreach($groupwisedata as $key=>$val)
			{
				$newgroupwisedata[$key][$ikey]=array_pop(array_values($val[$ikey]));
				$segmentrow=count(array_pop(array_values($val[$ikey])));
			}
		}

		$totalrows= count($newgroupwisedata)*$segmentrow;
		$totalcolumns=count($icon);

		$session = JFactory::getSession();
		$activetable=$session->get("activetable");

		if($activetable  == "india_information"){
			$datatablename = "District Name";
		}

		if($activetable  == "rail_state"){
			$datatablename = "State Name";
		}

		if($activetable == "jos_mica_urban_agglomeration"){
			$datatablename = "UA Name";
		}

		if($activetable == "my_table"){
			$datatablename = "City Name";
		}

		/*$customattribute=$session->get('customattribute');
		$customattribute=explode(",",$customattribute);
		foreach($customattribute as $eachcustom)
		{
			$eachcustom=explode(":",$eachcustom);
			$eachcustomattr[$eachcustom[0]]=$eachcustom[1];

		}*/

		$c=array_keys($icon);
		$headernames=implode("</td><td width='200'>",$c);
		$str ="<tr class='firsttableheader'><td width='200'>".$datatablename."</td><td width='200'>".$headernames."</td></tr>";
		$i=0;
		foreach($newgroupwisedata as $key=>$val)
		{
			if($i<=$totalcolumns)
				{
					$str .="<tr ><td width='200'>".$key."</td>";
				}
				else
				{
					//$newgroupwisedata
					$i=0;
				}

			foreach($icon as $k=>$v)
			{

				$av=array_values($val[$k][$i]);
					$str .="<td width='200'>".$av[0]."</td>";


			}

			$str .="</tr>";
		}
		return $str;
	}

	public function buildTree(array $elements, $parentId = 0)
	{
		$branch = [];
		foreach ($elements as $key => $element) {
			if ($parentId == $element->variable_parent) {
				$branch[] = $element;
			}
		}

	    return $branch;
	}

	/**
	 * A function called from view.
	 */
	//getNewAttributeTableold currently unused
	public function getNewAttributeTable(){
		$db         = JFactory::getDBO();
		$session    = JFactory::getSession();

		$district   = $session->get('district');
		$state      = $session->get('state');
		$attributes = $session->get('attributes');
		$attributes = explode(",",$attributes);
		
		$fileID     = array_key_exists('exportFile', $_POST) ? $_POST['exportFile'] : '';
		$states_array = array_key_exists('state', $_POST) ? $_POST['state'] : [];
		$states = [];
		foreach ($states_array as $d) {
			$states[] = base64_decode($d);
		}
		$states = implode(",", $states);
		$states = str_replace(",","','",$states);
		$states = "'".$states."'";
		$stateIds = [];
		$query = "SELECT state, StateCode
			FROM ".$db->quoteName('india_information')."

			WHERE ".$db->quoteName('state')." IN (".$states.") ";
		$db->setQuery($query);
		try {
			$stateNameArray = $db->loadObjectList();
		} catch (Exception $e) {

			$stateNameArray = array();
		}
		$stateMapArray = [];
		foreach ($stateNameArray as $key => $value) {
			$stateMapArray[$value->state] = $value->StateCode;
			$stateIds[] = $value->StateCode;
		}
		
		$stateIds = implode(",", array_filter(array_unique($stateIds)));
		/*$stateIds = str_replace(",","','",$stateIds);
		$stateIds = "'".$stateIds."'";*/

		$district = array_key_exists('district', $_POST) ? implode(',', $_POST['district']) : '';
		$districtIds = [];
		$query = "SELECT district, District_name
			FROM ".$db->quoteName('villages_summary_dist')."
			WHERE ".$db->quoteName('india_information_OGR_FID')." IN (".$district.") ";
		$db->setQuery($query);
		$districtNameArray = $db->loadObjectList();
		$districtMapArray = [];
		foreach ($districtNameArray as $key => $value) {
			$districtMapArray[$value->District_name] = $value->district;
			$districtIds[] = $value->district;
		}
		$districtIds = implode(",", array_filter(array_unique($districtIds)));

		
		$attributes = array_key_exists('attributes', $_POST) ? $_POST['attributes'] : '';
		if ($fileID) {
			$custom_table_name = '#__mica_data_table_'.$fileID;

			$identifiers = "SELECT * FROM `variable_rows` WHERE `variable_type` LIKE 'identifiers' and dataset_master_id = '".$fileID."' and is_filterable = '1' ";
			$db->setQuery($identifiers);
			$identifiers_list = $db->loadObjectList();
			$stateColData = '';
			$distColData = '';
			foreach ($identifiers_list as $key => $value) {
				if (stripos($value->variable_name, 'state') !== FALSE ) {
					$stateColData = $value->variable_name;
				}
				if (stripos($value->variable_name, 'district') !== FALSE ) {
					$distColData = $value->variable_name;
				}
			}
			$q = 0;
			$and_varialble_query = ' (';
			$and_maping_query = '(';
			foreach ($attributes as $attribute) {
				if ($q == 0) {
					$and_varialble_query .= "variable_name like '".$attribute."' ";
					$and_maping_query .= "name like '".$attribute."' ";
				} else {
					$and_varialble_query .= "or variable_name like '".$attribute."' ";
					$and_maping_query .= "or name like '".$attribute."' ";
				}
				$q++;
			}
			$and_varialble_query .= ')';
			$and_maping_query .= ')';

			$mapping_query = "SELECT variable_row_id FROM `data_mapping` WHERE dataset_master_id = '".$fileID."' and ".$and_maping_query." ";

			$variabels = "SELECT * FROM `variable_rows` WHERE `variable_type` LIKE 'variable' and dataset_master_id = '".$fileID."' and is_filterable = '1' and ".$and_varialble_query." ";
			
			$db->setQuery($variabels);
			$variabels = $db->loadObjectList();
			$allvariablesIds = [];
			foreach ($variabels as $variabel) {
				$allvariablesIds[] = $variabel->id;
				$allvariablesIds[] = $variabel->variabel_parent;
			}

			$db->setQuery($mapping_query);
			$mapping_query_variabels = $db->loadObjectList();
			foreach ($mapping_query_variabels as $key => $value) {
				$allvariablesIds[] = $value->variable_row_id;
			}


			$query = "SELECT vr.id as id,
			 coalesce(dm.name, vr.variable_name) as variable_name,
			 vr.variable_parent as variable_parent,
			 vr.variable_type as variable_type FROM `variable_rows` as vr left join data_mapping as dm on vr.id = dm.variable_row_id
			  WHERE vr.id in (".implode(',', array_unique((array_filter($allvariablesIds)))).") ";
			$db->setQuery($query);
			$allVariable = $db->loadObjectList();
			$variableTree = [];
			foreach ($allVariable as $key => $value) {
				$temp = $this->buildTree($allVariable, $value->id);
				if (count($temp) > 0) {
					$variableTree[] = [$value, 'child' => $temp];
				}
			}

			// $var_cols_q = "SELECT * FROM `variable_columns` WHERE dataset_master_id = '".$fileID."' and ".$and_varialble_query." ";
			$var_cols_q = "SELECT * FROM `variable_columns` WHERE dataset_master_id = '".$fileID."'  ";
			$db->setQuery($var_cols_q);
			$var_cols = $db->loadObjectList();

			
			$var_cols_data = [];
			$var_col_query = '';
			foreach ($var_cols as $k => $v) {
				$var_cols_data[$v->variable_group_title][] = $v->id;
			}
			$app = JFactory::getApplication();
			$prefix = $app->getCfg('dbprefix');
			$custom_table_name2 = $prefix."mica_data_table_".$fileID;
			$query = "SELECT column_name FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = N'".$custom_table_name2."' ";
			$db->setQuery($query);
			$custom_table_cols = $db->loadObjectList();
			$custom_table_columns = [];
			foreach ($custom_table_cols as $key => $value) {
				$custom_table_columns[] = $value->column_name;
			}

			$query = "SELECT vr.variable_name FROM `filter_managment` as fm join variable_rows as vr on vr.id = fm.variable_row_id where fm.dataset_master_id = ".$fileID." ";
			$db->setQuery($query);
			$filter_managment = $db->loadObjectList();
			foreach ($filter_managment as $key => $value) {
				$k = array_search($value->variable_name, $custom_table_columns);
				$custom_table_columns = array_slice($custom_table_columns, $k+1);
			}
			array_pop($custom_table_columns);
			array_pop($custom_table_columns);
			
			$var_col_groupby = "group by";
			foreach ($custom_table_columns as $key => $value) {
				if (count($custom_table_columns) == $key+1 ) {
					$var_col_groupby .= " `".$value."` ";
				} else {
					$var_col_groupby .= " `".$value."`, ";
				}
			}

			$firstColTable = '';
			$secondColTable = '';
			foreach ($var_cols_data as $k => $v) {
				if (in_array($k, $custom_table_columns)) {
					$v_data = implode(",", array_filter(array_unique($v)));
					// $v_data = str_replace(",","','",$v_data);
					// $v_data = "'".$v_data."'";
					$var_col_query .= "and `".$k."` in (".$v_data.") ";
					$firstColTable .= "<td>".$k."</td>";
					$secondColTable .= "<td></td>";
				}
			}

			
			$column_query = "SELECT id FROM `variable_columns` WHERE dataset_master_id = '".$fileID."' ".$and_varialble_query."  ";


			$state_condition = '';
			if ($stateColData != '') {
				$stateColTable = "<td>State Name</td>";
				$secondColTable .= "<td></td>";
				$column_query_state = "SELECT id FROM `variable_columns` WHERE dataset_master_id = '".$fileID."' and variable_name in (".$stateIds.") ";
				$state_condition = "and `".$stateColData."` in (".$column_query_state.") ";
			}

			$district_condition = '';
			if ($distColData != '') {
				$districtColTable = "<td>District Name</td>";
				$secondColTable .= "<td></td>";
				$column_query_district = "SELECT id FROM `variable_columns` WHERE dataset_master_id = '".$fileID."' and variable_name in (".$districtIds.") ";
				$district_condition = "and `".$distColData."` in (".$column_query_district.") ";
			}

			// main list query
			// $query = "SELECT * FROM ".$db->quoteName($custom_table_name)." where variable_row_id in (".$mapping_query.") ".$state_condition." ".$district_condition." ".$var_col_query." ";
			$query = "SELECT * FROM ".$db->quoteName($custom_table_name)." where variable_row_id != '' ".$state_condition." ".$district_condition." ".$var_col_query." ".$var_col_groupby." order by id limit 100, 100 ";
			$db->setQuery($query);
			$list = $db->loadObjectList();
			foreach ($variableTree as $key => $value) {
				$colspan_count = array_key_exists('child', $value) ? count($value['child']) : '';
				$firstColTable .= "<td colspan='".$colspan_count."'>".$value[0]->variable_name."</td>";
				if ($colspan_count) {
					foreach ($value['child'] as $ck => $cv) {
						$secondColTable .= "<td>".$cv->variable_name."</td>";
					}
				}
				
			}

			$query = "SELECT * FROM ".$db->quoteName($custom_table_name)." where variable_row_id in (".$mapping_query.") ".$state_condition." ".$district_condition." ".$var_col_query." ";
			$db->setQuery($query);
			$variable_value_array = $db->loadAssocList();
			/*echo "<pre>";
			print_r($variable_value_array);
			die();*/

			$custom_column = [];
			$variable_data_final_queries = [];
			foreach ($var_cols_data as $k => $v) {
				if (in_array($k, $custom_table_columns)) {
					$query = "SELECT * FROM `variable_columns` WHERE dataset_master_id = '".$fileID."' ";
					$db->setQuery($query);
					$result = $db->loadObjectList();
					foreach ($result as $key => $value) {
						$custom_column[$value->id] = $value->variable_name;
					}
					$variable_data_final_queries[] = " `".$k."` in (".implode(',', $v).") ";
				}
			}

			$start = microtime(true);
			$custom_count = 1;
			$custom_row = '';
			$data_row = '';
			foreach ($list as $key => $value) {
				$search_array_part = [];
				$variable_data_final_query = '';
				$data_row .= "<tr>";
				if ($stateColData) {
					$data_row .= "<td>".array_search($custom_column[$value->{$stateColData}], $stateMapArray)."</td>";
					$variable_data_final_query .= "and `".$stateColData."` = ".$value->{$stateColData}." ";
					$search_array_part[$stateColData] = $value->{$stateColData};
				}
				if ($distColData) {
					$data_row .= "<td>".array_search($custom_column[$value->{$distColData}], $districtMapArray)."</td>";
					$variable_data_final_query .= "and `".$distColData."` = ".$value->{$distColData}." ";
					$search_array_part[$distColData] = $value->{$distColData};
				}
				// $custom_td = '';
				foreach ($var_cols_data as $k => $v) {
					if (in_array($k, $custom_table_columns)) {
						$data_row .= "<td>".( array_key_exists($value->{$k}, $custom_column) ? $custom_column[$value->{$k}] : "" )."</td>";
						$variable_data_final_query .= "and `".$k."` = ".$value->{$k}." ";
						$search_array_part[$k] = $value->{$k};
					}
				}
				foreach ($variableTree as $vtk => $vtv) {
					$colspan_count = array_key_exists('child', $vtv) ? count($vtv['child']) : '';
					if ($colspan_count) {
						foreach ($vtv['child'] as $ck => $cv) {
							$search_array = [];
							$query = "SELECT * FROM ".$db->quoteName($custom_table_name)." where variable_row_id = ".$cv->id." ".$variable_data_final_query." limit 1";
							$db->setQuery($query);
							$fData = $db->loadObject();
							$data_row .= "<td>".$fData->value."</td>";
							// $data_row .= "<td>".$cv->id."</td>";
							
						}
					}
				}
				$data_row .= "</tr>";
				$custom_count++;
				/*if ($custom_count == 100 ) {
					break;
				}*/
			}
			$time_elapsed_secs = microtime(true) - $start;

			$str = "<table cellspacing='0' cellpadding='0' border='0' width='100%'  class='draggable' id='datatable'>";
			$str .= "<tr class='firsttableheader'>".$stateColTable.$districtColTable.$firstColTable." ";
			$str .= "</tr>";
			$str .= "<tr class='secondtableheader'>".$secondColTable;
			$str .= $data_row;
			$str .= "</tr>";
			$str .= "</table>";
			// $str .= "custom_time_took".$time_elapsed_secs."_COUNT".$custom_count."_ABC";
			return $str;

		} else {
			$str = '';
			return $str;
		}
	}

	function search($array, $search_list) { 
	    // Create the result array 
	    $result = array(); 
	  
	    // Iterate over each array element 
	    foreach ($array as $key => $value) { 
	  
	        // Iterate over each search condition 
	        foreach ($search_list as $k => $v) { 
	      
	            // If the array element does not meet 
	            // the search condition then continue 
	            // to the next element 
	            if (!isset($value[$k]) || $value[$k] != $v) 
	            { 
	                  
	                // Skip two loops 
	                continue 2; 
	            } 
	        } 
	      
	        // Append array element's key to the 
	        //result array 
	        $result[] = $value; 
	    } 
	  
	    // Return result  
	    return $result; 
	} 


	function hasValues($input, $deepCheck = true) {
	    foreach($input as $value) {
	        if(is_array($value) && $deepCheck) {
	            if($this->hasValues($value, $deepCheck))
	                return true;
	        }
	        elseif(!empty($value) && !is_array($value))
	            return true;
	    }
	    return false;
	}

	/**
	 *
	 */
	public function getColspan($groupwisedata, $onlevel){
		$keys = array_keys($groupwisedata);
		$vals = array_values($groupwisedata);

		if($onlevel==3){
			return count($keys);
		}else if($onlevel==2){
			return count($keys)*count($vals[0]);
		}else if($onlevel==1){
			return  $this->getColspan($groupwisedata,2)+$this->getColspan($groupwisedata,3);
		}
	}

	function getAttributeTable()
	{

		$session = JFactory::getSession();
		$m_type=$session->get('m_type');
		$states=$session->get('states');
		$district=$session->get('district');

		$customattribute=$session->get('customattribute');

		$customattribute=explode(",",$customattribute);
		foreach($customattribute as $eachcustom)
		{
			$eachcustom=explode(":",$eachcustom);
			$eachcustomattr[$eachcustom[0]]=$eachcustom[1];


		}


		$compareflag=0;
		if(count($this->_data) ==0)
		{
			return "No Data Available";
		}
		else if(count($this->_data) >1)
		{
			$compareflag=1;
		}
		$heads=array();
		if(1 != 1)
		{
			if(trim($district)=="")
			{	$i=0;
				foreach($this->_data as $key=>$val)
				{      foreach($val as $ek=>$ev)
					{
					if($ek!=$this->urbansearchvariable && $ek!=$this->townsearchvariable && $ek != $this->districtsearchvariable)
						{
							$heads[$ek][]=$ev;
						}
						else
						{
							$deleteindex[]=$ev;

						}


					}
				}
			}
			else
			{
				$i=0;
				foreach($this->_data as $key=>$val)
				{      foreach($val as $ek=>$ev)
					{

					if($ek!=$this->urbansearchvariable && $ek!=$this->townsearchvariable && $ek != $this->districtsearchvariable)
						{
							$heads[$ek][]=$ev;
						}
						else
						{
							$deleteindex[]=$ev;

						}

					}
				}
			}


			$heads=array_reverse($heads);
			$heads=array_filter($heads);
				$heads=array_merge($heads,$eachcustomattr);
			$deleteindex=array_reverse($deleteindex);
			$mykeys=array_keys($heads);
				$eachcustomattr=array_filter($eachcustomattr);


			$head="";
			$i=0;
			$data="";
			foreach($mykeys as $eachkey)
			{
				if($eachkey=="name")
				{
					$head .="<th>State Name</th>";
				}
				else if($eachkey=="distshp")
				{
				$head .="<th>District Name</th>";
				}
				else if($eachkey=="place_name")
				{
					$head .="<th>Town Name</th>";
				}
				else if($eachkey=="UA_name")
				{
					$head .="<th>Urban Name</th>";
				}
				else
				{
					$head .="<th>".$eachkey."</th>";
				}


			}
			 $head ="<tr>".$head."</tr>";

			foreach($this->_data as $eachkey)
			{
				$data .="<tr>";
				for($i=0;$i<count($mykeys);$i++)
				{
					if(!empty($eachcustomattr[$mykeys[$i]]) && isset($eachcustomattr[$mykeys[$i]]))
					{
					$ev=$this->getCustomAttributeValue($eachkey->$mykeys[$i],$this->_data);
					$data .="<td>".JTEXT::_($ev)."</td>";

					}
					else if(is_object($eachkey))
					{
					$data .="<td>".JTEXT::_($mykeys[$i])."</td>";
					}
				}
				$data .="</tr>";
			}
		}
		else
		{

		foreach($this->_data as $key=>$val)
				{
					foreach($val as $ek=>$ev)
					{
						if($ek!=$this->urbansearchvariable && $ek!=$this->townsearchvariable && $ek != $this->districtsearchvariable)
						{
							$heads[$ek][]=$ev;
						}
						else
						{
							$deleteindex[]=$ev;

						}

					}
				}
				//echo "<pre>";
				//print_R($this->_data)
				$heads=array_reverse($heads);
				//$deleteindex=array_reverse($deleteindex);
				$mykeys=array_keys($heads);
				$data ="";
				$j=0;
				$heads=array_merge($heads,$eachcustomattr);
				$heads=array_filter($heads);

		     	foreach($heads as $ek=>$ev)
					{	$originalname[]=$ek;
						if($ek=="name")
						{
							$ek="State Name";
						}
						else if($ek=="distshp")
						{
							$ek="District Name";
						}
						else if($ek=="place_name")
						{
							$ek="Town Name";
						}
						else if($ek=="UA_Name")
						{
							$ek="Urban Name";
						}
						else
						{
							$ek =$ek;
						}

						if(!empty($eachcustomattr[$ek]))
							{
							$ev=$this->getCustomAttributeValue($eachcustomattr[$ek],$this->_data);
							$rowclass="deletecustom";
							}
						else
						{
							$rowclass="deleterow";
						}
						if($j!=0)
						{
						$todisplay ="<td ><input type='checkbox' class='".$rowclass."' value='".$originalname[$j]."' /></td>";
						}
						else
						{
							$todisplay="<td >Delete</td>";
						}
						$data .="<tr id='".$originalname[$j]."'>".$todisplay."<td>".$ek."</td>";


						$i=0;

						foreach($ev as $k=>$v)
						{

							if($i==0)
							{
								if($j == 0)
								{
									$myid="id = '".$v."'";
									$myclass="class='".$ek." '";
									$deletedcolumn="<input type='checkbox' id='removecolumn' class ='removecolumn' value='".$originalname[$j]."`".$deleteindex[$k]."`".$v."' />";
								}
								else
								{
									$myid="id = ''";
									$myclass="";
									$deletedcolumn="";
								}


							}
							else
							{
								if($j==0)
								{
									$myid="id = '".$v."'";
								}
								else
								{
									$myid="id = ''";
								}
								$myclass="class = ''";

							}

							$data .="<td ".$myclass." ".$id.">".$deletedcolumn.JTEXT::_($v)."</td>";

						}
						$data .="</tr>";
						$j++;
					}

		}
		return $head.$data;

	}

	/**
	 *
	 */
	function getCustomAttributeValue($name,$data,$identifier=null){

		$session = JFactory::getSession();
		$db      = $this->getDbo();

		$returnable = array();
		$i          = 0;

		$name1 =str_replace("+","",$name);
		$name1 =str_replace("*","",$name1);
		$name1 =str_replace("-","",$name1);
		$name1 =str_replace("/","",$name1);
		$name1 =str_replace("%","",$name1);
		$name1 =str_replace("SIN(","",$name1);
		$name1 =str_replace("COS(","",$name1);
		$name1 =str_replace("TAN(","",$name1);
		$name1 =str_replace("LOG(","",$name1);
		$name1 =str_replace("LOG10(","",$name1);
		$name1 =str_replace("SQRT(","",$name1);
		$name1 =str_replace("EXP(","",$name1);
		$name1 =str_replace("^","",$name1);
		$name1 =str_replace("EXP(","",$name1);
		$name1 =str_replace("LN(","",$name1);
		$name1 =str_replace("PI(","",$name1);
		$name1 =str_replace(")","",$name1);
		$name1 =str_replace("(","",$name1);
		$name1 =str_replace(range(0,9),'',$name1);

		//$name1 = str_replace(",,",",",$name1);
		$chkval  = explode(" ",$name1);
		$chkval  = array_filter($chkval);
		$field   = implode(",",$chkval);

		$table          = $session->get("activetable");
		$searchvariable = $session->get("activenamevariable");

		//foreach($data as $ed)
		{
			$dupname    = $name;

			$identifier = explode("-",$identifier);
			$query = "SELECT ".$field." FROM ".$table." WHERE ".$searchvariable." LIKE '".$identifier[0]."'";
			$db->setQuery($query);
			try {
				$list  = $db->loadObjectList();
			} catch (Exception $e) {
				$list  = NULL;
			}

			$list = array_pop($list);
			//if($ed->name==$identifier)
			{
				foreach($list as $k => $v){
					//echo $dupname."<br /><br />";
					$dupname        = str_replace($k,$v,$dupname);
					$returnable[$i] = $dupname;
				}
			}
			$i++;
		}
		$i=0;

		foreach($returnable as $reeval)
		{
			//echo 'aaa'.$reeval."<br />";
			$result="";
			if(strstr($reeval,"^")) {
				$cExplode = str_replace(" ","",$reeval);
				$cExplode = explode("^",$cExplode);
				$result   = pow($cExplode[0],$cExplode[1]);
				//@eval('$result = '.$formulaNew.';');
			}else if(strstr($reeval,"%")) {
				//$perExplode = str_replace("(","",$reeval);
				//$perExplode = str_replace(")","",$perExplode);
				$perExplode     = str_replace(" ","",$reeval);
				$perExplodeData = explode("%",$perExplode);
				$formulaNew     = "(".$perExplodeData[0].")/100";
				@eval('$result = '.$formulaNew.';');
			}else if(strstr($reeval,"LN(")){
				$lnexplode =explode('LN(',$reeval);
				$test      = explode(")",$lnexplode[1]);
				//$result  =pow(10,$test[0]);
				$result    =number_format(log($test[0]),5);
			}else if(strstr($reeval,"LOG(")){
				$lnexplode1 =explode('LOG(',$reeval);
				$test1      = explode(")",$lnexplode1[1]);
				//$result   =pow(10,$test[0]);
				$resultTmp  =log($test1[0],10);
				$result     = number_format($resultTmp,5);
			}else{
				//echo $reeval."<br />";
				@eval('$result = '.$reeval.';');
			}

			$returnables[$i]=  $result;
			$i++;
		}

		//echo $returnables[0];exit;
		return $returnables[0];
	}

	function getWorkspace(){
		return workspaceHelper::loadWorkspace();
	}

	function updateWorkspace(){
		return workspaceHelper::updateWorkspace();
	}

	function deleteWorkspace(){
		return workspaceHelper::deleteWorkspace();
	}

	function saveWorkspace(){
		return workspaceHelper::saveWorkspace();
	}

	function unsetDefault(){
		return workspaceHelper::unsetDefault();
	}

	function getSldToDataBase()
  	{
  		$session=JFactory::getSession();
  		$activeworkspace=$session->get('activeworkspace');
  		$sld=cssHelper::getLayerProperty($activeworkspace,1);
		echo $sld;
		exit;
  	}

  	function getthematicQueryFromDb(){
		$db      = $this->getDbo();
		$session = JFactory::getSession();

  		$activeworkspace=$session->get('activeworkspace');
		$query  = "SELECT * FROM ".$db->quoteName('#__mica_sld_legend')."
			WHERE ".$db->quoteName('workspace_id')." = ".$db->quote($activeworkspace);
		$db->setQuery($query);
		$result = $db->loadObjectList();
		//cont..
		/*$customformulafromsession=$session->get('customformula',$attributes);
		$customformula=$formula.":".$from."->".$to."->".$color."|";
		$customformulafromsession = $customformulafromsession.$customformula;
		$session->set('customformula',$customformulafromsession);*/
  	}

  	function getSldLevel(){
  		$db      = $this->getDbo();
		$session = JFactory::getSession();

  		$activeworkspace=$session->get('activeworkspace');

		$query  = "SELECT level FROM ".$db->quoteName('#__mica_sld_legend')."
			WHERE ".$db->quoteName('workspace_id')." = ".$activeworkspace."
			GROUP BY level";
		$db->setQuery($query);
		$result = $db->loadObjectList();

		$str   = "";
		$level = "0,1,2";
		foreach($result as $eachlevel){
			$level =str_replace($eachlevel->level.",","",$level);
		}
		return $level;
  	}

  	/**
  	 * A function called from view.
  	 */
	public function getBreadCrumb(){
		$session=JFactory::getSession();

		$urban    = $session->get('urban');
		$town     = $session->get('town');
		$m_type   = $session->get('m_type');

		$district = $session->get('district');
		$state    = $session->get('state');
		$dataof   = $session->get('dataof');

		$statecount = explode(",",$state);
		$separator  = "<span class='ja-pathway-text'>&nbsp; &gt; &nbsp;</span>";//JHTML::_('image.site', 'arrow.png');
		$breadcrumb = "<div class='breadcrumb ' id='ja-pathway'><span class='ja-pathway-text'>Your Query :</span>";

		if(count($statecount) == 1){
			$breadcrumb .="<span class='ja-pathway-text'>State(".$state.")</span>".$separator;
			if($dataof == "District"){
				foreach($this->_data as $eachdata){
					if($eachdata->OGR_FID == $district){
						$selected = $eachdata->OGR_FID."&m_type=".$m_type;
						$name     = $eachdata->name;
						if($m_type == "Total"){
							$m_typedisp = "All";
						}else{
							$m_typedisp = $m_type;
						}
						break;

					}else{

						if($m_type == "Total"){
							$m_typedisp = "All";
						}else{
							$m_typedisp = $m_type;
						}

						if($district == "all"){
							$name     = "All";
							$selected = "all&m_type=".$m_type;
						}else{
							$name     = "Compare";
							$selected = "all&m_type=".$m_type;
						}
					}
				}
				$breadcrumb .="<span class='ja-pathway-text'>District(".$name.")</span>".$separator;
				//$breadcrumb .="<span class='ja-pathway-text'>Type(".ucfirst($m_typedisp).")</span>";
			}

			if($dataof == "Town"){
				foreach($this->_data as $eachdata){
					if($eachdata->OGR_FID == $town){
						$selected = $eachdata->OGR_FID;
						$name     = $eachdata->name;
						break;
					}else{
						if($town == "all"){
							$name = "All";
						}else{
							$name = "Compare";
						}
						$selected = "all";
					}
				}
				$breadcrumb .="<span class='ja-pathway-text'>Town(".$name.")</span>";
			}

			if($dataof == "UA" || $dataof == "Urban"){
				$dataof = "UA";
				foreach($this->_data as $eachdata){
					if($eachdata->OGR_FID==trim($urban)){
						$selected = $eachdata->OGR_FID;
						$name     = $eachdata->name;
						break;
					}else{
						if($urban=="all"){
							$name="All";
						}else{
							$name="Compare";
						}
						$selected="all";
					}
				}
				$breadcrumb .="<span class='ja-pathway-text'>Urban(".$name.")</span>";
			}
			$selected=$district;

		}else{
			$breadcrumb .="<span class='ja-pathway-text'>State</span>".$separator;
			if($dataof=="District"){
				$breadcrumb .="<span class='ja-pathway-text'>District</span>".$separator;
				//$breadcrumb .="<span class='ja-pathway-text'>".ucfirst($m_type)."</span>";
			}

			if($dataof=="Town"){
				$breadcrumb .="<span class='ja-pathway-text'>Town</span>";
			}

			if($dataof=="UA"){
				$breadcrumb .="<span class='ja-pathway-text'>Urban</span>";
			}
			$selected=$district;
		}

		$ac .="<div class='modifystruct'><div class='ja-pathway-text' id='activeworkspacename' style='float: left;padding-right: 4px;font-weight: bold;font-size:12px;'></div>";
		$ac .="<a class='ja-pathway-text' href='index.php?option=com_mica&view=micafront'>Modify</a></div>";
		$breadcrumb .="</div>";
		return $breadcrumb."~~~".$ac;
	}

	/**
	 * A function called from view.
	 */
	public function getcustomattrlib(){
		$session = JFactory::getSession();
		$db      = $this->getDBO();

		$activetable = $session->get("activetable");

		$query = "SELECT name, attribute
			FROM ".$db->quoteName('#__mica_user_custom_attribute_lib')."
			WHERE ".$db->quoteName('profile_id')." = ".$db->quote($session->get("activeworkspace"))."
				AND ".$db->quoteName('tables')." LIKE ".$db->quote($activetable);
		$db->setQuery($query);
		$allcostomattr = $db->loadAssocList();

		$str = "";
		foreach($allcostomattr as $eachcustomattr){
			$eachcustomattr['attribute'] = str_replace("'"," ",$eachcustomattr['attribute']);
			$str .= $eachcustomattr['name'].":".$eachcustomattr['attribute'].",";
		}

		$session->set('customattributelib',$str);
	}

	/**
	 * a function called from view.
	 */
  	public function getInitialGeometry(){

		$session = JFactory::getSession();
		$db      = $this->getDBO();

		$state = $session->get('state');


		$state = explode(",",$state);
		$state = array_filter($state);
		$state = implode("','",$state);

		if($state != "all" && substr_count($state,",") == 0){
			//$where="1=1";
			//$query="select AsText(GROUP_CONCAT(india_state)) as poly from rail_state where ".$where;
			 $query = "SELECT AsText((india_state)) as poly
				FROM ".$db->quoteName('rail_state')."
				WHERE ".$db->quoteName('name')." IN ('".$state."') ";

		}else{
			return 0;
		}

		$db->setQuery($query);
		$allvertex = $db->loadAssocList();

		$str       = "";
		$allvertex = array_reverse($allvertex);
		foreach($allvertex as $eachvertex){
			//$str[] = $eachvertex['AsText(india_state)'];
		 	return $eachvertex['poly'];
		}
		return implode(",", $str);
	}

	/**
	 *
	 */
	public function getPagination(){
		// Load the content if it doesn't already exist
		if (empty($this->_pagination)) {
			$app = JFactory::getApplication();
		    jimport('joomla.html.pagination');
		    $this->_pagination = new JPagination(ceil(count($this->_data)/50), ( ($app->input->get("limitstart") != "") ? $app->input->get("limitstart") : 0), 1 );
		}
		return $this->_pagination;
	}

	/**
	 * A function called from view.
	 */
	public function getUnselectedDistrict(){
		$session = JFactory::getSession();
		$db      = $this->getDBO();

		$district = $session->get('district');
		$explode_district = explode(",", $district);

		if (count($explode_district) > 0 && strlen($explode_district[0]) > 0 ) {
			$query    = "SELECT OGR_FID FROM ".$db->quoteName('india_information')."
				WHERE OGR_FID NOT IN (".$district.")";
			$db->setQuery($query);
			try {
				$return = $db->loadAssocList();
			} catch (Exception $e) {
				$return = NULL;
			}

			foreach($return as $each){
				$returnable[]=$each['OGR_FID'];
			}
			return implode(",",$returnable);
		}else{
			return '';
		}
	}

	public function planAuthentication()
	{
		$userid = JFactory::getUser()->id;

		$db    = $this->getDBO();

		$query = "SELECT os.allow_districtdata FROM " . $db->quoteName('#__osmembership_plans') . " AS op
				INNER JOIN " . $db->quoteName('#__osmembership_subscribers') . " AS os
					ON " . $db->quoteName('op.id') . " = " . $db->quoteName('os.plan_id') . "
						AND " . $db->quoteName('os.user_id') . " LIKE " . $db->quote($userid) . "
			ORDER BY " . $db->quoteName('os.id') . " DESC ";

		$db->setQuery($query);

		$allow_districtdata = $db->loadResult();

		return $allow_districtdata;
	}

	public function userViewPlanLimit()
	{
		$userid = JFactory::getUser()->id;
		$get_detail =JFactory::getUser();
		$parent_user= $get_detail->parent_user;

		$db = $this->getDBO();

		$query = "SELECT os.records_limit FROM " . $db->quoteName('#__osmembership_plans') . " AS op
				INNER JOIN " . $db->quoteName('#__osmembership_subscribers') . " AS os
					ON " . $db->quoteName('op.id') . " = " . $db->quoteName('os.plan_id') . "
						AND (" . $db->quoteName('os.user_id') . " LIKE " . $db->quote($userid) ;// . "
						if($parent_user!='' && $parent_user!=0)
						{
							$query.= " OR ". $db->quoteName('os.user_id') . " LIKE " . $parent_user . ")";
						} else {
							$query .=") ";
						}

		$query.="  AND os.published = 1 ORDER BY " . $db->quoteName('os.id') . " DESC ";

		$db->setQuery($query);

		$userViewPlanLimit = $db->loadResult();

		return $userViewPlanLimit;
	}

	public function viewRecordPlanLimit()
	{
		$userid = JFactory::getUser()->id;

		// Get a db connection.
		$db = $this->getDBO();

		// Create a new query object.
		$view_record_plan_limit_query = $db->getQuery(true);

		$view_record_plan_limit_query = "SELECT user_id,mimi_view_records FROM " . $db->quoteName('user_plan_setting') . "WHERE " . $db->quoteName('user_id') . " LIKE " . $db->quote($userid);
		

		$db->setQuery($view_record_plan_limit_query);

		$view_record_plan_limit = $db->loadAssoc();


		$view_record_plan_limit_value = $view_record_plan_limit['mimi_view_records'];

		if ($view_record_plan_limit['user_id']) {
			$view_record_plan_limit_update_query = $db->getQuery(true);

			$view_record_plan_limit = $view_record_plan_limit_value+1;

			 $userViewPlanLimit = $this->userViewPlanLimit();



			//$userViewPlanLimit -1 for unlimited
			if($userViewPlanLimit >= $view_record_plan_limit || $userViewPlanLimit==-1)
			{
				// Fields to update.
				$fields = array(
				    $db->quoteName('mimi_view_records') . ' = ' . $view_record_plan_limit
				);

				// Conditions for which records should be updated.
				$conditions = array(
				    $db->quoteName('user_id') . ' = ' . $userid
				);

				$view_record_plan_limit_update_query->update($db->quoteName('user_plan_setting'))->set($fields)->where($conditions);
				
				$db->setQuery($view_record_plan_limit_update_query);

				$db->execute();

				$allow_view = 1;
			}
			else
			{
				$allow_view = 0;
			}

		}else{
			

			// Create a new query object.
			$query = $db->getQuery(true);

			// Insert columns.
			$columns = array('user_id', 'mimi_view_records');

			// Insert values.
			$values = array($userid, 1);

			// Prepare the insert query.
			$query
			    ->insert($db->quoteName('user_plan_setting'))
			    ->columns($db->quoteName($columns))
			    ->values(implode(',', $values));

			// Set the query using our newly populated query object and execute it.
			$db->setQuery($query);
			$db->execute();
			$allow_view =1;
		}
		

		return $allow_view;
	}

	/*
		Find user Plan Download allow limit
	*/
	public function userDownloadPlanLimit()
	{
		$userid = JFactory::getUser()->id;
		$get_detail =JFactory::getUser();
		$parent_user= $get_detail->parent_user;

		$db = $this->getDBO();

		$query = "SELECT os.downloadpm_limit FROM " . $db->quoteName('#__osmembership_plans') . " AS op
				INNER JOIN " . $db->quoteName('#__osmembership_subscribers') . " AS os
					ON " . $db->quoteName('op.id') . " = " . $db->quoteName('os.plan_id') . "
						AND (" . $db->quoteName('os.user_id') . " LIKE " . $db->quote($userid) ;//. "
					if($parent_user!='')
						{
							$query.= " 
						or ". $db->quoteName('os.user_id') . " LIKE " . $parent_user . ")";
						}
						else {
							$query.=")";
						}
						
			$query.= " AND os.published = 1  ORDER BY " . $db->quoteName('os.id') . " DESC ";

		$db->setQuery($query);

		$userDownloadPlanLimit = $db->loadResult();

		return $userDownloadPlanLimit;
	}

	/*
	For Download Allowed Limit check
	 */
	public function downloadAllowedPlanLimit()
	{
		$userid = JFactory::getUser()->id;

		// Get a db connection.
		$db = $this->getDBO();

		// Create a new query object.
		$download_plan_limit_query = $db->getQuery(true);

		$download_plan_limit_query = "SELECT user_id,mimi_download_data FROM " . $db->quoteName('user_plan_setting') . "WHERE " . $db->quoteName('user_id') . " LIKE " . $db->quote($userid);

		$db->setQuery($download_plan_limit_query);

		$download_plan_limit = $db->loadAssoc();

		$download_plan_limit_value = $download_plan_limit['mimi_download_data'];

		if ($download_plan_limit['user_id'] > 0) {

			$download_plan_limit_update_query = $db->getQuery(true);

			// Increase download limit
			$download_plan_limit = $download_plan_limit_value+1;

			// Get User download plan limit
			$userDownloadPlanLimit = $this->userDownloadPlanLimit();

			if($userDownloadPlanLimit >= $download_plan_limit)
			{
				// Fields to update.
				$fields = array(
				    $db->quoteName('mimi_download_data') . ' = ' . $download_plan_limit
				);

				// Conditions for which records should be updated.
				$conditions = array(
				    $db->quoteName('user_id') . ' = ' . $userid
				);

				$download_plan_limit_update_query->update($db->quoteName('user_plan_setting'))->set($fields)->where($conditions);

				$db->setQuery($download_plan_limit_update_query);
				$db->execute();

				$allow_download = 1;
			}
			else
			{
				$allow_download = 0;
			}

		}else{
			// Create a new query object.
			$query = $db->getQuery(true);

			// Insert columns.
			$columns = array('user_id', 'mimi_download_data');

			// Insert values.
			$values = array($userid, 1);

			// Prepare the insert query.
			$query
			    ->insert($db->quoteName('user_plan_setting'))
			    ->columns($db->quoteName($columns))
			    ->values(implode(',', $values));

			// Set the query using our newly populated query object and execute it.
			$db->setQuery($query);
			$db->execute();
		}

		return $allow_download;
	}
}
