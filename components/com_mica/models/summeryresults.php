<?php
/**
 * @package     Joomla.Administrator
 * @subpackage  com_mica
 *
 * @copyright   Copyright (C) 2005 - 2017 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

require_once( JPATH_COMPONENT.DIRECTORY_SEPARATOR.'helpers/Workspace.php' );
require_once( JPATH_COMPONENT.DIRECTORY_SEPARATOR.'helpers/css.php' );

/**
 * MICA SummeryResults model.
 *
 * @since  1.6
 */
class MicaModelSummeryResults extends JModelLegacy
{

	var $_data                  = null;
	var $_total                 = null;
	var $_pagination            = null;
	var $townsearchvariable     = "OGR_FID";
	var $urbansearchvariable    = "OGR_FID";
	var $districtsearchvariable = "district";
	var $statesearchvariable    = "OGR_FID";

	var $ruralprefix            = "Rural_";
	var $urnbanprefix           = "Urban_";
	var $totalprefix            = "Total_";
	var $total                  = "(Total)";

	function __construct(){
		parent::__construct();

		$app  = JFactory::getApplication();
		$user = JFactory::getUser();
		if($user->id == 0){
			$app->redirect("index.php");exit;
		}
		$this->_table_prefix = '#__mica_';
		$limit               = $app->input->get('limit','limit', 25, 0);
		//$limitstart        = $mainframe->getUserStateFromRequest($context.'limitstart','limitstart',0);
		//$limitstart        = ($limit != 0 ? (floor($limitstart / $limit) * $limit) : 0);
		$jlimitstart         = $app->input->get('limitstart');
		if(!$jlimitstart)	$limitstart=0;

		$this->setState('limit',1);
		$page = ($app->input->get("limitstart") != "") ? $app->input->get("limitstart") : 0;
		$statingpage = null;
		$this->setState('limitstart', $page);
		//$this->setState('limitstart',$limitstart);
	}

	/**
	 * A function called from view to get states.
	 */
	public function getStateItems(){
		$res = null;
		$db    = $this->getDBO();
		$query = "SELECT id, name FROM ".$db->quoteName('rail_state')." GROUP BY ".$db->quoteName('name');
		$db->setQuery($query);
		try {
			$res = $db->loadObjectList();
		} catch (Exception $e) {

		}
		return $res;
	}

	/**
	 *
	 */
	public function getAttributeCheck($db_table, $attributes, $m_type = null){
		$app     = JFactory::getApplication();
		$session =JFactory::getSession();
		$db      = $this->getDBO();

		$query = "DESCRIBE ".$db_table;
		$db->setQuery($query);
		$fields = $db->loadObjectList();

		//$m_types=$session->get('m_type');
		foreach($fields as $eachfield){
			$tablefield[] = $eachfield->Field;
		}

		if(!is_array($attributes)){
			$attributes=explode(",",$attributes);
		}
		$attrstr=array();

		//foreach($m_types as $m_type)
		{
			foreach($attributes as $eachattr)
			{
				$status=0;
				if($m_type=="Total" && $eachattr!="MPI" && $eachattr!="Score")
				{
					if(in_array($this->totalprefix.$eachattr,$tablefield))
					{
						$attrstr[]=$this->totalprefix.$eachattr;
						$status=1;
					}

				}
				if($m_type=="Urban" && $eachattr!="MPI"  && $eachattr!="Score")
				{
					if(in_array($this->urnbanprefix.$eachattr,$tablefield))
					{
						$attrstr[]=$this->urnbanprefix.$eachattr;
						$status=1;
					}
				}
				if($m_type=="Rural" && $eachattr!="MPI"  && $eachattr!="Score")
				{
					if(in_array($this->ruralprefix.$eachattr,$tablefield))
					{
						$attrstr[]=$this->ruralprefix.$eachattr;
						$status=1;
					}
				}
				if($eachattr=="MPI")
				{
					$m_type_rating = $app->input->get("m_type_rating",array("Total"), 'raw');
					foreach($m_type_rating as $eachprefix)
					{
						if(in_array($eachprefix."_".$eachattr,$tablefield))
						{
							$attrstr[]=$eachprefix."_".$eachattr;
							$status=1;
						}
					}

				}

				if($status==0)
				{
					if(in_array($eachattr,$tablefield))
					{
						$attrstr[]=$eachattr;
					}
				}
			}
		}

		//echo implode(",",$attrstr);exit;
		return implode(",",$attrstr);
	}

	/**
	 * A function called from view.
	 */
	public function getCustomData($infotype=null, $pagefromcontroller=null, $speedvar=null, $getminmax=null, $chartSelectedDistrict=null,$chartSelectedVariable=null, $initialGraph=0, $isSubDist=0)
	{
		$session      = JFactory::getSession();
		$app          = JFactory::getApplication();
		$db           = $this->getDBO();
		$states       = $session->get('summarystate');
		$mystates     = explode(",",$states);
		$mystates     = array_filter($mystates);
		$statesinsert = implode("','",$mystates);

		$district     = $session->get('summarydistrict');
		$subDistrict  = $session->get('summarySubDistrict');
		$urban        = $session->get('urban');
		$m_types      = $session->get('m_type');

		if($chartSelectedVariable != "")
		{
			$attributes = $chartSelectedVariable;
		}
		else if($speedvar!=null)
		{
			$attributes = $speedvar;
		}
		else
		{
			$attributes = $session->get('summeryattributes');
		}

		if($chartSelectedDistrict!="" && $isSubDist == 0)
		{
			$district = $chartSelectedDistrict;
		}

		if($chartSelectedDistrict!="" && $isSubDist == 1)
		{
			$subDistrict = $chartSelectedDistrict;
		}

		$attributes   = explode(",", $attributes);
		$attr   = implode(",", $attributes);

		$session->set('villageSummeryThemeticattribute', $attr);

		//echo "<pre>";print_r($session->get('themeticattribute'));exit;

		// Initial 5 attribute load in graph
		$initialGraph = $app->input->get("initialGraph");
		if ($initialGraph == 1) {
			$attributes = array_slice($attributes, 0, 5, true);
			//$attributes = implode(",",$attributes);
		}

		$query = "SELECT `field`,`operator`, `table`, `field_label`, `field_sublabel` FROM #__mica_group_field_summary_map WHERE  `field` in('".implode("','",$attributes). "')";

		$db->setQuery($query);
		$obOperator = $db->loadobjectList();

		$district_fields       = array();
		$villages_fields       = array();
		$villages_where        = array();
		$urban_fields          = array();
		$villagesummery_fields = array();
		$railstate_fields      = array();
		$where                 = array();
		$groupby               = array();

		$states      = explode(",",$states);
		$states      = array_filter($states);
		$states      = implode("','",$states);

		$district    = explode(",",$district);
		$district    = array_filter($district);
		$district    = implode("','",$district);

		$subDistrict = explode(",",$subDistrict);
		$subDistrict = array_filter($subDistrict);
		$subDistrict = implode("','",$subDistrict);

		//if($getminmax!=null)
		{
			$selectqueryfields = [];
		}

		// edited by satish: Start...
		$village_column_names = [];

		foreach($obOperator as $key => $value)
		{
			$village_column_names[$value->field] = $value;
			$field_label     = $value->field_label;
			$field_labels    = str_replace(" ","_",$field_label);
			$field_sublabel  = $value->field_sublabel;
			$field_sublabels = str_replace(" ","_",$field_sublabel);

			switch ($value->table) {
				case 'Villages':
				switch ($value->operator) {

					case 'Total':
					array_push($selectqueryfields,$value->field.'____0');
					array_push($villages_fields,"ROUND( sum(".$value->field."),2) as ".$value->field."____0" );
					break;
					case 'Countsofab':
					array_push($selectqueryfields,$value->field.'____0');
					array_push($selectqueryfields,$value->field.'____1');
					array_push($villages_fields," count(case ".$value->field." when '1' then 1 else null end) ".$value->field."____0,count(case ".$value->field." when '2' then 1 else null end) ".$value->field."____1");
					break;
					case 'FixedvalueDistrictlevel':
					array_push($selectqueryfields,$value->field.'____0');
					//array_push($villages_fields,"ROUND(sum(".$value->field."),2) as ".$value->field."____0");
					array_push($villages_fields,"ROUND(sum(".$value->field.")/count(".$value->field."),2) as ".$value->field."____0");
					break;
					case 'Countsofabc':
					array_push($selectqueryfields,$value->field.'____0');
					array_push($selectqueryfields,$value->field.'____1');
					array_push($selectqueryfields,$value->field.'____2');
					array_push($villages_fields,"count(case ".$value->field." when 'a' then 1 else null end) ".$value->field."____0,count(case ".$value->field." when 'b' then 1 else null end) ".$value->field."____1,count(case ".$value->field." when 'c' then 1 else null end) ".$value->field."____2");
					break;
					case 'RowElements':
						if ($value->field == "village_count") {
							array_push($selectqueryfields,$value->field);
						}
						//array_push($selectqueryfields,$value->field.'____0');
						//array_push($villages_fields,"(".$value->field.") as ".$value->field."____0");
					break;

					case 'Median/Average':
						//array_push($selectqueryfields,$value->field.'____0');
						//array_push($selectqueryfields,$value->field.'____1');
						array_push($selectqueryfields,$value->field.'____2');
						array_push($villages_fields," ROUND(AVG(".$value->field."),2) ".$value->field."____0,
						((SELECT MAX(".$value->field.") FROM(SELECT ".$value->field." FROM villages) AS BottomHalf)
						+
						(SELECT MIN(".$value->field.") FROM
						(SELECT ".$value->field." FROM villages) AS TopHalf)
						) / 2 AS ".$value->field."____1");
						break;
					case 'Median_hrs/Average_hrs':
						array_push($selectqueryfields,$value->field.'____0');
						array_push($selectqueryfields,$value->field.'____1');
						array_push($villages_fields," ROUND(AVG(".$value->field."),2) ".$value->field."____0,
						((SELECT MAX(".$value->field.") FROM(SELECT ".$value->field." FROM villages) AS BottomHalf)
						+
						(SELECT MIN(".$value->field.") FROM
						(SELECT ".$value->field." FROM villages) AS TopHalf)
						) / 2 AS ".$value->field."____1");
						break;
				}
				array_push($villages_where," ".$value->field." is not null ");
				if($getminmax==null)
				{
					 if (!empty($subDistrict)) {
					 	$villages_appendquery = "WHERE Sub_District_Code IN ('".$subDistrict."') AND State_name IN ('".$states."')  GROUP BY Sub_District_Code";

					 	$villages_appendquery1 = "WHERE Sub_District_Code IN ('".$subDistrict."') AND State_name IN ('".$states."')";
					 }else{
						$villages_appendquery = "WHERE india_information_OGR_FID IN ('".$district."') AND State_name IN ('".$states."')  GROUP BY  district";
						$villages_appendquery1 = "WHERE india_information_OGR_FID IN ('".$district."') AND State_name IN ('".$states."')  ";
					}
				}
				else if($getminmax=="max" || $getminmax=="min")
				{
					//$villages_appendquery = "GROUP BY  district_name";
					$villages_appendquery = "GROUP BY  district_name";
				}
				//break;
			}

		}

		$tabledata =array();
		if(!empty($villages_fields))
		{
			if($getminmax=="max")
			{
				$query1 = 'select max('. implode("),max(",$selectqueryfields).') from (';

			}
			else if($getminmax=="min")
			{
				$query1 = 'select min('. implode("),min(",$selectqueryfields).') from (';
			}
			else
			{
				$query ="";
			}

			if (!empty($subDistrict)) {
				//$query = "Select ".implode(",",$villages_fields). ",concat(Sub_District_Name,'-',District_name,'-',State_name) as name  ,concat(district) as District_Code, Sub_District_Code, Sub_District_Name, District_Name, india_information_OGR_FID as OGR_FID from villages ".$villages_appendquery;
				//$query = $query ." and " .implode(' and ',$villages_where);


				/* 	Added by hirin for faster query results -

					created new tables to dump all formula results in another table and fetching records from new table

					Sub_District_Name 	District_name 	Distance_to_NearestPrimary_SchoolAvailable_2 	State_name 	district 	Sub_District_Code 	india_information_OGR_FID
				*/

				//$query = "Create Table villages_summary_sub_dist as Select ".str_replace("____","_",implode(",",$villages_fields)). ", Sub_District_Name ,District_name,State_name,district, Sub_District_Code, india_information_OGR_FID from villages ".$villages_appendquery;

				$selectqueryfields2=[];
				for ($d =0;$d<count($selectqueryfields);$d++){
					# code...
					$selectqueryfields2[$d]=strrev(preg_replace(strrev("/____/"),strrev("_"),strrev($selectqueryfields[$d]),1)). " as ".$selectqueryfields[$d] ;
				}
				$query = "select  ".implode(", ",$selectqueryfields2). " ,concat(Sub_District_Name,'-',District_name,'-',State_name) as name  ,concat(district) as District_Code, Sub_District_Code, Sub_District_Name,  District_Name , india_information_OGR_FID as OGR_FID from  villages_summary_sub_dist ".$villages_appendquery1;


			}else{
				//$query = "Select ".implode(",",$villages_fields). ",concat(District_name,'-',State_name) as name  ,concat(district) as District_Code, Sub_District_Code, Sub_District_Name, india_information_OGR_FID as OGR_FID  from villages ".$villages_appendquery;
				//$query = $query ." and " .implode(' and ',$villages_where);

				//$query = "Create Table villages_summary_dist as Select ".str_replace("____","_",implode(",",$villages_fields)). ",District_name,State_name ,district, india_information_OGR_FID from villages ".$villages_appendquery;

				$selectqueryfields2=[];
				for ($d =0;$d<count($selectqueryfields);$d++){
					# code...
					$selectqueryfields2[$d]=strrev(preg_replace(strrev("/____/"),strrev("_"),strrev($selectqueryfields[$d]),1)). " as ".$selectqueryfields[$d] ;
				}
				if (! empty(implode(", ",$selectqueryfields2)))
				{
					$query = "select  ".implode(", ",$selectqueryfields2). ",concat(District_name,'-',State_name) as name  ,concat(district) as District_Code,  india_information_OGR_FID as OGR_FID from villages_summary_dist ".$villages_appendquery1;
				}
				else
				{
					$query = "select  concat(District_name,'-',State_name) as name  ,concat(district) as District_Code,  india_information_OGR_FID as OGR_FID from villages_summary_dist ".$villages_appendquery1;
				}
			}

			if($getminmax=="max")
			{
				$queryfinalmax  = $query1.$query.') a ';

				$db->setQuery($queryfinalmax);
				$obOperator = $db->loadobjectList();
			}
			elseif ($getminmax=="min") {
				$queryfinalmin = $query1.$query.') b ';
				$db->setQuery($queryfinalmin);
				$obOperator = $db->loadobjectList();
			}
			else
			{
				$db->setQuery($query);
				$obOperator = $db->loadobjectList();
			}
		}
		$session->set('village_column_names',$village_column_names);
		return $obOperator;
	}

	/**
	 * function called from controller.
	 */
	public function getMaxForMatrix(){


		$session = JFactory::getSession();
		$db      = JFactory::getDBO();

		$table         = "villages";
		$variable      = $session->get('summeryattributes');

		$maxValsSelect = "";

		$maxValsSelect[] = "MAX(" .$variable. ") AS '" . $variable . "'";
		$maxValsSelect = implode(" , ",$maxValsSelect);
		//Complete the query
		$maxValsQuery = "SELECT $maxValsSelect FROM $table";
		$db->setQuery($maxValsQuery);
		return $db->loadAssocList();
	}

	/**
	 *
	 */
	public function getGraph(){
		$session    = JFactory::getSession();
		$states     = $session->get('summarystate');
		$district   = $session->get('summarydistrict');
		$villages   = $session->get('villages');
		$urban      = $session->get('urban');
		$attributes = $session->get('summeryattributes');

		$attributes = explode(",",$attributes);
		$attributes = array_filter($attributes);
		$attr       = array();
		$i          = 0;

		foreach($this->getCustomData(1,1) as $key=>$val){
			foreach($val as $ek=>$ev){
				if($ev == ""){
					$ev="0";
				}

				foreach($attributes as $eachattr){
					if($ek == $eachattr){
						$attr[$ek][]=$ev;
					}

					if($ek == $this->urnbanprefix.$eachattr){
						$attr[JTEXT::_($this->urnbanprefix.$eachattr)][]=$ev;
					}

					if($ek == $this->totalprefix.$eachattr){
						$attr[JTEXT::_($this->totalprefix.$eachattr)][]=$ev;
					}

					if($ek == $this->ruralprefix.$eachattr){
						$attr[JTEXT::_($this->ruralprefix.$eachattr)][]=$ev;
					}

					if($ek == "name"){
						$attrs['name'][$i]=$ev;
					}
				}
				if($i == 5){break;}
			}
			if($i == 5){break;}
			$i++;
		}

		$customattribute = $session->get('customattribute');
		$eachcustomattr  = array();
		$customattribute = explode(",",$customattribute);
		$customattribute = array_filter($customattribute);
		$z=0;

		foreach($attrs['name'] as $eachname){
			foreach($customattribute as $eachcustom){
				$eachcustom = explode(":",$eachcustom);
				$eachcustomattr[$eachcustom[0]][$z] = $this->getCustomAttributeValue($eachcustom[1],$this->_data,$eachname);
				if($z==10){break;}
			}
			if($z==5){break;}
			$z++;
		}

		$attr          = array_merge($attr,$eachcustomattr);
		$attr          = array_slice($attr,0,10);
		$arraytoreturn = "";
		$lastkey       = count(($attr));
		$z             = 1;

		foreach($attr as $key => $val){
			if($z !== $lastkey){
				$arraytoreturn .=JTEXT::_($key).";".implode(";",$val)."sln";
			}else{
				$arraytoreturn .=JTEXT::_($key).";".implode(";",$val);
			}
			$z++;
		}

		return $arraytoreturn;
	}

	/*
		Get Graph Data
	*/
	public function getGraph_v2()
	{
		$session    = JFactory::getSession();
		$db         = JFactory::getDBO();
		$states     = $session->get('summarystate');
		$district   = $session->get('summarydistrict');
		$villages   = $session->get('villages');
		$urban      = $session->get('urban');
		$attributes = $session->get('summeryattributes');
		$table      = 'villages';

		$attributes = explode(",",$attributes);
		$attributes = array_filter($attributes);
		$attr       = array();
		$i          = 0;
		$datasets   = []; //dataset of all cities

		$query ="SELECT * FROM ".$db->quoteName('#__mica_group_field')." AS allfields
		INNER JOIN ".$db->quoteName('#__mica_group')." AS grp ON ".$db->quoteName('grp.id')." = ".$db->quoteName('allfields.groupid')."
		LEFT JOIN ".$db->quoteName('#__mica_group_field_summary_map')." AS map ON ".$db->quoteName('allfields.field')." = ".$db->quoteName('map.field')."
		AND ".$db->quoteName('map.field')." = ". $db->quoteName('allfields.field') . "
		WHERE ".$db->quoteName('allfields.table')." LIKE ".$db->quote($table)."
		AND ".$db->quoteName('grp.publish')." = ".$db->quote(1)."
		And allfields.".$db->qn('field')." in ('".implode("','",$attributes)."')
		ORDER BY ".$db->quoteName('grp.ordering').", ".$db->quoteName('allfields.field')." ASC ";

		$db->setQuery($query);
		$list                  = $db->loadAssocList();
		$app                   = JFactory::getApplication();
		$chartSelectedDistrict = $app->input->getVar("selectedDistrict");
		$chartSelectedVariable = $app->input->getVar("selectedVariable");
		$isSubDist             = $app->input->getVar("isSubDist");

		if($chartSelectedDistrict != "" && $chartSelectedVariable != "") {
			$getCustomData = $this->getCustomData(0, 0, 1, null, $chartSelectedDistrict, $chartSelectedVariable, 0, $isSubDist);
		}
		else
		{
			$getCustomData = $this->getCustomData(1,1);
		}

		foreach($getCustomData as $key=>$val){
			$cityname=$val->name."~~".$val->OGR_FID;

				foreach($val as $ek=>$ev){ //$ek:-variable name..., $ev:-value.....,
					if($ev == ""){
						$ev="0";
					}

					$fieldname = (explode("____",$ek));
					$key       = array_search($fieldname[0],array_column($list, 'field'));
					$newlabel  = $list[$key]['field_label'];
					$sublabels = explode(",",$list[$key]['field_sublabel']);
					$sublabel  = $sublabels[$fieldname[1]];
					$fn        = $fieldname[0];
					$calc      = $fieldname[1];

					$i=0;
					foreach($attributes as $eachattr){
                       if($fn == $eachattr){
							$attr[$ek][]=$ev;
						}

						if($fn == $eachattr){
							$attr[JTEXT::_($eachattr)][]=$ev;
							$sublabel = str_replace("Total","",$sublabel);
							$datasets[$newlabel." ".$sublabel."~~".$fn][$cityname]  = $ev;
						}
						if($fn == "name"){
							$attr['name'][$cityname][$i] = $ev;
						}
   					}
   				}
   			}

   			$secondlevelDatasets = array ();
			$lableKey = 0;
			foreach ($datasets as $key => $dataset) {
				$secondlevelDatasets[$lableKey]["label"]    = $key;
				$secondlevelDatasets[$lableKey]["data"]     = array_values($dataset);

				$lableKey++;
			}

			$datasets["secondlevelDatasets"] = $secondlevelDatasets;

   			return $datasets;
   		}
/*   		public function getMeter_v2(){

		$session    = JFactory::getSession();
		$states     = $session->get('summarystate');
		$district   = $session->get('summarydistrict');
		$villages   = $session->get('villages');
		$urban      = $session->get('urban');
		$attributes = $session->get('summeryattributes');


		$attributes = explode(",",$attributes);
		$attributes = array_filter($attributes);
		$attr       = array();
		$i          = 0;
		$datasets=[]; //dataset of all cities

		foreach($this->getCustomData(1,1) as $key=>$val){
			$cityname=$val->name."~~".$val->District_Code;

				foreach($val as $ek=>$ev){ //$ek:-var name $ev:-value
				if($ev == ""){
					$ev="0";
				}

              $fieldname = (explode("____",$ek));
              $fn = $fieldname[0];
			  $calc = $fieldname[1];
			  $i=0;
				foreach($attributes as $eachattr){

					if($fn == $eachattr){
						$attr[$ek][]=$ev;
					}

					if($fn == $eachattr){
						$attr[JTEXT::_($eachattr)][]=$ev;
						$datasets[JTEXT::_($eachattr)][$cityname]=$ev;
					}

					if($fn == $eachattr){
						$attr[JTEXT::_($eachattr)][]=$ev;
						$datasets[JTEXT::_($eachattr)][$cityname]=$ev;
					}

					if($fn == $eachattr){
						$attr[JTEXT::_($eachattr)][]=$ev;
						$datasets[JTEXT::_($eachattr)][$cityname]=$ev;
					}
					if($fn == "name"){
						$attr['name'][$cityname][$i] = $ev;
					}

				}

			}

		}

		return $datasets;
	}*/



   		public function getMeter_v2(){

		$session    = JFactory::getSession();
		$states     = $session->get('summarystate');
		$district   = $session->get('summarydistrict');
		$villages   = $session->get('villages');
		$urban      = $session->get('urban');
		$attributes = $session->get('summeryattributes');


		$attributes = explode(",",$attributes);
		$attributes = array_filter($attributes);
		$attr       = array();
		$i          = 0;
		$datasets=[]; //dataset of all cities

		foreach($this->getCustomData(1,1) as $key=>$val){

			$cityname=$val->name."~~".$val->District_Code;

				foreach($val as $ek=>$ev){ //$ek:-var name $ev:-value
					if($ev == ""){
						$ev="0";
					}


					$fieldname = (explode("____",$ek));
					$fn = $fieldname[0];
					$calc = $fieldname[1];
					$if=0;

					foreach($attributes as $eachattr){

                       if($fn == $eachattr){
							$attr[$ek][]=$ev;
						}

						if($fn == $eachattr){
							$attr[JTEXT::_($eachattr)][]=$ev;
							$datasets[$fn][$cityname]=$ev;
						}
						if($fn == "name"){
							$attr['name'][$cityname][$i] = $ev;
						}

   					}

   				}

   			}


   			return $datasets;
   		}


   	/*
		Get Selected Variable List for Speedo Meter Varible
	*/
	public function getSelectedVarListForSpeedoMeter()
	{
		$session  = JFactory::getSession();
		$attr     = $session->get('themeticattribute');//themeticattribute

		$htmlData = '<div class="row"><div class="col-sm-6"><div id="speed_variable"><div class="popup-heading"><input type="checkbox" name="metervariables_checkall" class="allcheck" id="metervariables_allcheck" /><label>Select All Variable</label></div><ul id ="variablesForMetre" class="list1 checkboxlist-popup">';

		$eachattr = explode(",",$attr);

		foreach($eachattr as $eachattrs){
			$htmlData .= '<li><input type="checkbox" class="metervariables_checkbox" id="metervariables_'.$eachattrs.'" value="'.$eachattrs.'" name="speed_variable[]" /><label for="metervariables_' . $eachattrs . '">'.JTEXT::_($eachattrs).'</label></li>';
		}

		$htmlData .= '</ul></div></div>';

		$district = $session->get('customdata');//Get Selected District

		$htmlData .= '<div class="col-sm-6"><div id="speed_region"><div class="popup-heading"><input type="checkbox" name="meterdistrict_checkall" class="allcheck" id="meterdistrict_allcheck" /><label for="meterdistrict_allcheck">Select Distrct</label></div><ul id ="DistrictForMetre" class="list1 checkboxlist-popup">';

		foreach($district as $eachdata){
			$htmlData .= '<li><input type="checkbox" class="meterdistrict_checkbox" id="meterdistrict_'.$eachdata->name.'" value="'.str_replace(" ","%20",$eachdata->name).'" name="speed_region[]" /><label for="meterdistrict_' . $eachattrs . '">'.$eachdata->name.'</label></li>';
		}

		$htmlData .= '</ul></div></div></div>';

		return $htmlData;
	}

	/**
	 *
	 */
	public function getGraphSettings(){
		$session = JFactory::getSession();
		$states        = $session->get('summarystate');
		$district      = $session->get('summarydistrict');
		$villages          = $session->get('villages');
		$urban         = $session->get('urban');
		//$attributes  = $session->get('attributes');
		$data_settings = "";
		/*
			if($villages!=""){
				$i=0;
				foreach($this->_data as $eachdata){
					$data_settings .= "<graph gid='".$i."'>";
					$data_settings .= "<title>".JTEXT::_($eachdata->name)."</title>";
					$data_settings .= "</graph>";
					$i++;
				}
			}else if($urban!=""){
				$i = 0;
				foreach($this->_data as $eachdata)
				{
					$data_settings .= "<graph gid='".$i."'>";
					$data_settings .= "<title>".JTEXT::_($eachdata->name)."</title>";
					$data_settings .= "</graph>";
					$i++;
				}
			}
		*/

			if(trim($district) == ","  || strlen($district) == 0){
				$i = 0;
				foreach($this->_data as $eachdata){
					$data_settings .= "<graph gid='".$i."'>";
					$data_settings .= "<title><![CDATA[".JTEXT::_($eachdata->name)."]]></title>";
					$data_settings .= "</graph>";
					$i++;
					if($i == 6){
						break;
					}
				}
			}else{
				$i = 0;
				foreach($this->_data as $eachdata){
					$data_settings .= "<graph gid='".$i."'>";
					$data_settings .= "<title><![CDATA[".JTEXT::_($eachdata->name)."]]></title>";
					$data_settings .= "</graph>";
					$i++;
					if($i == 6){
						break;
					}
				}
			}

		/*$session = JFactory::getSession();
		$attrnamesession=$session->get('customattribute');
		if(strlen($attrnamesession)>0)
		{
			$attrnamesession=explode(",",$attrnamesession);
			$attrnamesession=array_filter($attrnamesession);
			foreach($attrnamesession as $eachattr)
			{
				$name=explode(":",$eachattr);
				$data_settings .= "<graph gid='".$i."'>";
				$data_settings .= "<title><![CDATA[".$name[0]."]]></title>";
				$data_settings .= "</graph>";
				$i++;

			}

		}*/
		return $data_settings;
	}

	function scorevariablename($srprefix,$key,$type){
		foreach($srprefix as $eachprefix){
			if(strpos($key,$eachprefix) !== false){
				if($type == 1){
					return  str_replace($eachprefix,"",$key);
				}else{
					return str_replace("_","",$eachprefix);
				}
			}
		}
	}

	public function getDisplayGroup	(){

		$session = JFactory::getSession();
		$db      = JFactory::getDBO();
		$app = JFactory::getApplication();

		$district = $app->input->get("summarydistrict","", 'raw');
		$urban           = $session->get('urban');
		$villages        = $session->get('villages');
		$district        = $session->get('summarydistrict');
		$state           = $session->get('summarystate');

		$attributes      = $session->get('summeryattributes');
		$attributes      = explode(",",$attributes);

		$customattribute = $session->get('customattribute');
		$customattribute = explode(",",$customattribute);
		$eachcustomattr  = array();
		foreach($customattribute as $eachcustom){
			$eachcustom                     = explode(":",$eachcustom);
			$eachcustomattr[$eachcustom[0]] = $eachcustom[1];
		}
		$eachcustomattr = array_filter($eachcustomattr);


		if($district != "")
		{
			$db_table = "villages";
			$session->set('dataof',"District");
		}

		$query ="SELECT * FROM ".$db->quoteName('#__mica_group_field')." AS allfields
		INNER JOIN ".$db->quoteName('#__mica_group')." AS grp ON ".$db->quoteName('grp.id')." = ".$db->quoteName('allfields.groupid')."
		LEFT JOIN ".$db->quoteName('#__mica_group_field_summary_map')." AS map ON ".$db->quoteName('allfields.field')." = ".$db->quoteName('map.field')."
		AND ".$db->quoteName('map.field')." = ". $db->quoteName('allfields.field') . "
		WHERE ".$db->quoteName('allfields.table')." LIKE ".$db->quote($db_table)."
		AND ".$db->quoteName('grp.publish')." = ".$db->quote(1)."
		And allfields.".$db->qn('field')." in ('".implode("','",$attributes)."')
		ORDER BY ".$db->quoteName('grp.ordering').", ".$db->quoteName('allfields.field')." ASC ";

		$db->setQuery($query);
		$list              = $db->loadAssocList();

		$change_group_name = 1;
		$addedarray        = array();
		$data              = $this->getCustomData();

		foreach ($data as $key => $value) {
			# code...
			$statename = $value->name;
			$codes_for_villages[$statename]['state']=base64_encode($state);
			foreach ($value as $field => $val) {
				if($field=="name" || $field=="District_Code" || $field=="Sub_District_Code" || $field=="Sub_District_Name" || $field=="District_name" || $field=="OGR_FID")
					{
						switch ($field) {
							case 'District_Code':
								$codes_for_villages[$statename]['district']=$val;
								break;
							case 'District_name':
								$codes_for_villages[$statename]['district_name']=$val;
								break;
							case 'Sub_District_Code':
								$codes_for_villages[$statename]['subdistrict']=$val;
								break;
							case 'Sub_District_Name':
								$codes_for_villages[$statename]['subdistrict_name']=$val;
								break;
						}
						continue;
				  }
				$fieldname = (explode("____",$field));
				$fn        = $fieldname[0];

				$group = array_shift(array_filter($list,function($k) use ($fn){
					if($k['field']==$fn )
						return true;
					else
						return false;
				}));

				if($group['operator'] == 'Median_hrs/Average_hrs')
				{
					$val= floor($val) . ':' . (($val * 60) % 60);
				}

				$calc     = explode(",",$group['field_sublabel']);// $fieldname[1];
				$sublable = $calc[$fieldname[1]]==""? $fieldname[1]:$calc[$fieldname[1]];
				$todisplay[$statename][$group['group']][$group['field_label']."____".$group['field']][][$sublable] = $val;

				//$todisplay[$statename][$group['group']][$fn][][$calc]=$val;
			}
		}

		return array($todisplay,$icon,$codes_for_villages);
	}

	function countkeys($data){
		$i = 0;
		foreach($data as $eachdata){
			$i = $i + count($eachdata);
		}
		return $i;
	}

	function getNewAttributeTable_cont()
	{
		$gpdata        = $this->getDisplayGroup();
		$groupwisedata = $gpdata[0];
		$icon          = $gpdata[1];
		$segmentrow    = 0;

		foreach($icon as $ikey=>$ival)
		{
			foreach($groupwisedata as $key=>$val)
			{
				$newgroupwisedata[$key][$ikey]=array_pop(array_values($val[$ikey]));
				$segmentrow=count(array_pop(array_values($val[$ikey])));
			}
		}
		//echo "<pre>";echo $segmentrow;
		$totalrows= count($newgroupwisedata)*$segmentrow;
		$totalcolumns=count($icon);

		$session = JFactory::getSession();
		$activetable=$session->get("activetable");

		if($activetable  == "india_information"){
			$datatablename = "District Name";
		}

		if($activetable  == "rail_state"){
			$datatablename = "State Name";
		}

		if($activetable == "jos_mica_urban_agglomeration"){
			$datatablename = "UA Name";
		}

		if($activetable == "my_table"){
			$datatablename = "City Name";
		}

		/*$customattribute=$session->get('customattribute');
		$customattribute=explode(",",$customattribute);
		foreach($customattribute as $eachcustom)
		{
			$eachcustom=explode(":",$eachcustom);
			$eachcustomattr[$eachcustom[0]]=$eachcustom[1];

		}*/

		$c=array_keys($icon);
		$headernames=implode("</td><td width='200'>",$c);
		$str ="<tr class='firsttableheader'><td width='200'>".$datatablename."</td><td width='200'>".$headernames."</td></tr>";
		$i=0;
		foreach($newgroupwisedata as $key=>$val)
		{
			if($i<=$totalcolumns)
			{
				$str .="<tr ><td width='200'>".$key."</td>";
			}
			else
			{
					//$newgroupwisedata
				$i=0;
			}

			foreach($icon as $k=>$v)
			{

				$av=array_values($val[$k][$i]);
				$str .="<td width='200'>".$av[0]."</td>";


			}

			$str .="</tr>";
		}
		return $str;
	}

	/**
	 * A function called from view.
	 */
	//getNewAttributeTableold currently unused
	public function getNewAttributeTable(){

		$session       = JFactory::getSession();
		$gpdata        = $this->getDisplayGroup();
		$groupwisedata = $gpdata[0];
		$icon          = $gpdata[1];
		$codes_for_villages = $gpdata[2];
		$activetable   = $session->get("activetable");

		switch ($activetable) {
			case 'india_information':
			$datatablename = "District Name";
			break;

			case 'rail_state':
			$datatablename = "State Name";
			break;

			case 'jos_mica_urban_agglomeration':
			$datatablename = "UA Name";
			break;

			case 'my_table':
			$datatablename = "City Name";
			break;
		}

		$customattribute = $session->get('customattribute');
		$customattribute = explode(",",$customattribute);
		foreach($customattribute as $eachcustom){
			$eachcustom                     = explode(":",$eachcustom);
			$eachcustomattr[$eachcustom[0]] = $eachcustom[1];
		}

		$i=1; $j=0; $k=0; $l=0;
		$a=0;
		$headerspan = 0;
		//$groupwisedata=array_reverse($groupwisedata);


		foreach($groupwisedata as $mainname => $data){   //$mainname:- city name


			$label = $codes_for_villages[$mainname]['subdistrict_name']."  ". $codes_for_villages[$mainname]['district_name'];
			//$header[$i]     = "<td><a href='javascript:void(0);' data-name='".$mainname."'  id=finalvillage onclick=\"getdatajax_villages('".$codes_for_villages[$mainname]['state']."','".$codes_for_villages[$mainname]['district']."','".$codes_for_villages[$mainname]['subdistrict']."','".$label."')\">".$mainname."</a></td>";

			$header[$i]     = "<td><a href='javascript:void(0);' data-name='".$mainname."'  id=finalvillage onclick=\"getdatajax_villages('".$codes_for_villages[$mainname]['state']."','".$codes_for_villages[$mainname]['district']."','".$codes_for_villages[$mainname]['subdistrict']."','".$mainname."')\">".$mainname."</a></td>";
			$grpspan        = 0;
			$rearrangeddata = array();
			$tmp            = "";
			$tmp1           = "";

			foreach($data as $grpname => $grpdata){ //grpname:- variabel name
				if($grpname == "Mkt Potential Index" || $grpname == "Mpi"){
					$tmp = $grpdata;
				}else if($grpname == "Composite Score" ||$grpname == "Score"){
					$tmp1 = $grpdata;
				}else{
					$rearrangeddata[$grpname] = $grpdata;
				}
				$rea++;
			}


			$Market  = array("Mkt Potential Index" => $tmp);
			$Market1 = array("Composite Score" => $tmp1);

			$data    = array_merge($Market, $Market1, $rearrangeddata);



			foreach($data as $grpname => $grpdata){

				foreach($grpdata as $variablegrp => $variabledata){ //variablegrp:- operator  variabledata:-degit(value)
					$fieldname=explode("____",$variablegrp);
					$variablegrp=$fieldname[0];
					$variablegrporg=$fieldname[1];
					$grpspan = 0;
					$checkboxclass ="variable_checkbox";
					$keytopass = $variablegrp[0]['key'];

					$variable=str_replace(" ","_",$variablegrporg);


					$vargrpname[$i][$k] = "<td colspan='".count($variabledata)."' ><span class='' id=variabel_".$a.">".$variablegrp.
					'</span><a href="JavaScript:void(0);" onclick="getRemove_v2(\''.$checkboxclass.'\',\''.$variable.'\');" class="right_arrow"><i class="fa fa-trash"></i></a></td>';

					foreach($variabledata as $eachvariabledata){

						foreach($eachvariabledata as $key => $value)
						{

							$mygrpvariablename[$i][$k][$l] = "<td>".$key." ".$total."</td>";
							$mygrpdata[$i][$k][$l]         = "<td>".$value."</td>";
							$grpspan++;$headerspan++;
						}
						$img = "";
						if($icon[$grpname]!=""){
							$img = "<img src='".JUri::base()."components/com_mica/images/".$icon[$grpname]."'/>";
						}
						$grpheader[$i][$j] = "<td colspan='".$this->countkeys($grpdata)."'><div class='rightgrptext'>".$grpname."</div></td>";
						$l++;
					}
					$k++;
				}
				$j++;
			}
			$i++;
		}
		$str = "<table cellspacing='0' cellpadding='0' border='0' width='100%'  class='draggable' id='datatable'>";
		$str .= "<tr class='firsttableheader'><td width='200'>".$datatablename."</td>";//<tr><td colspan='".(($headerspan)+1)."'>Data</td></tr>
		foreach($grpheader as $inserheader){
			foreach($inserheader as $inserheaderval){
				$str .= $inserheaderval;
			}
			break;
		}

		$str .="</tr>";
		$str .="<tr class='secondtableheader'><td></td>";

		foreach($vargrpname as $eachgrpname){
			foreach($eachgrpname as $name){
				$str .= $name;
			}
			break;
		}
		$str .="</tr>";

		$a = array_pop(array_unique(array_pop(array_values(array_unique($mygrpvariablename)))));

		$a = array_filter(array_values($a));
		if(strlen(str_replace("<td></td>","",$a[0]))==0){
			$skip = 1;
		}else{
			$skip = 0;
		}

		if($skip == 0){
			$str .="<tr class='thirdtableheader'><td></td>";


			foreach($mygrpvariablename as $variablenamegrp)
			{

				foreach($variablenamegrp as $varname)
				{
					//$i=0;
					foreach($varname as $each)
					{

						$str .= $each;

					}
				}
				break;
			}
			$str .="</tr>";
		}

		$lastelements = 0;

		foreach($mygrpdata as $key=>$gdata){
			$loadclass = ($lastelements >=39)?"loadanother":"";
			$str      .= "<tr class='".$loadclass." data_align'>".$header[$key];

			foreach($gdata as $data){
				foreach($data as $d){
					$str .=($d=="")?"N/A":$d;
				}
			}
			$str .="</tr>";
			$lastelements++;
		}

		$str .="</table>";

		//echo $str;print_R($grpheader);print_R($vargrpname);print_R($mygrpvariablename);print_r($mygrpdata);echo $str;exit;


		return $str;
	}


	function hasValues($input, $deepCheck = true) {
		foreach($input as $value) {
			if(is_array($value) && $deepCheck) {
				if($this->hasValues($value, $deepCheck))
					return true;
			}
			elseif(!empty($value) && !is_array($value))
				return true;
		}
		return false;
	}

	public function getColspan($groupwisedata, $onlevel){
		$keys = array_keys($groupwisedata);
		$vals = array_values($groupwisedata);
		if($onlevel==3){
			return count($keys);
		}else if($onlevel==2){
			$count = $this->getCount($vals);
			return $count;
			return count($keys)*count($vals[0]);
		}else if($onlevel==1){
			return  $this->getColspan($groupwisedata,2)+$this->getColspan($groupwisedata,3);
		}
	}

	public function getCount($arr, $count = 0) {
	    foreach ($arr as $value) {
	        if (is_array($value)) {
	            $count = $this->getCount($value, $count);
	        } else {
	            $count = $count + 1;
	        }
	    }
	    return $count;
	}

	function getAttributeTable()
	{

		$session         = JFactory::getSession();
		$m_type          = $session->get('m_type');
		$states          = $session->get('summarystate');
		$district        = $session->get('summarydistrict');
		$customattribute = $session->get('customattribute');
		$customattribute = explode(",",$customattribute);

		foreach($customattribute as $eachcustom)
		{
			$eachcustom=explode(":",$eachcustom);
			$eachcustomattr[$eachcustom[0]]=$eachcustom[1];

		}


		$compareflag=0;
		if(count($this->_data) ==0)
		{
			return "No Data Available";
		}
		else if(count($this->_data) >1)
		{
			$compareflag=1;
		}
		$heads=array();
		if(1 != 1)
		{
			if(trim($district)=="")
				{	$i=0;
					foreach($this->_data as $key=>$val)
						{      foreach($val as $ek=>$ev)
							{
								if($ek!=$this->urbansearchvariable && $ek!=$this->townsearchvariable && $ek != $this->districtsearchvariable)
								{
									$heads[$ek][]=$ev;
								}
								else
								{
									$deleteindex[]=$ev;

								}


							}
						}
					}
					else
					{
						$i=0;
						foreach($this->_data as $key=>$val)
							{      foreach($val as $ek=>$ev)
								{

									if($ek!=$this->urbansearchvariable && $ek!=$this->townsearchvariable && $ek != $this->districtsearchvariable)
									{
										$heads[$ek][]=$ev;
									}
									else
									{
										$deleteindex[]=$ev;

									}

								}
							}
						}


						$heads=array_reverse($heads);
						$heads=array_filter($heads);
						$heads=array_merge($heads,$eachcustomattr);
						$deleteindex=array_reverse($deleteindex);
						$mykeys=array_keys($heads);
						$eachcustomattr=array_filter($eachcustomattr);


						$head="";
						$i=0;
						$data="";
						foreach($mykeys as $eachkey)
						{
							if($eachkey=="name")
							{
								$head .="<th>State Name</th>";
							}
							else if($eachkey=="distshp")
							{
								$head .="<th>District Name</th>";
							}
							else if($eachkey=="place_name")
							{
								$head .="<th>Villages Name</th>";
							}
							else if($eachkey=="UA_name")
							{
								$head .="<th>Urban Name</th>";
							}
							else
							{
								$head .="<th>".$eachkey."</th>";
							}


						}
						$head ="<tr>".$head."</tr>";

						foreach($this->_data as $eachkey)
						{
							$data .="<tr>";
							for($i=0;$i<count($mykeys);$i++)
							{
								if(!empty($eachcustomattr[$mykeys[$i]]) && isset($eachcustomattr[$mykeys[$i]]))
								{
									$ev=$this->getCustomAttributeValue($eachkey->$mykeys[$i],$this->_data);
									$data .="<td>".JTEXT::_($ev)."</td>";

								}
								else if(is_object($eachkey))
								{
									$data .="<td>".JTEXT::_($mykeys[$i])."</td>";
								}
							}
							$data .="</tr>";
						}
					}
					else
					{

						foreach($this->_data as $key=>$val)
						{
							foreach($val as $ek=>$ev)
							{
								if($ek!=$this->urbansearchvariable && $ek!=$this->townsearchvariable && $ek != $this->districtsearchvariable)
								{
									$heads[$ek][]=$ev;
								}
								else
								{
									$deleteindex[]=$ev;

								}

							}
						}
				//echo "<pre>";
				//print_R($this->_data)
						$heads=array_reverse($heads);
				//$deleteindex=array_reverse($deleteindex);
						$mykeys=array_keys($heads);
						$data ="";
						$j=0;
						$heads=array_merge($heads,$eachcustomattr);
						$heads=array_filter($heads);

						foreach($heads as $ek=>$ev)
							{	$originalname[]=$ek;
								if($ek=="name")
								{
									$ek="State Name";
								}
								else if($ek=="distshp")
								{
									$ek="District Name";
								}
								else if($ek=="place_name")
								{
									$ek="Villages Name";
								}
								else if($ek=="UA_Name")
								{
									$ek="Urban Name";
								}
								else
								{
									$ek =$ek;
								}

								if(!empty($eachcustomattr[$ek]))
								{
									$ev=$this->getCustomAttributeValue($eachcustomattr[$ek],$this->_data);
									$rowclass="deletecustom";
								}
								else
								{
									$rowclass="deleterow";
								}
								if($j!=0)
								{
									$todisplay ="<td ><input type='checkbox' class='".$rowclass."' value='".$originalname[$j]."' /></td>";
								}
								else
								{
									$todisplay="<td >Delete</td>";
								}
								$data .="<tr id='".$originalname[$j]."'>".$todisplay."<td>".$ek."</td>";


								$i=0;

								foreach($ev as $k=>$v)
								{

									if($i==0)
									{
										if($j == 0)
										{
											$myid="id = '".$v."'";
											$myclass="class='".$ek." '";
											$deletedcolumn="<input type='checkbox' id='removecolumn' class ='removecolumn' value='".$originalname[$j]."`".$deleteindex[$k]."`".$v."' />";
										}
										else
										{
											$myid="id = ''";
											$myclass="";
											$deletedcolumn="";
										}


									}
									else
									{
										if($j==0)
										{
											$myid="id = '".$v."'";
										}
										else
										{
											$myid="id = ''";
										}
										$myclass="class = ''";

									}

									$data .="<td ".$myclass." ".$id.">".$deletedcolumn.JTEXT::_($v)."</td>";

								}
								$data .="</tr>";
								$j++;
							}

						}
						return $head.$data;

					}

	/**
	 *
	 */
	function getCustomAttributeValue($name,$data,$identifier=null){
		$session = JFactory::getSession();
		$db      = $this->getDbo();

		$returnable = array();
		$i          = 0;

		$name1 =str_replace("+","",$name);
		$name1 =str_replace("*","",$name1);
		$name1 =str_replace("-","",$name1);
		$name1 =str_replace("/","",$name1);
		$name1 =str_replace("%","",$name1);
		$name1 =str_replace("SIN(","",$name1);
		$name1 =str_replace("COS(","",$name1);
		$name1 =str_replace("TAN(","",$name1);
		$name1 =str_replace("LOG(","",$name1);
		$name1 =str_replace("LOG10(","",$name1);
		$name1 =str_replace("SQRT(","",$name1);
		$name1 =str_replace("EXP(","",$name1);
		$name1 =str_replace("^","",$name1);
		$name1 =str_replace("EXP(","",$name1);
		$name1 =str_replace("LN(","",$name1);
		$name1 =str_replace("PI(","",$name1);
		$name1 =str_replace(")","",$name1);
		$name1 =str_replace("(","",$name1);
		$name1 =str_replace(range(0,9),'',$name1);

		//$name1 = str_replace(",,",",",$name1);
		$chkval  = explode(" ",$name1);
		$chkval  = array_filter($chkval);
		$field   = implode(",",$chkval);

		$table          = $session->get("activetable");
		$searchvariable = $session->get("activenamevariable");

		//foreach($data as $ed)
		{
			$dupname    = $name;
			$identifier = explode("-",$identifier);

			$query = "SELECT ".$field." FROM ".$table." WHERE ".$searchvariable." LIKE '".$identifier[0]."'";
			$db->setQuery($query);
			try {
				$list  = $db->loadObjectList();
			} catch (Exception $e) {
				$list  = NULL;
			}

			$list = array_pop($list);

			//if($ed->name==$identifier)
			{
				foreach($list as $k => $v){
					$dupname        = str_replace($k,$v,$dupname);
					$returnable[$i] = $dupname;
				}
			}
			$i++;
		}
		$i=0;

		foreach($returnable as $reeval)
		{
			$result="";
			if(strstr($reeval,"^")) {
				$cExplode = str_replace(" ","",$reeval);
				$cExplode = explode("^",$cExplode);
				$result   = pow($cExplode[0],$cExplode[1]);
				//@eval('$result = '.$formulaNew.';');
			}else if(strstr($reeval,"%")) {
				//$perExplode = str_replace("(","",$reeval);
				//$perExplode = str_replace(")","",$perExplode);
				$perExplode     = str_replace(" ","",$reeval);
				$perExplodeData = explode("%",$perExplode);
				$formulaNew     = "(".$perExplodeData[0].")/100";
				@eval('$result = '.$formulaNew.';');
			}else if(strstr($reeval,"LN(")){
				$lnexplode =explode('LN(',$reeval);
				$test      = explode(")",$lnexplode[1]);
				//$result  =pow(10,$test[0]);
				$result    =number_format(log($test[0]),5);
			}else if(strstr($reeval,"LOG(")){
				$lnexplode1 =explode('LOG(',$reeval);
				$test1      = explode(")",$lnexplode1[1]);
				//$result   =pow(10,$test[0]);
				$resultTmp  =log($test1[0],10);
				$result     = number_format($resultTmp,5);
			}else{
				@eval('$result = '.$reeval.';');
			}

			$returnables[$i]=  $result;
			$i++;
		}

		return $returnables[0];
	}

	function getWorkspace(){
		return workspaceHelper::loadWorkspace();
	}

	function updateWorkspace(){
		return workspaceHelper::updateWorkspace();
	}

	function deleteWorkspace(){
		return workspaceHelper::deleteWorkspace();
	}

	function saveWorkspace(){
		return workspaceHelper::saveWorkspace();
	}

	function unsetDefault(){
		return workspaceHelper::unsetDefault();
	}

	function getSldToDataBase()
	{
		$session=JFactory::getSession();
		$activeworkspace=$session->get('activeworkspace');
		$sld=cssHelper::getLayerProperty($activeworkspace,1);
		echo $sld;
		exit;
	}

	function getthematicQueryFromDb(){
		$db      = $this->getDbo();
		$session = JFactory::getSession();

		$activeworkspace=$session->get('activeworkspace');
		$query  = "SELECT * FROM ".$db->quoteName('#__mica_sld_legend')."
		WHERE ".$db->quoteName('workspace_id')." = ".$db->quote($activeworkspace);
		$db->setQuery($query);
		$result = $db->loadObjectList();
		//cont..
		/*$customformulafromsession=$session->get('customformula',$attributes);
		$customformula=$formula.":".$from."->".$to."->".$color."|";
		$customformulafromsession = $customformulafromsession.$customformula;
		$session->set('customformula',$customformulafromsession);*/
	}

	function getSldLevel(){
		$db      = $this->getDbo();
		$session = JFactory::getSession();

		$activeworkspace=$session->get('activeworkspace');

		$query  = "SELECT level FROM ".$db->quoteName('#__mica_sld_legend')."
		WHERE ".$db->quoteName('workspace_id')." = ".$activeworkspace."
		GROUP BY level";
		$db->setQuery($query);
		$result = $db->loadObjectList();

		$str   = "";
		$level = "0,1,2";
		foreach($result as $eachlevel){
			$level =str_replace($eachlevel->level.",","",$level);
		}
		return $level;
	}

  	/**
  	 * A function called from view.
  	 */
  	public function getBreadCrumb(){
  		$session=JFactory::getSession();

  		$urban    = $session->get('urban');
  		$villages = $session->get('villages');
  		$m_type   = $session->get('m_type');

  		$district = $session->get('summarydistrict');
  		$state    = $session->get('summarystate');
  		$dataof   = $session->get('dataof');


  		$statecount = explode(",",$state);
		$separator  = "<span class='ja-pathway-text'>&nbsp; &gt; &nbsp;</span>";//JHTML::_('image.site', 'arrow.png');
		$breadcrumb = "<div class='breadcrumb ' id='ja-pathway'><span class='ja-pathway-text'>Your Query :</span>";

		if(count($statecount) == 1){
			$breadcrumb .="<span class='ja-pathway-text'>State(".$state.")</span>".$separator;
			if($dataof == "District"){
				foreach($this->_data as $eachdata){
					if($eachdata->district_id == $district){
						$selected = $eachdata->district_id."&m_type=".$m_type;
						$name     = $eachdata->name;
						if($m_type == "Total"){
							$m_typedisp = "All";
						}else{
							$m_typedisp = $m_type;
						}
						break;

					}else{

						if($m_type == "Total"){
							$m_typedisp = "All";
						}else{
							$m_typedisp = $m_type;
						}

						if($district == "all"){
							$name     = "All";
							$selected = "all&m_type=".$m_type;
						}else{
							$name     = "Compare";
							$selected = "all&m_type=".$m_type;
						}
					}
				}
				$breadcrumb .="<span class='ja-pathway-text'>District(".$name.")</span>".$separator;
				//$breadcrumb .="<span class='ja-pathway-text'>Type(".ucfirst($m_typedisp).")</span>";
			}

			if($dataof == "Villages"){
				foreach($this->_data as $eachdata){
					if($eachdata->OGR_FID == $villages){
						$selected = $eachdata->OGR_FID;
						$name     = $eachdata->name;
						break;
					}else{
						if($villages == "all"){
							$name = "All";
						}else{
							$name = "Compare";
						}
						$selected = "all";
					}
				}
				$breadcrumb .="<span class='ja-pathway-text'>Villages(".$name.")</span>";
			}

			if($dataof == "UA" || $dataof == "Urban"){
				$dataof = "UA";
				foreach($this->_data as $eachdata){
					if($eachdata->OGR_FID==trim($urban)){
						$selected = $eachdata->OGR_FID;
						$name     = $eachdata->name;
						break;
					}else{
						if($urban=="all"){
							$name="All";
						}else{
							$name="Compare";
						}
						$selected="all";
					}
				}
				$breadcrumb .="<span class='ja-pathway-text'>Urban(".$name.")</span>";
			}
			$selected=$district;

		}else{
			$breadcrumb .="<span class='ja-pathway-text'>State</span>".$separator;
			if($dataof=="District"){
				$breadcrumb .="<span class='ja-pathway-text'>District</span>".$separator;
				//$breadcrumb .="<span class='ja-pathway-text'>".ucfirst($m_type)."</span>";
			}

			if($dataof=="Villages"){
				$breadcrumb .="<span class='ja-pathway-text'>Villages</span>";
			}

			if($dataof=="UA"){
				$breadcrumb .="<span class='ja-pathway-text'>Urban</span>";
			}
			$selected=$district;
		}

		$ac .="<div class='modifystruct'><div class='ja-pathway-text' id='activeworkspacename' style='float: left;padding-right: 4px;font-weight: bold;font-size:12px;'></div>";
		$ac .="<a class='ja-pathway-text' href='index.php?option=com_mica&view=summeryfront'>Modify</a></div>";
		$breadcrumb .="</div>";
		return $breadcrumb."~~~".$ac;
	}

	/**
	 * A function called from view.
	 */
	public function getcustomattrlib(){
		$session = JFactory::getSession();
		$db      = $this->getDBO();

		$activetable = $session->get("activetable");

		$query = "SELECT name, attribute
		FROM ".$db->quoteName('#__mica_user_custom_attribute_lib')."
		WHERE ".$db->quoteName('profile_id')." = ".$db->quote($session->get("activeworkspace"))."
		AND ".$db->quoteName('tables')." LIKE ".$db->quote($activetable);
		$db->setQuery($query);
		$allcostomattr = $db->loadAssocList();

		$str = "";
		foreach($allcostomattr as $eachcustomattr){
			$eachcustomattr['attribute'] = str_replace("'"," ",$eachcustomattr['attribute']);
			$str .= $eachcustomattr['name'].":".$eachcustomattr['attribute'].",";
		}
		$session->set('customattributelib',$str);
	}

	/**
	 * a function called from view.
	 */

	public function getInitialGeometryVillage(){

		$session = JFactory::getSession();
		$db      = $this->getDBO();

		$state = $session->get('villagestate');
		// $state =base64_decode($state);

		
		$state = explode(",",$state);
		$state = array_filter($state);
		$state = implode("','",$state);

		if($state != "all" && substr_count($state,",") == 0){
			//$where="1=1";
			//$query="select AsText(GROUP_CONCAT(india_state)) as poly from rail_state where ".$where;
			 $query = "SELECT AsText((india_state)) as poly
				FROM ".$db->quoteName('rail_state')."
				WHERE ".$db->quoteName('name')." IN ('".$state."') ";

		}else{
			return 0;
		}

		$db->setQuery($query);
		$allvertex = $db->loadAssocList();
		/*echo "<pre>";
		print_r($allvertex);
		exit;*/
	
		$str       = "";
		$allvertex = array_reverse($allvertex);

		foreach($allvertex as $eachvertex){
			// $str[] = $eachvertex['AsText(india_state)'];
		 	return $eachvertex['poly'];
		}		
		return implode(",", $str);
	}

	public function getInitialGeometry(){
		$session = JFactory::getSession();
		$db      = $this->getDBO();

		$state = $session->get('summarystate');
		$state = explode(",",$state);
		$state = array_filter($state);
		$state = implode("','",$state);

		if($state != "all" && substr_count($state,",") == 0){
			//$where="1=1";
			//$query="select AsText(GROUP_CONCAT(india_state)) as poly from rail_state where ".$where;
			$query = "SELECT AsText((india_state)) as poly
			FROM ".$db->quoteName('rail_state')."
			WHERE ".$db->quoteName('name')." IN ('".$state."') ";
		}else{
			return 0;
		}

		$db->setQuery($query);
		$allvertex = $db->loadAssocList();
		$str       = "";
		$allvertex = array_reverse($allvertex);
		foreach($allvertex as $eachvertex){
			//$str[] = $eachvertex['AsText(india_state)'];
			return $eachvertex['poly'];
		}
		return implode(",", $str);
	}

	/**
	 *
	 */
	public function getPagination(){
		// Load the content if it doesn't already exist
		if (empty($this->_pagination)) {
			$app = JFactory::getApplication();
			jimport('joomla.html.pagination');
			$this->_pagination = new JPagination(ceil(count($this->_data)/50), ( ($app->input->get("limitstart") != "") ? $app->input->get("limitstart") : 0), 1 );
		}
		return $this->_pagination;
	}

	/**
	 * A function called from view.
	 */
	public function getUnselectedDistrict(){
		$session = JFactory::getSession();
		$db      = $this->getDBO();

		$district = $session->get('summarydistrict');

		$explode_district = explode(",", $district);
		if (count($explode_district) > 0 && strlen($explode_district[0]) > 0 ) {
			 $query    = "SELECT DISTINCT district FROM ".$db->quoteName('villages')."
			WHERE district NOT IN (".$district.") and (district!=0 or district!=null)";
			$db->setQuery($query);
			try {
				$return = $db->loadAssocList();
			} catch (Exception $e) {
				$return = NULL;
			}

			foreach($return as $each){
				$returnable[]=$each['district'];
			}
			return implode(",",$returnable);
		}else{
			return '';
		}
	}
	public function planAuthentication()
	{
		$userid = JFactory::getUser()->id;
		$get_detail =JFactory::getUser();
		$parent_user= $get_detail->parent_user;

		$db    = $this->getDBO();

		$query = "SELECT os.allow_villagedata FROM " . $db->quoteName('#__osmembership_plans') . " AS op
				INNER JOIN " . $db->quoteName('#__osmembership_subscribers') . " AS os
				ON " . $db->quoteName('op.id') . " = " . $db->quoteName('os.plan_id') . "
				AND ( " . $db->quoteName('os.user_id') . " LIKE " . $db->quote($userid);
				if($parent_user!='')
				{
					$query.= " OR ". $db->quoteName('os.user_id') . " LIKE " . $parent_user . ")";
				}
				else {
					$query.=")";
				}
		$query.=" AND os.published = 1 ORDER BY " . $db->quoteName('os.id') . " DESC ";

		$db->setQuery($query);

		$allow_villagedata = $db->loadResult();

		return $allow_villagedata;
	}

	public function getSelectedDistListForGraph()
	{
		$session  = JFactory::getSession();
		$district = $session->get('customdata');//Get Selected District
		//$summarySubDistrict = $session->get('summarySubDistrict');//Get Selected District

		/*if($_SERVER['REMOTE_ADDR'] == '122.170.14.145'){
				echo "<pre>";
				print_r($summarySubDistrict);
				exit;
			}*/
		$jinput   = JFactory::getApplication()->input;
		$checkedDistValue = $jinput->getVar('checkedDistValue', '');
		$enablelegend = $jinput->getVar('enablelegend', '');

		/*if($_SERVER['REMOTE_ADDR'] == '122.170.14.145'){
				echo "enablelegend: " . $village_allcheck;
				
			}*/

		$selectedDistrictArray = explode(",", $checkedDistValue);

		if($selectedDistrictArray[0] == ''){
			$selectedDistrictArray = explode(",", $session->get('summarydistrict'));
		}

		$htmlData = '<ul id="statesForGraph" class="list1">';

		// Array sort by name
		array_multisort(array_column($district, 'name'), SORT_ASC, $district);

		foreach($district as $eachdata){
			// Handle Sub-District Data
			if (isset($eachdata->Sub_District_Code)) {
				if(in_array($eachdata->Sub_District_Code, $selectedDistrictArray)){
					$checked = "checked";
				}
				else{
					$checked = "";
				}
				$htmlData .= '<li class="issubdist" data-issubdist="1"><input type="checkbox" class="chartstates_checkbox"' .$checked. ' id="chartstates_'.$eachdata->name.'" value="'.$eachdata->Sub_District_Code.'" /><label for="chartstates_' . $eachdata->name . '">'.$eachdata->name.'</label></li>';
			}
			else{
				if(in_array($eachdata->OGR_FID, $selectedDistrictArray)){
					$checked = "checked";
				}
				else{
					$checked = "";
				}
				$htmlData .= '<li class="issubdist" data-issubdist="0"><input type="checkbox" class="chartstates_checkbox"' .$checked. ' id="chartstates_'.$eachdata->name.'" value="'.$eachdata->OGR_FID.'" /><label for="chartstates_' . $eachdata->name . '">'.$eachdata->name.'</label></li>';
			}
		}

		$htmlData .= '</ul>';

		return $htmlData;
	}

	/*
		Get Selected Variable List for Graph
	*/
	public function getSelectedVarListForGraph()
	{
		$session  = JFactory::getSession();
		$attr     = $session->get('villageSummeryThemeticattribute');//themeticattribute
		$attr_column_data = $session->get('village_column_names');

		$jinput   = JFactory::getApplication()->input;
		$checkedVarValue = $jinput->getVar('checkedVarValue', '');

		$selectedVarArray = explode(",",$checkedVarValue);
		if($selectedVarArray[0] == ''){
			$selectedVarArray = explode(",", $session->get('summeryattributes'));
		}

		$htmlData = '<ul id="variablesForGraph" class="list1">';

		$eachattr = explode(",",$attr);
		foreach($eachattr as $eachattrs){
			if(in_array($eachattrs, $selectedVarArray)){
				$checked = "checked";
			}
			else{
				$checked = "";
			}
			$htmlData .= '<li><input type="checkbox" class="chartvariables_checkbox"' .$checked. ' id="chartvariables_'.$eachattrs.'" value="'.$eachattrs.'" /><label for="chartvariables_' . $eachattrs . '">'.$attr_column_data[$eachattrs]->field_label.'</label></li>';
		}

		$htmlData .= '</ul>';

		return $htmlData;
	}

}
