<?php
/**
 * @package     Joomla.Site
 * @subpackage  com_mica
 *
 * @copyright   Copyright (C) 2005 - 2017 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

JLoader::register('MicaController', JPATH_COMPONENT . '/controller.php');

//DEVNOTE: import CONTROLLER object class
require_once( JPATH_COMPONENT.DIRECTORY_SEPARATOR.'helpers/Workspace.php' );
require_once( JPATH_COMPONENT.DIRECTORY_SEPARATOR.'helpers/css.php' );
require_once( JPATH_COMPONENT.DIRECTORY_SEPARATOR.'helpers/pdf.php' );

/**
 * Fullmap controller class for MICA.
 *
 * @since  1.6
 */
class MicaControllerFullmap extends MicaController
{

	//Custom Constructor
	var $tomcat_url             = "";
	var $townsearchvariable     = "OGR_FID";
	var $statesearchvariable    = "OGR_FID";
	var $urbansearchvariable    = "OGR_FID";
	var $districtsearchvariable = "OGR_FID";
	var $ruralprefix            = "Rural_";
	var $urnbanprefix           = "Urban_";
	var $totalprefix            = "Total_";
	var $_modal                 = "";

	function __construct($default = array()){
		parent::__construct($default);

		if(!defined('TOMCAT_URL')){
			$this->getTomcatUrl();
			DEFINE('TOMCAT_URL', $this->tomcat_url);
		}

		//DEFINE('TOMCAT_STYLE_ABS_PATH',"/usr/share/tomcat7/webapps/geoserver/data/www/styles/");
		//DEFINE('TOMCAT_STYLE_ABS_PATH',"/home/mimi/apache-tomcat-7-0-26/webapps/geoserver/data/www/styles/");
		if(!defined('TOMCAT_STYLE_ABS_PATH')){
			//DEFINE('TOMCAT_STYLE_ABS_PATH' ,"/usr/local/easy/share/easy-tomcat7/webapps/geoserver/data/www/styles/");
			DEFINE('TOMCAT_STYLE_ABS_PATH' , JPATH_SITE."/geoserver/data/www/styles/");
		}
		$this->_table_prefix = '#__mica_';
	}

	/**
	 * Proxy for getModel.
	 *
	 * @param   string  $name    The model name. Optional.
	 * @param   string  $prefix  The class prefix. Optional.
	 * @param   array   $config  Configuration array for model. Optional.
	 *
	 * @return  object  The model.
	 *
	 * @since   1.6
	 */
	public function getModel($name = 'Fullmap', $prefix = 'MicaModel', $config = array('ignore_request' => true))
	{
		return parent::getModel($name, $prefix, $config);
	}

	/**
	 * ajax task.
	 */
	public function deleteattribute(){
		$app     = JFactory::getApplication();
		$session = JFactory::getSession();

		$removeattr = $this->input->get('attr', '', 'raw');
		$attributes = $session->get('attributes');

		$original   = strlen($attributes);

		$attributes = str_replace($removeattr,"",$attributes);
		$attributes = explode(",",$attributes);
		$attributes = array_filter($attributes);
		$attributes = implode(",",$attributes);
		$removed    = strlen($attributes);

		if($original == $removed){
			$this->deleteCustomAttribute();
		}
		$session->set('attributes',$attributes);

		$app->redirect("index.php?option=com_mica&view=showresults&Itemid=188");exit;
	}

	function deleteCustomAttribute(){
		$removeattr = $this->input->get('attr', '', 'raw');
		$this->input->set('attrname',$removeattr);

		$this->deleteCustomAttr(1);
		$this->deletethematicQueryToSession(1);

		return true;

		/*
			$session = JFactory::getSession();
			$attributes=$session->get('customattribute');
			$attributes=explode(",",$attributes);
			foreach($attributes as $k=>$eachattr)
			{	if(strstr($eachattr,$removeattr))
				{
					unset($attributes[$k]);
				}

			}
			//echo "<pre>";
			//print_r($attributes);
			$attributes=array_filter($attributes);
			$attributes=implode(",",$attributes);
			$session->set('customattribute',$attributes);
		*/
		//return true;
	}

	function deleteCustomVariableFromLib()
	{
		$session     = JFactory::getSession();
		$db          = JFactory::getDbo();

		$workspaceid = $session->get('activeworkspace');
		$activetable = $session->get('activetable');

		$name = $this->input->get("attrname", '', 'raw');
		$name = explode(",",$name);

		foreach($name as $eachvariable){
			$this->input->set('attr', $eachvariable);
			$this->deleteCustomAttribute();

			$query = "DELETE FROM ".$db->quoteName('#__mica_user_custom_attribute')."
				WHERE ".$db->quoteName('profile_id')." = ".$db->quote($workspaceid)."
					AND ".$db->quoteName('name')." LIKE ".$db->quote($eachvariable);
			$db->setQuery($query);
			$db->execute();

			$query = "DELETE FROM ".$db->quoteName('#__mica_user_custom_attribute_lib')."
				WHERE ".$db->quoteName('profile_id')." = ".$db->quote($workspaceid)."
					AND ".$db->quoteName('name')." LIKE ".$db->quote($eachvariable)."
					AND ".$db->quoteName('tables')." LIKE ".$db->quote($activetable);
			$db->setQuery($query);
			$db->execute();
		}

		$app = JFactory::getApplication();
		$app->redirect("index.php?option=com_mica&view=showresults&Itemid=188");
	}

	/**
	 * ajax task.
	 */
	public function deleteVariable(){
		$session = JFactory::getSession();

		$table = $this->input->get('table', '', 'raw');
		$value = $this->input->get('value', '', 'raw');

		if($table == "name"){

			$state      =$session->get('state');
			$attributes =str_replace($value,"",$state);
			$attributes =explode(",",$attributes);
			$attributes =array_filter($attributes);
			$attributes =implode(",",$attributes);
			$session->set('state',$attributes);

		}else if($table=="distshp"){

			$district   =$session->get('district');
			$attributes =str_replace($value,"",$district);
			$attributes =explode(",",$attributes);
			$attributes =array_filter($attributes);
			$attributes =implode(",",$attributes);
			$session->set('district',$attributes);

		}else if($table=="place_name"){

			$town       =$session->get('town');

			$attributes =str_replace($value,"",$town);
			$attributes =explode(",",$attributes);
			$attributes =array_filter($attributes);
			$attributes =implode(",",$attributes);
			$session->set('town',$attributes);

		}else if($table=="UA_Name"){

			$urban      =$session->get('urban');
			$attributes =str_replace($value,"",$urban);
			$attributes =explode(",",$attributes);
			$attributes =array_filter($attributes);
			$attributes =implode(",",$attributes);
			$session->set('urban',$attributes);
		}
		exit;
	}


	/**
	 * Internal function fetches URL.
	 */
	function getTomcatUrl(){
		$db    = JFactory::getDBO();
		$query = "SELECT tomcatpath FROM ".$db->quoteName('#__mica_configuration');
		$db->setQuery($query);
		$this->tomcat_url = $db->loadResult();
	}

	function popupAttributes()
	{
		$session=JFactory::getSession();
		$attributes=$session->get('attributes');
		$town=$session->get('town');
		$urban=$session->get('urban');
		$id=$this->input->get("id", '', 'raw');
		$zoom=$this->input->get("zoom", '', 'raw');


		if($zoom ==5 || $zoom==6)
		{
			$searchvariable=$this->statesearchvariable;
			$state=$session->get('state_orgfid');
			$session->set('state_orgfid',$state.",".$id);
			$sld=$this->sldPopup($searchvariable,$id,"rail_state","name");
		}
		else if($zoom==7)
		{
			$searchvariable=$this->districtsearchvariable;
			//$district=$session->get('district');
			//$session->set('district',$district.",".$id);
			$district_orgfid=$session->get('district_orgfid');
			$session->set('district_orgfid',$district_orgfid.",".$id);
			$sld=$this->sldPopup($searchvariable,$id,"india_information","distshp");
		}
		else  if($zoom==8)
		{
			$searchvariable=$this->urbansearchvariable;
				$urban_orgfid=$session->get('urban_orgfid');
			$session->set('urban_orgfid',$urban_orgfid.",".$id);
			$sld=$this->sldPopup($searchvariable,$id,"jos_mica_urban_agglomeration","UA_Name");




		}
		else
		{
				$searchvariable=$this->townsearchvariable;
				$town_orgfid=$session->get('town_orgfid');
			$session->set('town_orgfid',$town_orgfid.",".$id);
			$sld=$this->sldPopup($searchvariable,$id,"my_table","place_name");
		}
		$session->set('state_orgfid',null);
		$session->set('district_orgfid',null);
		$session->set('town_orgfid',null);
		$session->set('urban_orgfid',null);
		$str= "<div ><table class='popupmaintable ' cellspacing='0' cellpadding='0' border='1'>";
		$header="";
		$sld=array_reverse($sld);
		if(count($sld)>0)
		{
			$header .= "<table class='popupheader'><tr>";
			$header .="<td class='popupleft'>Name</td>";
			$header .="<td class='popupright'>".$sld[0]["text"]."</td>";
			$header .= "</tr></table>";
			foreach($sld as $eachsld)
			{



						if($eachsld['custom_name']=="")
						{
							$name=$eachsld['custom_formula'];
						}
						else
						{
							$name=$eachsld['custom_name'];
						}
						$str .= "<tr >";

						$str .="<td >".$name."</td>";//custom_value
						$str .="<td >".$eachsld['custom_value']."</td>";
						if($eachsld['level']!=0)
						{
						$color= str_replace("#","",$eachsld['color']);
			$str .="<td ><img src='".JURI::base()."components/com_mica/maps/img/layer".$eachsld['level']."/pin".$color.".png' /></td>";
						}
						else
						{
						$str .="<td style='background:".$eachsld['color'].";'></td>";
						}

						 $str .= "</tr>";

			}

		}
		else
		{


			$this->_modal=$this->getModel();//'showresults','showresultsModel'
			$data= $this->_modal->getCustomData('popup');


			$header="";
			foreach($data as $basekey=>$object)
			{
				foreach($object as $key=>$val)
				{	//echo $object->$searchvariable."->".$id."<br>";
					if( $object->$searchvariable == $id && $key != $searchvariable)
					{	if(strstr($key,"name"))
						{
							$header .= "<table class='popupheader'><tr>";
							$header .="<td class='popupleft'>".$key."</td>";
							$header .="<td class='popupright'>".$val."</td>";
							$header .= "</tr></table>";
						}
						else
						{
							$str .= "<tr >";
							$str .="<td class='popupleft'>".$key."</td>";
							$str .="<td class='popupright'>".$val."</td>";
							$str .= "</tr>";
						}
					}
				}
			}

		}
		$str .= "</table></div>";
		echo $header.$str;
		exit;
		//echo $id;
		//echo $attributes;exit;
	}

	function sldPopup($searchvarable,$searchvalue,$basetable,$searchtextvariable)
	{
		$session = JFactory::getSession();
		$db      = JFactory::getDBO();

		$activeworkspace = $session->get('activeworkspace');
		$finalarray      = array();

		$query = "SELECT sld.*, sld.layerfill, t1.".$searchtextvariable." , t2.*
			FROM ".$db->quoteName('#__mica_map_sld')." AS sld ,
				".$db->quoteName($basetable)." AS t1 ,
				".$db->quoteName('#__mica_sld_legend')." AS t2
			WHERE ".$db->quoteName('sld.text')." = ".$db->quoteName("t1.".$searchtextvariable)."
				AND ".$db->quoteName("t1.".$searchvarable)." = ".$db->quote($searchvalue)."
				AND ".$db->quoteName('profile_id')." = ".$db->quote($activeworkspace)."
				AND ".$db->quoteName('t2.workspace_id')." = ".$db->quoteName('profile_id')."
				AND ".$db->quoteName('sld.layerfill')." = ".$db->quoteName('t2.color')."
			GROUP BY ".$db->quoteName('t2.level')." DESC ";
		$db->setQuery($query);
		$list = $db->loadAssocList();

		foreach($list as $eacharray){
			$query = "SELECT name
				FROM ".$db->quoteName('#__mica_user_custom_attribute')."
				WHERE ".$db->quoteName('attribute')." = ".$db->quote($eacharray['custom_formula'])."
					AND ".$db->quoteName('profile_id')." = ".$db->quote($eacharray['profile_id']);
			$db->setQuery($query);
			$customformulaname = $db->loadResult();

			if($customformulaname == ''){
				$customname = array("custom_name"=>"");
			}else{
				$customname = array("custom_name"=>$customformulaname);
			}

			$eacharray = array_merge($customname,$eacharray);

			$query = "SELECT (".$eacharray['custom_formula'].") as customval FROM ".$basetable." WHERE ".$searchvarable."=".$searchvalue;
			$db->setQuery($query);
			$customformulavalue = $db->loadResult();

			$customvalue  = array("custom_value"=>$customformulavalue);
			$eacharray    = array_merge($customvalue,$eacharray);
			$finalarray[] = $eacharray;
		}

		return $finalarray;
	}

	/**
	 * A task to fetch attributes from AJAX call.
	 */
	public function getAttribute(){
		$zoom = $this->input->get('zoom', '', 'raw');
		$type = $this->input->get('type', '', 'raw');

		$user = JFactory::getUser();
		if($user->id <= 0){
			echo -1;exit;//JRoute::_('index.php?option=com_user&view=login');
		}
		echo $values = $this->getPlanAttr($zoom,$type);
		exit;
	}


	function getPlanAttr($zoom,$type=null)
	{	$prefix=0;
		if($type==null)
		{
			if($zoom == 5)
			{
				$db_table="State";
				$prefix=1;
			}
			else if($zoom > 5 && $zoom <= 7)
			{
				$db_table="District";
				$prefix=1;
			}
			else
			{
				$db_table="Town";
			}
		}
		else
		{
			$db_table=$type;
		}


		 $userAttr=$this->userSelectedAttr($db_table);

		if($db_table=="state")
		{
			$prefix=1;
		}
		else if($db_table=="district")
		{
			$prefix=1;
		}
		else
		{
			$prefix=0;
		}



		if($userAttr==-2)
		{
			return -2;
		}
		$str="";
		$todisplay=array();
		$db = JFactory::getDBO();
		$query="select * from #__mica_group_field allfields JOIN #__mica_group grp ON grp.id=allfields.groupid where allfields.table like '".$db_table."' and grp.publish =1 order by grp.group ASC";
		$db->setQuery($query);
		$list=$db->loadAssocList();

		foreach($list as $each)
		{
			if($each['group']!="Mkt Potential Index")
			{
				if($prefix==1)
				{
					$each['field']=str_replace($this->ruralprefix,"",$each['field']);
					$each['field']=str_replace($this->urnbanprefix,"",$each['field']);
					$each['field']=str_replace($this->totalprefix,"",$each['field']);
					$todisplay[$each['group']][]=$each['field'];
				}
				else
				{
					$todisplay[$each['group']][]=$each['field'];
				}
				$icon[$each['group']]=$each['icon'];
			}
			else
			{
				if($prefix==1)
				{
					$each['field']=str_replace($this->ruralprefix,"",$each['field']);
					$each['field']=str_replace($this->urnbanprefix,"",$each['field']);
					$each['field']=str_replace($this->totalprefix,"",$each['field']);
					$todisplay1[$each['group']][]=$each['field'];
				}
				else
				{
					$todisplay1[$each['group']][]=$each['field'];
				}
				$icon[$each['group']]=$each['icon'];
			}

		}
		$todisplay=array_merge($todisplay1,$todisplay);

		foreach($todisplay as $eachgrp=>$vals)
		{
			$todisplay[$eachgrp]=array_unique($todisplay[$eachgrp]);
		}
		foreach($userAttr as $each)
		{
			if($prefix==1)
			{
				$each=str_replace($this->ruralprefix,"",$each);
				$each=str_replace($this->urnbanprefix,"",$each);
				$each=str_replace($this->totalprefix,"",$each);
				$userAttr1[]=$each;
			}
			else
			{
				$userAttr1[]=$each;
			}

		}

		$userAttr =array_unique($userAttr1);
		$session  =JFactory::getSession();
		$oldattr  =$session->get('oldattributes');
		$urban    ="";
		$town     =array();
		$other    ="";
		$str      ="";
		$i        =0;
		$str1     ="";
		$other    =array();
		$urban    =array();
		$total    =array();
		$grpname  =array();
		$header   ="";
		foreach($todisplay as $key=>$val)
		{
			$grpname[]=$key;
			$str[$i]="";
			if($icon[$grpname[$i]]!=""){$img=$icon[$grpname[$i]];}else{$img="default.png";}
			$header .="<li ><a href='#".str_replace(" ","_",trim($grpname[$i]))."grp'><div class='grpimg'><img src='".JUri::base()."components/com_mica/images/".$img."'/></div><div class='grptitle'>".$grpname[$i]."</div></a></li>";
			//$oldattr=explode(",",$oldattr);
			foreach($userAttr as $eachattr)
			{
				//echo $eachattr."<br>";
				if(in_array($eachattr,$val))
				{
					if($eachattr=="Rating"){$checked="checked";}
					else if(stristr($oldattr,$eachattr)){$checked="checked";}else{ $checked="";}
					$other[$i][]='
				<div class="variablechkbox ">
				<input type="checkbox" name="attributes[]" class="statetotal_attributes hideurban '.str_replace(" ","_",$key).'" value="'.$eachattr.'" '.$checked.'>
				</div>
				<div class="variabletext hideurban"><span class="hovertext '.JTEXT::_(trim($eachattr)).'">'.$eachattr.'</span></div>';
				}
			}
			$i++;
		}

		$str1 .='<div class="maintableattr">
		<div class="left">
		<a class="statetotalselectall" href="javascript:void(0);" onclick="checkAll(statetotal_attributes);">'. JText::_('SELECT_ALL_LABEL').'</a> / <a class="statetotalunselectall" onclick="uncheckall(statetotal_attributes);" href="javascript:void(0);">'.JText::_("UNSELECT_ALL_LABEL").'</a>
		</div></div>';

		$finalstr   ="";
		$town1      ="";
		$urban1     ="";
		$others1    ="";
		$mystrarray =array();
		$total1     =array();

		for($i=0;$i<count($str);$i++)
		{
			$finalstr ="";
			$town1    ="";
			$urban1   ="";
			$others1  ="";
			$total1   ="";


			for($j=0;$j<count($other[$i]);$j++)
			{
				if(($j%3)==0)
				{

					$others1 .="</div><div class='maintableattr'>";
				}
				$others1 .=$other[$i][$j];

			}
			$othersss ="<div id='".str_replace(" ","_",trim($grpname[$i]))."grp' class='singlegrp'>";

			$others1 =$othersss."<div>".$others1;
			//$finalstr .=$str[$i];
			//$finalstr .="</div><div class='maintableattr ' >".$urban1."";
			//$finalstr .="</div><div class='maintableattr '>".$total1."</div>";
			//$others1 .="</div>";
			$finalstr .=$others1."";
			$finalstr .='<div class="maintableattr">
			<div class="selectallfront">
			<a class="statetotalselectall" href="javascript:void(0);" onclick="checkAll(\''.str_replace(" ","_",trim($grpname[$i])).'\');">'. JText::_('SELECT_ALL_LABEL').'</a> / <a class="statetotalunselectall" onclick="uncheckall(\''.str_replace(" ","_",trim($grpname[$i])).'\');" href="javascript:void(0);">'.JText::_("UNSELECT_ALL_LABEL").'</a>
			</div></div></div></div>';

			$mystrarray[$i]=$finalstr;

		}

		return "<div id='tabs'><ul>".$header."</ul>".implode("",$mystrarray)."</div></div></div>";
		//return $str;

	}

	/**
	 *
	 */
	function userSelectedAttr($db_table){
		$db = JFactory::getDBO();
		$query = " SELECT plan_id
			FROM ".$db->quoteName('#__osmembership_subscribers')." AS a
			WHERE ".$db->quoteName('plan_subscription_status')." =  ".$db->quote(1)."
				AND ".$db->quoteName('user_id')." = ".$db->quote(JFactory::getUser()->id);
		$db->setQuery($query);
		$plan_id = (int) $db->loadResult();

		$query = "SELECT attribute FROM ".$db->quoteName("#__mica_user_attribute")."
			WHERE ".$db->quoteName('aec_plan_id')." = ".$db->quote($plan_id)."
				AND ".$db->quoteName('dbtable')." LIKE ".$db->quote($db_table);
		$db->setQuery($query);
		$row = $db->loadResult();

		$attr = explode(",",$row);
		return $attr;
	}


	/**
	 * ajax task.
	 */
	public function AddCustomAttr(){
		$db      = JFactory::getDBO();
		$user    = JFactory::getUser();
		$session = JFactory::getSession();

		if($user->id == 0){
			return -1;
		}

		$attributevale = $this->input->get('attributevale', '', 'raw');
		$attrname      = $this->input->get('attrname', '', 'raw');

		/*
			$query = "SELECT count(id) FROM ".$this->_table_prefix."user_profile WHERE user_id = ".$user->id.";";
			$db->setQuery($query);
			$count = $db->loadResult();
			if($count==0)
			{
				return -2;
			}
			$query="SELECT id FROM ".$this->_table_prefix."user_profile WHERE user_id = ".$user->id;
			$db->setQuery($query);
			$db->query();
			$row = $db->loadRow();
			$query="INSERT into ".$this->_table_prefix."user_custom_attribute (profile_id,name,attribute) VALUES (".$row[0].",'".$attrname."','".$attributevale."') ";
			$db->setQuery($query);
			$db->query();
		*/

		$attrnamesession        = $session->get('customattribute');
		//$attrname             = $attrname."`".$attrnamesession;
		//$attributevalesession = $session->get('attributevale');
		$attributevale          = $attrname.":".$attributevale.",".$attrnamesession;
		$session->set('customattribute',$attributevale);

		workspaceHelper::updateWorkspace();
		//$session->set('attributevale',$attributevale);
		exit;
	}

	/**
	 * ajax task.
	 */
	public function getCustomAttr(){
		$db      = JFactory::getDBO();
		$user    = JFactory::getUser();
		$session = JFactory::getSession();
		if($user->id == 0){
			return -1;
		}

		$query = "SELECT count(id) FROM ".$db->quoteName('#__user_profile')." WHERE ".$db->quoteName('user_id')." = ".$db->quote($user->id);
		$db->setQuery($query);
		$count = $db->loadResult();
		if($count == 0){
			return -2;
		}

		$query = "SELECT id FROM ".$db->quoteName('#__user_profile')." WHERE ".$db->quoteName('user_id')." = ".$db->quote($user->id);
		$db->setQuery($query);
		$row = $db->loadResult();

		$query = "SELECT * FROM ".$db->quoteName('#__user_custom_attribute')." WHERE ".$db->quoteName('profile_id')." = ".$db->quote($row);
		$db->setQuery($query);
		$attr = $db->loadAssocList();

		$str = "";
		foreach($attr as $eachattr){
			$str .= '
			<tr >
				<td align="left" valign="top"><input type="checkbox" name="customattributes[]" class="customtotal_attributes" id="d'.$eachattr['id'].'" value="'.$eachattr['attribute'].'" checked="checked"></td>
				<td align="left" valign="top" class="d'.$eachattr['id'].'">'.$eachattr['name'].'</td>
				<td id="del_custom_'.$eachattr['id'].'" class="delcustom"> X </td>
			</tr>';
		}
		echo $str;exit;
	}


	/**
	 * An ajax task
	 */
	public function updateCustomAttr(){
		$session = JFactory::getSession();

		$attributevale = $this->input->get('attributevale', '', 'raw');
		$oldattrval    = $this->input->get('oldattrval', '', 'raw');
		$attrname      = $this->input->get('attrname', '', 'raw');

		$attributes = $session->get('customattribute');
		$attributes = str_replace($oldattrval, $attributevale, $attributes);
		$session->set('customattribute',$attributes);

		$activeworkspace          = $session->get('activeworkspace');
		$customformulafromsession = $session->get('customformula');
		$customformulafromsession = str_replace($oldattrval,$attributevale,$customformulafromsession);
		$session->set('customformula',$customformulafromsession);

		workspaceHelper::updateWorkspace();
		exit;
	}


	/**
	 * An ajax task
	 */
	public function deleteCustomAttr($innercall = null){
		/*$db = JFactory::getDBO();
		$user = JFactory::getUser();
		$id=JRequest::getVar("id");
		if($user->id == 0){
			return -1;
		}
		$query="DELETE FROM ".$this->_table_prefix."user_custom_attribute WHERE id = ".$id;
		$db->setQuery($query);
		$db->query();
		echo 1;*/

		$session = JFactory::getSession();

		$attrname   = $this->input->get('attrname', '', 'raw');
		$attributes = $session->get('customattribute');
		$attributes = explode(",",$attributes);
		$val        = array();
		foreach($attributes as $k =>$eachattr){
			$nameandattr = explode(":",$eachattr);
			if($nameandattr[0] == "" || $nameandattr[0] == $attrname){
				unset($attributes[$k]);
				$val[] = $nameandattr[1];
			}

		}

		$attributes = array_filter($attributes);
		$attributes = implode(",", $attributes);

		$session->set('customattribute',$attributes);
		$customformulafromsession = $session->get('customformula');
		$geteachformula           = explode("|",$customformulafromsession);
		foreach($geteachformula as $key => $eachtocheck){
			$singleformula = explode(":",$eachtocheck);
			if(in_array($singleformula[0],$val)){
				unset($geteachformula[$key]);
				$segment = explode("->",$singleformula[1]);

				$this->input->set('level',end($segment));
			}
		}

		$customformulafromsession = implode("|",$geteachformula);
		$session->set('customformula', $customformulafromsession);
		//echo $customformulafromsession;exit;
		workspaceHelper::updateWorkspace();
		if($innercall != null){
			return true;
		}else{
			exit;
		}
	}



	function loadWorkspace($msg=null){
		$result = workspaceHelper::loadWorkspace();

		$app     = JFactory::getApplication();
		$db      = JFactory::getDBO();
		$session = JFactory::getSession();
		$user    = JFactory::getUser();
		$id      = $this->input->get("id", '', 'raw');
		if($user->id == 0){
			return -1;
		}

		if(!empty($result) && count($result) > 0){
			$restore = unserialize($result[0]['data']);

			$session->set('attributes',$restore['attribute']);
			$session->set('m_type',$restore['m_type']);
			$session->set('state',$restore['state']);
			$session->set('district',$restore['district']);
			$session->set('urban',$restore['urban']);
			$session->set('town',$restore['town']);
			$session->set('is_default',$result[0]['is_default']);

			$workspaceid = $this->input->get("workspaceid", '', 'raw');
			$session->set('gusetprofile','');
			$session->set('activeworkspace',$workspaceid);

			$query = "SELECT count(*) FROM ".$db->quoteName('#__mica_user_custom_attribute_lib')."
				WHERE ".$db->quoteName('profile_id')." = ".$db->quote($workspaceid);
			$db->setQuery($query);
			$count = $db->loadResult();

			if($count != 0){
				$query = "SELECT name, attribute FROM ".$db->quoteName('#__mica_user_custom_attribute')."
					WHERE ".$db->quoteName('profile_id')." = ".$db->quote($workspaceid);
				$db->setQuery($query);
				$allcostomattr = $db->loadAssocList();

				$str="";
				foreach($allcostomattr as $eachcustomattr){
					$eachcustomattr['attribute'] = str_replace("'"," ",$eachcustomattr['attribute']);
					$str .= $eachcustomattr['name'].":".$eachcustomattr['attribute'].",";
				}
				$attributes = $session->set('customattribute',$str);

				$query = "SELECT name, attribute
					FROM ".$db->quoteName('#__mica_user_custom_attribute_lib')."
					WHERE ".$db->quoteName('profile_id')." = ".$db->quote($session->get('activeworkspace'));
				$db->setQuery($query);
				$allcostomattr = $db->loadAssocList();

				$str = "";
				foreach($allcostomattr as $eachcustomattr){
					$eachcustomattr['attribute'] = str_replace("'"," ",$eachcustomattr['attribute']);
					$str .= $eachcustomattr['name'].":".$eachcustomattr['attribute'].",";
				}

				$session->set('customattributelib',$str);
				$session->set('customattribute',$str);

			}else{
				$session->set('customattribute',null);
				$session->set('customattributelib',null);
			}
		}


		if($msg =="insert"){
			$msg = "&msg=0";
			$this->saveSldToDataBase("1");
			$session->set('customformula',null);
			//$session->set('customformula',null);
			//$this->saveSldToDataBase("1");
		}else{
			$msg = "";
		}
		$app->redirect("index.php?option=com_mica&view=showresults&Itemid=188".$msg);
	}

	function updateWorkspace(){
		$updatedworkspace = workspaceHelper::updateWorkspace();
		$this->saveSldToDataBase("1","1");
		exit;
	}

	function deleteWorkspace(){
		$session = JFactory::getSession();
		$db      = JFactory::getDBO();

		$activeworkspace=$session->get('activeworkspace');

		$query = "DELETE FROM ".$db->quoteName('#__mica_user_custom_attribute')."
			WHERE ".$db->quoteName('profile_id')." = ".$db->quote($activeworkspace);
		$db->setQuery($query);

		workspaceHelper::deleteWorkspace();exit;
	}

	function saveWorkspace(){
		$id = workspaceHelper::saveWorkspace(true);
		$this->input->set('workspaceid', $id);
		$this->loadWorkspace("insert");
	}

	function getMinMax(){
		$session = JFactory::getSession();
		$db      = JFactory::getDBO();

		$value       = $this->input->get('value', '', 'raw');

		$activetable = $session->get('activetable');
		$activedata  = $session->get('activedata');
		foreach($activedata as $eachdata){
			$ogrfid[] = $eachdata->OGR_FID;
		}
		$result = array();
		$query  ="SELECT MIN(".$value.") as mymin, MAX(".$value.") as mymax
			FROM ".$db->quoteName($activetable)."
			WHERE ".$db->quoteName('OGR_FID')." IN (".implode(",",$ogrfid).") ";
		$db->setQuery($query);
		$result = $db->loadResult();

		echo implode(",",$result);exit;
	}

	function getColorGradiant(){
		$value = $this->input->get('value', '', 'raw');
		$steps = $this->input->get('steps', '', 'raw');

		$theColorBegin = (isset($value)) ? hexdec("#".$value) : 0x000000;
		$theColorEnd   =  0xffffff;
		$theNumSteps   = (isset($_REQUEST['steps'])) ? intval($steps) : 16;

		$theR0 = ($theColorBegin & 0xff0000) >> 16;
		$theG0 = ($theColorBegin & 0x00ff00) >> 8;
		$theB0 = ($theColorBegin & 0x0000ff) >> 0;

		$theR1 = ($theColorEnd & 0xff0000) >> 16;
		$theG1 = ($theColorEnd & 0x00ff00) >> 8;
		$theB1 = ($theColorEnd & 0x0000ff) >> 0;

		$str = array();

		for ($i = 0; $i <= $theNumSteps; $i++) {
			$theR    = $this->interpolate($theR0, $theR1, $i, $theNumSteps);
			$theG    = $this->interpolate($theG0, $theG1, $i, $theNumSteps);
			$theB    = $this->interpolate($theB0, $theB1, $i, $theNumSteps);
			$theVal  = ((($theR << 8) | $theG) << 8) | $theB;
			$str[$i] = dechex($theVal);
	   }
	   echo implode(",",$str);exit;
	}

	function interpolate($pBegin, $pEnd, $pStep, $pMax) {
		if ($pBegin < $pEnd) {
			return (($pEnd - $pBegin) * ($pStep / $pMax)) + $pBegin;
		} else {
			return (($pBegin - $pEnd) * (1 - ($pStep / $pMax))) + $pEnd;
		}
	}

	function addthematicQueryToSession(){
		$session = JFactory::getSession();
		$user    = JFactory::getUser();

		if($user->id == 0){
			return -1;
		}

		$formula = $this->input->get('formula', '', 'raw');
		$level   = $this->input->get('level', '', 'raw');
		if($level == ""){
			$level = $this->getSldLevel();
			$level = explode(",",$level);
			$level = $level[0];
		}

		$from  = $this->input->get('from', '', 'raw');
		$to    = $this->input->get('to', '', 'raw');
		$color = $this->input->get('color', '', 'raw');

		$customformulafromsession = $session->get('customformula');
		$geteachformula = explode("|",$customformulafromsession);
		foreach($geteachformula as $key=>$eachtocheck){
			$singleformula = explode(":",$eachtocheck);
			if($singleformula[0] ==$formula ||$singleformula[0]==""){
				unset($geteachformula[$key]);
			}
		}

		$customformulafromsession = implode("|",$geteachformula);
		$customformula            = "|".$formula.":".$from."->".$to."->".$color."->".$level;
		$customformulafromsession = $customformulafromsession.$customformula;
		$customformulafromsession = explode(":",$customformulafromsession);
		$customformulafromsession = array_filter($customformulafromsession);
		$customformulafromsession = array_unique($customformulafromsession);
		$customformulafromsession = implode(":",$customformulafromsession);
		$session->set('customformula',$customformulafromsession);

		echo $customformulafromsession;exit;
	}

	function deletethematicQueryToSession($innercall=null){
		$session = JFactory::getSession();
		$user    = JFactory::getUser();
		$db      = JFactory::getDBO();
		if($user->id == 0){
			return -1;
		}

		$formula = $this->input->get('formula', '', 'raw');
		$level   = $this->input->get('level', '', 'raw');
		/*$from =$this->input->get('from', '', 'raw');
		$to     =$this->input->get('to', '', 'raw');
		$color  =$this->input->get('color', '', 'raw');*/
		$customformulafromsession = $session->get('customformula',$attributes);
		$geteachformula = explode("|",$customformulafromsession);
		foreach($geteachformula as $key => $eachtocheck){
			$singleformula = explode(":",$eachtocheck);
			if($singleformula[0] == $formula){
				unset($geteachformula[$key]);
			}
		}

		$customformulafromsession   = implode("|",$geteachformula);
		//$customformula            = $formula.":".$from."->".$to."->".$color."|";
		//$customformulafromsession = $customformulafromsession.$customformula;
		$customformulafromsession   = explode(":",$customformulafromsession);
		$customformulafromsession   = array_filter($customformulafromsession);
		$customformulafromsession   = array_unique($customformulafromsession);
		$customformulafromsession   = implode(":",$customformulafromsession);
		//echo $customformulafromsession;
		$session->set('customformula',$customformulafromsession);

		$workspaceid = $session->get('activeworkspace');

		$query = "DELETE FROM ".$db->quoteName('#__mica_sld_legend')."
			WHERE ".$db->quoteName('workspace_id')." = ".$db->quote($workspaceid)."
				AND ".$db->quoteName('level')." = ".$db->quote($level);
		$db->setQuery($query);
		$db->execute();

		$query="DELETE FROM ".$db->quoteName('#__mica_map_sld')."
			WHERE ".$db->quoteName('profile_id')." = ".$db->quote($workspaceid)."
				AND ".$db->quoteName('level')." = ".$db->quote($level);
		$db->setQuery($query);
		$db->execute();

		if($innercall != null){
			return true;
		}else{
			exit;
		}
	}

	function saveSldToDataBase($innercall=null,$isUpdate=null){
		cssHelper::saveSldToDatabase($innercall,$isUpdate);
	}

	function getSldLevel(){
		$db      = JFactory::getDBO();
		$session = JFactory::getSession();

		$activeworkspace = $session->get('activeworkspace');
		$query = "SELECT level FROM ".$db->quoteName('#__mica_sld_legend')."
			WHERE ".$db->quoteName('workspace_id')." = ".$db->quote($activeworkspace)."
			GROUP BY ".$db->quoteName('level');
		$db->setQuery($query);
		$result = $db->loadObjectList();

		$str   = "";
		$level = "0,1,2";
		foreach($result as $eachlevel){
			$level =str_replace($eachlevel->level.",","",$level);
		}
		return $level;
	}

	function amExport()
	{

		$imgtype =$this->input->get('imgtype', '', 'raw');
		$name    =$this->input->get('name', '', 'raw');
		if(!$imgtype) // To be sure that the parameters are correctly forwarded
		{
			echo "no image type";
			exit;
		}

		// set image quality (from 0 to 100, not applicable to gif)
		$imgquality = 100;

		// get data from $_POST or $_GET ?
		$data = &$_POST;

		// get image dimensions
		$width  = (int) $data['width'];
		$height = (int) $data['height'];

		// create image object
		$img = imagecreatetruecolor($width, $height);

		// populate image with pixels
		for ($y = 0; $y < $height; $y++) {
			// innitialize
			$x = 0;

			// get row data
			$row = explode(',', $data['r'.$y]);

			// place row pixels
			$cnt = sizeof($row);
			for ($r = 0; $r < $cnt; $r++) {
				// get pixel(s) data
				$pixel = explode(':', $row[$r]);

				// get color
				$pixel[0] = str_pad($pixel[0], 6, '0', STR_PAD_LEFT);

				$cr_e = '0x'.strtoupper(substr($pixel[0], 0, 2));
				$cg_e = '0x'.strtoupper(substr($pixel[0], 2, 2));
				$cb_e = '0x'.strtoupper(substr($pixel[0], 4, 2));

				/*
				// VALORI DECIMALI
				$cr = hexdec(substr($pixel[0], 0, 2));
				$cg = hexdec(substr($pixel[0], 2, 2));
				$cb = hexdec(substr($pixel[0], 4, 2));
				*/

				// allocate color
				$color = imagecolorallocatealpha($img, $cr_e, $cg_e, $cb_e,'0');

				// place repeating pixels
				$repeat = isset($pixel[1]) ? (int) $pixel[1] : 1;
				for ($c = 0; $c < $repeat; $c++) {
			 		 // place pixel
			  		imagesetpixel($img, $x, $y, $color);

			  		// iterate column
			 		 $x++;
				}
		  	}
		}


		$path=JPATH_BASE."/userchart/001.".$imgtype; // relative or absolute path to server; check permissions to write on that dir!
		switch($imgtype)
		{
			case 'png': imagepng($img,$path) or die("Unable to create the file");
				break;
			case 'jpeg': imagejpeg($img,$path) or die("Unable to create the file");
				break;
			case 'jpg': imagejpeg($img,$path) or die("Unable to create the file");
				break;
			case 'gif': imagegif($img,$path) or die("Unable to create the file");


				break;
		}
		$logo=JURI::base()."templates/ja_beryl/images/logo.png";
		$pdfhelper = new pdfHelper();
		$pdfhelper->newPage();
		$pdfhelper->addHeader("MIMI",$logo);
		$pdfhelper->addImage($path);
		$pdfhelper->setOutput($name.".pdf");
		exit;

		//	    // set proper content type
		//	    $ch=fopen($path);
		//	    $content=fread($ch,filesize($path));
		//	   $ch1=fopen('/var/www/micamap/'.$name.".pdf",'w');
		//	   //write image to file
		//	   fwrite($ch1,@readfile($path),filesize($path));
		//	   fclose($ch1);
		//	   imagedestroy($img);
		//	    header('Content-type: application/pdf');
		//	    header('Content-Disposition: attachment; filename="'.$name.'.pdf"');
		//	    header("Content-Transfer-Encoding: binary");
		//	    header('Content-Length: ' . filesize($path));
		//		@readfile('/var/www/micamap/'.$name.".pdf");
		//
		//
		//	exit;
	}

	function exportexcel()
	{
		$session = JFactory::getSession();
		$model   = $this->getModel();//'showresults','showresultsModel'

		//	$model->getCustomData();
		//	$groupwisedata=$model->getDisplayGroup();
		$groupwisedata = $session->get('Groupwisedata');


		$i          =0;
		$j          =0;
		$k          =0;
		$l          =0;
		$headerspan =0;

		foreach($groupwisedata as $mainname=>$data)
		{
			$header[$i]=$mainname;
			foreach($data as $grpname=>$grpdata)
			{
				$grpheader[$i][$j]=$grpname.str_repeat("\t",($model->getColspan($grpdata,2)));
				$headerspan +=$model->getColspan($grpdata,2);
				foreach($grpdata as $variablegrp=>$variabledata)
				{
					$vargrpname[$i][$k]=$variablegrp.str_repeat("\t",($model->getColspan($variabledata,3)));
					foreach($variabledata as $eachvariabledata)
					{

						foreach($eachvariabledata as $key=>$value)
						{
							$mygrpvariablename[$i][$k][$l]=$key."\t";
							$mygrpdata[$i][$k][$l]=$value."\t";
						}
						$l++;
					}
					$k++;
				}
				$j++;
			}
			$i++;
		}

			//		echo "<pre>";
			//		print_R($grpheader);
			//		print_R($vargrpname);
			//		print_R($mygrpvariablename);
			//
			//		print_r($mygrpdata);
			//		exit;

		$op=$session->get('state');
		if($session->get('district')!="")
		{
			$m_type=$session->get('m_type');
			if($m_type=="Total")
			{
				$m_type="All";
			}
			$op .="-> District (".$m_type.")";
		}
		else if($session->get('urban')!="")
		{
			$op .="->Urban Agglomeration ";
		}
		else if($session->get('town')!="")
		{
			$op .="->Town";
		}

		$str=str_repeat("\t",($model->getColspan($headerspan,2)/2))."Data of ".$op.str_repeat("\t",($model->getColspan($headerspan,2)/2))."\n";
		$str .="\t";
		foreach($grpheader as $inserheader)
		{	foreach($inserheader as $inserheaderval)
			{
				$str .=$inserheaderval;

			}
			break;
		}
		$str .="\n";
		$str .="\t";
			foreach($vargrpname as $eachgrpname)
			{
				foreach($eachgrpname as $name)
				{
				$str .=$name;

				}
				break;
			}
			$str .="\n";

		$str .="\t";
		foreach($mygrpvariablename as $variablenamegrp)
		{

			foreach($variablenamegrp as $varname)
			{
				foreach($varname as $each)
				{
					$str .=$each;
				}

			}
			break;
		}

		$str .="\n";
		foreach($mygrpdata as $key=>$gdata)
		{$str .=$header[$key]."\t";
			foreach($gdata as $data)
			{	foreach($data as $d)
				{
						$str .=$d;

				}

			}
			$str .="\n";
		}

		header('Content-type: application/ms-excel');
		header('Content-Disposition: attachment; filename=datasheet.xls');
		echo $str;
		exit;
	}

	function exportMap(){
		$session = JFactory::getSession();
		$db      = JFactory::getDBO();

		$activeworkspace=$session->get('activeworkspace');

		$query  ="select * from #__mica_sld_legend where workspace_id = ".$activeworkspace." order by level,range_from ASC";
		$db->setQuery($query);
		$result =$db->loadObjectList();

		$id    =array();
		$level =array();
		$i     =1;
		$j     =1;
		$html  ="";
		foreach($result as $range)
		{
			if($range->level=="0")
			{
				$str ='<td bgcolor="'.$range->color.'" width="15" height="15"> </td>';
			}
			else
			{
				$pin      =str_replace("#","",$range->color);
				$pinimage =JUri::base().'components/com_mica/maps/img/layer'.trim($range->level).'/pin'.$pin.'.png';
				$str      ='<td><img src="'.$pinimage.'"   alt="test alt attribute"></td>';
			}
			$grouping[$range->custom_formula][]='<tr><td >'.$range->range_from.' - '.$range->range_to."</td>".$str."<td></td></tr>";
			//$id[]=$range->custom_formula;
			//$grouping[$range->custom_formula]=$range->level;
			$level[$range->custom_formula][]=$range->level;
			$i++;
			$j++;
		}

		$i            =0;
		$range        =array();
		$str          =array();
		$totalcount   =0;
		$l            =0;
		$tojavascript =array();
		foreach($grouping as $key=>$val)
		{
			$grname         =$this->getCustomAttrName($key,$activeworkspace);
			$grpname[]      =$grname;
			$str[]          =implode(" ",$val);
			$ranges[]       =count($val);
			$levelunique[]  =$level[$key][$l];
			$tojavascript[] =$key;
			$l++;
		}

		$tojavascript =implode(",",$tojavascript);
		//$str        ="";
		$op           =$session->get('state');
		if($session->get('district')!="")
		{
			$m_type=$session->get('m_type');
			if($m_type=="Total")
			{
				$m_type="All";
			}
			$op .="-> District (".$m_type.")";
		}
		else if($session->get('urban')!="")
		{
			$op .="->Urban Agglomeration ";
		}
		else if($session->get('town')!="")
		{
			$op .="->Town";
		}
		$html='<tr><td colspan="2">Data of '.$op.'</td></tr>';

		if($grpname!="")
		{
			$html .='<tr><td></td><td width="15"></td><td rowspan="{rowspan}" width="350">{mapimage}</td></tr>';
			for($i=0;$i<count($grpname);$i++)
			{
				$html .= '<tr><td>'.$grpname[$i][0].'</td><td></td><td></td></tr>'.$str[$i].'';
			}
		}
		else
		{
			$html .='<tr><td width="600">{mapimage}</td></tr>';
		}




		$mapparameter  =rawurlencode($this->input->get('mapparameter', '', 'raw'));
		$mapparameter1 =rawurlencode($this->input->get('baselayer', '', 'raw'))."&SERVICE=WMS&VERSION=1.1.1&REQUEST=GetMap&STYLES=&SRS=EPSG%3A4326&FORMAT=image%2Fpng&LAYERS=india%3Arail_state&TRANSPARENT=true&styles=dummy_state&WIDTH=1024&HEIGHT=1024";
		$makeurl       ="";
		$mapparameter  =str_replace("EQT","=",$mapparameter);
		$mapparameter  =str_replace("AND","&",$mapparameter);
		$mapparameter  =str_replace('\n'," ",$mapparameter);
		$mapparameter  =$this->tomcat_url."?".$mapparameter;

		$mapparameter1 =str_replace("EQT","=",$mapparameter1);
		$mapparameter1 =str_replace("AND","&",$mapparameter1);

		$mapparameter1 =str_replace('\n'," ",$mapparameter1);
		$mapparameter1 =$this->tomcat_url."?".$mapparameter1;
		//$image= getimagesize($mapparameter);
		$layer1    =JPATH_BASE."/userchart/map.png";
		$baselayer =JPATH_BASE."/userchart/base.png";
		$finalop   =JPATH_BASE."/userchart/final.png";

		file_put_contents($layer1, file_get_contents($mapparameter));
		file_put_contents($baselayer, file_get_contents($mapparameter1));

		$dest =imagecreatefrompng($baselayer);
		$src=  imagecreatefrompng($layer1);
		imagecopy($dest,$src,0,0,0,0,imagesx($src),imagesy($src));
		imagepng($dest,$finalop);
		imagedestroy($dest);
				//file_put_contents($finalop, $final_img);
		require_once(JPATH_BASE.'/components/com_mica/helpers/tcpdf/tcpdf.php');
		$pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
		ob_clean();

		// set default header data
		$logo="logo.png";
		$pdf->SetHeaderData($logo, '', '');
		$pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
		// set header and footer fonts
		$pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
		//$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);
		$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

		//set image scale factor
		$pdf->SetFont('times', '', 8);
		$pdf->AddPage();
		$y = $pdf->getY();
		$pdf->SetFillColor(255, 255, 255);
		// set color for text
		$pdf->SetTextColor(0, 0, 0);
		$rowspan=(count($result)+count($grpname))+2;

		$mapimage='<img src="'.JUri::base().'userchart/final.png"  border="0">';
		$html = str_replace("{rowspan}",$rowspan,$html);
		$html = str_replace("{mapimage}",$mapimage,$html);
		$html = '<table  cellspacing="5" cellpadding="0">'.$html.'</table>';



		$pdf->writeHTML($html, true, false, true, false, '');
		$pdf->lastPage();
		$pdf->Output('map.pdf', 'D');
		exit;
		/* $pdfhelper = new pdfHelper();
		  $pdfhelper->newPage();
		  $pdfhelper->addHeader("MIMI",$logo);
		   $pdfhelper->addImage($finalop,60,30,650,650,'PNG');
			$pdfhelper->addHtml(5,$html);

		  $pdfhelper->setOutput("map.pdf");*/
	}

	function getCustomAttrName($name,$activeworkspace)
	{
		$db = JFactory::getDBO();

		$query = "SELECT name, attribute
			FROM ".$db->quoteName('#__mica_user_custom_attribute')."
			WHERE ".$db->quoteName('profile_id')." = ".$db->quote($activeworkspace)."
				AND ".$db->quoteName('attribute')." LIKE '".$name."'";
		$db->setQuery($query);
		$result = $db->loadAssoc();

		if(count($result) == 0){
			return array($name, $name);
		}else{
			return array($result['name'], $result['attribute']);
		}
	}

}
