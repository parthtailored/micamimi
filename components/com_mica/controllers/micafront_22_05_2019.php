<?php
/**
 * @package     Joomla.Site
 * @subpackage  com_mica
 *
 * @copyright   Copyright (C) 2005 - 2017 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

JLoader::register('MicaController', JPATH_COMPONENT . '/controller.php');
require_once( JPATH_COMPONENT.DIRECTORY_SEPARATOR.'helpers/Workspace.php' );
require_once( JPATH_COMPONENT.DIRECTORY_SEPARATOR.'helpers/css.php' );
require_once( JPATH_COMPONENT.DIRECTORY_SEPARATOR.'helpers/pdf.php' );

/**
 * MICA Front controller class for MICA.
 *
 * @since  1.6
 */
class MicaControllerMicafront extends MicaController
{
	//Custom Constructor
	var $tomcat_url             = "";
	var $townsearchvariable     = "OGR_FID";
	var $urbansearchvariable    = "OGR_FID";
	var $districtsearchvariable = "OGR_FID";

	function __construct($default = array()){
		parent::__construct($default);

		$this->_table_prefix = '#__mica_';
		//$u    = JURI::getInstance( JURI::base() );
	 	//$this->tomcat_url="http://".$u->_host."/tomcat/geoserver/india/wms";
		$this->tomcat_url = "http://http://162.144.250.77:8080/geoserver/india/wms";
		DEFINE('TOMCAT_URL',$this->tomcat_url);

		$app     = JFactory::getApplication();
		$session = JFactory::getSession();

		$user = JFactory::getUser();
		if ($user->id == 0) {
			$session->set('user_id',null);
			JFactory::getApplication()->redirect(JURI::base().'index.php?option=com_users&view=login', $error, 'error' ); //redirect to login page
		}
		else{
			$session->set('user_id', $user->id);
		}

		$reset = $app->input->get('reset', 0, 'int');
		if($reset == 0){
			$session->set('oldattributes', $session->get('attributes'));
		}else{
			$session->set('oldattributes', "");
			$session->set('attributes', null);
			$session->set('state', null);
			$session->set('district', null);
			$session->set('urban', null);
			$session->set('town', null);
			$session->set('activeworkspace', null);
			$session->set('attrname', null);
			$session->set('customattribute', null);
			$session->set('m_type', null);
			$session->set('activetable', null);
			$session->set('activedata' ,null);
			$session->set('composite', null);
			$session->set('m_type_rating', null);
			$session->set('customformula', null);
			$session->set('Groupwisedata', null);
			$session->set('indlvlgrps', null);
		}

		//////.//////////// Workspace ///////////////////////
		if($session->get('activeworkspace') == ""){
			$result = workspaceHelper::loadWorkspace(1);
			if(!empty($result))
			{
	    		$session->set('activeworkspace',$result[0]['id']);
				$innercall = 1;
				$session->set('gusetprofile',$result[0]['id']);
				$session->set('is_default',"1");
				$app->input->set('activeworkspace', $session->get('activeworkspace'));
				cssHelper::flushOrphandSld($session->get('activeworkspace'));
			}
			else
			{
				$name      = $app->input->set("name", "Guest");
				$default   = $app->input->set("default", 1);
				$innercall = 1;
				$id        = workspaceHelper::saveWorkspace($innercall);
				$session->set('gusetprofile', $id);
				$session->set('activeworkspace', $id);
				$session->set('is_default',"1");
			}
		}
		else if($session->get('is_default') == 1)
		{
			workspaceHelper::updateWorkspace($innercall);
		}
		else
		{
			$app->input->set('activeworkspace', $session->get('activeworkspace'));
		}

		$state = $app->input->get('state', '', 'raw');
		$stats = array();
		foreach($state as $eachstat)
		{
			$stats[] = base64_decode($eachstat);
		}
		$state = $stats;
		if(is_array($state)){
			$state = implode(",",$state);
			$app->input->set('state',$state);
		}

		$sessionstate = $session->get('state');
		$app->input->set('state', $sessionstate);
		if($sessionstate != ""){
			$addtosession = ($sessionstate.",".$state);
			$tmp          = explode(",",$addtosession);
			$mytmp        = array_unique($tmp);
			$mytmp        = array_filter($mytmp);
			$sessionstate = implode(",",$mytmp);
		}else{
			$sessionstate = $state;
		}
		$session->set('state', $sessionstate);

		$district = $app->input->get('district', '', 'raw');
		if(is_array($district)){
			$district = implode(",", $district);
			$app->input->set('district', $district);
		}

		$sessiondistrict = $session->get('district');
		if($sessiondistrict != ""){
			$addtosession1   = ($sessiondistrict.",".$district);
			$tmp             = explode(",",$addtosession1);
			$mytmp           = array_unique($tmp);
			$mytmp           = array_filter($mytmp);
			$sessiondistrict = implode(",",$mytmp);
		}else{
			$sessiondistrict = $district;
		}

		$sessiondistrict = $session->get('district');
		$app->input->set('district',$sessiondistrict);
		$session->set('district',$sessiondistrict);

		$m_type = $app->input->get('m_type', '', 'raw');
		if(!is_array($m_type)){
			$m_type = $session->get('m_type');
		}else{
			$session->set('m_type', $m_type);
			$m_type = $session->get('m_type');
		}

		$town        = $app->input->get('town', '', 'raw');
		$sessiontown = $session->get('town');
		if($sessiontown != ""){
			$addtosession =($sessiontown.",".$town);
			$tmp          = explode(",",$addtosession);
			$mytmp        = array_unique($tmp);
			$mytmp        = array_filter($mytmp);
			$sessiontown  = implode(",",$mytmp);
		}else{
			$sessiontown = $town;
		}
		$session->set('town',$sessiontown);
		$app->input->set('town',$sessiontown);


		$urban        = $app->input->get('urban', '', 'raw');
		$sessionurban = $session->get('urban');
		if($sessionurban !="" ){
			$addtosession = ($sessionurban.",".$urban);
			$tmp          = explode(",",$addtosession);
			$mytmp        = array_unique($tmp);
			$mytmp        = array_filter($mytmp, 'strlen');
			$sessionurban = implode(",",$mytmp);
			$session->set('dataof',"Urban");
		}else{
			$sessionurban = $urban;
		}
		$session->set('urban',$sessionurban);
		$app->input->set('urban',$sessionurban);

		if($sessiontown != ""){
			$app->input->set('db_table','Town');
			$zoom = 9;//JRequest::setVar('db_table','Town');
		}else if($sessionurban != ""){
			$app->input->set('db_table','Urban');
			$zoom = 8;//JRequest::setVar('db_table','Urban');
		}else if($sessiondistrict != ""){
			$app->input->set('db_table','District');
			$zoom = 7;//JRequest::setVar('db_table','District');
		}else{
			$app->input->set('db_table','State');
			$zoom = 6;//JRequest::setVar('db_table','State');
		}

		$dataof = $app->input->get('dataof', '', 'raw');
		if($dataof == ""){
			$dataof = $session->get('dataof');
			//$app->input->get('dataof',$dataof);
		}else{
			$session->set('dataof',$dataof);
		}

		$composite = $app->input->get('composite', '', 'raw');
		if(!empty($composite)){
			$session->set('composite', $app->input->get('composite', '', 'raw'));
			//$attributes = JRequest::getVar('attributes');
			$composite = array_merge(array("MPI"), $composite);
		}else{
			$session->clear('composite');
		}

		$attributes = $app->input->get('attributes', '', 'raw');
		if(!empty($attributes)){
			$composite = $session->get('composite');
		    //echo  '<br/>==<br/>'.implode(",",$composite);
		    //echo  '<br/>==<br/>'.implode(",",$attributes);
			//$attributes = JRequest::getVar('attributes');
			if(!empty($composite)){
				$attributes = array_merge(array("MPI"),$attributes,$composite);
			}else{
				$attributes = array_merge(array("MPI"),$attributes);
			}
			//echo  '<br/>==<br/>'.implode(",",$attributes);
		}
		//echo "a<pre>";print_r($attributes);exit;
		if(is_array($attributes)){
			$attributes = array_filter($attributes);
			$attributes = implode(",",$attributes);
		}
		$sessionattr = $session->get('attributes');
		if($sessionattr != ""){
			if($app->input->get('attributes', '', 'raw') != ""){
				$addtosession = ($attributes);
			}else{
				$addtosession = ($sessionattr);
			}
			$tmp          = explode(",",$addtosession);
			$mytmp        = array_unique($tmp);
			$mytmp        = array_filter($mytmp, 'strlen');
			$addtosession = implode(",",$mytmp);
		}else{
			$addtosession = $attributes;
		}
		$session->set('attributes',$addtosession );
		$app->input->set('sessionattributes',$addtosession);


		$app->input->set('composite', $session->get('composite'));
		$app->input->set('m_type_rating', $session->get('m_type_rating'));
		$app->input->set('attributes', $session->get('attributes'));
		$app->input->set('state', $session->get('state'));
		$app->input->set('m_type', $session->get('m_type'));
		$app->input->set('preselected', $session->get('district'));

		$msg = $app->input->get('msg', '', 'raw');
		if($msg == "-1"){
			$app->enqueueMessage( JTEXT::_('WORKSPACE_DELETED') );
		}else if($msg == "0"){
			$app->enqueueMessage( 'WORKSPACE_CREATED' );
		}else if($msg == "1"){
			$app->enqueueMessage( 'WORKSPACE_UPDATED' );
		}

		$this->state_items            = $this->get('StateItems');
		$this->composite_attr         = $this->get('compositeAttr');
		$this->typesArray             = $this->get('AttributeType');
		$this->industrylevelgroups    = $this->get('IndustryLevelGroups');
		$this->state                  = $sessionstate;
		$this->district               = $sessiondistrict;
		$this->town                   = $sessiontown;
		$this->urban                  = $sessionurban;
		$this->m_type                 = $m_type;
		$this->themeticattribute      = $session->get('themeticattribute');
		$this->user                   = JFactory::getUser();
		$this->userid                 = $this->user->id;
		$this->activeworkspace        = $session->get('activeworkspace');
		$this->townsearchvariable     = $this->townsearchvariable;
		$this->urbansearchvariable    = $this->urbansearchvariable;
		$this->districtsearchvariable = $this->districtsearchvariable;
		$this->zoom                   = $zoom;
		$this->customattributelib = $session->get('customattributelib');


		/*$model = JModelLegacy::getInstance('Showresults', 'MicaModel', array('ignore_request' => true));
		$geometry                 = $model->getInitialGeometry();
		$this->geometry           = $geometry;
		$UnselectedDistrict       = $model->getUnselectedDistrict();
		$this->UnselectedDistrict = $UnselectedDistrict;

		$this->customdata = $model->getCustomData();*/
	}


	/**
	 * Proxy for getModel.
	 *
	 * @param   string  $name    The model name. Optional.
	 * @param   string  $prefix  The class prefix. Optional.
	 * @param   array   $config  Configuration array for model. Optional.
	 *
	 * @return  object  The model.
	 *
	 * @since   1.6
	 */
	public function getModel($name = 'Micafront', $prefix = 'MicaModel', $config = array('ignore_request' => true))
	{
		return parent::getModel($name, $prefix, $config);
	}

	public function getDataajax()
	{


		require_once( JPATH_COMPONENT.DIRECTORY_SEPARATOR.'helpers/Workspace.php' );
		require_once( JPATH_COMPONENT.DIRECTORY_SEPARATOR.'helpers/css.php' );
		$model = $this->getModel('micaresult');

		if(!defined('TOMCAT_URL')){
			$this->getTomcatUrl();
			DEFINE('TOMCAT_URL', $this->tomcaturl);
		}

		if(!defined('TOMCAT_STYLE_ABS_PATH')){
			//DEFINE('TOMCAT_STYLE_ABS_PATH' ,"/usr/local/easy/share/easy-tomcat7/webapps/geoserver/data/www/styles/");
			DEFINE('TOMCAT_STYLE_ABS_PATH' , JPATH_SITE."/geoserver/data/www/styles/");
		}

		if(!defined('TOMCAT_SLD_FOLDER')){
			DEFINE('TOMCAT_SLD_FOLDER' ,"geoserver/www/styles");
		}


		$app = JFactory::getApplication();

		$msg = $app->input->get('msg');
		if($msg == "-1"){
			$app->enqueueMessage(JTEXT::_('WORKSPACE_DELETED') );
		}else if($msg=="0"){
			$app->enqueueMessage(JTEXT::_('WORKSPACE_CREATED') );
		}else if($msg=="1"){
			$app->enqueueMessage(JTEXT::_('WORKSPACE_UPDATED') );
		}

		$refererview = $app->input->get("refeterview");
		$session     = JFactory::getSession();

		if($refererview == "micafront"){
			$session->set('oldattributes',"");
			$session->set('attributes',null);
			$session->set('state',null);
			$session->set('district',null);
			$session->set('urban',null);
			$session->set('town',null);
			$session->set('activeworkspace',null);
			$session->set('attrname',null);
			$session->set('customattribute',null);
			$session->set('m_type',null);
			$session->set('activetable',null);
			$session->set('activedata',null);
			$session->set('composite',null);
			$session->set('m_type_rating',null);
			$session->set('customformula',null);
			$session->set('Groupwisedata',null);
			$session->set('popupactivedata',null);
			$session->set('themeticattribute',null);
			$session->set('activenamevariable',null);
			$session->set('activesearchvariable',null);
			$session->set('dataof',null);
			$session->set('customattributelib',null);
		}

		$session->set('m_type_rating', $app->input->get('m_type_rating', '', 'raw'));

       //echo "<pre/>";print_r($session->set('m_type_rating', $app->input->get('m_type_rating', '', 'raw')));exit;
		if($session->get('activeworkspace') == ""){

			$result = workspaceHelper::loadWorkspace(1);
			//echo "<pre/>";print_r($result);exit;

	    	if(!empty($result)){

	    		$session->set('activeworkspace',$result[0]['id']);
				$innercall = 1;
				$session->set('gusetprofile',$result[0]['id']);
				$session->set('is_default',"1");
				$app->input->set('activeworkspace', $session->get('activeworkspace'));
				//die($app);
				cssHelper::flushOrphandSld($session->get('activeworkspace'));

	    	}else{

				$name      = $app->input->set("name", "Guest");
				$default   = $app->input->set("default", 1);
				$innercall = 1;
				$id        = workspaceHelper::saveWorkspace($innercall);
				$session->set('gusetprofile', $id);
				$session->set('activeworkspace', $id);
				$session->set('is_default',"1");
				//$app->redirect("index.php?option=com_mica&view=showresults&Itemid=188");
	    	}

		}else if($session->get('is_default') == 1){

	    	workspaceHelper::updateWorkspace($innercall);

	    }else{

    	    $app->input->set('activeworkspace', $session->get('activeworkspace'));
			//$this->saveSldToDataBase("1","1");
	    }

		$state = $app->input->get('state', '', 'raw');
		$stats = array();
		foreach($state as $eachstat){
			$stats[] = base64_decode($eachstat);
		}
		$state = $stats;
		if(is_array($state)){
			$state = implode(",",$state);
			$app->input->set('state',$state);
		}

		$sessionstate = $session->get('state');
		$app->input->set('state', $sessionstate);
		if($sessionstate != ""){
			$addtosession = ($sessionstate.",".$state);
			$tmp          = explode(",",$addtosession);
			$mytmp        = array_unique($tmp);
			$mytmp        = array_filter($mytmp);
			$sessionstate = implode(",",$mytmp);
		}else{
			$sessionstate = $state;
		}
		$session->set('state', $sessionstate);


		$district = $app->input->get('district', '', 'raw');
		if(is_array($district)){
			$district = implode(",", $district);
			$app->input->set('district', $district);
		}


		$sessiondistrict = $session->get('district');
		if($sessiondistrict != ""){
			$addtosession1   = ($sessiondistrict.",".$district);
			$tmp             = explode(",",$addtosession1);
			$mytmp           = array_unique($tmp);
			$mytmp           = array_filter($mytmp);
			$sessiondistrict = implode(",",$mytmp);
		}else{
			$sessiondistrict = $district;
		}
		$app->input->set('district',$sessiondistrict);
		$session->set('district',$sessiondistrict);

		$m_type = $app->input->get('m_type', '', 'raw');
		//$session_mtype=($session->get('m_type'));
		if(!is_array($m_type)){
			$m_type = $session->get('m_type');
		}else{
			$session->set('m_type', $m_type);
			$m_type = $session->get('m_type');
		}

		$town        = $app->input->get('town', '', 'raw');
		$sessiontown = $session->get('town');
		if($sessiontown != ""){
			$addtosession =($sessiontown.",".$town);
			$tmp          = explode(",",$addtosession);
			$mytmp        = array_unique($tmp);
			$mytmp        = array_filter($mytmp);
			$sessiontown  = implode(",",$mytmp);
		}else{
			$sessiontown = $town;
		}
		$session->set('town',$sessiontown);
		$app->input->set('town',$sessiontown);


		$urban        = $app->input->get('urban', '', 'raw');
		$sessionurban = $session->get('urban');
		if($sessionurban !="" ){
			$addtosession = ($sessionurban.",".$urban);
			$tmp          = explode(",",$addtosession);
			$mytmp        = array_unique($tmp);
			$mytmp        = array_filter($mytmp, 'strlen');
			$sessionurban = implode(",",$mytmp);
			$session->set('dataof',"Urban");
		}else{
			$sessionurban = $urban;
		}
		$session->set('urban',$sessionurban);
		$app->input->set('urban',$sessionurban);

		if($sessiontown != ""){
			$app->input->set('db_table','Town');
			$zoom = 9;//JRequest::setVar('db_table','Town');
		}else if($sessionurban != ""){
			$app->input->set('db_table','Urban');
			$zoom = 8;//JRequest::setVar('db_table','Urban');
		}else if($sessiondistrict != ""){
			$app->input->set('db_table','District');
			$zoom = 7;//JRequest::setVar('db_table','District');
		}else{
			$app->input->set('db_table','State');
			$zoom = 6;//JRequest::setVar('db_table','State');
		}

		$dataof = $app->input->get('dataof', '', 'raw');
		if($dataof == ""){
			$dataof = $session->get('dataof');
			//$app->input->get('dataof',$dataof);
		}else{
			$session->set('dataof',$dataof);
		}


		$composite = $app->input->get('composite', '', 'raw');
		if(!empty($composite)){
			$session->set('composite', $app->input->get('composite', '', 'raw'));
			//$attributes = JRequest::getVar('attributes');
			$composite = array_merge(array("MPI"), $composite);
		}else{
			$session->clear('composite');
		}


		$attributes = $app->input->get('attributes', '', 'raw');
		if(!empty($attributes)){
			$composite = $session->get('composite');
		    //echo  '<br/>==<br/>'.implode(",",$composite);
		    //echo  '<br/>==<br/>'.implode(",",$attributes);
			//$attributes = JRequest::getVar('attributes');
			if(!empty($composite)){
				$attributes = array_merge(array("MPI"),$attributes,$composite);
			}else{
				$attributes = array_merge(array("MPI"),$attributes);
			}
			//echo  '<br/>==<br/>'.implode(",",$attributes);
		}
		//echo "a<pre>";print_r($attributes);exit;
		if(is_array($attributes)){
			$attributes = array_filter($attributes);
			$attributes = implode(",",$attributes);
		}
		$sessionattr = $session->get('attributes');
		if($sessionattr != ""){
			if($app->input->get('attributes', '', 'raw') != ""){
				$addtosession = ($attributes);
			}else{
				$addtosession = ($sessionattr);
			}
			$tmp          = explode(",",$addtosession);
			$mytmp        = array_unique($tmp);
			$mytmp        = array_filter($mytmp, 'strlen');
			$addtosession = implode(",",$mytmp);
		}else{
			$addtosession = $attributes;
		}
		$session->set('attributes',$addtosession );
		$app->input->set('sessionattributes',$addtosession);

		$state_items = $this->get('StateItems');

		$result['dataof']                 = $dataof;
		$result['state_items']            = $state_items;
		$result['townsearchvariable']     = $this->townsearchvariable;
		$result['urbansearchvariable']    = $this->urbansearchvariable;
		$result['districtsearchvariable'] = $this->districtsearchvariable;

		$this->Itemid = $app->input->get('Itemid', 188);

		$customattribute  = $session->get('customattribute');
		$new_name         = $app->input->get('new_name', array(), 'array');
		$custom_attribute = $app->input->get('custom_attribute', array(), 'array');

		$customdata = $this->get('CustomData');

		//$MaxForMatrix= $this->get('MaxForMatrix');
		//$this->MaxForMatrix = $MaxForMatrix;
		$this->customdata = $customdata;
		$this->state      = $sessionstate;
		$this->district   = $sessiondistrict;
		$this->town       = $sessiontown;
		$this->urban      = $sessionurban;

		$page = $app->input->get("page");

		$AttributeTable  = $this->get('NewAttributeTable');// $this->get('AttributeTable');
		$graph           = $this->get('Graph');
		$GraphSettings   = $this->get('GraphSettings');

		$activesearchvariable       = $session->get('activesearchvariable');
		$this->activesearchvariable = $activesearchvariable;
		$this->zoom                 = $zoom;
		/*$tomcaturl                  = TOMCAT_URL;
		$this->tomcaturl            = $tomcaturl;*/

		$BreadCrumbcontent = $this->get('BreadCrumb');
		list($BreadCrumb, $workspacelink) = explode("~~~", $BreadCrumbcontent);
		$this->workspacelinkdiv   = $workspacelink;
		$this->BreadCrumb         = $BreadCrumb;

		$geometry                 = $this->get('InitialGeometry');
		$UnselectedDistrict       = $this->get("UnselectedDistrict");
		$this->UnselectedDistrict = $UnselectedDistrict;
		$this->geometry           = $geometry;


		$this->user               = JFactory::getUser();
		$this->userid             = $this->user->id;
		$this->activeworkspace    = $session->get('activeworkspace');

		$this->is_default         = $session->get('is_default');
		$this->customattribute    = $customattribute;

		$this->get('customattrlib');
		$this->customattributelib = $session->get('customattributelib');
		$this->AttributeTable     = $AttributeTable;
		$this->GraphSettings      = $GraphSettings;
		$this->graph              = $graph;

		/*$this->m_type = $m_type;$app->input->set('m_type',$m_type);*/
		$this->attributes        = $addtosession;
		$this->new_name          = $new_name;
		$this->custom_attribute  = $custom_attribute;

		$pagination = $this->get('Pagination');
		$this->pagination        = $pagination;
		$this->themeticattribute = $session->get('themeticattribute');

		workspaceHelper::updateWorkspace($innercall);
		cssHelper::saveSldToDatabase(1, 1);

		return parent::display($tpl);
	}



	/**
	 * A task to return data for AJAX.
	 */
	public function getsecondlevel(){
		$districtsearchvariable = $this->districtsearchvariable;
		$db = JFactory::getDBO();

		$stat                   = $this->input->get('stat','','raw');
		//$preselected          = $this->input->get('preselected','');

		$stat = explode(",",$stat);
		//echo "<pre/>";print_r($stat);exit;

		$session     = JFactory::getSession();
		$preselected = $session->get("district");

		$preselected = explode(",",$preselected);
		foreach($stat as $eachstat){
			$stats[] = base64_decode($eachstat);


		}

		$query = "SELECT state_code,name
				FROM rail_state";
			$db->setQuery($query);

			try {
				$state_codes = $db->loadAssocList();

			} catch (Exception $e) {

			}

		$stat = implode(",",$stats);
		$stat = str_replace(",","','",$stat);
		$stat = "'".$stat."'";

		if($stat){
			$query = "SELECT ".$districtsearchvariable.", distshp, state
				FROM ".$db->quoteName('india_information')."

				WHERE ".$db->quoteName('state')." IN (".$stat.")
					AND ".$db->quoteName('OGR_FID')." != ".$db->quote(490)."
				GROUP BY ".$db->quoteName('distshp').", ".$db->quoteName('state')."
				ORDER BY ".$db->quoteName('state').", ".$db->quoteName('distshp')." ASC ";


			$db->setQuery($query);

			try {
				$district_data = $db->loadObjectList();
			} catch (Exception $e) {

				$district_data = array();
			}

		}else{
			$district_data = array();
		}

		$state    = array();
		$gpbydist = array();
		foreach($district_data as $eachdist){

			$gpbydist[$eachdist->state][]=$eachdist;
		}

		$district_data = $gpbydist;
		$returndata    = '';
		$x=0;
		$groupstate='';

		if(count($district_data))
		{

			//$ret .='<ul class="list1 districtlist" onchange="getdistrictattr_v2(this.value)">';

			$groupstate.='<div class="slider">';
			foreach($district_data as $key=>$val)
			{

				$state_id = array_search($key, array_column($state_codes, 'name'));

				$paraMeter = "'"."districtGroup_".$state_id."'";

				$ret.='<li class="district_group" id='.$state_codes[$state_id]['state_code'].'><label class="district_label districtGroup_'.$state_id.'" >'.$key.'</label><div class="fright"><input type="checkbox" class="districtNameGrp allcheck districtlabel_checkbox-'.$state_id.'" id="districtlabel_allcheck-'.$state_id.'"><label>&nbsp;</label>|<a href="javascript:void(0);" onclick="district_child_sortListDir('.$paraMeter.')" class="districtGroup_'.$state_id.'"><i class="fa fa-sort"></i></a></div></li>';

				$groupstate.="<div><span class='tooltip-txt-box'><a href=#".$state_codes[$state_id]['state_code'].">".$state_codes[$state_id]['state_code']."</a><span class='tooltiptext'>".$state_codes[$state_id]['name']."</span></span></div>";
				$class=strtolower(str_replace(" ", "_", $key));
				$class=strtolower(str_replace("&", "_", $class));
				//print_r($groupstate);
				for($i = 0; $i < count($val); $i++){

					if(in_array($val[$i]->$districtsearchvariable,$preselected)){$selected="selected";$selected1="checked";}else{$selected="";$selected1="";}


					$ret.='<li class="'.$class.'">
							<input type="checkbox" class="district_checkbox districtlabel_checkbox-'.$state_id.'" name="district[]" value="'.$val[$i]->$districtsearchvariable.'" '.$selected1.' id="district_check_'.$x.'">
							<label for="district_check_'.$x.'">'.$val[$i]->distshp.'</label></li>';
							$x++;
				}


			}
			$groupstate.= '</div>';

		}
		else{

			$ret.='<ul class="list1 districtlist"></ul>';
		}
		//$ret .='</ul>';

		/*$getUrbanAgglormationNames=$this->getUrbanAgglormationNamesByStateNames($stat);
		$getUATownNamesByStateNames=$this->getUATownNamesByStateNames($stat);*/
		$getUrbanAgglormationNames = $getUATownNamesByStateNames = "";
		echo $ret."split".$groupstate."split".$getUrbanAgglormationNames."split".$getUATownNamesByStateNames ;exit;

		//return $district_data;

	}

	public function getsecondlevelvillage()
	{
		$districtsearchvariable = $this->districtsearchvariable;

		$districtIds            = $this->input->getString('villagedistrict');
		$session                = JFactory::getSession();
		$preselectedVillages    = $session->get('villages_villages');

		if (!is_array($preselectedVillages))
		{
			$preselectedVillages = explode(',', $preselectedVillages);
		}

		if ($districtIds == 'null')
		{
			$districtIds = $session->get('villagedistrict');
		}

		$db = JFactory::getDBO();

		if (!empty($districtIds))
		{
			$districtIds = explode(",", $districtIds);
			$districtIds = implode(",", $districtIds);
			$districtIds = str_replace(",", "','", $districtIds);
			$inCoumnName = "'" . $districtIds . "'";

			/*$query = "SELECT " . $districtsearchvariable . ", place_name, district, district_name
				FROM " . $db->quoteName('villages') . "
				WHERE " . $db->quoteName('district') . " IN (" . $inCoumnName . ")
				GROUP BY " . $db->quoteName('place_name') . ", " . $db->quoteName('district') . "
				ORDER BY " . $db->quoteName('district') . ", " . $db->quoteName('place_name') . " ASC ";*/
			$query = "SELECT " . $districtsearchvariable . ", place_name, district, district_name
				FROM " . $db->quoteName('villages') . "
				WHERE " . $db->quoteName('district') . " IN (" . $inCoumnName . ")
				ORDER BY " . $db->quoteName('district') . ", " . $db->quoteName('place_name') . " ASC ";



			$db->setQuery($query);


			try
			{
				$villageData = $db->loadObjectList();
			}
			catch (Exception $e)
			{
				$villageData = array();
			}
		}
		else
		{
			$villageData = array();
		}

		$gpbydist = array();

		foreach ($villageData as $eachdist)
		{
			$gpbydist[$eachdist->district_name][] = $eachdist;
		}

		$villageData = $gpbydist;
		$returndata    = '';
		$x=0;

		if (count($villageData))
		{
			$returndata .= '<select name="villages_villages[]" id="" class="inputbox" multiple="multiple" >';
			$ret .='<ul class="list1 villagelist">';

			foreach ($villageData as $key => $val)

			{
				$returndata .= '<optgroup label="' . $key . '">';
				$ret.='<li class="village_group"><label>'.$key.'</label></li>';

				for ($i = 0; $i < count($val); $i++)
				{
					if (in_array($val[$i]->$districtsearchvariable, $preselectedVillages))
					{
						$selected = "selected";
					}
					else
					{
						$selected = "";
					}

					$returndata .= '<option value="' . $val[$i]->$districtsearchvariable . '" ' . $selected . '>' . $val[$i]->place_name . '</option>';
					$ret.='<li>
							<input type="checkbox" class="village_checkbox" name="village[]" value="'.$val[$i]->$districtsearchvariable.'" '.$selected.' id="village_check_'.$x.'" >
							<label for="village_check_'.$x.'">'.$val[$i]->place_name.'</label></li>';
							$x++;

				}

				$returndata .= '</optgroup>';
			}


			$returndata .= '</select>';
		}
		else
		{
			$returndata .= '<select name="villages_villages" id="villages_villages" class="inputbox" onchange="getvillage(this.value)"></select>';
			$ret.='<ul class="list1 villagelist"></ul>';
		}
		$ret .='</ul>';

		$getUrbanAgglormationNames = $getUATownNamesByStateNames = "";
		echo $ret. "split" . $getUrbanAgglormationNames . "split" . $getUATownNamesByStateNames;
		exit;
	}

// public function getsecondlevelvillage()
// 	{
// 		$districtsearchvariable = $this->districtsearchvariable;
// 		$districtIds            = $this->input->getString('villagedistrict');
// 		$session                = JFactory::getSession();
// 		$preselectedVillages    = $session->get('villages_villages');

// 		if (!is_array($preselectedVillages))
// 		{
// 			$preselectedVillages = explode(',', $preselectedVillages);
// 		}

// 		if ($districtIds == 'null')
// 		{
// 			$districtIds = $session->get('villagedistrict');
// 		}

// 		$db = JFactory::getDBO();

// 		if (!empty($districtIds))
// 		{
// 			$districtIds = explode(",", $districtIds);
// 			$districtIds = implode(",", $districtIds);
// 			$districtIds = str_replace(",", "','", $districtIds);
// 			$inCoumnName = "'" . $districtIds . "'";

// 			/*$query = "SELECT " . $districtsearchvariable . ", place_name, district, district_name
// 				FROM " . $db->quoteName('villages') . "
// 				WHERE " . $db->quoteName('district') . " IN (" . $inCoumnName . ")
// 				GROUP BY " . $db->quoteName('place_name') . ", " . $db->quoteName('district') . "
// 				ORDER BY " . $db->quoteName('district') . ", " . $db->quoteName('place_name') . " ASC ";*/
// 			$query = "SELECT " . $districtsearchvariable . ", place_name, district, district_name
// 				FROM " . $db->quoteName('villages') . "
// 				WHERE " . $db->quoteName('district') . " IN (" . $inCoumnName . ")
// 				ORDER BY " . $db->quoteName('district') . ", " . $db->quoteName('place_name') . " ASC ";

// 			$db->setQuery($query);

// 			try
// 			{
// 				$villageData = $db->loadObjectList();
// 			}
// 			catch (Exception $e)
// 			{
// 				$villageData = array();
// 			}
// 		}
// 		else
// 		{
// 			$villageData = array();
// 		}

// 		$gpbydist = array();

// 		foreach ($villageData as $eachdist)
// 		{
// 			$gpbydist[$eachdist->district_name][] = $eachdist;
// 		}

// 		$villageData = $gpbydist;
// 		$ret    = "";
// 		$x= 0;

// 		if (count($villageData))
// 		{
// 			$returndata .= '<select name="villages_villages[]" id="villages_villages" class="inputbox" multiple="multiple" >';
// 			$ret .='<ul class="list1 villagelist">';

// 			foreach ($villageData as $key => $val)
// 			{
// 				$returndata .= '<optgroup label="' . $key . '">';
// 				$ret.='<li class="village_group">'.$key.'</li>';

// 				for ($i = 0; $i < count($val); $i++)
// 				{
// 					if (in_array($val[$i]->$districtsearchvariable, $preselectedVillages))
// 					{
// 						$selected = "selected";
// 					}
// 					else
// 					{
// 						$selected = "";
// 					}

// 					$returndata .= '<option value="' . $val[$i]->$districtsearchvariable . '" ' . $selected . '>' . $val[$i]->place_name . '</option>';
// 					$ret.='<li>
// 	 						<input type="checkbox" class="village_checkbox" name="village[]" value="'.$val[$i]->$villagesearchvariable.'" '.$selected.' id="village_check_'.$x.'" >
// 	 						<label for="village_check_'.$x.'">'.$val[$i]->place_name.'</label></li>';
// 	 						$x++;
// 				}

// 				$returndata .= '</optgroup>';
// 			}

// 			$returndata .= '</select>';
// 		}
// 		else
// 		{
// 			$returndata .= '<select name="villages_villages" id="villages_villages" class="inputbox" onchange="getvillage(this.value)"></select>';
// 			$ret.='<ul class="list1 villagelist"></ul>';
// 		}
// 		 	$ret .='</ul>';

// 		$getUrbanAgglormationNames = $getUATownNamesByStateNames = "";
// 		echo $ret . "split" . $getUrbanAgglormationNames . "split" . $getUATownNamesByStateNames;
// 		exit;
// 	}

	// public function getsecondlevelvillage()
	// {
	// 	$districtsearchvariable = $this->districtsearchvariable;

	// 	$districtIds            = $this->input->getString('villagedistrict');

	// 	$session                = JFactory::getSession();
	// 	$preselectedVillages    = $session->get('villages_villages');
	// 	//echo "<pre/>";print_r($preselectedVillages);exit;


	// 	if (!is_array($preselectedVillages))
	// 	{
	// 		$preselectedVillages = explode(',', $preselectedVillages);
	// 	}

	// 	if ($districtIds == 'null')
	// 	{
	// 		$districtIds = $session->get('villagedistrict');
	// 	}

	// 	$db = JFactory::getDBO();

	// 	if (!empty($districtIds))
	// 	{
	// 		$districtIds = explode(",", $districtIds);
	// 		$districtIds = implode(",", $districtIds);
	// 		$districtIds = str_replace(",", "','", $districtIds);
	// 		$inCoumnName = "'" . $districtIds . "'";

	// 		/*$query = "SELECT " . $districtsearchvariable . ", place_name, district, district_name
	// 			FROM " . $db->quoteName('villages') . "
	// 			WHERE " . $db->quoteName('district') . " IN (" . $inCoumnName . ")
	// 			GROUP BY " . $db->quoteName('place_name') . ", " . $db->quoteName('district') . "
	// 			ORDER BY " . $db->quoteName('district') . ", " . $db->quoteName('place_name') . " ASC ";*/
	// 		$query = "SELECT " . $districtsearchvariable . ", place_name, district, district_name
	// 			FROM " . $db->quoteName('villages') . "
	// 			WHERE " . $db->quoteName('district') . " IN (" . $inCoumnName . ")
	// 			ORDER BY " . $db->quoteName('district') . ", " . $db->quoteName('place_name') . " ASC ";

	// 		$db->setQuery($query);

	// 		try
	// 		{
	// 			$villageData = $db->loadObjectList();
	// 		}
	// 		catch (Exception $e)
	// 		{
	// 			$villageData = array();
	// 		}
	// 	}
	// 	else
	// 	{
	// 		$villageData = array();
	// 	}

	// 	$gpbydist = array();

	// 	foreach ($villageData as $eachdist)
	// 	{
	// 		$gpbydist[$eachdist->district_name][] = $eachdist;
	// 	}

	// 	$villageData = $gpbydist;
	// 	$returndata    = '';
 //         $x=0;
	// 	if (count($villageData))
	// 	{

	// 		$returndata .= '<select name="villages_villages[]" id="villages_villages" class="inputbox" multiple="multiple" >';
	// 		$ret .='<ul class="list1 villagelist">';

	// 		foreach ($villageData as $key => $val)
	// 		{
	// 			$returndata .= '<optgroup label="' . $key . '">';
	// 			$ret.='<li class="village_group">'.$key.'</li>';

	// 			for ($i = 0; $i < count($val); $i++)
	// 			{
	// 				if (in_array($val[$i]->$districtsearchvariable, $preselectedVillages))
	// 				{
	// 					$selected = "selected";
	// 				}
	// 				else
	// 				{
	// 					$selected = "";
	// 				}

	// 				$returndata .= '<option value="' . $val[$i]->$districtsearchvariable . '" ' . $selected . '>' . $val[$i]->place_name . '</option>';

	// 				$ret.='<li>
	// 						<input type="checkbox" class="village_checkbox" name="village[]" value="'.$val[$i]->$villagesearchvariable.'" '.$selected.' id="village_check_'.$x.'" >
	// 						<label for="village_check_'.$x.'">'.$val[$i]->distshp.'</label></li>';
	// 						$x++;

	// 			}

	// 			$returndata .= '</optgroup>';
	// 		}

	// 		$returndata .= '</select>';
	// 	}
	// 	else


	// 	{
	// 		//$returndata .= '<select name="villages_villages" id="villages_villages" class="inputbox" onchange="getvillage(this.value)"></select>';
	// 		$ret.='<ul class="list1 villagelist"></ul>';
	// 	}
	// 	$ret .='</ul>';

	// 	$getUrbanAgglormationNames = $getUATownNamesByStateNames = "";
	// 	echo $returndata . "split" . $getUrbanAgglormationNames . "split" . $getUATownNamesByStateNames;
	// 	exit;
	// }


    function getUrbanAgglormationNamesByStateNames($state){
			$urbansearchvariable=$this->urbansearchvariable;

			$db = JFactory::getDBO();
			if($state){
				$query = "SELECT ".$urbansearchvariable.",place_name
					FROM ".$db->quoteName('jos_mica_urban_agglomeration')."
					WHERE ".$db->quoteName('place_name')." LIKE '%".$state."%'";
				$db->setQuery($query);
				$urban_data = $db->loadObjectList();
			}else{
				$urban_data = array();
			}

			$returndata = '';
			if(count($urban_data)){
				$returndata .= '<select name="urban" id="urban" class="inputbox" onchange="geturban(this.value)" >';
				$returndata .= '<option value="">'.JText::_('PLEASE_SELECT').'</option><option value="all">'.JText::_('ALL').'</option>';
				for($i=0;$i<count($urban_data);$i++){
					$returndata .= '<option value="'.$urban_data[$i]->$urbansearchvariable.'">'.$urban_data[$i]->place_name.'</option>';
				}
				$returndata .= '</select>';
			}else{
				$returndata .= '<select name="urban" id="urban" class="inputbox" ></select>';
			}
			return $returndata;
		}


	function getUATownNamesByStateNames($state){
		$townsearchvariable = $this->townsearchvariable;

		$db = JFactory::getDBO();
		if($state){
			$query = "SELECT ".$this->townsearchvariable.",place_name
				FROM ".$db->quoteName('my_table')."
				WHERE ".$db->quoteName('state')." LIKE '%".$state."%'";
			$db->setQuery($query);
			$town_data = $db->loadObjectList();
		}else{
			$town_data = array();
		}

		$returndata = '';
		if(count($town_data)){
			$returndata .= '<select name="town" id="town" class="inputbox" onchange="gettown(this.value)" >';
			$returndata .= '<option value="">'.JText::_('PLEASE_SELECT').'</option><option value="all">'.JText::_('ALL').'</option>';
			for($i=0;$i<count($town_data);$i++){
				$returndata .= '<option value="'.$town_data[$i]->$townsearchvariable.'">'.$town_data[$i]->place_name.'</option>';
			}
			$returndata .= '</select>';
		}else{
			$returndata .= '<select name="town" id="town" class="inputbox" onchange="gettown(this.value)"></select>';
		}
		return $returndata;
	}

	function gettown(){
		$db = JFactory::getDBO();

		$stat     = $this->input->get('stat','','raw');
		$district = $this->input->get('district','','raw');

		if($stat && $district){
			$query = "SELECT id,TownName
				FROM ".$db->quoteName('#__mica_towns')."
				WHERE ".$db->quoteName('StateName')." LIKE '%".$stat."%'
					AND ".$db->quoteName('DistrictName')." LIKE '%".$district."%'";
			$db->setQuery($query);
			$town_data = $db->loadObjectList();
		}else{
			$town_data = array();
		}

		$returndata = '';
		if(count($town_data)){
			$returndata .= '<select name="town" id="town" class="inputbox" onchange="showhidetype(this.value)">';
			$returndata .= '<option value="all">'.JText::_('ALL').'</option>';
			for($i=0;$i<count($town_data);$i++){
				$returndata .= '<option value="'.$town_data[$i]->TownName.'">'.$town_data[$i]->TownName.'</option>';
			}
			$returndata .= '</select>';
		}else{
			$returndata .= '<select name="town" id="town" class="inputbox" onchange="showhidetype(this.value)">
						   	<option value="">'.JText::_('PLEASE_SELECT').'</option>
						   	</select>';
		}
		echo $returndata;exit;
	}




	function getStateField(){
		$state = $this->input->get('state','','raw');

		$db = JFactory::getDBO();
		$query = "SELECT * FROM ".$db->quoteName('#	')." WHERE ".$db->quoteName('StateName')." LIKE '%".$state."%' ";
		$db->setQuery($query);
		$state_data = $db->loadObjectList();

		$returnable = array();
		foreach($state_data as $key=>$val){
			foreach($val as $key1=>$val1){
				$returnable[$key1]=$val1;
			}
		}
		echo json_encode($state_data);exit;
	}

	function getDistrictField(){
		$stat = $this->input->get('state','','raw');
		$dist = $this->input->get('dist','','raw');

		$db = JFactory::getDBO();
		$query = "SELECT * FROM  ".$db->quoteName('#__mica_district_r_u')."
			WHERE ".$db->quoteName('StateName')." LIKE '%".$stat."%'
				AND ".$db->quoteName('DistrictName')." LIKE '%".$dist."%' ";
		$db->setQuery($query);
		$district_data = $db->loadObjectList();

		$returnable = array();
		foreach($district_data as $key=>$val){
			foreach($val as $key1=>$val1){
				$returnable[$key1]=$val1;
			}
		}
		echo json_encode($district_data );exit;
	}

	/**
	 * GetIndLvlGrp Ajax call method to get Industry Level Group related fields.
	 *
	 * @return  void
	 */
	public function getIndLvlGrp()
	{
		$app      = JFactory::getApplication();
		$groupIds = str_replace(',', '\',\'' , $app->input->getString('groupid'));

		// Industry Level Group Data Set
		$session                = JFactory::getSession();
		$indlvlgrps = str_replace(',', '\',\'' , $app->input->getString('groupid'));
		if($indlvlgrps == ""){
			$indlvlgrps = $session->get('indlvlgrps');
		}else{
			$session->set('indlvlgrps',$indlvlgrps);
		}

		// Initialiase variables.
		$db    = JFactory::getDbo();
		$query = $db->getQuery(true);

		// Create the base select statement.
		$query->select('field')
			->from($db->qn('#__mica_industry_level_group_field'))
			->where($db->qn('groupid') . " IN('" . $groupIds . "')");

		// Set the query and load the result.
		$db->setQuery($query);

		try
		{
			$data = array(
				'variables' => $db->loadColumn(),
				'type'      => $this->getIndLvlGrpType($groupIds),
			);

			echo json_encode($data);
			$app->close();
			die;
		}
		catch (RuntimeException $e)
		{
			JError::raiseWarning(500, $e->getMessage());
		}

		echo "false";
		$app->close();
		die;
	}

	/**
	 * GetIndLvlGrpType Method to get type based on seleceted Industry Level Group(s).
	 *
	 * @return  array
	 */
	public function getIndLvlGrpType($groupIds)
	{
		// Initialiase variables.
		$db    = JFactory::getDbo();
		$query = $db->getQuery(true);

		// Create the base select statement.
		$query->select('type')
			->from($db->qn('#__mica_industry_level_group'))
			->where($db->qn('id') . " IN('" . $groupIds . "')")
			->where($db->qn('publish') . " = 1");

		// Set the query and load the result.
		$db->setQuery($query);

		try
		{
			// $types = $db->loadColumn();
			return explode(',', $db->loadResult());
		}
		catch (RuntimeException $e)
		{
			JError::raiseWarning(500, $e->getMessage());
		}

		return false;
	}
}
