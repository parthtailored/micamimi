<?php
/**
 * @package     Joomla.Site
 * @subpackage  com_mica
 *
 * @copyright   Copyright (C) 2005 - 2017 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

/**
 * Dashboard view class for Mica.
 *
 * @since  1.6
 */
class MicaViewDashboard extends JViewLegacy
{
	/**
	 * Execute and display a template script.
	 *
	 * @param   string  $tpl  The name of the template file to parse; automatically searches through the template paths.
	 *
	 * @return  mixed   A string if successful, otherwise an Error object.
	 *
	 * @since   1.6
	 */
	public function display($tpl = null)
	{
		$this->userinfo                  = $this->get('Account');
		$this->Workspace                 = $this->get('Workspace');
		$this->getCreatedAccount         = $this->get('CreatedAccount');
		$this->getCreatedAccountFromAEC  = $this->get('CreatedAccountFromAEC');
		$this->getParentinfo             = $this->get('Parentinfo');
		$this->getParentsubscriptioninfo = $this->get('Parentsubscriptioninfo');
		$this->allowAdd                  = $this->getmodel()->isAllowedAdd();
		$this->UserStatistics            =  $this->get('UserStatistics');

		// Set Layout
		$this->setLayout($_GET['layout']);

		return parent::display($tpl);
	}

}
