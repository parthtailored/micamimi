<?php
/**
 * @package     Joomla.Site
 * @subpackage  com_mica
 *
 * @copyright   Copyright (C) 2005 - 2017 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

require_once( JPATH_COMPONENT.DIRECTORY_SEPARATOR.'helpers/Workspace.php' );
require_once( JPATH_COMPONENT.DIRECTORY_SEPARATOR.'helpers/css.php' );

/**
 * VillageShowresults view class for Mica.
 *
 * @since  1.6
 */
class MicaViewVillageShowresults extends JViewLegacy
{
	var $townsearchvariable     = "OGR_FID";
	var $urbansearchvariable    = "OGR_FID";
	var $districtsearchvariable = "OGR_FID";
	var $statesearchvariable    = "OGR_FID";
	var $tomcaturl = "";

	/**
	 * Execute and display a template script.
	 *
	 * @param   string  $tpl  The name of the template file to parse; automatically searches through the template paths.
	 *
	 * @return  mixed   A string if successful, otherwise an Error object.
	 *
	 * @since   1.6
	 */
	public function display($tpl = null)
	{


		if(!defined('TOMCAT_URL')){
			$this->getTomcatUrl();
			DEFINE('TOMCAT_URL', $this->tomcaturl);
		}
DEFINE('TOMCAT_STYLE_ABS_PATH' ,"/usr/share/tomcat/webapps/geoserver/data/styles/www/data/micamimi18/");
		if(!defined('TOMCAT_STYLE_ABS_PATH')){
			//DEFINE('TOMCAT_STYLE_ABS_PATH' ,"/usr/local/easy/share/easy-tomcat7/webapps/geoserver/data/www/styles/");
			DEFINE('TOMCAT_STYLE_ABS_PATH' , JPATH_SITE."/geoserver/data/www/styles/");
		}

		if(!defined('TOMCAT_SLD_FOLDER')){
			DEFINE('TOMCAT_SLD_FOLDER' ,"geoserver/www/styles");
		}

		$app = JFactory::getApplication();
		$msg = $app->input->get('msg');
		if($msg == "-1"){
			$app->enqueueMessage(JTEXT::_('WORKSPACE_DELETED') );
		}else if($msg=="0"){
			$app->enqueueMessage(JTEXT::_('WORKSPACE_CREATED') );
		}else if($msg=="1"){
			$app->enqueueMessage(JTEXT::_('WORKSPACE_UPDATED') );
		}

		$refererview = $app->input->get("refeterview");
		$session     = JFactory::getSession();

		if($refererview == "villagefront"){
			$session->set('oldattributes',"");
			$session->set('villagesattributes',null);
			$session->set('villagestate',null);
			$session->set('villagedistrict',null);
			$session->set('urban',null);
			$session->set('villages_villages', null);
			$session->set('activeworkspace',null);
			$session->set('attrname',null);
			$session->set('customattribute',null);
			$session->set('villagem_type',null);
			$session->set('activetable',null);
			$session->set('activedata',null);
			$session->set('village_composite',null);
			$session->set('village_m_type_rating',null);
			$session->set('customformula',null);
			$session->set('Groupwisedata',null);
			$session->set('popupactivedata',null);
			$session->set('themeticattribute',null);
			$session->set('activenamevariable',null);
			$session->set('activesearchvariable',null);
			$session->set('dataof',null);
			$session->set('customattributelib',null);
		}

		$session->set('village_m_type_rating', $app->input->get('village_m_type_rating', '', 'raw'));

		if ($session->get('activeworkspace') == "")
		{
			$result = workspaceHelper::loadWorkspace(1);

	    	if (!empty($result))
	    	{

	    		$session->set('activeworkspace',$result[0]['id']);
				$innercall = 1;
				$session->set('gusetprofile',$result[0]['id']);
				$session->set('is_default',"1");
				$app->input->set('activeworkspace', $session->get('activeworkspace'));
				cssHelper::flushOrphandSld($session->get('activeworkspace'));
	    	}
	    	else
	    	{
				$name      = $app->input->set("name", "Guest");
				$default   = $app->input->set("default", 1);
				$innercall = 1;
				$id        = workspaceHelper::saveWorkspace($innercall);
				$session->set('gusetprofile', $id);
				$session->set('activeworkspace', $id);
				$session->set('is_default',"1");
				//$app->redirect("index.php?option=com_mica&view=villageshowresults&Itemid=188");
	    	}
		}
		elseif ($session->get('is_default') == 1)
		{
	    	workspaceHelper::updateWorkspace($innercall);
	    }
	    else
	    {
    	    $app->input->set('activeworkspace', $session->get('activeworkspace'));
			//$this->saveSldToDataBase("1","1");
	    }

		$state = $app->input->get('villagestate', '', 'raw');
		$stats = array();

		foreach ($state as $eachstat)
		{
			$stats[] = base64_decode($eachstat);
		}

		$state = $stats;

		if (is_array($state))
		{
			$state = implode(",", $state);
			$app->input->set('villagestate', $state);
		}

		$sessionstate = $session->get('villagestate');
		$app->input->set('villagestate', $sessionstate);

		if ($sessionstate != "")
		{
			$addtosession = ($sessionstate . "," . $state);
			$tmp          = explode(",", $addtosession);
			$mytmp        = array_unique($tmp);
			$mytmp        = array_filter($mytmp);
			$sessionstate = implode(",", $mytmp);
		}
		else
		{
			$sessionstate = $state;
		}

		$session->set('villagestate', $sessionstate);
		$district = $app->input->get('villagedistrict', '', 'raw');

		if (is_array($district))
		{
			$district = implode(",", $district);
			$app->input->set('villagedistrict', $district);
		}

		$sessiondistrict = $session->get('villagedistrict');

		if ($sessiondistrict != "")
		{
			$addtosession1   = ($sessiondistrict . "," . $district);
			$tmp             = explode(",", $addtosession1);
			$mytmp           = array_unique($tmp);
			$mytmp           = array_filter($mytmp);
			$sessiondistrict = implode(",", $mytmp);
		}
		else
		{
			$sessiondistrict = $district;
		}

		$app->input->set('villagedistrict', $sessiondistrict);
		$session->set('villagedistrict', $sessiondistrict);

		$m_type = $app->input->get('villagem_type', '', 'raw');

		// $session_mtype=($session->get('m_type'));
		if (!is_array($m_type))
		{
			$m_type = $session->get('villagem_type');
		}
		else
		{
			$session->set('villagem_type', $m_type);
			$m_type = $session->get('villagem_type');
		}

		/*  if(!is_array($session_mtype)){
				$session->set('m_type',$m_type);
				$m_type= $session->get('m_type');
			}else{
				echo 2;exit;
				$m_type= $session->get('m_type');
				$m_typepost=JRequest::getVar('m_type');
				$m_type=array_merge($m_type,$m_typepost);
				$mytmp=array_unique($m_type);
		  		$mytmp=array_filter($mytmp);
				$session->set('m_type',$mytmp);
				$m_type= $mytmp;
			}
		*/

		/*
		 * Ronak: To fix issue of big data post truncated.
		 *	$villages    = $app->input->get('villages_villages', '', 'raw');
		 */
		$villages = json_decode($app->input->getString('tempvillagejson'));
		// Ronak: End

		$sessiontown = $session->get('villages_villages');

		if ($sessiontown != "")
		{
			if (is_array($villages))
			{
				$villages = implode(',', $villages);
			}

			$addtosession = ($sessiontown.",".$villages);
			$tmp          = explode(",",$addtosession);
			$mytmp        = array_unique($tmp);
			$mytmp        = array_filter($mytmp);
			$sessiontown  = implode(",", $mytmp);
		}
		else
		{
			$sessiontown = $villages;

			if (is_array($villages))
			{
				$villagesmytmp = array_unique($villages);
				$villagesmytmp = array_filter($villagesmytmp);
				$sessiontown   = implode(",", $villagesmytmp);
			}
		}

		$session->set('villages_villages', $sessiontown);
		$app->input->set('villages_villages', $sessiontown);
		$urban        = $app->input->get('urban', '', 'raw');
		$sessionurban = $session->get('urban');

		if ($sessionurban != "")
		{
			$addtosession = ($sessionurban . "," . $urban);
			$tmp          = explode(",", $addtosession);
			$mytmp        = array_unique($tmp);
			$mytmp        = array_filter($mytmp, 'strlen');
			$sessionurban = implode(",", $mytmp);
			$session->set('dataof', "Urban");
		}
		else
		{
			$sessionurban = $urban;
		}

		$session->set('urban', $sessionurban);
		$app->input->set('urban', $sessionurban);

		if ($sessiontown != "")
		{
			$app->input->set('db_table', 'Villages');
			$zoom = 9;
		}
		elseif ($sessionurban != "")
		{
			$app->input->set('db_table', 'Urban');
			$zoom = 8;
		}
		elseif ($sessiondistrict != "")
		{
			$app->input->set('db_table', 'District');
			$zoom = 7;
		}
		else
		{
			$app->input->set('db_table', 'State');
			$zoom = 6;
		}

		$dataof = $app->input->get('dataof', '', 'raw');

		if ($dataof == "")
		{
			$dataof = $session->get('dataof');

			// $app->input->get('dataof',$dataof);
		}
		else
		{
			$session->set('dataof', $dataof);
		}

		$composite = $app->input->get('village_composite', '', 'raw');

		if (!empty($composite))
		{
			$session->set('village_composite', $app->input->get('village_composite', '', 'raw'));

			// $attributes = JRequest::getVar('villagesattributes');
			$composite = array_merge(array("MPI", "Total_Geographical_Area_in_Hectares", "Nearest_Town_Name"), $composite);
		}
		else
		{
			$session->clear('village_composite');
		}

		$attributes = $app->input->get('villagesattributes', '', 'raw');

		if (!empty($attributes))
		{
			$composite = $session->get('village_composite');

			if (!empty($composite))
			{
				$attributes = array_merge(array("MPI", "Total_Geographical_Area_in_Hectares", "Nearest_Town_Name"), $attributes, $composite);
			}
			else
			{
				$attributes = array_merge(array("MPI", "Total_Geographical_Area_in_Hectares", "Nearest_Town_Name"), $attributes);
			}
		}
		else
		{
			$attributes = array("MPI", "Total_Geographical_Area_in_Hectares", "Nearest_Town_Name");
		}

		if (is_array($attributes))
		{
			$attributes = array_filter($attributes);
			$attributes = implode(",", $attributes);
		}

		$sessionattr = $session->get('villagesattributes');

		if ($sessionattr != "")
		{
			if ($app->input->get('villagesattributes', '', 'raw') != "")
			{
				$addtosession = $attributes;
			}
			else
			{
				$addtosession = $sessionattr;
			}

			$tmp          = explode(",", $addtosession);
			$mytmp        = array_unique($tmp);
			$mytmp        = array_filter($mytmp, 'strlen');
			$addtosession = implode(",", $mytmp);
		}
		else
		{
			$addtosession = $attributes;
		}

		$session->set('villagesattributes', $addtosession);
		$app->input->set('sessionattributes',$addtosession);
		$state_items                  = $this->get('StateItems');
		$this->dataof                 = $dataof;
		$this->state_items            = $state_items;
		$this->townsearchvariable     = $this->townsearchvariable;
		$this->urbansearchvariable    = $this->urbansearchvariable;
		$this->districtsearchvariable = $this->districtsearchvariable;

		$this->Itemid                 = $app->input->get('Itemid', 188);
		$customattribute              = $session->get('customattribute');
		$new_name                     = $app->input->get('new_name', array(), 'array');
		$custom_attribute             = $app->input->get('custom_attribute', array(), 'array');
		$customdata                   = $this->get('CustomData');

		//$MaxForMatrix= $this->get('MaxForMatrix');
		//$this->MaxForMatrix = $MaxForMatrix;
		$this->customdata = $customdata;
		$excludeVarArray = array(
		    'Agricultural_Commodities_First',
		    'Agricultural_Commodities_Second',
		    'Agricultural_Commodities_Third',
		    'Manufacturers_Commodities_First',
		    'Manufacturers_Commodities_Second',
		    'Manufacturers_Commodities_Third',
		    'Handicrafts_Commodities_First',
		    'Handicrafts_Commodities_Second',
		    'Handicrafts_Commodities_Third',
			'Agricultural_Marketing_Society_Status_A1NA2',
			'MandisRegular_Market_Status_A1NA2',
			'Public_Distribution_System_PDS_Shop_Status_A1NA2',
			'Weekly_Haat_Status_A1NA2',
			'CinemaVideo_Hall_Status_A1NA2',
			'Community_Centre_withwithout_TV_Status_A1NA2',
			'Daily_Newspaper_Supply_Status_A1NA2',
			'Mobile_Phone_Coverage_Status_A1NA2',
			'Public_Library_Status_A1NA2',
			'Sports_ClubRecreation_Centre_Status_A1NA2',
			'Sports_Field_Status_A1NA2',
			'Tractors_Status_A1NA2',
			'Commercial_Bank_Status_A1NA2'
		);
		$this->popVariables = array();

		foreach ($customdata[0] as $key => $value) {
			if (!in_array($key, $excludeVarArray)) {
				$this->popVariables[$key] = $value;
			}
		}

		$this->state      = $sessionstate;
		$this->district   = $sessiondistrict;
		$this->villages   = $sessiontown;
		$this->urban      = $sessionurban;

		$page = $app->input->get("page");

		$AttributeTable  = $this->get('NewAttributeTable');// $this->get('AttributeTable');
		$graph           = $this->get('Graph');
		$GraphSettings   = $this->get('GraphSettings');

		$activesearchvariable       = $session->get('activesearchvariable');
		$this->activesearchvariable = $activesearchvariable;
		$this->zoom                 = $zoom;
		/*$tomcaturl                  = TOMCAT_URL;
		$this->tomcaturl            = $tomcaturl;*/

		$BreadCrumbcontent = $this->get('BreadCrumb');
		list($BreadCrumb, $workspacelink) = explode("~~~", $BreadCrumbcontent);
		$this->workspacelinkdiv   = $workspacelink;
		$this->BreadCrumb         = $BreadCrumb;

		$geometry                 = $this->get('InitialGeometry');
		$UnselectedDistrict       = $this->get("UnselectedDistrict");
		$this->UnselectedDistrict = $UnselectedDistrict;
		$this->geometry           = $geometry;


		$this->user               = JFactory::getUser();
		$this->userid             = $this->user->id;
		$this->activeworkspace    = $session->get('activeworkspace');

		$this->is_default         = $session->get('is_default');
		$this->customattribute    = $customattribute;

		$this->get('customattrlib');
		$this->customattributelib = $session->get('customattributelib');
		$this->AttributeTable     = $AttributeTable;
		$this->GraphSettings      = $GraphSettings;
		$this->graph              = $graph;

		/*$this->m_type = $m_type;$app->input->set('m_type',$m_type);*/
		$this->attributes        = $addtosession;
		$this->new_name          = $new_name;
		$this->custom_attribute  = $custom_attribute;

		$pagination = $this->get('Pagination');
		$this->pagination        = $pagination;
		$this->themeticattribute = $session->get('themeticattribute');
		$this->themeticattribute = explode(',', $this->themeticattribute);

		$excludeVarArray = array(
		    'Agricultural_Marketing_Society_Status_A1NA2',
			'MandisRegular_Market_Status_A1NA2',
			'Public_Distribution_System_PDS_Shop_Status_A1NA2',
			'Weekly_Haat_Status_A1NA2',
			'CinemaVideo_Hall_Status_A1NA2',
			'Community_Centre_withwithout_TV_Status_A1NA2',
			'Daily_Newspaper_Supply_Status_A1NA2',
			'Mobile_Phone_Coverage_Status_A1NA2',
			'Public_Library_Status_A1NA2',
			'Sports_ClubRecreation_Centre_Status_A1NA2',
			'Sports_Field_Status_A1NA2',
			'Tractors_Status_A1NA2',
			'Commercial_Bank_Status_A1NA2',
			'Agricultural_Commodities_First',
		    'Agricultural_Commodities_Second',
		    'Agricultural_Commodities_Third',
		    'Manufacturers_Commodities_First',
		    'Manufacturers_Commodities_Second',
		    'Manufacturers_Commodities_Third',
		    'Handicrafts_Commodities_First',
		    'Handicrafts_Commodities_Second',
		    'Handicrafts_Commodities_Third'
		);

		$this->themeticattribute = implode(',', array_values(array_diff($this->themeticattribute, $excludeVarArray)));

		workspaceHelper::updateWorkspace($innercall);
		cssHelper::saveSldToDatabase(1, 1);

		return parent::display($tpl);
	}

	/**
	 * Internal function fetches URL.
	 */
	function getTomcatUrl()
	{
		$db    = JFactory::getDBO();
		$query = "SELECT tomcatpath FROM " . $db->quoteName('#__mica_configuration');
		$db->setQuery($query);
		$this->tomcaturl = $db->loadResult();
	}
}

