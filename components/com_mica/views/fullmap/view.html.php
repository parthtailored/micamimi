<?php
/**
 * @package     Joomla.Site
 * @subpackage  com_mica
 *
 * @copyright   Copyright (C) 2005 - 2017 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

require_once( JPATH_COMPONENT.DIRECTORY_SEPARATOR.'helpers/Workspace.php' );
require_once( JPATH_COMPONENT.DIRECTORY_SEPARATOR.'helpers/css.php' );

/**
 * Fullmap view class for Mica.
 *
 * @since  1.6
 */
class MicaViewFullmap extends JViewLegacy
{

	var $townsearchvariable     = "OGR_FID";
	var $urbansearchvariable    = "OGR_FID";
	var $districtsearchvariable = "OGR_FID";
	var $statesearchvariable    = "OGR_FID";
	var $tomcaturl = "";

	/**
	 * Execute and display a template script.
	 *
	 * @param   string  $tpl  The name of the template file to parse; automatically searches through the template paths.
	 *
	 * @return  mixed   A string if successful, otherwise an Error object.
	 *
	 * @since   1.6
	 */
	public function display($tpl = null){

		$app     = JFactory::getApplication();
		$session = JFactory::getSession();
		$user    = JFactory::getUser();

		if(!defined('TOMCAT_URL')){
			$this->getTomcatUrl();
			DEFINE('TOMCAT_URL', $this->tomcaturl);
		}

		if(!defined('TOMCAT_STYLE_ABS_PATH')){
			//DEFINE('TOMCAT_STYLE_ABS_PATH' ,"/usr/local/easy/share/easy-tomcat7/webapps/geoserver/data/www/styles/");
			DEFINE('TOMCAT_STYLE_ABS_PATH' , JPATH_SITE."/geoserver/data/www/styles/");
		}

		if(!defined('TOMCAT_SLD_FOLDER')){
			DEFINE('TOMCAT_SLD_FOLDER' ,"geoserver/www/styles/micamimi18");
		}


		$msg = $app->input->get('msg');
		if($msg=="-1"){
			$app->enqueueMessage( JTEXT::_('WORKSPACE_DELETED') );
		}else if($msg=="0"){
			$app->enqueueMessage(JTEXT::_('WORKSPACE_CREATED') );
		}else if($msg=="1"){
			$app->enqueueMessage(JTEXT::_('WORKSPACE_UPDATED') );
		}

		$m_type = $app->input->get('m_type', '', 'raw');

		if($session->get('activeworkspace') == ""){
			$result = workspaceHelper::loadWorkspace(1);

	    	if(!empty($result)){

	    		$session->set('activeworkspace', $result[0]['id']);
				$innercall = 1;
				$session->set('gusetprofile', $result[0]['id']);
				$session->set('is_default', "1");
				$app->input->set('activeworkspace', $session->get('activeworkspace'));
				cssHelper::flushOrphandSld($session->get('activeworkspace'));

	    	}else{

				$name      = $app->input->set("name", "Guest");
				$default   = $app->input->set("default", 1);
				$innercall = 1;
				$id        = workspaceHelper::saveWorkspace($innercall);
				$session->set('gusetprofile', $id);
				$session->set('activeworkspace', $id);
				$session->set('is_default', "1");
				$app->redirect("index.php?option=com_mica&view=showresults&Itemid=188");
	    	}

		}else if($session->get('is_default') == 1){

	    	workspaceHelper::updateWorkspace($innercall);

	    }else{

    	    $app->input->set('activeworkspace', $session->get('activeworkspace'));

	    }

		$state = $app->input->get('state', '', 'raw');
		if(is_array($state)){
			$state = implode(",",$state);
			$app->input->set('state',$state);
		}
		$sessionstate = $session->get('state');
		$app->input->set('state', $sessionstate);
		if($sessionstate !="" ){
			$addtosession = ($sessionstate.",".$state);
			$tmp          = explode(",",$addtosession);
			$mytmp        = array_unique($tmp);
			$mytmp        = array_filter($mytmp);
			$sessionstate = implode(",",$mytmp);
		}else{
			$sessionstate = $state;
		}
		$session->set('state',$sessionstate);


		$district = $app->input->get('district', '', 'raw');
		if(is_array($district)){
			$district = implode(",",$district);
			$app->input->set('district',$district);
		}
		$sessiondistrict = $session->get('district');
		if($sessiondistrict !=""){
			$addtosession1   = ($sessiondistrict.",".$district);
			$tmp             = explode(",",$addtosession1);
			$mytmp           = array_unique($tmp);
			$mytmp           = array_filter($mytmp);
			$sessiondistrict = implode(",",$mytmp);
		}else{
			$sessiondistrict = $district;
		}
		$app->input->set('district',$sessiondistrict);
		if($sessiondistrict != ""){
			if($session->get('m_type') == ""){
				$session->set('m_type',$m_type);
			}else{
				$m_type = $session->get('m_type');
			}
		}
		$session->set('district',$sessiondistrict);


		$town        = $app->input->get('town', '', 'raw');
		$sessiontown = $session->get('town');
		if($sessiontown!=""){
			$addtosession = ($sessiontown.",".$town);
			$tmp          = explode(",",$addtosession);
			$mytmp        = array_unique($tmp);
			$mytmp        = array_filter($mytmp);
			$sessiontown  = implode(",",$mytmp);
		}else{
			$sessiontown = $town;
		}
		$session->set('town',$sessiontown);
		$app->input->set('town',$sessiontown);


		$urban        = $app->input->get('urban', '', 'raw');
		$sessionurban = $session->get('urban');
		if($sessionurban != ""){
			$addtosession = ($sessionurban.",".$urban);
			$tmp          = explode(",",$addtosession);
			$mytmp        = array_unique($tmp);
			$mytmp        = array_filter($mytmp, 'strlen');
			$sessionurban = implode(",",$mytmp);
			$session->set('dataof',"Urban");
		}else{
			$sessionurban = $urban;
		}
		$session->set('urban',$sessionurban);
		$app->input->set('urban',$sessionurban);
		if($sessiontown!=""){
			$app->input->set('db_table','Town');
			$session->set('m_type',"");
			$zoom = 9;
		}else if($sessionurban != ""){
			$app->input->set('db_table','Urban');
			$session->set('m_type',"");
			$zoom = 8;
		}else if($sessiondistrict!=""){
			$app->input->set('db_table','District');
			$zoom = 7;
		}else{
			$app->input->set('db_table','State');
			$session->set('m_type',"Total");
			$zoom = 6;
		}


		$dataof = $app->input->get('dataof', '', 'raw');
		if($dataof == ""){
			$dataof = $session->get('dataof');
			$app->input->get('dataof',$dataof);
		}else{
			$session->set('dataof',$dataof);
		}


		$attributes = $app->input->get('attributes', '', 'raw');
		if(is_array($attributes)){
			$attributes = array_filter($attributes);
			$attributes = implode(",",$attributes);
		}
		$sessionattr = $session->get('attributes');
		if($sessionattr != ""){
			if($app->input->get('attributes', '', 'raw') != ""){
				$addtosession = ($attributes);
			}else{
				$addtosession = ($sessionattr);
			}
			$tmp          = explode(",",$addtosession);
			$mytmp        = array_unique($tmp);
			$mytmp        = array_filter($mytmp, 'strlen');
			$addtosession = implode(",",$mytmp);
		}else{
			$addtosession = $attributes;
		}
		$session->set('attributes',$addtosession );
		$app->input->set('sessionattributes',$addtosession);


		$state_items = $this->get('StateItems');

		$this->state_items            = $state_items;
		$this->townsearchvariable     = $this->townsearchvariable;
		$this->urbansearchvariable    = $this->urbansearchvariable;
		$this->districtsearchvariable = $this->districtsearchvariable;

		$this->Itemid = $app->input->get('Itemid', 188);

		$customattribute  = $session->get('customattribute');
		$new_name         = $app->input->get('new_name', array(), 'array');
		$custom_attribute = $app->input->get('custom_attribute', array(), 'array');
		$customdata       = $this->get('CustomData');

		$MaxForMatrix     = $this->get('MaxForMatrix');

		$this->MaxForMatrix = $MaxForMatrix;
		$this->customdata   = $customdata;
		$this->state        = $sessionstate;
		$this->district     = $sessiondistrict;
		$this->town         = $sessiontown;
		$this->urban        = $sessionurban;

		$AttributeTable = $this->get('NewAttributeTable');//$this->get('AttributeTable');
		$GraphSettings  = $this->get('GraphSettings');
		$graph          = $this->get('Graph');
		$GraphSettings  = $this->get('GraphSettings');

		$activesearchvariable = $session->get('activesearchvariable');
		$this->zoom           = $zoom;
		/*$tomcaturl = TOMCAT_URL;
		$this->tomcaturl = $tomcaturl;*/

		$BreadCrumb       = $this->get('BreadCrumb');
		$this->BreadCrumb = $BreadCrumb;

		$geometry       = $this->get('InitialGeometry');
		$this->geometry = $geometry;

		$this->user            = $user;
		$this->userid          = $user->id;
		$this->activeworkspace = $session->get('activeworkspace');
		$this->is_default      = $session->get('is_default');
		$this->customattribute = $customattribute;

		$this->get('customattrlib');
		$this->customattributelib = $session->get('customattributelib');
		$this->AttributeTable     = $AttributeTable;
		$this->GraphSettings      = $GraphSettings;
		$this->graph              = $graph;

		$this->m_type = $m_type;
		$app->input->set('m_type',$m_type);

		$this->attributes        = $addtosession;
		$this->new_name          = $new_name;
		$this->custom_attribute  = $custom_attribute;
		$this->pagination        = $pagination;
		$this->themeticattribute = $session->get('themeticattribute');

		cssHelper::saveSldToDatabase(1, 0);

		return parent::display($tpl);
	}

	/**
	 * Internal function fetches URL.
	 */
	function getTomcatUrl(){
		$db    = JFactory::getDBO();
		$query = "SELECT tomcatpath FROM ".$db->quoteName('#__mica_configuration');
		$db->setQuery($query);
		$this->tomcaturl = $db->loadResult();
	}

}
