<?php
/**
 * @package     Joomla.Site
 * @subpackage  com_mica
 *
 * @copyright   Copyright (C) 2005 - 2017 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

/**
 * Mica Route Helper
 *
 * @since       1.6
 * @deprecated  4.0
 */
class MicaHelperRoute
{
	/**
	 * Method to get the menu items for the component.
	 *
	 * @return  array  	An array of menu items.
	 *
	 * @since       1.6
	 * @deprecated  4.0
	 */
	public static function &getItems()
	{
		static $items;

		// Get the menu items for this component.
		if (!isset($items))
		{
			$app   = JFactory::getApplication();
			$menu  = $app->getMenu();
			$com   = JComponentHelper::getComponent('com_mica');
			$items = $menu->getItems('component_id', $com->id);

			// If no items found, set to empty array.
			if (!$items)
			{
				$items = array();
			}
		}

		return $items;
	}

	/**
	 * Method to get a route configuration for the login view.
	 *
	 * @return  mixed  	Integer menu id on success, null on failure.
	 *
	 * @since       1.6
	 * @deprecated  4.0
	 */
	public static function getLoginRoute()
	{
		// Get the items.
		$items  = self::getItems();
		$itemid = null;

		// Search for a suitable menu id.
		foreach ($items as $item)
		{
			if (isset($item->query['view']) && $item->query['view'] === 'login')
			{
				$itemid = $item->id;
				break;
			}
		}

		return $itemid;
	}

	/**
	 * Method to get a route configuration for the dashboard view.
	 *
	 * @return  mixed  	Integer menu id on success, null on failure.
	 *
	 * @since       1.6
	 * @deprecated  4.0
	 */
	public static function getDashboardRoute()
	{
		// Get the items.
		$items  = self::getItems();
		$itemid = null;

		// Search for a suitable menu id.
		// Menu link can only go to users own dashboard.

		foreach ($items as $item)
		{
			if (isset($item->query['view']) && $item->query['view'] === 'dashboard')
			{
				$itemid = $item->id;
				break;
			}
		}

		return $itemid;
	}

	/**
	 * Method to get a route configuration for the fullmap view.
	 *
	 * @return  mixed  	Integer menu id on success, null on failure.
	 *
	 * @since       1.6
	 * @deprecated  4.0
	 */
	public static function getFullmapRoute()
	{
		// Get the items.
		$items  = self::getItems();
		$itemid = null;

		// Search for a suitable menu id.
		foreach ($items as $item)
		{
			if (isset($item->query['view']) && $item->query['view'] === 'fullmap')
			{
				$itemid = $item->id;
				break;
			}
		}

		return $itemid;
	}

	/**
	 * Method to get a route configuration for the micafront view.
	 *
	 * @return  mixed  	Integer menu id on success, null on failure.
	 *
	 * @since       1.6
	 * @deprecated  4.0
	 */
	public static function getMicafrontRoute()
	{
		// Get the items.
		$items  = self::getItems();
		$itemid = null;

		// Search for a suitable menu id.
		foreach ($items as $item)
		{
			if (isset($item->query['view']) && $item->query['view'] === 'micafront')
			{
				$itemid = $item->id;
				break;
			}
		}

		return $itemid;
	}

	/**
	 * Method to get a route configuration for the showresults view.
	 *
	 * @return  mixed  	Integer menu id on success, null on failure.
	 *
	 * @since       1.6
	 * @deprecated  4.0
	 */
	public static function getShowresultsRoute()
	{
		// Get the items.
		$items  = self::getItems();
		$itemid = null;

		// Search for a suitable menu id.
		foreach ($items as $item)
		{
			if (isset($item->query['view']) && $item->query['view'] === 'showresults')
			{
				$itemid = $item->id;
				break;
			}
		}

		return $itemid;
	}

	/**
	 * Method to get a route configuration for the villagefront view.
	 *
	 * @return  mixed  	Integer menu id on success, null on failure.
	 *
	 * @since       1.6
	 * @deprecated  4.0
	 */
	public static function getVillagefrontRoute()
	{
		// Get the items.
		$items  = self::getItems();
		$itemid = null;

		// Search for a suitable menu id.
		foreach ($items as $item)
		{
			if (isset($item->query['view']) && $item->query['view'] === 'villagefront')
			{
				$itemid = $item->id;
				break;
			}
		}

		return $itemid;
	}

	/**
	 * Method to get a route configuration for the villageshowresults view.
	 *
	 * @return  mixed  	Integer menu id on success, null on failure.
	 *
	 * @since       1.6
	 * @deprecated  4.0
	 */
	public static function getVillageShowresultsRoute()
	{
		// Get the items.
		$items  = self::getItems();
		$itemid = null;

		// Search for a suitable menu id.
		foreach ($items as $item)
		{
			if (isset($item->query['view']) && $item->query['view'] === 'villageshowresults')
			{
				$itemid = $item->id;
				break;
			}
		}

		return $itemid;
	}
}
