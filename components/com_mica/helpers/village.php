<?php
/**
 * @package     Joomla.Site
 * @subpackage  com_mica
 *
 * @copyright   Copyright (C) 2005 - 2017 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

// no direct access
defined('_JEXEC') or die('Restricted access');

jimport('joomla.application.component.helper');

class villageHelper
{
	public static function setThematicQueryData() {
		$db      = JFactory::getDBO();
		$session     = JFactory::getSession();
		
		$activeworkspace = $session->get('activeworkspace');


		$query = "SELECT * FROM ".$db->quoteName('#__mica_sld_legend')."
			WHERE ".$db->quoteName('workspace_id')." = ".$db->quote($activeworkspace)."
			ORDER BY ".$db->quoteName('level').", ".$db->quoteName('range_from')." ASC";
		
		$db->setQuery($query);
		$result = $db->loadObjectList();
		$id    = array();
		$level = array();
		$i     = 1;
		$j     = 1;
		$grouping = array();
		foreach($result as $range){
			if($range->level == "0"){
				$str = "<div class='col_".$i."' style='height:10px;width:10px;background-color:".$range->color.";float:right;'/></div></li>";
			}else{
				$pin = str_replace("#","",$range->color);
				$str = "<div class='col_".$i."' style='float:right;'/><img src='".JUri::base()."components/com_mica/maps/img/layer".$range->level."/pin".$pin.".png' /></div></li>";
			}

			$grouping[$range->custom_formula][] = "<li class='range_".$range->level."' id='".$range->range_from." - ".$range->range_to."'><div style='float:left;width:80%;'>".$range->range_from." - ".$range->range_to."</div>".$str;
			//$id[] = $range->custom_formula;
			//$grouping[$range->custom_formula] = $range->level;
			$level[$range->custom_formula][] = $range->level;
			$i++;
			$j++;
		}


		$i            = 0;
		$range        = array();
		$str          = array();
		$totalcount   = 0;
		$l            = 0;
		$tojavascript = array();
		$grpname = array();
		foreach($grouping as $key => $val){

			$grname         = self::getCustomAttrNameThematic($key, $activeworkspace);
			$grpname[]      = $grname;
			$str[]          = implode("\n",$val);
			$ranges[]       = count($val);
			$levelunique[]  = $level[$key][0];
			$tojavascript[] = $key;
			$l++;
		}

		$tojavascript = implode(",",$tojavascript);
		$html = "";
		for($i = 0; $i < count($grpname); $i++){
			$outputstr.= "<div class='contentblock1 '>
				<div class='blockcontent'>
					<ul class='maingrp bullet-add clear' >
						<li>
							<div class='themehead'>".$grpname[$i][0]."</div>
							<div class='themeedit'>
								<a class='edittheme ".$ranges[$i]."' id='".$grpname[$i][1]."__".$levelunique[$i]."' >
									<img src='".JUri::base()."components/com_mica/images/edit.png' />
								</a>
								<a class='deletegrp ".$levelunique[$i]."' id='del_".$grpname[$i][1]."' >
									&nbsp;&nbsp;&nbsp;<img src='".JUri::base()."components/com_mica/images/delete.png' />
								</a>
							</div>
							<ul class='range' >".$str[$i]."</ul>
						</li>
					</ul>
				</div>
			</div>";
		}


		$output['totalthemecount']     = count($grpname);
		$output['usedlevel']           = implode(",",$levelunique);
		$output['havingthematicquery'] = $tojavascript;

		return $output;
	}

	public static function getCustomAttrNameThematic($name,$activeworkspace) {
		$db    = JFactory::getDBO();
		$query = "SELECT name, attribute FROM ".$db->quoteName('#__mica_user_custom_attribute')."
		WHERE ".$db->quoteName('profile_id')." = ".$db->quote($activeworkspace)."
			AND ".$db->quoteName('attribute')." LIKE ".$db->quote($name);
		$db->setQuery($query);
		$result = $db->loadAssoc();

		if(count($result) == 0){
			return array($name,$name);
		}else{
			return array($result[0],$result[1]);
		}
	}
}
