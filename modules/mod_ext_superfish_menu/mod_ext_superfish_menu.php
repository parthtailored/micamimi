<?php
/*
# ------------------------------------------------------------------------
# Extensions for Joomla 2.5 - Joomla 3.x
# ------------------------------------------------------------------------
# Copyright (C) 2011-2013 Ext-Joom.com. All Rights Reserved.
# @license - PHP files are GNU/GPL V2.
# Author: Ext-Joom.com
# Websites:  http://www.ext-joom.com 
# Date modified: 02/08/2013 - 10:00
# ------------------------------------------------------------------------
*/

// no direct access
defined('_JEXEC') or die;

require_once (JPATH_SITE.'/modules/mod_menu/helper.php'); 

$list		= ModMenuHelper::getList($params);
$base		= ModMenuHelper::getBase($params);
$active		= ModMenuHelper::getActive($params);
$active_id 	= $active->id;
$path		= $base->tree;

$showAll	= $params->get('showAllChildren');
$class_sfx	= htmlspecialchars($params->get('class_sfx'));


$ext_style_menu	= $params->get('stylemenu');
$ext_menu		= $params->get('ext_menu');
$animation		= $params->get('animation');
$delay			= $params->get('delay');
$speed			= $params->get('speed');
$autoarrows		= $params->get('autoarrows');

if(count($list)) {
	require JModuleHelper::getLayoutPath('mod_ext_superfish_menu', $params->get('layout', 'default'));
	echo JText::_(COP_JOOMLA);
}
