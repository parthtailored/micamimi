<?php

#@license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL

defined('_JEXEC') or die;

?>



<div class="flexslider">
  <ul class="slides">
  	<?php
  		foreach($listofimages as $item){
  			echo $item;
  		}
  	?>
  </ul>
</div>

<!-- Amenties Popup -->
<div id="ModalQuestionInquiry" class="modal fade questionInquiry" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <div class="wrap-contact100">
                <div class="contact100-form-title">
                    <span class="contact100-form-title-1">
                        Get Answer Instantly
                    </span>

                    <span class="contact100-form-title-2">
                        Please fill in your information and get answer!
                    </span>
                </div>
                <form class="contact100-form validate-form" name="inquiryForm" id="inquiryForm">
                    <div class="wrap-input100 validate-input" data-validate="Name is required">
                        <span class="label-input100">Full Name:*</span>
                        <input class="input100" type="text" name="name" id="name" placeholder="Enter full name">
                        <span class="focus-input100"></span>
                    </div>

                    <div class="wrap-input100 validate-input" data-validate = "Valid email is required: ex@abc.xyz">
                        <span class="label-input100">Email:*</span>
                        <input class="input100" type="text" name="email" id="email" placeholder="Enter email addess">
                        <span class="focus-input100"></span>
                    </div>

                    <div class="wrap-input100" data-validate="Phone Number is required (Numeric only)">
                        <span class="label-input100">Phone:</span>
                        <input class="input100" type="text" name="phone" id="phone" placeholder="Enter phone number">
                        <span class="focus-input100"></span>
                    </div>

                    <div class="wrap-input100">
                        <span class="label-input100">Message:</span>
                        <textarea class="input100" name="message" id="message" placeholder="Your Comment..."></textarea>
                        <span class="focus-input100"></span>
                    </div>

                    <div class="container-contact100-form-btn">
                        <button class="contact100-form-btn">
                            <span>
                                Submit
                                <i class="fa fa-long-arrow-right m-l-7" aria-hidden="true"></i>
                            </span>
                        </button>
                    </div>
                    <input type=hidden name=imageQues value="">
                    <input type=hidden name=imageAns value="">
                    <input type=hidden name=imageNo value="">
                </form>
            </div>
        </div>
    </div>
</div>
<!-- Amenties Popup -->

<script type="text/javascript" charset="utf-8">
	var imageQues;
	var imageAns;
	var imageNo;
	var is_show_ans = 0;

  (function ($) {
    "use strict";

    /*==================================================================
    [ Focus Contact2 ]*/
    $('.input100').each(function(){
        $(this).on('blur', function(){
            if($(this).val().trim() != "") {
                $(this).addClass('has-val');
            }
            else {
                $(this).removeClass('has-val');
            }
        })
    })

    /*==================================================================
    [ Validate ]*/
    var input = $('.validate-input .input100');

    $('.container-contact100-form-btn').on('click',function(){
        var check = true;

        for(var i=0; i<input.length; i++) {
            if(validate(input[i]) == false){
                showValidate(input[i]);
                check=false;
            }
        }

        if (check == true) {
        	var data = jQuery("#inquiryForm").serialize();
        	imageNo = jQuery("input[name*='imageNo']").val();
        	imageAns = jQuery("input[name*='imageAns']").val();

        	jQuery.ajax({
        		url     : "<?php echo JURI::base() . "index.php?option=com_ajax&module=reslider&method=addInquiry&format=json"?>",
			    method  : 'POST',
			    async   : true,
			    data    : data,
			    beforeSend: function() {
			        jQuery('#main_loader').show();
			   	},
			    success : function(response){
			    	if(response.success == "1"){
                        jQuery('#name').val("");
                        jQuery('#email').val("");
                        jQuery('#phone').val("");
                        jQuery('#message').val("");
                        jQuery('#ModalQuestionInquiry').modal('hide');
				    	jQuery('#myImage'+imageNo).attr('src', imageAns);
				    	jQuery(".imageQues"+imageNo).addClass("imageAns"+imageNo);
						jQuery(".imageQues"+imageNo).removeClass("imageQues"+imageNo);
						jQuery(".viewAnswer"+imageNo).text("Question");
				    	jQuery('#main_loader').hide();
				    	is_show_ans = 1;
                    }
			    }
			});
        }

        return false;
    });


    $('.validate-form .input100').each(function(){
        $(this).focus(function(){
           hideValidate(this);
        });
    });

    function validate (input) {
        if($(input).attr('type') == 'email' || $(input).attr('name') == 'email') {
            if($(input).val().trim().match(/^([a-zA-Z0-9_\-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([a-zA-Z0-9\-]+\.)+))([a-zA-Z]{1,5}|[0-9]{1,3})(\]?)$/) == null) {
                return false;
            }
        }
        else if($(input).attr('type') == 'phone' || $(input).attr('name') == 'phone') {
            if($(input).val().trim().match(/\(?([0-9]{3})\)?([ .-]?)([0-9]{3})\2([0-9]{4})/) == null) {
                return false;
            }
        }
        else {
            if($(input).val().trim() == ''){
                return false;
            }
        }
    }

    function showValidate(input) {
        var thisAlert = $(input).parent();

        $(thisAlert).addClass('alert-validate');
    }

    function hideValidate(input) {
        var thisAlert = $(input).parent();

        $(thisAlert).removeClass('alert-validate');
    }


})(jQuery);

  jQuery(window).load(function() {
    jQuery('.flexslider').flexslider({
       <?php if($params->get('fadeorslide') == 0){?> animation: "slide", <?php } else if ($params->get('fadeorslide') == 1){ ?> animation: "fade", <?php } ?>
    	 <?php if($params->get('directionNav') == 1){?> directionNav: true, <?php } else if ($params->get('directionNav') == 0){ ?> directionNav: false, <?php } ?>
    	 <?php if($params->get('controlNav') == 1){?> controlNav: true, <?php } else if ($params->get('controlNav') == 0){ ?> controlNav:false, <?php } ?>
    	 <?php if($params->get('keyboardNav') == 1){?> keyboardNav: true, <?php } else if ($params->get('keyboardNav') == 0){ ?> keyboardNav:false, <?php } ?>
       <?php if($params->get('direction') == 0){?> direction: "horizontal", <?php } else if ($params->get('direction') == 1){ ?> direction: "vertical", <?php } ?>
       <?php if($params->get('slidespeed')){ echo "slideshowSpeed:".$params->get('slidespeed')."," ;} else { ?> slideshowSpeed: 7000, <?php } ?>
       <?php if($params->get('animatespeed')){ echo "animationSpeed:".$params->get('animatespeed')."," ;} else { ?> animationSpeed: 600, <?php } ?>
       <?php if($params->get('randomorder') == 0){?> randomize: false <?php } else if ($params->get('randomorder') == 1){ ?> randomize: true <?php } ?>
    });
  });

	function displayResult(imageQues, imageAns,imageNo){

		var imageQues = imageQues;
		var imageAns  = imageAns;
		var imageNo   = imageNo;

		if (is_show_ans == 0) {
			is_show_ans  =	"<?php $session = JFactory::getSession(); echo $session->get("is_show_ans"); ?>";
		}

		if (is_show_ans != 1) {
			var imageQues = jQuery("input[name=imageQues]:hidden").val(imageQues);
			var imageAns  = jQuery("input[name=imageAns]:hidden").val(imageAns);
			var imageNo   = jQuery("input[name=imageNo]:hidden").val(imageNo);

			jQuery('#ModalQuestionInquiry').modal('show');
		}else{
			if (jQuery(".imageQues"+imageNo)[0]){
			    jQuery(".imageQues"+imageNo).attr('src', imageAns);
				jQuery(".imageQues"+imageNo).addClass("imageAns"+imageNo);
				jQuery(".imageQues"+imageNo).removeClass("imageQues"+imageNo);
				jQuery(".viewAnswer"+imageNo).text("Question");
			} else {
			    // Do something if class does not exist
				jQuery(".imageAns"+imageNo).attr('src', imageQues);
			    jQuery(".imageAns"+imageNo).addClass("imageQues"+imageNo);
			    jQuery(".imageAns"+imageNo).removeClass("imageAns"+imageNo);
				jQuery(".viewAnswer"+imageNo).text("Answer");
			}
		}
  	}
</script>
