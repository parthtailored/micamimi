<?php
error_reporting( E_ALL ); 
/**
 * @package    Joomla.Site
 *
 * @copyright  Copyright (C) 2005 - 2018 Open Source Matters, Inc. All rights reserved.
 * @license    GNU General Public License version 2 or later; see LICENSE.txt
 */

/**
 * Define the application's minimum supported PHP version as a constant so it can be referenced within the application.
 */
define('JOOMLA_MINIMUM_PHP', '5.3.10');

if (version_compare(PHP_VERSION, JOOMLA_MINIMUM_PHP, '<'))
{
	die('Your host needs to use PHP ' . JOOMLA_MINIMUM_PHP . ' or higher to run this version of Joomla!');
}

/**
 * Constant that is checked in included files to prevent direct access.
 * define() is used in the installation folder rather than "const" to not error for PHP 5.2 and lower
 */
define('_JEXEC', 1);

if (file_exists(__DIR__ . '/defines.php'))
{
	include_once __DIR__ . '/defines.php';
}

if (!defined('_JDEFINES'))
{
	define('JPATH_BASE', __DIR__);
	require_once JPATH_BASE . '/includes/defines.php';
}

require_once JPATH_BASE . '/includes/framework.php';
 
$db = JFactory::getDBO();
 
?>

 

<!-- <!DOCTYPE html>
<html>
<head>
	<link rel="stylesheet" href="bootstrap/bootstrap.min.css" />
	<link rel="stylesheet" href="bootstrap/bootstrap-theme.min.css" />
	<script src="bootstrap/jquery.min.js"></script>
	<script src="bootstrap/bootstrap.min.js"></script>	 
</head>
<body>
	<div class="export-form">
		<div class="container">
			<form action="" method="post">
		  		
		  		<div class="col-sm-12 col-xs-12">
		  			<div class="form-group">
				    	<label for="users">Select users:</label>
			    	    <select multiple class="form-control" id="users" name="users[]">
				      		<option>1</option>
					      	<option>2</option>
					      	<option>3</option>
					      	<option>4</option>
					      	<option>5</option>
					    </select>
					    <span class="notice">To select multiple items, hold CTRL and click</span>
			  		</div>
		  		</div>
			  	
		  		<div class="col-sm-6 col-xs-12">
		  			<div class="form-group">
	  					<label for="from-date">From date:</label>
	  					<input type="date" id="from-date" name="from-date">
		  			</div>
		  		</div>

		  		<div class="col-sm-6 col-xs-12">
		  			<div class="form-group">
	  					<label for="to-date">From date:</label>
	  					<input type="date" id="to-date" name="to-date">
		  			</div>
		  		</div>
 	 			
  			  	
			  	<button type="submit" class="btn btn-default">Export</button>
			</form>
		</div>	
	</div>
 	<?php
 		//echo '<pre>'; print_r($_REQUEST); echo '</pre>';
 	?>
</body>

<script type="text/javascript">
	jQuery(document).ready(function(){
		var today = new Date();
		var dd = today.getDate();
		var mm = today.getMonth()+1; //January is 0!
		var yyyy = today.getFullYear();
		 if(dd<10){
		        dd='0'+dd
		    } 
		    if(mm<10){
		        mm='0'+mm
		    } 

		today = yyyy+'-'+mm+'-'+dd;
		document.getElementById("from-date").setAttribute("max", today);
		document.getElementById("today-date").setAttribute("max", today);
	});
</script>

</html> -->

 

<?php

// Create a new query object.
$query = $db->getQuery(true);

$query = 'SELECT u.email, p.user_id,p.ip, p.visitDate FROM `pqwoe_saxum_iplogger` as p , `pqwoe_users` as u WHERE p.`visitDate` >= "2018-02-01" and p.`visitDate` <= "2018-04-30" and `user_id` in(347,348) and u.id= p.user_id order by `visitDate` ASC';
$db->setQuery($query);
$rows = $db->loadObjectList();

      
include 'phpspreadsheet/spreadsheet/vendor/autoload.php';
  
//include the classes needed to create and write .xlsx file
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

$spreadsheet = new Spreadsheet();  

// Set document properties
$spreadsheet->getProperties()->setCreator('PhpOffice')
        ->setLastModifiedBy('PhpOffice')
        ->setTitle('Office 2007 XLSX Test Document')
        ->setSubject('Office 2007 XLSX Test Document')
        ->setDescription('PhpOffice')
        ->setKeywords('PhpOffice')
        ->setCategory('PhpOffice');

// Add some data
$spreadsheet->setActiveSheetIndex(0)
        ->setCellValue('A1', 'Hello');
// Rename worksheet
$spreadsheet->getActiveSheet()->setTitle('URL Added');

$spreadsheet->createSheet();
// Add some data
$spreadsheet->setActiveSheetIndex(1)
        ->setCellValue('A1', 'world!');
// Rename worksheet
$spreadsheet->getActiveSheet()->setTitle('URL Removed');
// Set active sheet index to the first sheet, so Excel opens this as the first sheet
$spreadsheet->setActiveSheetIndex(0);
// Redirect output to a client’s web browser (Xlsx)
 
$writer = new Xlsx($spreadsheet);
$fxls ="archive-" . date('Ymd') . ".xls";
$writer->save($fxls);
 
header('Content-Type: application/octet-stream');
header('Content-Disposition: attachment; filename='.basename($fxls));
header('Expires: 0');
header('Cache-Control: must-revalidate');
header('Pragma: public');
header('Content-Length: ' . filesize('file.txt'));
readfile($fxls);
exit;
?>