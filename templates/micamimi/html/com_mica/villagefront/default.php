<?php
/**
 * @package     Joomla.Site
 * @subpackage  com_mica
 *
 * @copyright   Copyright (C) 2005 - 2017 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

$doc = JFactory::getDocument();
$app = JFactory::getApplication('site');
$doc->addStyleSheet(JURI::base().'components/com_mica/css/micastylefront.css', 'text/css');
$doc->addStyleSheet(JURI::base().'components/com_mica/js/jquery-ui/css/smoothness/jquery-ui-1.8.18.custom.css', 'text/css');
$doc->addStyleSheet(JURI::base().'components/com_mica/js/jquery-ui-multiselect-checkbox/jquery-ui.css', 'text/css');
$doc->addStyleSheet(JURI::base().'components/com_mica/js/jquery-ui-multiselect-checkbox/prettify.css', 'text/css');
$doc->addStyleSheet(JURI::base().'components/com_mica/css/micastyle.css', 'text/css');
$doc->addStyleSheet(JURI::base().'components/com_mica/css/jquery.jscrollpane.css', 'text/css');
// For Vertical Slider
$doc->addStyleSheet(JURI::base().'components/com_mica/js/slick/slick.css', 'text/css');
$itemid        = 188;
$type1         = "";//$app->input->get('villagem_type', '', 'raw');
$m_type_rating = $app->input->get("village_m_type_rating", '', 'raw');


//$composite     = $app->input->get("village_composite", '', 'raw');
?>
<script type="text/javascript">
	var siteurl ='<?php echo JURI::base();?>';
</script>

<script src="<?php echo JURI::base()?>components/com_mica/js/newjs/jquery-ui.min.js"></script>
<script src="<?php echo JURI::base()?>components/com_mica/js/jquery-ui/js/jquery-ui-1.8.18.custom.min.js"></script>
<script src="<?php echo JURI::base()?>components/com_mica/js/jquery-ui-multiselect-checkbox/jquery.multiselect.js"></script>
<script src="<?php echo JURI::base()?>components/com_mica/js/jquery-ui-multiselect-checkbox/prettify.js"></script>

<!-- Start Chart JS -->
<script src="<?php echo JURI::base()?>components/com_mica/js/Chart.bundle.js"></script>
<script src="<?php echo JURI::base()?>components/com_mica/js/utils.js"></script>
<!-- End Chart JS -->
<!-- For Vertical Slider -->
<script src="<?php echo JURI::base()?>components/com_mica/js/slick/slick.min.js"></script>

<script type="text/javascript">

	var JQuery = jQuery.noConflict();
	var $      = jQuery.noConflict();
	jQuery(document).ready(function($) {
		jQuery('#micaform').submit(function(event) {
			var tempVar = JSON.stringify(jQuery('#villages_villages').val());
			jQuery('#tempvillagejson').val(tempVar);
			jQuery('#villages_villages').attr('disabled', 'disabled');
		});
	});
</script>
<!-- dowlode pdf -->
<script src="<?php echo JURI::base()?>components/com_mica/js/jspdf.min.js"></script>
<script src="<?php echo JURI::base()?>components/com_mica/js/canvg.min.js"></script>
<script src="<?php echo JURI::base()?>components/com_mica/js/villagefield.js"></script>
<script src="<?php echo JURI::base()?>components/com_mica/maps/OpenLayers.js"></script>
<script src="<?php echo JURI::base()?>components/com_mica/js/mvillage.js"></script>
<script src="<?php echo JURI::base()?>components/com_mica/js/slick/slick.min.js"></script>
<script src="<?php echo JURI::base()?>components/com_mica/js/speed/jquery.speedometer.js"></script>
<script src="<?php echo JURI::base()?>components/com_mica/js/speed/jquery.jqcanvas-modified.js"></script>
<script src="<?php echo JURI::base()?>components/com_mica/js/speed/excanvas-modified.js"></script>
<script src="<?php echo JURI::base()?>components/com_mica/js/colorpicker.js"></script>
<script src="<?php echo JURI::base()?>components/com_mica/js/raphael.js"></script>
<style type="text/css">
canvas {
    -moz-user-select: none;
    -webkit-user-select: none;
    -ms-user-select: none;
}

.btn-disabled,
	.btn-disabled[disabled] {
	cursor: default !important;
	pointer-events: none;
	}

#maplegends{
	clear: both;
    position: absolute;
    top: 110px;
    right: 50px;
    background: white;
    z-index: 99;
    padding: 10px;
    min-width: 300px;
}
#featurePopup_contentDiv{
	overflow: scroll;
}
/* font-size: 24px !important; */
#featurePopup{
	/*width: 700px !important;*/
}
</style>
<script type="text/javascript">
	var JQuery            = jQuery.noConflict();
	var $                 = jQuery.noConflict();
	var statealert        = '<?php echo JText::_("SELECT_STATE_ALERT")?>';
	var districtalert     = '<?php echo JText::_("SELECT_DISTRICT_ALERT")?>';
	var subdistrictalert  = '<?php echo JText::_("Please Select Sub-district first!!!")?>';
	var villagealert      = '<?php echo JText::_("Please Select Village first!!!")?>';
	var variablealert     = '<?php echo JText::_("SELECT_VARIABLE_ALERT")?>';
	var metervariabel     = '<?php echo JText::_("Please Select Variabel first!!!")?>';
	var meterdistrict     = '<?php echo JText::_("Please Select District first!!!")?>';
	var preselected       = '<?php echo $app->input->get("villagedistrict", "", "raw"); ?>';
	var preselecteddata   = '<?php echo $app->input->get("selected", "", "raw"); ?>';
	var preselectedm_type = '<?php echo $app->input->get("villagem_type", "", "raw"); ?>';
	var chart1;
	var keys              = [];
	var trashedLabels     = [];
	var trashedData       = [];
	var datasetValues     = [];
	var validation_flag   = false;

	x = 0;  //horizontal coord
	y = document.height; //vertical coord
	window.scroll(x,y);
	JQuery(function() {
		JQuery("#tabs").tabs().addClass('ui-tabs-vertical ui-helper-clearfix');
		JQuery("#tabs li").removeClass('ui-corner-top').addClass('ui-corner-left');
	});
	JQuery(document).ready(function(){ });

	function checkAll(classname){
		JQuery("."+classname).each(function (){
			JQuery(this).attr("checked",true);
		});
	}

	function uncheckall(classname){
		JQuery("."+classname).each(function (){
			JQuery(this).attr("checked",false);
		});
	}
</script>

<script src="templates/micamimi/js/jquery.scrollbar.js"></script>

<script type="text/javascript">
    jQuery(document).ready(function(){
        jQuery('.scrollbar-inner').scrollbar();
    });
</script>


<div class="row-fluid top-row">
	<div class="col-md-4">
		<div class="sidebar-toggle-box" title="Fillscreen">
        	<i class="fa fa-bars" aria-hidden="true"></i>
      	</div>
		<h1 class="title-explore"><?php echo JText::_('Village Data');?></h1>
	</div>
	<div class="col-md-8 text-right">
		<div class="sorter-tab">
			<ul>
				<li><a href="javascript:void(0);" id="tabledata" class="actionbtn btn-disabled" disabled="disabled"><i class="fas fa-table"></i><?php echo JText::_('TEXT_TAB_LABEL'); ?></a></li>
				<li><a href="javascript:void(0);" id="graphdata" class="actionbtn btn-disabled" disabled="disabled"><i class="far fa-chart-bar"></i><?php echo JText::_('GRAPHS_TAB_LABEL'); ?></a></li>

				 <li><a href="javascript:void(0);" id="gisdata" class="actionbtn btn-disabled" disabled="disabled"><i class="fas fa-map-marker-alt"></i><?php echo JText::_('GIS_TAB_LABEL'); ?></a></li>
				<!--<li><a href="javascript:void(0);" id="matrix" class="actionbtn btn-disabled" disabled="disabled"><i class="fas fa-list-ul"></i><?php echo JText::_('Matrix'); ?></a></li>

				<li><a href="javascript:void(0);" id="pmdata" class="actionbtn btn-disabled" disabled="disabled"><i class="fas fa-tachometer-alt"></i><?php echo JText::_('Speedometer'); ?></a></li> -->

				<!-- <li><a href="javascript:void(0);" id="tabledata" class="actionbtn btn-disabled" disabled="disabled" onclick="getDataNew();"><i class="fas fa-table"></i><?php echo JText::_('TEXT_TAB_LABEL'); ?></a></li>
				<li><a href="javascript:void(0);" id="graphdata" class="actionbtn btn-disabled" disabled="disabled" onclick="loadCharts(null, null, null, 1);"><i class="far fa-chart-bar"></i><?php echo JText::_('GRAPHS_TAB_LABEL'); ?></a></li>
				<li><a href="javascript:void(0);" id="gisdata" class="actionbtn btn-disabled" disabled="disabled"><i class="fas fa-map-marker-alt"></i><?php echo JText::_('GIS_TAB_LABEL'); ?></a></li>
				<li><a href="javascript:void(0);" id="matrix" class="actionbtn btn-disabled" disabled="disabled"><i class="fas fa-list-ul"></i><?php echo JText::_('Matrix'); ?></a></li>
				<li><a href="javascript:void(0);" id="pmdata" class="actionbtn btn-disabled" disabled="disabled"><i class="fas fa-tachometer-alt"></i><?php echo JText::_('Speedometer'); ?></a></li> -->
			</ul>
		</div>

		<div class="worspacce-edit">
			My workspace <a class="workspacce-edit" href="javascript:void(0)" onclick="document.getElementById('workspacceedit').style.display='block';document.getElementById('fade').style.display='block'">
				<i class="fas fa-pencil-alt"></i>
			</a>
			<h2>
				<div id="activeworkspacename"></div>
			</h2>
		</div>
	</div>
</div>

<div id="workspacceedit" style="display:none;" class="white_content2 large-popup">
	<div class="divclose">
		<a href="javascript:void(0);" onclick="document.getElementById('workspacceedit').style.display='none';document.getElementById('fade').style.display='none';">
			<img src="media/system/images/closebox.jpeg" alt="X">
		</a>
	</div>
	<div class="poptitle">
		<?php echo JTEXT::_('MY_WORKSPACE');?>
	</div>

	<div class="scroll_div">
		<div class="col-md-8">
			<div class="blockcontent">
				<ul class="allworkspace">
					<?php
						$this->user = JFactory::getUser();
						$id = $this->user->id;

						if($id == 0){
							return -1;
						}

						$db   = JFactory::getDBO();
						$query="SELECT name, id, is_default
							FROM ".$db->quoteName('#__mica_user_workspace')."
							WHERE ".$db->quoteName('userid')." = ".$db->quote($id)."
								AND ".$db->quoteName('is_default')." <> ".$db->quote(1). "AND" . $db->quoteName('data') . " REGEXP '.*\"dataof\";s:[0-9]+:\"villages\".*'";
						$db->setQuery($query);
						$result = $db->loadAssocList();

						$app               = JFactory::getApplication('site');
						$session = JFactory::getSession();
						$str               = "";

						//$activeworkspace   = $app->input->get('activeworkspace', '', 'raw');
						$activeworkspace   = $session->get('activeworkspace');
						$selected          = "";
						$activeprofilename = $eachresult['name'];
						$i                 = 1;
						foreach($result as $eachresult){
							if(($i%2)==0){
								$clear="clear";
							}else{
								$clear="";
							}

							if($activeworkspace == $eachresult['id']){

								$selected          = "selected";
								$edit              = "Edit/Delete";
								$activeprofilename = $eachresult['name'];

								echo "<script type='text/javascript'>JQuery('#activeworkspacename').html('<font style=\'font-weight:normal;\'> ".$activeprofilename."</font>  ');</script>";
								$onclick = "document.getElementById('lightn').style.display='block';document.getElementById('fade').style.display='block'";
								echo "<input type='hidden' value='".$eachresult['id']."' id='profile' />";
								$active_text = "<span style='font-weight:normal;'>(Active)<span>";
								$active_text = "";

							}else{

								$selected    = "";
								$edit        = "Select";
								$onclick     = "changeWorkspace(".$eachresult['id'].")";
								$active_text = "";
							}

							$str .= '
								<li class="'.$clear." ".$selected.'">
									<div style="width:70%;float:left;">
										<label class="workspacelabel">'.$eachresult['name'].$active_text.'</label>
									</div>
									<div style="width:30%;float:right;">
										<a href="#" onclick="'.$onclick.'">'.$edit.'</a>
									</div>
								</li>';// '.$selected.'
						}
						echo $str;
						echo '<div id="lightn" class="white_contentn" style="display: none;">
								<div class="divclose">
									<a href="javascript:void(0);" onclick="document.getElementById(\'lightn\').style.display=\'none\';document.getElementById(\'fade\').style.display=\'none\';">
										<img src="media/system/images/closebox.jpeg" alt="X" />
									</a>
								</div>
								<div align="left">
									<input type="textbox" id="updateworkspacetext" value="'.$activeprofilename.'" class="inputbox"/>
									<div class="frontbutton readon">
										<input type="button" id="updateworkspace" onClick="JQuery(\'#new_w_txt\').val(JQuery(\'#updateworkspacetext\').val())" Value="Update" class="frontbutton readon"/>
									</div>
									<div class="frontbutton">
										<input type="button" name="new"  id="deleteworkspace"  value="Delete" class=""/>
									</div>
								</div>';
					?>
				</ul>
			</div>
		</div>

		<div class="col-md-4">
			<div class="contentblock endcontent">
				<div class="contenthead">New Workspace</div>
				<div class="blockcontent newworkspace">
					<input type="text"  id="new_w_txt" class="inputbox" value="<?php echo $activeprofilename;?>"  class="newtextbox"/>
					<input type="button" name="Create" class="newbutton frontbutton" value="Create" id="createworkspace" />
				</div>
			</div>
		</div>
	</div>
</div>

<div class="explore-data">
	<form name="adminForm" id="micaform" action="" method="POST">
		<div class="row">
			<div class="col-md-4 left-section">
				<ul class="nav nav-tabs" role="tablist">
		            <li role="presentation" class="active" id="state_tab" title="State">
		            	<a href="#state" aria-controls="state" role="tab" data-toggle="tab">
		            		<em class="icon-state"></em>State
		            	</a>
		            </li>

		            <li role="presentation" title="District">
		            	<a href="#district" aria-controls="district" role="tab" data-toggle="tab" id="district_tab">
		            		<em class="icon-district"></em>District
		            	</a>
		            </li>

		            <li role="presentation" title="Sub District">
		            	<a href="#sub_district" aria-controls="sub_district" role="tab" data-toggle="tab" id="sub_district_tab">
		            		<em class="icon-district"></em>Sub District
		            	</a>
		            </li>

		            <li role="presentation" title="Village">
		            	<a href="#village" aria-controls="village" role="tab" data-toggle="tab" id="village_tab">
		            		<em class="icon-type"></em>Village
		            	</a>
		            </li>

		            <li role="presentation" title="Variables">
		            	<a href="#variables" aria-controls="variables" role="tab" data-toggle="tab" id="variables_tab">
		            		<em class="icon-variables"></em>Variables
		            	</a>
		            </li>
		        </ul>


				<!-- START Tab panes -->
		        <div class="tab-content leftcontainer" id="leftcontainer">
		        	<!-- START State -->
		        	<div role="tabpanel" class="tab-pane active" id="state">
		        		<div class="loader-div" id="loader" style="display: none">
					    	<img src="images/loader.gif" style="position: relative;top: 50%;margin-top: -24px;">
						</div>
						<div id="statespan">
			        		<div class="top-sorter-div">
		                		<div class="row">
		                			<div class="col-md-5 col-xs-5">
		                				<h3><?php echo JText::_('STATE_LABEL'); ?></h3>
		                			</div>
		                			<div class="col-md-7 col-xs-7 text-right">
		                				<div class="searchbox">
		                					<input type="search" class="searchtop" id="state_searchtop" placeholder="Search">
		                				</div>
		                				<!-- |
		                				<input type="checkbox" name="state_checkall" class="allcheck" id="state_allcheck">
		                				<label for="allcheck"></label> -->
		                				|
		                				<a href="javascript:void(0);" onclick="sortListDir('id01')">
												<i class="fa fa-sort"></i>
											</a>
		                			</div>
		                		</div>
		                	</div>
		                </div>
	                	<div class="scrollbar-inner">
                            <ul class="list1 statelist" id="id01">
                            	<?php
								$states = explode(",", $app->input->get("villagestate", '', 'raw'));

									for($i = 0; $i < count($this->state_items); $i++){
										$checked = (in_array($this->state_items[$i]->name, $states)) ? " checked " :  ""; ?>
										<li>
											<input type="radio" class="state_checkbox" name="villagestate[]" value="<?php echo base64_encode($this->state_items[$i]->id); ?>" <?php echo $checked;?> id="check<?php echo $i;?>">
											<label for="check<?php echo $i;?>"><?php echo $this->state_items[$i]->name; ?></label>
										</li>
								<?php  } ?>
							</ul>
                    	</div>
		        	</div>
		        	<!-- END State -->

					<!-- START District -->
		        	<div role="tabpanel" class="tab-pane" id="district">
		        		<div id="districtspan">
			        		<div class="top-sorter-div">
		                		<div class="row">
		                			<div class="col-md-5 col-xs-5">
		                				<h3><?php echo JText::_('DISCTRICT_LABEL'); ?></h3>
		                			</div>
		                			<div class="col-md-7 col-xs-7 text-right">
		                				<div class="searchbox">
		                					<input type="search" class="searchtop" id="district_searchtop" placeholder="Search">
		                				</div>
		                				|


		                				<a href="javascript:void(0);" onclick="district_sortListDir()">
		                					<i class="fa fa-sort"></i>
		                				</a>
		                			</div>
		                		</div>
		                	</div>
		                	<div class="row">
	                			<div class="col-md-10">
	                				<div class="scrollbar-inner">
			                            <ul class="list1 districtlist" onchange="getSubDistrict(this.value)">
										</ul>
									</div>
	                			</div>
	                			<div class="col-md-2 padding-none">
	                				<div class="dist-icon-list">
	                					<div id="statecode">
	                					</div>
	                				</div>
	                			</div>
	                		</div>
		                </div>
		        	</div>
		        	<!-- END District -->

		        	<!-- START Sub District -->
		        	<div role="tabpanel" class="tab-pane" id="sub_district">
		        		<div id="subdistrictspan">
			        		<div class="top-sorter-div">
		                		<div class="row">
		                			<div class="col-md-5 col-xs-5">
		                				<h3><?php echo JText::_('SUB_DISCTRICT_LABEL'); ?></h3>
		                			</div>
		                			<div class="col-md-7 col-xs-7 text-right">
		                				<div class="searchbox">
		                					<input type="search" class="searchtop" id="subdistrict_searchtop" placeholder="Search">
		                				</div>
		                				|
		                				<a href="javascript:void(0);" onclick="subdistrict_sortListDir()">
		                					<i class="fa fa-sort"></i>
		                				</a>
		                			</div>
		                		</div>
		                	</div>
		                	<div class="row">
	                			<div class="col-md-10">
	                				<div class="scrollbar-inner">
			                            <ul class="list1 subdistrictlist" onchange="getdistrictattr(this.value)">
										</ul>
									</div>
	                			</div>
	                			<div class="col-md-2 padding-none">
	                				<div class="dist-icon-list">
	                					<div id="subdistrictcode">
	                					</div>
	                				</div>
	                			</div>
	                		</div>
		                </div>
		        	</div>
		        	<!-- END Sub District -->

					<!-- START Village -->
		        	<div role="tabpanel" class="tab-pane" id="village">
		        		<div id="villagespan">
			        		<div class="top-sorter-div">
		                		<div class="row">
		                			<div class="col-md-5 col-xs-5">
		                				<h3><?php echo JText::_('VILLAGE_LABEL'); ?></h3>
		                			</div>
		                			<div class="col-md-7 col-xs-7 text-right">
		                				<div class="searchbox">
		                					<input type="search" class="searchtop" id="village_searchtop" placeholder="Search">
		                				</div>
		                				|
		                				<input type="checkbox" name="village_allcheck" class="allcheck" id="village_allcheck"  <?php echo ($session->get('village_allcheck') =='on') ? 'checked="checked"' : ''; ?> >

		                				<label for="allcheck"></label>
		                				|
		                				<a href="javascript:void(0);" onclick="villages_sortListDir()">
		                					<i class="fa fa-sort"></i>
		                				</a>
		                			</div>
		                		</div>
		                	</div>
		                	<div class="scrollbar-inner">
		                		<ul class="list1 villagelist" id="villageList">
								</ul>
		                	</div>
		                </div>
		        	</div>
		        	<!-- END Village -->

					<!-- START Variable -->
		        	<div role="tabpanel" class="tab-pane" id="variables">
		        		<div id="variablespan">
			        		<div class="top-sorter-div">
		                		<div class="row">
		                			<div class="col-md-5 col-xs-5">
		                				<h3><?php echo JText::_('Select Variable'); ?></h3>
		                			</div>
		                			<div class="col-md-7 col-xs-7 text-right">
		                				<div class="searchbox">
		                					<input type="search" class="searchtop" id="variable_searchtop" placeholder="Search">
		                				</div>
		                				|
		                				<input type="checkbox" name="variable_allcheck" class="allcheck" id="variable_allcheck">
		                				<label for="allcheck"></label>
		                				|
		                					<a href="#sort" onclick="variable_sortListDir()">
		                					<i class="fa fa-sort"></i>
		                				</a>
		                			</div>
		                		</div>
		                	</div>

	                	<div class="row">
							<div class="col-md-10">
								<div class="scrollbar-inner">
									<div id="statetotal"></div>
	        						<div id="variables"></div>
								</div>
							</div>
							<div class="col-md-2 padding-none">
								<div id="variableshortcode"></div>
							</div>
						</div>
						 </div>
		        	</div>
		        	<!-- END Variable -->
		        </div>
		        <!-- END Tab panes -->
			</div>

			<div class="col-md-8 right-section">
				<div class="full-data-view">
					<div id="result_table" style="display:none">
						<div class="text contenttoggle1" id="textscroll">
							<div class="button-area text-right">
								<a href="#alldata" class="download_excel"  onclick="downloadAction();"><i class="fas fa-download"></i><?php echo JText::_('DOWNLOAD_XLS_LABEL'); ?>
								</a>
							</div>

							<div class="collpsable1">
							</div>

							<div class="activecontent right-scroll-div tableview" >
								<div class="alldata contenttoggle2" id="jsscrollss alldata">
								</div>
								<?php echo "<br /><br />"; ?>
							</div>
						</div>
					</div>

					<!--Start GRAPH -->
					<div id="graph" style="display:none;">
						<div class="graphs contenttoggle1">
							<div class="row">
								<div class="col-md-4">
									<div class="chart_type">
										<label  style="display:none;">Chart Type:</label>
										<select name="chartype" class="inputbox" id="chartype" onchange="loadCharts(null, null, null, 1);"  style="display:none;">
											<option value="bar" selected>Bar Chart</option>
											<option value="line">line Chart</option>
											<option value="radar">Radar Chart</option>
										</select>

										<label>Show Legend</label>
										<input type="checkbox" id="enablelegend" name="enablelegend"  value="1" checked/>
									</div>
								</div>
								<div class="col-md-5">
										</div>
								<div class="col-md-3 graphpopup">
									<div id="graphfunction" class="white_content2 large-popup variable_add_popup" style="display: none;">
										<div class="frontbutton btn-create">
											<input type="button" name="Cancel" id="Clear" class="" value="Clear">
											<input type="button" name="apply_changes" id="graph_apply_changes" class="" value="Apply">
										</div>
											<div class="divclose">
												<a id="closeextra" href="javascript:void(0);" onclick="document.getElementById('graphfunction').style.display='none';document.getElementById('fade').style.display='none';">
													<img src="media/system/images/closebox.jpeg" alt="X" />
												</a>
											</div>
											<div class="graphFilter">
												<ul class="nav">
													<li id="graphDistrict" class="active"><a data-toggle="tab" href="#graphDistrictTab" class="getmanagedata">Villages</a></li>
													<li id="graphVariables"><a data-toggle="tab" href="#graphVariablesTab" >Variables</a></li>

												</ul>
												<div class="tab-content">
												    <div id="graphDistrictTab" class="tab-pane fade in active">
												    	<div class="poptitle">
															Select Village
														</div>
														<div id="cblist"></div>

												    </div>

												    <div id="graphVariablesTab" class="tab-pane fade">
												    	<div class="poptitle">
															Select Variable
														</div>
												     	<div id="variabelist"></div>
												    </div>
												</div>
											</div>
										</div>
									<div class="button-area-2">
										<div id="filterChartButton" class="exportbtn">
												<input type="button" id="graphfunction" onClick="document.getElementById('graphfunction').style.display='block';document.getElementById('fade').style.display='block'"  class="button btn-red" value="Filter">
											</div>
										<div id="exportchartbutton" class="exportbtn">
											<input type="button"  name="downloadchart" id="export" class="button" value="Export" />
										</div>
									</div>
								</div>
							</div>
							<canvas id="myChart" style="width:100%; height:520px; background-color:#FFFFFF"></canvas>
							<!-- end of amcharts script -->
						</div>
					</div>
					<!-- END GRAPH -->

					<!--Start GIS -->
					<div id="gis" style="display:none">

						<!-- <h1>GIS Villages</h1> -->
						<div class="gis contenttoggle1">
							<div class="text contenttoggle1">
								<div class="button-area text-right">
									<a href="javascript:void(0);" onclick="downloadMapPdf_v2();" id="options" class="frontbutton" style="text-decoration: none;">
									<input type="button" class="frontbutton" name="Export" value="Export">
									</a>
								</div>
								<div class="button-area text-right">
									<?php  $fromthematic = $session->get('fromthematic'); ?>
									<a href="javascript:void(0);" id="fullscreen" class="frontbutton" <?php if($fromthematic==1){ ?> fromthematic="1" <?php }else{?> fromthematic="0"  <?php }?> >Full Screen									
									</a>
								</div>
								<div class="button-area text-right">
									<a href="javascript:void(0);" id="thematic_btn" class="frontbutton" onclick="openthematicQuery()">Thematic Query
										
									</a>
								</div>
								<div class="button-area text-right">
									<a href="javascript:void(0);" id="show_legends_btn" class="frontbutton" onclick="toggleLegends()">Show Legends							
									</a>
								</div>
							</div>
							<div class="map_controllers"></div>
							<div id="map"   style="width: 900px; border: 1px solid #C5E1FC; height:535px; float:left; margin-bottom:25px" ></div>	

							<?php
								function getCustomAttrNameThematic($name,$activeworkspace) {
									$db    = JFactory::getDBO();
									$query = "SELECT name, attribute FROM ".$db->quoteName('#__mica_user_custom_attribute')."
									WHERE ".$db->quoteName('profile_id')." = ".$db->quote($activeworkspace)."
										AND ".$db->quoteName('attribute')." LIKE ".$db->quote($name);
									$db->setQuery($query);
									$result = $db->loadAssoc();

									if(count($result) == 0){
										return array($name,$name);
									}else{
										return array($result[0],$result[1]);
									}
								}

								$activeworkspace = $session->get('activeworkspace');
								$query = "SELECT * FROM ".$db->quoteName('#__mica_sld_legend')."
									WHERE ".$db->quoteName('workspace_id')." = ".$db->quote($activeworkspace)."
									ORDER BY ".$db->quoteName('level').", ".$db->quoteName('range_from')." ASC";
								$db->setQuery($query);
								$result = $db->loadObjectList();				
								$id    = array();
								$level = array();
								$i     = 1;
								$j     = 1;
								$grouping = array();
								$groupingtable = array();

								foreach($result as $range){
									if($range->level == "0"){
										$str = " <div class='col_".$i."' style='height:10px;width:10px;background-color:".$range->color.";float:right;'></div></li>";
										$strtable = "<div class='col_".$i."' style='height:10px;width:10px;background-color:".$range->color.";float:right;'></div>";
										
									}else{
										$pin = str_replace("#","",$range->color);
										$str = " <div class='col_".$i."' style='float:right;'><img src='".JUri::base()."components/com_mica/maps/img/layer".$range->level."/pin".$pin.".png' /></div></li>";
										$strtable ="<img src='".JUri::base()."components/com_mica/maps/img/layer".$range->level."/pin".$pin.".png'  style='max-width:30px' />";
									}

									$grouping[$range->custom_formula][] = "<li class='range_".$range->level."' id='".$range->range_from." - ".$range->range_to."'><div style='float:left;width:80%;'>".$range->range_from." - ".$range->range_to."</div>".$str;
									$groupingtable[$range->custom_formula][] = "<tr><td class='range_".$range->level."' id='".$range->range_from." - ".$range->range_to."'>
									".$range->range_from." - ".$range->range_to."</td><td>".$strtable."</td></tr>";
									//$id[] = $range->custom_formula;
									//$grouping[$range->custom_formula] = $range->level;
									$level[$range->custom_formula][] = $range->level;
									$i++;
									$j++;
								}

								$i            = 0;
								$range        = array();
								$str          = array();
								$strtable          = array();
								$totalcount   = 0;
								$l            = 0;
								$tojavascript = array();
								$grpname = array();

								foreach($grouping as $key => $val){
									$grname         = getCustomAttrNameThematic($key, $activeworkspace);
									$grpname[]      = $grname;
									$str[]          = implode(" ",$val);
									$strtable[]     = implode(" ",$groupingtable[$key]);
									$ranges[]       = count($val);
									$levelunique[]  = $level[$key][0];
									$tojavascript[] = $key;
									$l++;
								}
								$tojavascript = implode(",",$tojavascript);
								$html = "";
								$legendsDiv = '';
								$legendsShort='';
								//echo "<pre />";print_r($str);exit;
								for($i = 0; $i < count($grpname); $i++){
									$legendsDiv .= "<div class='contentblock1 '>
										<div class='blockcontent'>
											<ul class='maingrp bullet-add clear' >
												<li>
													<div class='themehead'>".$grpname[$i][0]."</div>
													<div class='themeedit'>
														<a class='edittheme ".$ranges[$i]."' id='".$grpname[$i][1]."__".$levelunique[$i]."' >
															<img src='".JUri::base()."components/com_mica/images/edit.png' />
														</a>
														<a class='deletegrp ".$levelunique[$i]."' id='del_".$grpname[$i][1]."' >
															&nbsp;&nbsp;&nbsp;<img src='".JUri::base()."components/com_mica/images/delete.png' />
														</a>
													</div>
													<ul class='range' >".$str[$i]."</ul>
												</li>
											</ul>
										</div>
									</div>";

									$legendsShort.="<tr><td colspan='2' class='themehead'>".$grpname[$i][0]."</td></tr>".$strtable[$i];
								}
								$legendsShort = '<table width="90%">'.$legendsShort."</table>";
								?>
							<div id="maplegends" style="display:none;clear:both"><?php echo $legendsShort;?></div>
						</div>
							

					</div>
					<!--Start GIS -->

					<!-- Start Matrix -->
					<div id="quartiles" class="matrix contenttoggle1">
						<div id="fullscreentable"></div>
					</div>
					<!-- End Matrix -->

					<!--Start Speed -->
					<div id="potentiometer" style="display:none;">
						<div class="speedometer contenttoggle1" >
							<div class="filter-section">
								<div style="display:none;" id="speedfiltershow"></div>
								<div class="sfilter">
									<div class="top-filter-sec">
										<div class="fl_title">
											<span class="filterbylabel"></span>
										</div>
										<div class="fl_type radio-list fl_right">
											<!-- start popup for potentiometer Filters-->
											<div id="variablefiltetr" style="display:none;" class="white_content2 large-popup">
												<div class="divclose">
													<a href="javascript:void(0);" data-me="variablefiltetr" class="closeme1">
													<img src="media/system/images/closebox.jpeg" alt="X">
													</a>
												</div>
												<div class="frontbutton btn-create">
													<input type="button" name="showspeed" id="showspeed" class="" value="Create">
												</div>
												<div class="filterbylabel"></div>
												<div class="sfilter">
													<div>
														<div class="lft speed" id="potentiolMeter" style="display:none;"></div>
													</div>
												</div>
											</div>
											<!-- end popup for potentiometer Filters-->
											<ul>
												<li>
													<a href="javascript:void(0);" id="filter1" data-val="0"  class="filterspeed">Filter By  Layer</a>
												</li>
												<li>
													<a href="javascript:void(0);" id="filter2"  data-val="1"  class="filterspeed">Filter By  Variable</a>
												</li>
											</ul>
										</div>
									</div>
								</div>
							</div>
							<div id="spedometer_region" class="right-scroll-div spedometer_sec" style="width: 915px; overflow: auto;"></div>
						</div>
					</div>
					<!--End Speed -->

					<!-- START Right End Side Steps -->
					<div id="default3" class="default3 contenttoggle1">
						<p>Step 1 : Select State</p>
						<p>Step 2 : Select District of selected State</p>
						<p>Step 3 : Select Sub districts of selected State</p>
						<p>Step 4 : Select Villages</p>
						<p>Step 5 : Select Variables</p>
						<div align="left">
							 <a href="javascript:void(0);" id="submitta" style="display:none;" class="actionbtn btn-submit" disabled="disabled" onclick="getDataNew();"><i class="fas fa-table"></i>Apply</a>
						</div>
					</div>
					<!-- START Right End Side Steps -->

					<!--START default -->
					<div id="default" class="white_content2 alert-variable" >
						<div class="popup_alert">
							<div class="default contenttoggle1" >
								<div class="divclose">
									<a href="javascript:void(0);" onclick="document.getElementById('default').style.display='none';document.getElementById('fade').style.display='none';">
										<img src="media/system/images/closebox.jpeg" alt="X">
									</a>
								</div>
								<div class="box-white">
									<p>Please select variables from each of the following dimensions to view a report. You can select from left panel or by clicking the links above.</p>
								</div>
								<ul class="list1">
									<li id="statetext"><i class="far fa-check-square"></i>State</li>
									<li id="districttext"><i class="far fa-check-square"></i>District</li>

									<li id="villageText"><i class="far fa-check-square"></i>Village</li>
									<li id="variabletext"><i class="far fa-check-square"></i>Variable</li>
								</ul>

							    <div align="right">
							    	<a href="javascript:void(0);" id="applyChangesInitial" class="actionbtn btn-disabled btn-submit" disabled="disabled" onclick="getDataNew();"><i class="fas fa-table"></i>Apply</a>
							    </div>
							</div>
						</div>
					</div>
					<!--END default -->

					<!--start default -->
					<div id="default1" class="white_content2 alert-variable" style="display:none;">
						<div class="popup_alert">
							<div class="divclose">
								<a href="javascript:void(0);" onclick="document.getElementById('default1').style.display='none';document.getElementById('fade').style.display='none';">
									<img src="media/system/images/closebox.jpeg" alt="X">
								</a>
							</div>
							<div class="box-white">
								<p>Selections have been modified. Click on "Apply" at any time to refresh the report with the changes made. Otherwise, click on "Cancel" to go back to previous selections.</p>
							</div>
							<div align="right">
							 	<a href="javascript:void(0);" id="applyChanges" class="actionbtn btn-disabled btn-submit" disabled="disabled"><i class="fas fa-table"></i>Apply</a>
							</div>
						</div>
					</div>
					<!-- END Right End Side Steps -->
				</div>
			</div>

			<div class="maintable" style="float:right;width:50%; margin:10px 0px 0px 0px;">
				<input type="hidden" name="refeterview" value="villagefront" />
				<input type="hidden" name="option" 		value="com_mica" />
				<input type="hidden" name="zoom" id="zoom" value="6" />
				<input type="hidden" name="view" 		value="villageshowresults" />
				<input type="hidden" name="Itemid" 		value="<?php echo $itemid;?>" />
				<input type="hidden" id="comparedata" 	value="0" />
				<input type="hidden" name="tempvillagejson" id="tempvillagejson" value="ronak"/>
				<input type="hidden" id="viewRecordPlanLimit" value="0" />
				<input type="hidden" id="downloadAllowedPlanLimit" value="0" />
			</div>

			<!-- custom variable calcultor start-->
			<div id="light" class="white_content2 large-popup" style="display: none;">
				<div class="divclose">
					<a id="closeextra" href="javascript:void(0);" onclick="undofield();document.getElementById('light').style.display='none';document.getElementById('fade').style.display='none';">
						<img src="media/system/images/closebox.jpeg" alt="X" />
					</a>
				</div>

				<script type="text/javascript">
					var lastchar = '';
					function moverightarea(){
						var val      = document.getElementById('custom_attribute').innerHTML;

						var selected = 0;
						for(i = document.micaform1.avail_attribute.options.length-1; i >= 0; i--){
							var avail_attribute = document.micaform1.avail_attribute;
							if(document.micaform1.avail_attribute[i].selected){
								selected = 1;
								if(lastchar=='attr'){
									alert("<?php echo JText::_('CONSECUTIVE_ATTR_ALERT');?>");
								}else{
									document.getElementById('custom_attribute').innerHTML = val+' '+document.micaform1.avail_attribute[i].value+' ';
									lastchar = 'attr';
								}
							}
						}
						if(selected==0){
							alert("<?php echo JText::_('NOT_SELECTED_COMBO_ALERT');?>");
						}
					}
					$("#copyBtn").click(function(){
					    var selected = $("#selectBox").val();
					    $("#output").append("\n * " + selected);
					});

					function addplussign(){
						var val = document.getElementById('custom_attribute').innerHTML;
						if(lastchar=='opr'){
							alert("<?php echo JText::_('CONSECUTIVE_OPR_ALERT');?>");
						}else{
							document.getElementById('custom_attribute').innerHTML = val+"+";
							lastchar = 'opr';
						}
					}

					function addminussign(){
						var val = document.getElementById('custom_attribute').innerHTML;
						if(lastchar=='opr'){
							alert("<?php echo JText::_('CONSECUTIVE_OPR_ALERT');?>");
						}else{
							document.getElementById('custom_attribute').innerHTML = val+"-";
							lastchar = 'opr';
						}
					}

					function addmultiplysign(){
						var val = document.getElementById('custom_attribute').innerHTML;
						if(lastchar=='opr'){
							alert("<?php echo JText::_('CONSECUTIVE_OPR_ALERT');?>");
						}else{
							document.getElementById('custom_attribute').innerHTML = val+"*";
							lastchar = 'opr';
						}
					}

					function adddivisionsign(){
						var val = document.getElementById('custom_attribute').innerHTML;
						if(lastchar=='opr'){
							alert("<?php echo JText::_('CONSECUTIVE_OPR_ALERT');?>");
						}else{
							document.getElementById('custom_attribute').innerHTML = val+"/";
							lastchar = 'opr';
						}
					}

					function addLeftBraket(){
						var val = document.getElementById('custom_attribute').innerHTML;
						document.getElementById('custom_attribute').innerHTML = val+"(";
						checkIncompleteFormula();
					}

					function addRightBraket(){
						var val = document.getElementById('custom_attribute').innerHTML;
						document.getElementById('custom_attribute').innerHTML = val+")";
						checkIncompleteFormula();
					}

					function addPersentage(){
						var val = document.getElementById('custom_attribute').innerHTML;
						//if(lastchar=='opr'){
							//alert("<?php echo JText::_('CONSECUTIVE_OPR_ALERT');?>");
						//}else{
							document.getElementById('custom_attribute').innerHTML = val+"%";
							lastchar = 'opr';
						//}
					}

					function addNumeric(){
						var val       = document.getElementById('custom_attribute').value;
						var customval = document.getElementById('custom_numeric').value;
						document.getElementById('custom_attribute').innerHTML = val+customval;
						lastchar = '';
					}

					function checkIncompleteFormula(){
						var str   = document.getElementById('custom_attribute').innerHTML;
						var left  = str.split("(");
						var right = str.split(")");

						if(left.length==right.length){
							document.getElementById('custom_attribute').style.color="black";
						}else{
							document.getElementById('custom_attribute').style.color="red";
						}
					}

					function Click(val1){
						var val = document.getElementById('custom_attribute').innerHTML;
						document.getElementById('custom_attribute').innerHTML = val+val1;
						checkIncompleteFormula();
					}

					function checkfilledvalue(){

						var tmp1 = document.getElementById('custom_attribute').innerHTML;
						var tmp2 = document.getElementById('new_name').value;
						var flg  = 0;
						var len1 = parseInt(tmp1.length) - 1;
						var len2 = parseInt(tmp2.length) - 1;
						if(tmp1.charAt(len1) == "%" || tmp2.indexOf(len2) == "%" ) {
							alert("<?php echo JText::_('CUSTOM_ATTR_BLANK_ALERT');?>");
						}else if(lastchar=='opr' && flg == 0){
							alert("<?php echo JText::_('LAST_CHAR_OPR_ALERT');?>");
						}

						if(document.getElementById('custom_attribute').innerHTML=='' || document.getElementById('new_name').value==''){

						}else{
							if(!validateFormula_v2()){
								return false;
							}
							addCustomAttr(document.getElementById('new_name').value,encodeURIComponent(document.getElementById('custom_attribute').innerHTML));
						}
					}

					function addCustomAttr(attrname,attributevale){

								JQuery.ajax({
								url     : "index.php?option=com_mica&task=showresults.AddCustomAttr&attrname="+attrname+"&attributevale="+attributevale,
								method  : 'GET',
								success : function(data)
								{


									//getAttribute_v2(5);
									$("#light").hide();
									getDataNew();

									return;
								}

							});
						}



					function undofield(){

						document.getElementById('custom_attribute').innerHTML = '';
						lastchar = '';
					}
					function ClearFields()
					{

				     	document.getElementById("custom_attribute").value = "";
						document.getElementById("custom_numeric").value = "";
					}

					$(function(){
					    $('select').change(function(){
					        $that = $(this);
					        $('textarea').val(function(){
					            return $(this).prop('defaultValue') + ' '+$that.val();
					        });
					    });
					});

					JQuery("#editvariable").live("click",function(){
						JQuery('#save').attr("id","updatecustom");
						JQuery('#updatecustom').attr("onclick","javascript:void(0)");
						is_updatecustom=1;
						JQuery('#updatecustom').attr("value","Update");
					});

					JQuery(".customedit").live('click',function(){
						if(is_updatecustom == 1){
							//alert(JQuery(this).find(":selected").text());
							JQuery('#new_name').val(JQuery(this).find(":selected").text());
							JQuery('#new_name').attr('disabled', true);
							oldattrval=JQuery(this).find(":selected").val();
							JQuery('#oldattrval').val(oldattrval);
							JQuery('textarea#custom_attribute').text("");
							lastchar = '';
							moverightarea();
						}
					});
				</script>

				<div id="light">
					<ul class="nav nav-tabs">
					    <li class="active"><a data-toggle="tab" href="#home">Custom</a></li>
					    <li><a data-toggle="tab" href="#menu1">Manage</a></li>
					    <li><a data-toggle="tab" href="#menu2">Add New</a></li>
					</ul>

				  	<div class="tab-content">
					    <div id="home" class="tab-pane fade in active">
					    	<div id="CustomVariable">
					      	</div>
					    </div>

					    <div id="menu1" class="tab-pane fade">
					     	<div id="customvariables">
								<div id="lightmanagevariable">
									<div class="poptitle">
										<?php echo JTEXT::_('MANAGE_CUSTOM');?>
									</div>

									<table width="100%" cellpadding="0" cellspacing="1" class="cacss">
										<thead>
											<tr>
												<th align="center" width="10%">Use</th>
												<th align="center" width="45%">Variable</th>
												<th align="center" width="45%">Action</th>
											</tr>
										</thead>
										<?php
										$i=0;

										$finalarray[1] =array_unique($finalarray[1]);
										foreach($finalarray[1] as $eacharray){
											if(in_array($eacharray,$finalarray[0])){
												$checked  = "checked";
												$selected = "Remove";
												$action   = "";
											}else{
												$checked  = "";
												$selected = "Select";
												$action   = "checked";
											}

											if(($i%2)==0){
												$clear = "clear";
											}else{
												$clear ="";
											}
											$namevalue = explode(":",$eacharray);


											echo '
											<tr>
										    <td align="center" width="10%">
											 <input type="checkbox" name="variable1"  class="dcv_checkbox" value="'.$namevalue[1].'" id="'.$namevalue[0].'">
											 <label></label>
												</td>
												<td align="center" class="customdelete" width="45%">'.$namevalue[0].'</td>
												<td align="center" width="45%">
													<a href="javascript:void(0);" style="float:left;padding-left:5px;">
														<img src="'.JUri::base().'/components/com_mica/images/edit.png" alt="Edit" class="customedit" id="'.$namevalue[0].'" value="'.$namevalue[1].'">
													</a>
													<a href="javascript:void(0);"  style="float:left;padding-left:5px;" onclick="JQuery(this).parent().prev().prev().children().attr(\'checked\',\'checked\');JQuery(\'.deletecustomvariable	\').click();">
														<img src="'.JUri::base().'/components/com_mica/images/delete.png" alt="Delete">
													</a>
												</td>
											</tr>';
											$i++;
										} ?>
									</table>

									<div class="readon frontbutton">
										<input type="button" name="Remove" value="Remove Selected" class="frontbutton deletecustomvariable" />
									</div>
								</div>
						  	</div>
					    </div>

						<div id="menu2" class="tab-pane fade">
							<div id="light" class="white_content2 large-popup" style="display: none;">
								<div class="divclose">
									<a id="closeextra" href="javascript:void(0);" onclick="undofield();document.getElementById('light').style.display='none';document.getElementById('fade').style.display='none';">
										<img src="media/system/images/closebox.jpeg" alt="X" />
									</a>
								</div>

								<script type="text/javascript">
									var lastchar = '';
									function moverightarea(){
										var val      = document.getElementById('custom_attribute').innerHTML;

										var selected = 0;
										for(i = document.micaform1.avail_attribute.options.length-1; i >= 0; i--){
											var avail_attribute = document.micaform1.avail_attribute;
											if(document.micaform1.avail_attribute[i].selected){
												selected = 1;
												if(lastchar=='attr'){
													alert("<?php echo JText::_('CONSECUTIVE_ATTR_ALERT');?>");
												}else{
													document.getElementById('custom_attribute').innerHTML = val+' '+document.micaform1.avail_attribute[i].value+' ';
													lastchar = 'attr';
												}
											}
										}
										if(selected==0){
											alert("<?php echo JText::_('NOT_SELECTED_COMBO_ALERT');?>");
										}
									}
									$("#copyBtn").click(function(){
									    var selected = $("#selectBox").val();
									    $("#output").append("\n * " + selected);
									});

									function addplussign(){
										var val = document.getElementById('custom_attribute').innerHTML;
										if(lastchar=='opr'){
											alert("<?php echo JText::_('CONSECUTIVE_OPR_ALERT');?>");
										}else{
											document.getElementById('custom_attribute').innerHTML = val+"+";
											lastchar = 'opr';
										}
									}

									function addminussign(){
										var val = document.getElementById('custom_attribute').innerHTML;
										if(lastchar=='opr'){
											alert("<?php echo JText::_('CONSECUTIVE_OPR_ALERT');?>");
										}else{
											document.getElementById('custom_attribute').innerHTML = val+"-";
											lastchar = 'opr';
										}
									}

									function addmultiplysign(){
										var val = document.getElementById('custom_attribute').innerHTML;
										if(lastchar=='opr'){
											alert("<?php echo JText::_('CONSECUTIVE_OPR_ALERT');?>");
										}else{
											document.getElementById('custom_attribute').innerHTML = val+"*";
											lastchar = 'opr';
										}
									}

									function adddivisionsign(){
										var val = document.getElementById('custom_attribute').innerHTML;
										if(lastchar=='opr'){
											alert("<?php echo JText::_('CONSECUTIVE_OPR_ALERT');?>");
										}else{
											document.getElementById('custom_attribute').innerHTML = val+"/";
											lastchar = 'opr';
										}
									}

									function addLeftBraket(){
										var val = document.getElementById('custom_attribute').innerHTML;
										document.getElementById('custom_attribute').innerHTML = val+"(";
										checkIncompleteFormula();
									}

									function addRightBraket(){
										var val = document.getElementById('custom_attribute').innerHTML;
										document.getElementById('custom_attribute').innerHTML = val+")";
										checkIncompleteFormula();
									}

									function addPersentage(){
										var val = document.getElementById('custom_attribute').innerHTML;
										//if(lastchar=='opr'){
											//alert("<?php echo JText::_('CONSECUTIVE_OPR_ALERT');?>");
										//}else{
											document.getElementById('custom_attribute').innerHTML = val+"%";
											lastchar = 'opr';
										//}
									}

									function addNumeric(){
										var val       = document.getElementById('custom_attribute').value;
										var customval = document.getElementById('custom_numeric').value;
										document.getElementById('custom_attribute').innerHTML = val+customval;
										lastchar = '';
									}

									function checkIncompleteFormula(){
										var str   = document.getElementById('custom_attribute').innerHTML;
										var left  = str.split("(");
										var right = str.split(")");

										if(left.length==right.length){
											document.getElementById('custom_attribute').style.color="black";
										}else{
											document.getElementById('custom_attribute').style.color="red";
										}
									}

									function Click(val1){
										var val = document.getElementById('custom_attribute').innerHTML;
										document.getElementById('custom_attribute').innerHTML = val+val1;
										checkIncompleteFormula();
									}

									function checkfilledvalue(){

										var tmp1 = document.getElementById('custom_attribute').innerHTML;
										var tmp2 = document.getElementById('new_name').value;

										var flg  = 0;
										var len1 = parseInt(tmp1.length) - 1;
										var len2 = parseInt(tmp2.length) - 1;
										if(tmp1.charAt(len1) == "%" || tmp2.indexOf(len2) == "%" ) {
											alert("<?php echo JText::_('CUSTOM_ATTR_BLANK_ALERT');?>");
										}else if(lastchar=='opr' && flg == 0){
											alert("<?php echo JText::_('LAST_CHAR_OPR_ALERT');?>");
										}

										if(document.getElementById('custom_attribute').innerHTML=='' || document.getElementById('new_name').value==''){

										}else{
											if(!validateFormula_v2()){
												return false;
											}
											addCustomAttr(document.getElementById('new_name').value,encodeURIComponent(document.getElementById('custom_attribute').innerHTML));
										}
									}

									function addCustomAttr(attrname,attributevale){

												JQuery.ajax({
												url     : "index.php?option=com_mica&task=showresults.AddCustomAttr&attrname="+attrname+"&attributevale="+attributevale,
												method  : 'GET',
												success : function(data)
												{


													//getAttribute_v2(5);
													$("#light").hide();
													getDataNew();

													return;
												}

											});
										}



									function undofield(){

										document.getElementById('custom_attribute').innerHTML = '';
										lastchar = '';
									}
									function ClearFields()
									{

								     	document.getElementById("custom_attribute").value = "";
										document.getElementById("custom_numeric").value = "";
									}

									$(function(){
									    $('select').change(function(){
									        $that = $(this);
									        $('textarea').val(function(){
									            return $(this).prop('defaultValue') + ' '+$that.val();
									        });
									    });
									});

									JQuery("#editvariable").live("click",function(){
										JQuery('#save').attr("id","updatecustom");
										JQuery('#updatecustom').attr("onclick","javascript:void(0)");
										is_updatecustom=1;
										JQuery('#updatecustom').attr("value","Update");
									});

									JQuery(".customedit").live('click',function(){
										if(is_updatecustom == 1){
											//alert(JQuery(this).find(":selected").text());
											JQuery('#new_name').val(JQuery(this).find(":selected").text());
											JQuery('#new_name').attr('disabled', true);
											oldattrval=JQuery(this).find(":selected").val();
											JQuery('#oldattrval').val(oldattrval);
											JQuery('textarea#custom_attribute').text("");
											lastchar = '';
											moverightarea();
										}
									});
								</script>

								<div id="light">
									<ul class="nav nav-tabs">
									    <li class="active"><a data-toggle="tab" href="#home">Custom</a></li>
									    <li><a data-toggle="tab" href="#menu1">Manage</a></li>
									    <li><a data-toggle="tab" href="#menu2">Add New</a></li>
									</ul>

								  	<div class="tab-content">
									    <div id="home" class="tab-pane fade in active">
									    	<div id="CustomVariable">
									      	</div>
									    </div>

									    <div id="menu1" class="tab-pane fade">
									     	<div id="customvariables">
												<div id="lightmanagevariable">
													<div class="poptitle">
														<?php echo JTEXT::_('MANAGE_CUSTOM');?>
													</div>

													<table width="100%" cellpadding="0" cellspacing="1" class="cacss">
														<thead>
															<tr>
																<th align="center" width="10%">Use</th>
																<th align="center" width="45%">Variable</th>
																<th align="center" width="45%">Action</th>
															</tr>
														</thead>
														<?php
														$i = 0;

														$finalarray[1] =array_unique($finalarray[1]);
														foreach($finalarray[1] as $eacharray){
															if(in_array($eacharray,$finalarray[0])){
																$checked  = "checked";
																$selected = "Remove";
																$action   = "";
															}else{
																$checked  = "";
																$selected = "Select";
																$action   = "checked";
															}

															if(($i%2)==0){
																$clear = "clear";
															}else{
																$clear ="";
															}
															$namevalue = explode(":",$eacharray);

															echo '
															<tr>
														    <td align="center" width="10%">
															 <input type="checkbox" name="variable1"  class="dcv_checkbox" value="'.$namevalue[1].'" id="'.$namevalue[0].'">
															 <label></label>
																</td>
																<td align="center" class="customdelete" width="45%">'.$namevalue[0].'</td>
																<td align="center" width="45%">
																	<a href="javascript:void(0);" style="float:left;padding-left:5px;">
																		<img src="'.JUri::base().'/components/com_mica/images/edit.png" alt="Edit" class="customedit" id="'.$namevalue[0].'" value="'.$namevalue[1].'">
																	</a>
																	<a href="javascript:void(0);"  style="float:left;padding-left:5px;" onclick="JQuery(this).parent().prev().prev().children().attr(\'checked\',\'checked\');JQuery(\'.deletecustomvariable	\').click();">
																		<img src="'.JUri::base().'/components/com_mica/images/delete.png" alt="Delete">
																	</a>
																</td>
															</tr>';
															$i++;
														} ?>
													</table>

													<div class="readon frontbutton">
														<input type="button" name="Remove" value="Remove Selected" class="frontbutton deletecustomvariable" />
													</div>
												</div>
										  	</div>
									    </div>

										<div id="menu2" class="tab-pane fade">
									       	<div class="row">
									       	 	<div class="col-md-12">
								      				<h5 class="text-danger"><?php echo JText::_('ADD_ATTRIBUTE_LABEL');?></h5>
								      			</div>

									       	 	<div class="col-md-12"><b><?php echo JText::_('NAME_LABEL'); ?></b> : <input type="text" name="new_name" id="new_name" class="inputbox" style="width: 90%!important">
									       	 	</div>

								      			<div class="col-md-12">
													<label><b></b>Select Variable </b></label>
													<select name="avail_attribute" id="avail_attribute" class="avial_select" style="width:80%!important;">
														<optgroup label="Default Variable" class="defaultvariable">
															<?php
															$attr= $this->themeticattribute;//themeticattribute
															$eachattr=explode(",",$attr);
															foreach($eachattr as $eachattrs){
																echo '<option value='.$eachattrs.'>'.JTEXT::_($eachattrs).'</option>';
															} ?>
														</optgroup>
													</select>
								      			</div>

								      			<div class="col-md-12">
							      					<table  border="0" align="center" cellpadding="0" cellspacing="0" class="cal_t" style="background: #dbdbdd">
														<tr>
															<td width="60">
																<input type="button" title="<?php echo JText::_('ADD_PLUS_TIP');?>" name="addplus" id="addplus" class="btn btn-warning btn-sm btn-block"  value="+" onclick="addplussign();">
															</td>
														   	<td width="60">
														   		<input type="button" title="<?php echo JText::_('ADD_MINUS_TIP');?>" name="addminus" id="addminus" class="btn btn-warning btn-sm btn-block"  value="-" onclick="addminussign();">
														   	</td>
															<td width="60">
																<input type="button" title="<?php echo JText::_('ADD_MULTIPLY_TIP');?>" name="addmultiply" id="addmultiply" class="btn btn-warning btn-sm btn-block"  value="*" onclick="addmultiplysign();">
															</td>
														   	<td width="60">
														   		<input type="button" title="<?php echo JText::_('ADD_DIVISION_TIP');?>" name="adddivision" id="adddivision" class="btn btn-warning btn-sm btn-block"  value="/" onclick="adddivisionsign();">
														   	</td>
															<td width="60">
																<input type="button" title="<?php echo JText::_('ADD_PERSENT_TIP');?>" name="ADDPERSENT" id="addPersent" class="btn btn-warning btn-sm btn-block"  value="%" onclick="addPersentage();">
															</td>

															<td width="60">
																<input name="sin" type="button" id="sin" value="sin" onclick="Click('SIN(')" class="btn btn-warning btn-sm btn-block">
															</td>
														</tr>
														<tr>
															<td width="60">
																<input name="cos" type="button" id="cos" value="cos" onclick="Click('COS(')" class="btn btn-warning btn-sm btn-block">
															</td>
														   	<td width="60">
														   		<input name="tab" type="button" id="tab" value="tan" onclick="Click('TAN(')" class="btn btn-warning btn-sm btn-block">
														   	</td>
															<td width="60">
																<input name="log" type="button" id="log" value="log" onclick="Click('LOG(')" class="btn btn-warning btn-sm btn-block">
															</td>
														 	<td width="60">
														 		<input name="1/x" type="button" id="1/x2" value="log10" onclick="Click('LOG10(')" class="btn btn-warning btn-sm btn-block">
														 	</td>

															<td width="60">
																<input name="sqrt" type="button" id="sqrt" value="sqrt" onclick="Click('SQRT(')" class="btn btn-warning btn-sm btn-block">
															</td>
															<td width="60">
																<input name="exp" type="button" id="exp" value="exp" onclick="Click('EXP(')" class="btn btn-warning btn-sm btn-block">
															</td>
														</tr>
														<tr>
															<td width="60">
																<input name="^" type="button" id="^" value="^" onclick="Click('^')" class="btn btn-warning btn-sm btn-block">
															</td>
															<td width="60">
																<input name="ln" type="button" id="abs22" value="ln" onclick="Click('LN(')" class="btn btn-warning btn-sm btn-block">
															</td>
															<td width="60">
																<input name="pi" type="button" id="pi3" value="pi" onclick="Click('PI()')" class="btn btn-warning btn-sm btn-block">
															</td>

															<td width="60">
																<input type="button" title="<?php echo JText::_('ADD_LEFTBRAC_TIP');?>" name="ADDLEFTBRAC" id="addleftbrac" class="btn btn-warning btn-sm btn-block"  value="(" onclick="addLeftBraket();">
															</td>
															<td width="60">
																<input type="button" title="<?php echo JText::_('ADD_RIGHTBRAC_TIP');?>" name="ADDRIGHTBRAC" id="addleftbrac" class="btn btn-warning btn-sm btn-block"   value=")" onclick="addRightBraket();">
															</td>
															<td></td>
														</tr>
													</table>
											   	</div>

								      			<div class="col-md-12">
			      									<label><?php echo JText::_('AVAILABLE_ATTRIBUTE_LABEL'); ?></label>
													<input type="text" name="custom_attribute" id="custom_numeric" class="inputbox" style="width:65%!important;">
													<input type="button" title="<?php echo JText::_('NUMERIC_VALUE_LABEL'); ?>" name="numericval" id="numericval" class="btn btn-success" value="Add Value" onclick="addNumeric();">
						      					</div>

								      			<div class="col-md-12">
													<textarea class="form-control" name="custom_attribute" id="custom_attribute" rows="3"  readonly="readonly" style="width:100%!important;"></textarea>
								      			</div>

									      		<div class="col-md-12">
									      			<div class="col-md-6">
														<input type="button" name="save" id="save" class="btn btn-primary" value="<?php echo JText::_('SAVE_LABEL');?>" onclick="checkfilledvalue();">
													</div>
													<div class="col-md-6">
														<input type="button" name="clear" id="clear" class="btn btn-danger" value="<?php echo JText::_('CLEAR_LABEL');?>" onclick="ClearFields();">
													</div>
									      		</div>
									       	</div>
									    </div>
								  	</div>
								</div>

						       	<div class="row">
						       	 	<div class="col-md-12">
					      				<h5 class="text-danger"><?php echo JText::_('ADD_ATTRIBUTE_LABEL');?></h5>
					      			</div>
						       	 	<div class="col-md-12"><b><?php echo JText::_('NAME_LABEL'); ?></b> : <input type="text" name="new_name" id="new_name" class="inputbox" style="width: 90%!important"></div>

					      			<div class="col-md-12">
										<label><b></b>Select Variable </b></label>
										<select name="avail_attribute" id="avail_attribute" class="avial_select" style="width:80%!important;">
											<optgroup label="Default Variable" class="defaultvariable">
												<?php
												$attr= $this->themeticattribute;//themeticattribute
												$eachattr=explode(",",$attr);
												foreach($eachattr as $eachattrs){
													echo '<option value='.$eachattrs.'>'.JTEXT::_($eachattrs).'</option>';
												} ?>
											</optgroup>
										</select>
					      			</div>

					      			<div class="col-md-12">
				      					<table  border="0" align="center" cellpadding="0" cellspacing="0" class="cal_t" style="background: #dbdbdd">
											<tr>
												<td width="60">
													<input type="button" title="<?php echo JText::_('ADD_PLUS_TIP');?>" name="addplus" id="addplus" class="btn btn-warning btn-sm btn-block"  value="+" onclick="addplussign();">
												</td>
											   	<td width="60">
											   		<input type="button" title="<?php echo JText::_('ADD_MINUS_TIP');?>" name="addminus" id="addminus" class="btn btn-warning btn-sm btn-block"  value="-" onclick="addminussign();">
											   	</td>
												<td width="60">
													<input type="button" title="<?php echo JText::_('ADD_MULTIPLY_TIP');?>" name="addmultiply" id="addmultiply" class="btn btn-warning btn-sm btn-block"  value="*" onclick="addmultiplysign();">
												</td>
											   	<td width="60">
											   		<input type="button" title="<?php echo JText::_('ADD_DIVISION_TIP');?>" name="adddivision" id="adddivision" class="btn btn-warning btn-sm btn-block"  value="/" onclick="adddivisionsign();">
											   	</td>
												<td width="60">
													<input type="button" title="<?php echo JText::_('ADD_PERSENT_TIP');?>" name="ADDPERSENT" id="addPersent" class="btn btn-warning btn-sm btn-block"  value="%" onclick="addPersentage();">
												</td>

												<td width="60">
													<input name="sin" type="button" id="sin" value="sin" onclick="Click('SIN(')" class="btn btn-warning btn-sm btn-block">
												</td>
											</tr>
											<tr>
												<td width="60">
													<input name="cos" type="button" id="cos" value="cos" onclick="Click('COS(')" class="btn btn-warning btn-sm btn-block">
												</td>
											   	<td width="60">
											   		<input name="tab" type="button" id="tab" value="tan" onclick="Click('TAN(')" class="btn btn-warning btn-sm btn-block">
											   	</td>
												<td width="60">
													<input name="log" type="button" id="log" value="log" onclick="Click('LOG(')" class="btn btn-warning btn-sm btn-block">
												</td>
											 	<td width="60">
											 		<input name="1/x" type="button" id="1/x2" value="log10" onclick="Click('LOG10(')" class="btn btn-warning btn-sm btn-block">
											 	</td>

												<td width="60">
													<input name="sqrt" type="button" id="sqrt" value="sqrt" onclick="Click('SQRT(')" class="btn btn-warning btn-sm btn-block">
												</td>
												<td width="60">
													<input name="exp" type="button" id="exp" value="exp" onclick="Click('EXP(')" class="btn btn-warning btn-sm btn-block">
												</td>
											</tr>
											<tr>
												<td width="60">
													<input name="^" type="button" id="^" value="^" onclick="Click('^')" class="btn btn-warning btn-sm btn-block">
												</td>
												<td width="60">
													<input name="ln" type="button" id="abs22" value="ln" onclick="Click('LN(')" class="btn btn-warning btn-sm btn-block">
												</td>
												<td width="60">
													<input name="pi" type="button" id="pi3" value="pi" onclick="Click('PI()')" class="btn btn-warning btn-sm btn-block">
												</td>

												<td width="60">
													<input type="button" title="<?php echo JText::_('ADD_LEFTBRAC_TIP');?>" name="ADDLEFTBRAC" id="addleftbrac" class="btn btn-warning btn-sm btn-block"  value="(" onclick="addLeftBraket();">
												</td>
												<td width="60">
													<input type="button" title="<?php echo JText::_('ADD_RIGHTBRAC_TIP');?>" name="ADDRIGHTBRAC" id="addleftbrac" class="btn btn-warning btn-sm btn-block"   value=")" onclick="addRightBraket();">
												</td>
												<td></td>
											</tr>
										</table>
									</div>

					      			<div class="col-md-12">
										<label><?php echo JText::_('AVAILABLE_ATTRIBUTE_LABEL'); ?></label>
										<input type="text" name="custom_attribute" id="custom_numeric" class="inputbox" style="width:65%!important;">
										<input type="button" title="<?php echo JText::_('NUMERIC_VALUE_LABEL'); ?>" name="numericval" id="numericval" class="btn btn-success" value="Add Value" onclick="addNumeric();">
			      					</div>

					      			<div class="col-md-12">
										<textarea class="form-control" name="custom_attribute" id="custom_attribute" rows="3"  readonly="readonly" style="width:100%!important;"></textarea>
					      			</div>

						      		<div class="col-md-12">
						      			<div class="col-md-6">
											<input type="button" name="save" id="save" class="btn btn-primary" value="<?php echo JText::_('SAVE_LABEL');?>" onclick="checkfilledvalue();">
										</div>
										<div class="col-md-6">
											<input type="button" name="clear" id="clear" class="btn btn-danger" value="<?php echo JText::_('CLEAR_LABEL');?>" onclick="ClearFields();">
										</div>
						      		</div>
						       	</div>
					   		</div>
				  		</div>
					</div>
				</div>
			</div>
		</div>
	</form>
</div>
<div id="fade" class="black_overlay" style="display:none"></div>

<div id="light_thematic" style="display: none; overflow: scroll; " class="tq white_content2 large-popup">

	<div class="divclose">
		<a href="javascript:void(0);" onclick="document.getElementById('light_thematic').style.display='none'; document.getElementById('fade').style.display='none';">
			<img src="media/system/images/closebox.jpeg" alt="X">
		</a>
	</div>

	<div class="" style="width:100%;float:left;">
		<div class="thematicarea clearfix">
			<div class="blockcontent">
				
				<?php echo $legendsDiv;?>
				<div class='contentblock3'>
					<script type='text/javascript'>
						var totalthemecount     = "<?php echo count($grpname); ?>";
						var usedlevel           = '<?php echo implode(",",$levelunique); ?>';
						var havingthematicquery = '<?php echo $tojavascript; ?>';
					</script>
					<div id="themeconent">
						<form name="micaform3" id="micaform3" action="index.php?option=com_mica&view=showresults&Itemid=<?php echo $this->Itemid; ?>" method="get" >
							<table cellspacing="4" cellpadding="0" border="0" width="100%">
								<tr>
									<td colspan="2" align="left"><h3><?php echo JText::_('SELECT_PARAMETER_LABEL'); ?></h3></td>
								</tr>
								<tr>
									<td width="35%">
										<b><?php echo JText::_('ATTRIBUTE_LABEL');   ?></b>
									</td>
									<td>
										<select name="thematic_attribute" id="thematic_attribute" class="inputbox" onchange="getMinmaxVariable_v2(this.value);">
											<option value=""><?php echo JText::_('PLEASE_SELECT');?></option>
											<optgroup label="Default Variable">
												<?php $attr = $this->themeticattribute;
												$eachattr = explode(",",$attr);
												foreach($eachattr as $eachattrs){
													echo '<option value='.$eachattrs.'>'.ucfirst(strtolower(str_replace("_", " ", $eachattrs))).'</option>';
												} ?>
											</optgroup>
											<optgroup label="Custom Variable">
												<?php $attr = $this->customattribute;
												$eachattr = explode(",",$attr);
												$eachattr = array_filter($eachattr);
												foreach($eachattr as $eachattrs){
													$eachattrs=explode(":",$eachattrs);
													echo '<option value="'.$eachattrs[1].'">'.JTEXT::_($eachattrs[0]).'</option>';
												} ?>
											</optgroup>
										</select>
									</td>
								</tr>
								<tr id="minmaxdisplay">
									<td></td>
									<td class="minmaxdisplay"></td>
								</tr>
								<tr>
									<td>
										<b><?php echo JText::_('NO_OF_INTERVAL_LABEL');?></b>
									</td>
									<td>
										<input type="text" name="no_of_interval" id="no_of_interval" class="inputbox" value="" style="width: 194px;">
									</td>
								</tr>
								<tr class="colorhide">
									<td>
										<b><?php echo JText::_('SELECT_COLOR_LABEL');?></b>
									</td>
									<td>
										<input class="simple_color" value="" style="width: 194px;"/>
									</td>
								</tr>
								<tr>
									<td colspan="2">
										<table cellspacing="0" cellpadding="0" border="1" width="100%" class="popupfromto" id="displayinterval"></table>
									</td>
								</tr>
								<tr>
									<td></td>
									<td align="left">
										<div class="readon ">
											<input type="button" name="save" id="savesld" class="frontbutton" style="width: 100px;" value="<?php echo JText::_('OK_LABEL');?>" onclick="saveSLD()"/>
										</div>
										<div class="readon ">
											<input type="button" name="cancel" id="cancel" class="frontbutton" style="width: 100px;" value="<?php echo JText::_('CANCEL_LABEL');?>" onclick="closePopup();"/>
										</div>
									</td>
								</tr>
							</table>
								<input type="hidden" name="maxvalh" id="maxvalh" value="">
								<input type="hidden" name="minvalh" id="minvalh" value="">
								<input type="hidden" name="level" id="level" value="">
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<!--End Thematic -->

<div id="light2" class="white_content2" style="display: none;">
	<div class="divclose">
		<a href="javascript:void(0);" onclick="closePopup_v2();"><img src="media/system/images/closebox.jpeg" alt="X" /></a>
	</div>
	<div class="themeconent"></div>
</div>
<script type="text/javascript">
	// document.getElementsByTagName("body").onload = function() {loadm()};
	
	// console.log(projection);
	/*$(document).on('click','#sub_district_tab','#district_tab', function(){
		JQuery(document).ready(function() {
  	  		JQuery('.slider').slick({
		  		dots: false,
		    	vertical: true,
		    	slidesToShow: 11,
		    	slidesToScroll: 1,
		    	verticalSwiping: true,
		    	arrrow:true,
		  	});
		});

	// });*/	

	function getColorPallet(){
		// alert('ara');
		JQuery('.simple_color').simpleColor({
			cellWidth   : 9,
			cellHeight  : 9,
			border      : '1px solid #333333',
			buttonClass : 'colorpickerbutton'
		});
	}
	$(document).on("click","#gisdata", function(){
    	isGISLoad=0;
    	// getColorPallet();
	});
	$(document).on("click","#district_tab", function(){	
      var dis = $("input[name='villages_villages[]']").removeAttr( "checked" );    

	});
	
// gisdata

	$(document).on("click","#variables_tab", function(){
		if($('.variableslider').length>0){
			console.log($('.variableslider').length);
  	  		$('.variableslider').slick({
		  		dots: false,
		    	vertical: true,
		    	slidesToShow: 11,
		    	slidesToScroll: 1,
		    	verticalSwiping: true,
		    	arrrow:true,
		  	});
		}
	});


	$(document).on("click","#district_tab", function(){
		if($('.districtslider').length>0){
			console.log($('.districtslider').length);
	  	  		$('.districtslider').slick({
			  		dots: false,
			    	vertical: true,
			    	slidesToShow: 11,
			    	slidesToScroll: 1,
			    	verticalSwiping: true,
			    	arrrow:true,
			  	});
			}
		});
	$(document).on("click","#sub_district_tab", function(){
		if($('.slider').length>0){
			console.log($('.districtslider').length);
	  	  		$('.slider').slick({
			  		dots: false,
			    	vertical: true,
			    	slidesToShow: 11,
			    	slidesToScroll: 1,
			    	verticalSwiping: true,
			    	arrrow:true,
			  	});
			}
		});


	/*-------------export -----------*/
document.getElementById('export').addEventListener("click", downloadPDF);

/*
	Download Chart
*/
function downloadPDF()
{
	//var img       = document.querySelector('h1.logo_img > a > img');
	var img = document.querySelector('.logo_img > a:nth-child(2) > img:nth-child(1)');
	var doc = new jsPDF('landscape');

	doc.setFontSize(20);
	doc.addImage(img, 'PNG', 5, 5);
	doc.text(130, 40, $('#chartype option:selected').text());

	if($('#chartype option:selected').text() == "Bubble Chart")
	{
		var svg = document.getElementsByTagName('svg')[0];

		svg.setAttributeNS('http://www.w3.org/2000/xmlns/', 'xmlns', 'http://www.w3.org/2000/svg');
		svg.setAttributeNS('http://www.w3.org/2000/xmlns/', 'xmlns:xlink', 'http://www.w3.org/1999/xlink');

		var serializer = new XMLSerializer();
		var serialSVG  = serializer.serializeToString(svg);
		var svg        = serialSVG;

	  	if (svg){
	  		svg = svg.replace(/\r?\n|\r/g, '').trim();
	  	}

	  	var canvas = document.createElement('canvas');
	  	var context = canvas.getContext('2d');

		canvg(canvas, svg);

		var canvasImg = canvas.toDataURL("image/jpeg", 1.0);
		doc.addImage(canvasImg, 'JPEG', 50, 50);

		/*var imgData = canvas.toDataURL('image/png');
		doc.addImage(imgData, 'PNG', 0, 50, 280, 150 );*/
		//doc.addImage(imgData, 'png', 0, 50);
	}
	else
	{
		var canvas    = document.querySelector('#myChart');
		var canvasImg = canvas.toDataURL("image/jpeg", 1.0);
		doc.addImage(canvasImg, 'JPEG', 0, 50, 280, 150 );
	}

	doc.save($('#chartype option:selected').text()+'.pdf');
}
/*-------------export end-----------*/

$('#statespan').on('change', '.state_checkbox', function() {
checkvalidation();
});

$('.state_checkbox').change(function() {
checkvalidation();
});
function toggleLegends(){
	$(document).find('#maplegends').slideToggle();	
}
function openthematicQuery(){
			$(document).ready(function(){
				$('#main_loader').show();
			   	setTimeout(function(){
			      $('#light_thematic').show();
			      $('#main_loader').hide();
			   },3000); // 3000 to load it after 3 seconds from page load
			});
		}

	//$(document).ready(function() {

		/*$('#applyChanges').click(function() {
		    $("#tabledata").css("pointerEvents", "auto");
		    $("#tabledata").css("cursor", "pointer");
		    $("#loadchart").css("pointerEvents", "auto");
		    $("#loadchart").css("cursor", "pointer");
		    $("#matrix").css("pointerEvents", "auto");
		    $("#matrix").css("cursor", "pointer");
		    $("#pmdata").css("pointerEvents", "auto");
		    $("#pmdata").css("cursor", "pointer");
		    $("#gisdata").css("pointerEvents", "auto");
		    $("#gisdata").css("cursor", "pointer");
		});*/
   // });
/*	JQuery("#showspeed").live("click",function(){
		var speedvar    =new Array();
		var speedregion =new Array();
		if (typeof(JQuery("#speed_variable").attr("multiple")) == "string") {
			JQuery("#speed_variable").each(function(){
				speedvar.push(encodeURIComponent(JQuery(this).val()));
			});
			speedregion.push(encodeURIComponent(JQuery("#speed_region").val()));
		}
		else {
			speedvar.push(encodeURIComponent(JQuery("#speed_variable").val()));
			JQuery("#speed_region").each(function(){
				speedregion.push(encodeURIComponent(JQuery(this).val()));
			});
		}

		JQuery.ajax({
			url     : "index.php?option=com_mica&task=villageshowresults.getsmeter&region="+speedregion+"&speedvar="+speedvar+"&filterspeed="+filterspeed_radio,
			type    : 'GET',
			success : function(data){
				JQuery("#spedometer_region").html("");
				JQuery("#spedometer_region").html(data);
				initspeed();
				JQuery("#speedfiltershow").css({"display":"block"});
				JQuery(".sfilter").css({"display":"none"});
				JQuery("#spedometer_region").css({"width":"915px"});
				JQuery("#spedometer_region").css({"overflow":"auto"});
			}
		});
	});*/
</script>