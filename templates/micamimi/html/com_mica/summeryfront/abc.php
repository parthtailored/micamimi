<pre>
<table cellpadding="0" cellspacing="0" border="0">
   <tr bgcolor="#0154A1" >
      <th width="150">Legends</th>
      <th width="150">Variable Name</th>
      <th width="150">Total Geographical Area (in Hectares)</th>
      <th width="150">Govt_Primary_School</th>
      <th width="150">Govt_Primary_School</th>
      <th width="150">Power_Supply_For_Commercial_Use_WinterOctMarper_dayin_Hrs</th>
      <th width="150">Power_Supply_For_Commercial_Use_WinterOctMarper_dayin_Hrs</th>
      <th width="150">Power_Supply_For_All_Users_Summer_AprSep_per_day_in_Hrs</th>
      <th width="150">Power_Supply_For_All_Users_Summer_AprSep_per_day_in_Hrs</th>
      <th width="150">Power_Supply_For_All_Users_Winter_OctMar_per_day_in_Hrs</th>
      <th width="150">Power_Supply_For_All_Users_Winter_OctMar_per_day_in_Hrs</th>
      <th width="150">Rural Score Commercial Usage of Premises</th>
      <th width="150">Rural Score Financial Services</th>
      <th width="150">Private_Primary_School</th>
      <th width="150">Private_Primary_School</th>
      <th width="150">Rural Score Basic Amenities</th>
      <th width="150">Distance_to_Nearest_Pre_Primary_School</th>
      <th width="150">Distance_to_Nearest_Pre_Primary_School</th>
      <th width="150">Distance_to_Nearest_Pre_Primary_School</th>
      <th width="150">Distance_to_NearestPrimary_SchoolAvailable</th>
      <th width="150">Distance_to_NearestPrimary_SchoolAvailable</th>
      <th width="150">Distance_to_NearestPrimary_SchoolAvailable</th>
      <th width="150">Total Population of Village</th>
      <th width="150">Total Households</th>
      <th width="150">District_Code</th>
      <th width="150">Sub_District_Code</th>
      <th width="150">Sub_District_Name</th>
   </tr>
   <tr>
      <td width="150">
         <table cellspacing="6" cellpadding="4">
            <tr>
               <td bgcolor="#FFD600">Low</td>
            </tr>
         </table>
      </td>
      <td width="150" >Kachchh-GUJARAT</td>
      <td  width="150px" style="background-color:#D7191C;border:1px solid #FFFFFF;" ></td>
      <td  width="150px" style="background-color:#377EB8;border:1px solid #FFFFFF;" ></td>
      <td  width="150px" style="background-color:#D7191C;border:1px solid #FFFFFF;" ></td>
      <td  width="150px" style="background-color:#D7191C;border:1px solid #FFFFFF;" ></td>
      <td  width="150px" style="background-color:#D7191C;border:1px solid #FFFFFF;" ></td>
      <td  width="150px" style="background-color:#D7191C;border:1px solid #FFFFFF;" ></td>
      <td  width="150px" style="background-color:#D7191C;border:1px solid #FFFFFF;" ></td>
      <td  width="150px" style="background-color:#D7191C;border:1px solid #FFFFFF;" ></td>
      <td  width="150px" style="background-color:#D7191C;border:1px solid #FFFFFF;" ></td>
      <td  width="150px" style="background-color:#D7191C;border:1px solid #FFFFFF;" ></td>
      <td  width="150px" style="background-color:#D7191C;border:1px solid #FFFFFF;" ></td>
      <td  width="150px" style="background-color:#4DAF4A;border:1px solid #FFFFFF;" ></td>
      <td  width="150px" style="background-color:#377EB8;border:1px solid #FFFFFF;" ></td>
      <td  width="150px" style="background-color:#377EB8;border:1px solid #FFFFFF;" ></td>
      <td  width="150px" style="background-color:#D7191C;border:1px solid #FFFFFF;" ></td>
      <td  width="150px" style="background-color:#4DAF4A;border:1px solid #FFFFFF;" ></td>
      <td  width="150px" style="background-color:#4DAF4A;border:1px solid #FFFFFF;" ></td>
      <td  width="150px" style="background-color:#FFD600;border:1px solid #FFFFFF;" ></td>
      <td  width="150px" style="background-color:#4DAF4A;border:1px solid #FFFFFF;" ></td>
      <td  width="150px" style="background-color:#D7191C;border:1px solid #FFFFFF;" ></td>
      <td  width="150px" style="background-color:#377EB8;border:1px solid #FFFFFF;" ></td>
      <td  width="150px" style="background-color:#377EB8;border:1px solid #FFFFFF;" ></td>
      <td  width="150px" style="background-color:#D7191C;border:1px solid #FFFFFF;" ></td>
      <td  width="150px" style="background-color:#D7191C;border:1px solid #FFFFFF;" ></td>
      <td  width="150px" style="background-color:#FFD600;border:1px solid #FFFFFF;" ></td>
   </tr>
   <tr>
      <td width="150">
         <table cellspacing="6" cellpadding="4">
            <tr>
               <td bgcolor="#4DAF4A">Medium</td>
            </tr>
         </table>
      </td>
      <td width="150" >Banas Kantha-GUJARAT</td>
      <td  width="150px" style="background-color:#377EB8;border:1px solid #FFFFFF;" ></td>
      <td  width="150px" style="background-color:#D7191C;border:1px solid #FFFFFF;" ></td>
      <td  width="150px" style="background-color:#4DAF4A;border:1px solid #FFFFFF;" ></td>
      <td  width="150px" style="background-color:#D7191C;border:1px solid #FFFFFF;" ></td>
      <td  width="150px" style="background-color:#D7191C;border:1px solid #FFFFFF;" ></td>
      <td  width="150px" style="background-color:#D7191C;border:1px solid #FFFFFF;" ></td>
      <td  width="150px" style="background-color:#D7191C;border:1px solid #FFFFFF;" ></td>
      <td  width="150px" style="background-color:#D7191C;border:1px solid #FFFFFF;" ></td>
      <td  width="150px" style="background-color:#D7191C;border:1px solid #FFFFFF;" ></td>
      <td  width="150px" style="background-color:#D7191C;border:1px solid #FFFFFF;" ></td>
      <td  width="150px" style="background-color:#377EB8;border:1px solid #FFFFFF;" ></td>
      <td  width="150px" style="background-color:#D7191C;border:1px solid #FFFFFF;" ></td>
      <td  width="150px" style="background-color:#D7191C;border:1px solid #FFFFFF;" ></td>
      <td  width="150px" style="background-color:#D7191C;border:1px solid #FFFFFF;" ></td>
      <td  width="150px" style="background-color:#D7191C;border:1px solid #FFFFFF;" ></td>
      <td  width="150px" style="background-color:#D7191C;border:1px solid #FFFFFF;" ></td>
      <td  width="150px" style="background-color:#377EB8;border:1px solid #FFFFFF;" ></td>
      <td  width="150px" style="background-color:#FFD600;border:1px solid #FFFFFF;" ></td>
      <td  width="150px" style="background-color:#FFD600;border:1px solid #FFFFFF;" ></td>
      <td  width="150px" style="background-color:#FFD600;border:1px solid #FFFFFF;" ></td>
      <td  width="150px" style="background-color:#D7191C;border:1px solid #FFFFFF;" ></td>
      <td  width="150px" style="background-color:#D7191C;border:1px solid #FFFFFF;" ></td>
      <td  width="150px" style="background-color:#D7191C;border:1px solid #FFFFFF;" ></td>
      <td  width="150px" style="background-color:#D7191C;border:1px solid #FFFFFF;" ></td>
      <td  width="150px" style="background-color:#FFD600;border:1px solid #FFFFFF;" ></td>
   </tr>
   <tr>
      <td width="150">
         <table cellspacing="6" cellpadding="4">
            <tr>
               <td bgcolor="#377EB8">High</td>
            </tr>
         </table>
      </td>
      <td width="150" >Patan  -GUJARAT</td>
      <td  width="150px" style="background-color:#4DAF4A;border:1px solid #FFFFFF;" ></td>
      <td  width="150px" style="background-color:#4DAF4A;border:1px solid #FFFFFF;" ></td>
      <td  width="150px" style="background-color:#FFD600;border:1px solid #FFFFFF;" ></td>
      <td  width="150px" style="background-color:#D7191C;border:1px solid #FFFFFF;" ></td>
      <td  width="150px" style="background-color:#D7191C;border:1px solid #FFFFFF;" ></td>
      <td  width="150px" style="background-color:#D7191C;border:1px solid #FFFFFF;" ></td>
      <td  width="150px" style="background-color:#D7191C;border:1px solid #FFFFFF;" ></td>
      <td  width="150px" style="background-color:#D7191C;border:1px solid #FFFFFF;" ></td>
      <td  width="150px" style="background-color:#D7191C;border:1px solid #FFFFFF;" ></td>
      <td  width="150px" style="background-color:#FFD600;border:1px solid #FFFFFF;" ></td>
      <td  width="150px" style="background-color:#FFD600;border:1px solid #FFFFFF;" ></td>
      <td  width="150px" style="background-color:#FFD600;border:1px solid #FFFFFF;" ></td>
      <td  width="150px" style="background-color:#4DAF4A;border:1px solid #FFFFFF;" ></td>
      <td  width="150px" style="background-color:#4DAF4A;border:1px solid #FFFFFF;" ></td>
      <td  width="150px" style="background-color:#4DAF4A;border:1px solid #FFFFFF;" ></td>
      <td  width="150px" style="background-color:#4DAF4A;border:1px solid #FFFFFF;" ></td>
      <td  width="150px" style="background-color:#4DAF4A;border:1px solid #FFFFFF;" ></td>
      <td  width="150px" style="background-color:#FFD600;border:1px solid #FFFFFF;" ></td>
      <td  width="150px" style="background-color:#FFD600;border:1px solid #FFFFFF;" ></td>
      <td  width="150px" style="background-color:#FFD600;border:1px solid #FFFFFF;" ></td>
      <td  width="150px" style="background-color:#4DAF4A;border:1px solid #FFFFFF;" ></td>
      <td  width="150px" style="background-color:#4DAF4A;border:1px solid #FFFFFF;" ></td>
      <td  width="150px" style="background-color:#D7191C;border:1px solid #FFFFFF;" ></td>
      <td  width="150px" style="background-color:#D7191C;border:1px solid #FFFFFF;" ></td>
      <td  width="150px" style="background-color:#FFD600;border:1px solid #FFFFFF;" ></td>
   </tr>
   <tr>
      <td width="150">
         <table cellspacing="6" cellpadding="4">
            <tr>
               <td bgcolor="#D7191C">Very High</td>
            </tr>
         </table>
      </td>
      <td width="150" >Mahesana-GUJARAT</td>
      <td  width="150px" style="background-color:#FFD600;border:1px solid #FFFFFF;" ></td>
      <td  width="150px" style="background-color:#4DAF4A;border:1px solid #FFFFFF;" ></td>
      <td  width="150px" style="background-color:#FFD600;border:1px solid #FFFFFF;" ></td>
      <td  width="150px" style="background-color:#D7191C;border:1px solid #FFFFFF;" ></td>
      <td  width="150px" style="background-color:#D7191C;border:1px solid #FFFFFF;" ></td>
      <td  width="150px" style="background-color:#D7191C;border:1px solid #FFFFFF;" ></td>
      <td  width="150px" style="background-color:#D7191C;border:1px solid #FFFFFF;" ></td>
      <td  width="150px" style="background-color:#D7191C;border:1px solid #FFFFFF;" ></td>
      <td  width="150px" style="background-color:#D7191C;border:1px solid #FFFFFF;" ></td>
      <td  width="150px" style="background-color:#4DAF4A;border:1px solid #FFFFFF;" ></td>
      <td  width="150px" style="background-color:#4DAF4A;border:1px solid #FFFFFF;" ></td>
      <td  width="150px" style="background-color:#4DAF4A;border:1px solid #FFFFFF;" ></td>
      <td  width="150px" style="background-color:#4DAF4A;border:1px solid #FFFFFF;" ></td>
      <td  width="150px" style="background-color:#377EB8;border:1px solid #FFFFFF;" ></td>
      <td  width="150px" style="background-color:#4DAF4A;border:1px solid #FFFFFF;" ></td>
      <td  width="150px" style="background-color:#377EB8;border:1px solid #FFFFFF;" ></td>
      <td  width="150px" style="background-color:#377EB8;border:1px solid #FFFFFF;" ></td>
      <td  width="150px" style="background-color:#FFD600;border:1px solid #FFFFFF;" ></td>
      <td  width="150px" style="background-color:#FFD600;border:1px solid #FFFFFF;" ></td>
      <td  width="150px" style="background-color:#FFD600;border:1px solid #FFFFFF;" ></td>
      <td  width="150px" style="background-color:#377EB8;border:1px solid #FFFFFF;" ></td>
      <td  width="150px" style="background-color:#377EB8;border:1px solid #FFFFFF;" ></td>
      <td  width="150px" style="background-color:#D7191C;border:1px solid #FFFFFF;" ></td>
      <td  width="150px" style="background-color:#D7191C;border:1px solid #FFFFFF;" ></td>
      <td  width="150px" style="background-color:#FFD600;border:1px solid #FFFFFF;" ></td>
   </tr>
   <tr>
      <td width="150"></td>
      <td width="150" >Sabar Kantha-GUJARAT</td>
      <td  width="150px" style="background-color:#4DAF4A;border:1px solid #FFFFFF;" ></td>
      <td  width="150px" style="background-color:#D7191C;border:1px solid #FFFFFF;" ></td>
      <td  width="150px" style="background-color:#D7191C;border:1px solid #FFFFFF;" ></td>
      <td  width="150px" style="background-color:#D7191C;border:1px solid #FFFFFF;" ></td>
      <td  width="150px" style="background-color:#D7191C;border:1px solid #FFFFFF;" ></td>
      <td  width="150px" style="background-color:#D7191C;border:1px solid #FFFFFF;" ></td>
      <td  width="150px" style="background-color:#D7191C;border:1px solid #FFFFFF;" ></td>
      <td  width="150px" style="background-color:#D7191C;border:1px solid #FFFFFF;" ></td>
      <td  width="150px" style="background-color:#D7191C;border:1px solid #FFFFFF;" ></td>
      <td  width="150px" style="background-color:#D7191C;border:1px solid #FFFFFF;" ></td>
      <td  width="150px" style="background-color:#D7191C;border:1px solid #FFFFFF;" ></td>
      <td  width="150px" style="background-color:#D7191C;border:1px solid #FFFFFF;" ></td>
      <td  width="150px" style="background-color:#D7191C;border:1px solid #FFFFFF;" ></td>
      <td  width="150px" style="background-color:#D7191C;border:1px solid #FFFFFF;" ></td>
      <td  width="150px" style="background-color:#D7191C;border:1px solid #FFFFFF;" ></td>
      <td  width="150px" style="background-color:#D7191C;border:1px solid #FFFFFF;" ></td>
      <td  width="150px" style="background-color:#D7191C;border:1px solid #FFFFFF;" ></td>
      <td  width="150px" style="background-color:#377EB8;border:1px solid #FFFFFF;" ></td>
      <td  width="150px" style="background-color:#4DAF4A;border:1px solid #FFFFFF;" ></td>
      <td  width="150px" style="background-color:#D7191C;border:1px solid #FFFFFF;" ></td>
      <td  width="150px" style="background-color:#D7191C;border:1px solid #FFFFFF;" ></td>
      <td  width="150px" style="background-color:#D7191C;border:1px solid #FFFFFF;" ></td>
      <td  width="150px" style="background-color:#D7191C;border:1px solid #FFFFFF;" ></td>
      <td  width="150px" style="background-color:#D7191C;border:1px solid #FFFFFF;" ></td>
      <td  width="150px" style="background-color:#FFD600;border:1px solid #FFFFFF;" ></td>
   </tr>
   <tr>
      <td width="150"></td>
      <td width="150" >Gandhinagar-GUJARAT</td>
      <td  width="150px" style="background-color:#FFD600;border:1px solid #FFFFFF;" ></td>
      <td  width="150px" style="background-color:#FFD600;border:1px solid #FFFFFF;" ></td>
      <td  width="150px" style="background-color:#FFD600;border:1px solid #FFFFFF;" ></td>
      <td  width="150px" style="background-color:#D7191C;border:1px solid #FFFFFF;" ></td>
      <td  width="150px" style="background-color:#D7191C;border:1px solid #FFFFFF;" ></td>
      <td  width="150px" style="background-color:#D7191C;border:1px solid #FFFFFF;" ></td>
      <td  width="150px" style="background-color:#D7191C;border:1px solid #FFFFFF;" ></td>
      <td  width="150px" style="background-color:#D7191C;border:1px solid #FFFFFF;" ></td>
      <td  width="150px" style="background-color:#D7191C;border:1px solid #FFFFFF;" ></td>
      <td  width="150px" style="background-color:#FFD600;border:1px solid #FFFFFF;" ></td>
      <td  width="150px" style="background-color:#FFD600;border:1px solid #FFFFFF;" ></td>
      <td  width="150px" style="background-color:#4DAF4A;border:1px solid #FFFFFF;" ></td>
      <td  width="150px" style="background-color:#FFD600;border:1px solid #FFFFFF;" ></td>
      <td  width="150px" style="background-color:#FFD600;border:1px solid #FFFFFF;" ></td>
      <td  width="150px" style="background-color:#FFD600;border:1px solid #FFFFFF;" ></td>
      <td  width="150px" style="background-color:#4DAF4A;border:1px solid #FFFFFF;" ></td>
      <td  width="150px" style="background-color:#FFD600;border:1px solid #FFFFFF;" ></td>
      <td  width="150px" style="background-color:#FFD600;border:1px solid #FFFFFF;" ></td>
      <td  width="150px" style="background-color:#FFD600;border:1px solid #FFFFFF;" ></td>
      <td  width="150px" style="background-color:#FFD600;border:1px solid #FFFFFF;" ></td>
      <td  width="150px" style="background-color:#4DAF4A;border:1px solid #FFFFFF;" ></td>
      <td  width="150px" style="background-color:#4DAF4A;border:1px solid #FFFFFF;" ></td>
      <td  width="150px" style="background-color:#D7191C;border:1px solid #FFFFFF;" ></td>
      <td  width="150px" style="background-color:#D7191C;border:1px solid #FFFFFF;" ></td>
      <td  width="150px" style="background-color:#FFD600;border:1px solid #FFFFFF;" ></td>
   </tr>
   <tr>
      <td width="150"></td>
      <td width="150" >Ahmadabad-GUJARAT</td>
      <td  width="150px" style="background-color:#4DAF4A;border:1px solid #FFFFFF;" ></td>
      <td  width="150px" style="background-color:#4DAF4A;border:1px solid #FFFFFF;" ></td>
      <td  width="150px" style="background-color:#FFD600;border:1px solid #FFFFFF;" ></td>
      <td  width="150px" style="background-color:#D7191C;border:1px solid #FFFFFF;" ></td>
      <td  width="150px" style="background-color:#D7191C;border:1px solid #FFFFFF;" ></td>
      <td  width="150px" style="background-color:#D7191C;border:1px solid #FFFFFF;" ></td>
      <td  width="150px" style="background-color:#D7191C;border:1px solid #FFFFFF;" ></td>
      <td  width="150px" style="background-color:#D7191C;border:1px solid #FFFFFF;" ></td>
      <td  width="150px" style="background-color:#D7191C;border:1px solid #FFFFFF;" ></td>
      <td  width="150px" style="background-color:#FFD600;border:1px solid #FFFFFF;" ></td>
      <td  width="150px" style="background-color:#4DAF4A;border:1px solid #FFFFFF;" ></td>
      <td  width="150px" style="background-color:#FFD600;border:1px solid #FFFFFF;" ></td>
      <td  width="150px" style="background-color:#4DAF4A;border:1px solid #FFFFFF;" ></td>
      <td  width="150px" style="background-color:#4DAF4A;border:1px solid #FFFFFF;" ></td>
      <td  width="150px" style="background-color:#4DAF4A;border:1px solid #FFFFFF;" ></td>
      <td  width="150px" style="background-color:#4DAF4A;border:1px solid #FFFFFF;" ></td>
      <td  width="150px" style="background-color:#4DAF4A;border:1px solid #FFFFFF;" ></td>
      <td  width="150px" style="background-color:#4DAF4A;border:1px solid #FFFFFF;" ></td>
      <td  width="150px" style="background-color:#FFD600;border:1px solid #FFFFFF;" ></td>
      <td  width="150px" style="background-color:#FFD600;border:1px solid #FFFFFF;" ></td>
      <td  width="150px" style="background-color:#4DAF4A;border:1px solid #FFFFFF;" ></td>
      <td  width="150px" style="background-color:#4DAF4A;border:1px solid #FFFFFF;" ></td>
      <td  width="150px" style="background-color:#D7191C;border:1px solid #FFFFFF;" ></td>
      <td  width="150px" style="background-color:#D7191C;border:1px solid #FFFFFF;" ></td>
      <td  width="150px" style="background-color:#FFD600;border:1px solid #FFFFFF;" ></td>
   </tr>
   <tr>
      <td width="150"></td>
      <td width="150" >Surendranagar-GUJARAT</td>
      <td  width="150px" style="background-color:#377EB8;border:1px solid #FFFFFF;" ></td>
      <td  width="150px" style="background-color:#4DAF4A;border:1px solid #FFFFFF;" ></td>
      <td  width="150px" style="background-color:#FFD600;border:1px solid #FFFFFF;" ></td>
      <td  width="150px" style="background-color:#D7191C;border:1px solid #FFFFFF;" ></td>
      <td  width="150px" style="background-color:#D7191C;border:1px solid #FFFFFF;" ></td>
      <td  width="150px" style="background-color:#D7191C;border:1px solid #FFFFFF;" ></td>
      <td  width="150px" style="background-color:#D7191C;border:1px solid #FFFFFF;" ></td>
      <td  width="150px" style="background-color:#D7191C;border:1px solid #FFFFFF;" ></td>
      <td  width="150px" style="background-color:#D7191C;border:1px solid #FFFFFF;" ></td>
      <td  width="150px" style="background-color:#FFD600;border:1px solid #FFFFFF;" ></td>
      <td  width="150px" style="background-color:#4DAF4A;border:1px solid #FFFFFF;" ></td>
      <td  width="150px" style="background-color:#4DAF4A;border:1px solid #FFFFFF;" ></td>
      <td  width="150px" style="background-color:#4DAF4A;border:1px solid #FFFFFF;" ></td>
      <td  width="150px" style="background-color:#4DAF4A;border:1px solid #FFFFFF;" ></td>
      <td  width="150px" style="background-color:#4DAF4A;border:1px solid #FFFFFF;" ></td>
      <td  width="150px" style="background-color:#4DAF4A;border:1px solid #FFFFFF;" ></td>
      <td  width="150px" style="background-color:#FFD600;border:1px solid #FFFFFF;" ></td>
      <td  width="150px" style="background-color:#FFD600;border:1px solid #FFFFFF;" ></td>
      <td  width="150px" style="background-color:#FFD600;border:1px solid #FFFFFF;" ></td>
      <td  width="150px" style="background-color:#FFD600;border:1px solid #FFFFFF;" ></td>
      <td  width="150px" style="background-color:#4DAF4A;border:1px solid #FFFFFF;" ></td>
      <td  width="150px" style="background-color:#377EB8;border:1px solid #FFFFFF;" ></td>
      <td  width="150px" style="background-color:#D7191C;border:1px solid #FFFFFF;" ></td>
      <td  width="150px" style="background-color:#D7191C;border:1px solid #FFFFFF;" ></td>
      <td  width="150px" style="background-color:#FFD600;border:1px solid #FFFFFF;" ></td>
   </tr>
   <tr>
      <td width="150"></td>
      <td width="150" >Rajkot-GUJARAT</td>
      <td  width="150px" style="background-color:#377EB8;border:1px solid #FFFFFF;" ></td>
      <td  width="150px" style="background-color:#377EB8;border:1px solid #FFFFFF;" ></td>
      <td  width="150px" style="background-color:#FFD600;border:1px solid #FFFFFF;" ></td>
      <td  width="150px" style="background-color:#D7191C;border:1px solid #FFFFFF;" ></td>
      <td  width="150px" style="background-color:#D7191C;border:1px solid #FFFFFF;" ></td>
      <td  width="150px" style="background-color:#D7191C;border:1px solid #FFFFFF;" ></td>
      <td  width="150px" style="background-color:#D7191C;border:1px solid #FFFFFF;" ></td>
      <td  width="150px" style="background-color:#D7191C;border:1px solid #FFFFFF;" ></td>
      <td  width="150px" style="background-color:#D7191C;border:1px solid #FFFFFF;" ></td>
      <td  width="150px" style="background-color:#377EB8;border:1px solid #FFFFFF;" ></td>
      <td  width="150px" style="background-color:#377EB8;border:1px solid #FFFFFF;" ></td>
      <td  width="150px" style="background-color:#D7191C;border:1px solid #FFFFFF;" ></td>
      <td  width="150px" style="background-color:#377EB8;border:1px solid #FFFFFF;" ></td>
      <td  width="150px" style="background-color:#377EB8;border:1px solid #FFFFFF;" ></td>
      <td  width="150px" style="background-color:#377EB8;border:1px solid #FFFFFF;" ></td>
      <td  width="150px" style="background-color:#4DAF4A;border:1px solid #FFFFFF;" ></td>
      <td  width="150px" style="background-color:#4DAF4A;border:1px solid #FFFFFF;" ></td>
      <td  width="150px" style="background-color:#FFD600;border:1px solid #FFFFFF;" ></td>
      <td  width="150px" style="background-color:#FFD600;border:1px solid #FFFFFF;" ></td>
      <td  width="150px" style="background-color:#FFD600;border:1px solid #FFFFFF;" ></td>
      <td  width="150px" style="background-color:#377EB8;border:1px solid #FFFFFF;" ></td>
      <td  width="150px" style="background-color:#377EB8;border:1px solid #FFFFFF;" ></td>
      <td  width="150px" style="background-color:#D7191C;border:1px solid #FFFFFF;" ></td>
      <td  width="150px" style="background-color:#D7191C;border:1px solid #FFFFFF;" ></td>
      <td  width="150px" style="background-color:#FFD600;border:1px solid #FFFFFF;" ></td>
   </tr>
   <tr>
      <td width="150"></td>
      <td width="150" >Jamnagar-GUJARAT</td>
      <td  width="150px" style="background-color:#377EB8;border:1px solid #FFFFFF;" ></td>
      <td  width="150px" style="background-color:#4DAF4A;border:1px solid #FFFFFF;" ></td>
      <td  width="150px" style="background-color:#D7191C;border:1px solid #FFFFFF;" ></td>
      <td  width="150px" style="background-color:#D7191C;border:1px solid #FFFFFF;" ></td>
      <td  width="150px" style="background-color:#D7191C;border:1px solid #FFFFFF;" ></td>
      <td  width="150px" style="background-color:#D7191C;border:1px solid #FFFFFF;" ></td>
      <td  width="150px" style="background-color:#D7191C;border:1px solid #FFFFFF;" ></td>
      <td  width="150px" style="background-color:#D7191C;border:1px solid #FFFFFF;" ></td>
      <td  width="150px" style="background-color:#D7191C;border:1px solid #FFFFFF;" ></td>
      <td  width="150px" style="background-color:#4DAF4A;border:1px solid #FFFFFF;" ></td>
      <td  width="150px" style="background-color:#377EB8;border:1px solid #FFFFFF;" ></td>
      <td  width="150px" style="background-color:#377EB8;border:1px solid #FFFFFF;" ></td>
      <td  width="150px" style="background-color:#4DAF4A;border:1px solid #FFFFFF;" ></td>
      <td  width="150px" style="background-color:#4DAF4A;border:1px solid #FFFFFF;" ></td>
      <td  width="150px" style="background-color:#377EB8;border:1px solid #FFFFFF;" ></td>
      <td  width="150px" style="background-color:#4DAF4A;border:1px solid #FFFFFF;" ></td>
      <td  width="150px" style="background-color:#FFD600;border:1px solid #FFFFFF;" ></td>
      <td  width="150px" style="background-color:#4DAF4A;border:1px solid #FFFFFF;" ></td>
      <td  width="150px" style="background-color:#D7191C;border:1px solid #FFFFFF;" ></td>
      <td  width="150px" style="background-color:#4DAF4A;border:1px solid #FFFFFF;" ></td>
      <td  width="150px" style="background-color:#4DAF4A;border:1px solid #FFFFFF;" ></td>
      <td  width="150px" style="background-color:#4DAF4A;border:1px solid #FFFFFF;" ></td>
      <td  width="150px" style="background-color:#D7191C;border:1px solid #FFFFFF;" ></td>
      <td  width="150px" style="background-color:#D7191C;border:1px solid #FFFFFF;" ></td>
      <td  width="150px" style="background-color:#FFD600;border:1px solid #FFFFFF;" ></td>
   </tr>
   <tr>
      <td width="150"></td>
      <td width="150" >Porbandar -GUJARAT</td>
      <td  width="150px" style="background-color:#FFD600;border:1px solid #FFFFFF;" ></td>
      <td  width="150px" style="background-color:#FFD600;border:1px solid #FFFFFF;" ></td>
      <td  width="150px" style="background-color:#4DAF4A;border:1px solid #FFFFFF;" ></td>
      <td  width="150px" style="background-color:#D7191C;border:1px solid #FFFFFF;" ></td>
      <td  width="150px" style="background-color:#D7191C;border:1px solid #FFFFFF;" ></td>
      <td  width="150px" style="background-color:#D7191C;border:1px solid #FFFFFF;" ></td>
      <td  width="150px" style="background-color:#D7191C;border:1px solid #FFFFFF;" ></td>
      <td  width="150px" style="background-color:#D7191C;border:1px solid #FFFFFF;" ></td>
      <td  width="150px" style="background-color:#D7191C;border:1px solid #FFFFFF;" ></td>
      <td  width="150px" style="background-color:#FFD600;border:1px solid #FFFFFF;" ></td>
      <td  width="150px" style="background-color:#FFD600;border:1px solid #FFFFFF;" ></td>
      <td  width="150px" style="background-color:#FFD600;border:1px solid #FFFFFF;" ></td>
      <td  width="150px" style="background-color:#FFD600;border:1px solid #FFFFFF;" ></td>
      <td  width="150px" style="background-color:#FFD600;border:1px solid #FFFFFF;" ></td>
      <td  width="150px" style="background-color:#FFD600;border:1px solid #FFFFFF;" ></td>
      <td  width="150px" style="background-color:#FFD600;border:1px solid #FFFFFF;" ></td>
      <td  width="150px" style="background-color:#FFD600;border:1px solid #FFFFFF;" ></td>
      <td  width="150px" style="background-color:#D7191C;border:1px solid #FFFFFF;" ></td>
      <td  width="150px" style="background-color:#4DAF4A;border:1px solid #FFFFFF;" ></td>
      <td  width="150px" style="background-color:#FFD600;border:1px solid #FFFFFF;" ></td>
      <td  width="150px" style="background-color:#FFD600;border:1px solid #FFFFFF;" ></td>
      <td  width="150px" style="background-color:#FFD600;border:1px solid #FFFFFF;" ></td>
      <td  width="150px" style="background-color:#D7191C;border:1px solid #FFFFFF;" ></td>
      <td  width="150px" style="background-color:#D7191C;border:1px solid #FFFFFF;" ></td>
      <td  width="150px" style="background-color:#FFD600;border:1px solid #FFFFFF;" ></td>
   </tr>
</table>