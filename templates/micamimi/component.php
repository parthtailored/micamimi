<?php
/**
 * @package     Joomla.Site
 * @subpackage  Templates.micamimi
 *
 * @copyright   Copyright (C) 2005 - 2017 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

/** @var JDocumentHtml $this */

$app = JFactory::getApplication();


$this->setHtml5(true);// Output as HTML5
JHtml::_('bootstrap.framework');// Add JavaScript Frameworks
JHtml::_('script', 'jui/html5.js', array('version' => 'auto', 'relative' => true, 'conditional' => 'lt IE 9'));// Add html5 shiv

if($app->input->get('option') != 'com_content' && $app->input->get('view') != 'article') {
	JHtml::_('stylesheet', 'template.css', array('version' => 'auto', 'relative' => true));// Add Stylesheets
}

JHtmlBootstrap::loadCss($includeMaincss = false, $this->direction);// Load optional rtl Bootstrap css and Bootstrap bugfixes
?>
<!DOCTYPE html>
<html lang="<?php echo $this->language; ?>" dir="<?php echo $this->direction; ?>">
<head>
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
	<jdoc:include type="head" />
</head>
<body class="contentpane modal">
	<jdoc:include type="component" />
</body>
</html>
