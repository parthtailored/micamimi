<?php
/**
 * @package        Joomla
 * @subpackage     Membership Pro
 * @author         Tuan Pham Ngoc
 * @copyright      Copyright (C) 2012 - 2017 Ossolution Team
 * @license        GNU/GPL, see LICENSE.php
 */

class Pkg_OsmembershipInstallerScript
{
	private $installType = null;
	
	public function preflight($type, $parent)
	{
		if (!version_compare(JVERSION, '3.5.0', 'ge'))
		{
			JError::raiseWarning(null, 'Cannot install Membership Pro in a Joomla release prior to 3.5.0');

			return false;
		}
	}
	
	/**
	 * method to install the component
	 *
	 * @return void
	 */
	public function install($parent)
	{
		$this->installType = 'install';

	}

	public function update($parent)
	{
		$this->installType = 'upgrade';

	}
		
	public function postflight($type, $parent)
	{
		JFactory::getApplication()->redirect(
			JRoute::_('index.php?option=com_osmembership&task=upgrade&install_type=' . $this->installType, false));
	}
}