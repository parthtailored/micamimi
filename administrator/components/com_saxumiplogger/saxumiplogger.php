<?php
/**
 * $Id: saxumiplogger.php 206 2016-08-14 21:02:15Z Szablac $
 * @Project		Saxum IPLogger Extension/Component
 * @author 		Laszlo Szabo
 * @package		Saxum IPLogger
 * @copyright	Copyright (C) 2010 Saxum 2003 Bt. All rights reserved.
 * @license 	http://www.gnu.org/licenses/old-licenses/gpl-3.0.html GNU/GPL version 3
*/

defined('_JEXEC') or die('Restricted access');
 
// Access check.
if (!JFactory::getUser()->authorise('core.manage', 'com_saxumiplogger')) 
{
	return JError::raiseWarning(404, JText::_('JERROR_ALERTNOAUTHOR'));
}

// Register helper class
JLoader::register('UsersHelper', JPATH_ADMINISTRATOR.DIRECTORY_SEPARATOR. '/components/com_users/helpers/users.php');

JText::script("COM_SAXUMIPLOGGER_PHP_MAX_FILESIZE");
JText::script("COM_SAXUMIPLOGGER_PHP_POST_SIZE");
JText::script("COM_SAXUMIPLOGGER_SURE");

// Require the base controller
//require_once (JPATH_COMPONENT.DS.'controller.php');
jimport('joomla.application.component.controller');

// Require specific controller if requested
$controller = JControllerLegacy::getInstance('Saxumiplogger');

// Perform the Request task
$controller->execute(JFactory::getApplication()->input->get('task'));

// Redirect if set by the controller
$controller->redirect();

$document  = JFactory::GetDocument();
// add the javascript to actually "do" the ajax client-side
$document->addScriptDeclaration("
Joomla.submitbutton = function(pressbutton){
    switch (pressbutton){
		case 'purge': 
  			var answer = confirm(Joomla.JText._(\"COM_SAXUMIPLOGGER_SURE\"));
			if (answer){
			        Joomla.submitform(pressbutton);
			        return;
			}
			break;
    	case 'export':
        	Joomla.submitform(pressbutton);
  			document.adminForm.task.value='';
			break;
    	default:
        	Joomla.submitform(pressbutton);
        	return;
	}
};
");