<?php
/**
 * $Id: report.php 206 2016-08-14 21:02:15Z Szablac $
 * @Project		Saxum IPLogger Extension/Component
 * @author 		Laszlo Szabo
 * @package		Saxum IPLogger
 * @copyright	Copyright (C) 2010 Saxum 2003 Bt. All rights reserved.
 * @license 	http://www.gnu.org/licenses/old-licenses/gpl-3.0.html GNU/GPL version 3
*/

defined('_JEXEC') or die('Restricted access');

jimport( 'joomla.application.component.modellist' );

class SaxumiploggerModelReport extends JModelList
{
	public function __construct($config = array())
	{
		if (empty($config['filter_fields'])) {
			$config['filter_fields'] = array(
				'name', 'name',
				'username','username',
				'email','email',
				'ip','a.ip',
				'a.country_code, a.city','a.client_id'		
			);
		}

		parent::__construct($config);
	}

	protected function getListQuery()
	{
		// Create a new query object.		
		$db = $this->getDBO();
		$query = $db->getQuery(true);
		// Select some fields
		$query->select(
			$this->getState(
				'list.select',
				'a.*'
				)
			);
		$query->select('null as params');
		$query->from('#__saxum_iplogger as a');
		
		$query->join('LEFT','#__users as u ON a.user_id=u.id');
		$query->select('ifnull(u.username,\'COM_SAXUMIPLOGGER_GUEST\') as username');
		$query->select('ifnull(u.name,\'COM_SAXUMIPLOGGER_GUEST\') as name');
		$query->select('ifnull(u.email,\'\') as email');
		$query->select('u.id as userid');
	
		// Filter by search in title.
		$search = $this->getState('filter.search');
		if (!empty($search))
		{
			if (stripos($search, 'user:') === 0)
			{
				$search = $db->quote('%' . $db->escape(substr($search, 5), true) . '%');
				$query->where('(ifnull(u.username,\'COM_SAXUMIPLOGGER_GUEST\') LIKE ' . $search . ')');
			}
			elseif (stripos($search, 'name:') === 0)
			{
				$search = $db->quote('%' . $db->escape(substr($search, 5), true) . '%');
				$query->where('(ifnull(u.name,\'COM_SAXUMIPLOGGER_GUEST\') LIKE ' . $search . ')');
			}
			elseif (stripos($search, 'email:') === 0)
			{
				$search = $db->quote('%' . $db->escape(substr($search, 6), true) . '%');
				$query->where('(ifnull(u.email,\'\') LIKE ' . $search . ')');
			}
			elseif (stripos($search, 'ip:') === 0)
			{
				$search = $db->quote('%' . $db->escape(substr($search, 3), true) . '%');
				$query->where('(ip LIKE ' . $search . ')');
			}
			elseif (stripos($search, 'country:') === 0)
			{
				$search = $db->quote('%' . $db->escape(substr($search, 8), true) . '%');
				$query->where('(country_name LIKE ' . $search . ')');
			}
			else
			{
				$search = $db->quote('%' . $db->escape($search, true) . '%');
				$query->where('(ifnull(u.username,\'COM_SAXUMIPLOGGER_GUEST\') LIKE ' . $search . ')');
			}
		}
		
		// Filter by user
		$userId = $this->getState('filter.userid');
		if (is_numeric($userId)) {
			$query->where('a.user_id = '.(int) $userId);
		}
		
		$orderCol	= $this->state->get('list.ordering');
		$orderDirn	= $this->state->get('list.direction');
		$query->order($db->escape($orderCol.' '.$orderDirn));
		
		return $query;
	}
	
	protected function populateState($ordering = null, $direction = null) 
	{
		// Initialise variables.
		$app = JFactory::getApplication('administrator');

		$search = $this->getUserStateFromRequest($this->context . '.filter.search', 'filter_search');
		$this->setState('filter.search', $search);		
		
		// Load the filter state.
		$userId = $this->getUserStateFromRequest($this->context.'.filter.userid', 'filter_userid', '');
		$this->setState('filter.userid', $userId);
		
        parent::populateState('a.visitDate', 'desc');
	}
	
	function getUsers()
    {
		$query = 'SELECT id, username'
			. ' FROM #__users'
			. ' ORDER BY username';

        $this->_db->setQuery($query);
        $list=$this->_db->loadObjectList();
        $guest['id']=0;
        $guest['username']=JText::_( 'COM_SAXUMIPLOGGER_GUEST' );
        $list[]=$guest;
        return $list;
    }

	function delete()
    {
		$query = 'DELETE FROM #__saxum_iplogger';

        $this->_db->setQuery($query);
        return $this->_db->query();
    }
	
	function block($ip)
    {	
    	JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_saxumiplogger/tables');
    	$blocktable = JTable::getInstance('Block', 'SaxumiploggerTable');
    	$data['ip']=$ip;
        if(!$blocktable->bind($data)) return false;
        if(!$blocktable->save($data)) return false;
	        
	    return true;
    }

	function blockUser($userid)
    {
		$query = 'UPDATE #__users set block=1 where id='.$userid;//.' and usertype!=\'Super Administrator\'';

        $this->_db->setQuery($query);
        return $this->_db->query();
    }
	
	function getDailyLogins($userid)
    {
		$query = 'select count(1) as n from #__saxum_iplogger '
				.' where date_format(visitdate, \'%Y-%m-%d\')=date_format(now(), \'%Y-%m-%d\') '
				.' and user_id='.$userid;

        $this->_db->setQuery($query);
        return $this->_db->loadResult();
    }
	
	function getDailyIps($userid)
    {
		$query = 'select count(distinct ip) as n from #__saxum_iplogger '
				.' where date_format(visitdate, \'%Y-%m-%d\')=date_format(now(), \'%Y-%m-%d\') '
				.' and user_id='.$userid;

        $this->_db->setQuery($query);
        return $this->_db->loadResult();
    }
	
	function getDailyLocs($userid)
    {
		$query = 'select count(distinct ifnull(country_code,\'\'),ifnull(city,\'\')) as n from #__saxum_iplogger '
				.' where date_format(visitdate, \'%Y-%m-%d\')=date_format(now(), \'%Y-%m-%d\') '//-interval 1 day 
				.' and !(country_code=\'\' or country_code is null)' 
				.' and user_id='.$userid;

        $this->_db->setQuery($query);
        return $this->_db->loadResult();
    }

}
