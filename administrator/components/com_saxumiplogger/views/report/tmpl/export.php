<?php
/**
 * $Id: export.php 18 2013-12-19 12:26:36Z Szablac $
 * @Project		Saxum IPLogger Extension/Component
 * @author 		Laszlo Szabo
 * @package		Saxum IPLogger
 * @copyright	Copyright (C) 2010 Saxum 2003 Bt. All rights reserved.
 * @license 	http://www.gnu.org/licenses/old-licenses/gpl-3.0.html GNU/GPL version 3
*/

defined('_JEXEC') or die('Restricted access');

while(ob_get_level()>1) ob_end_clean();
ob_start();
$eol= "\r\n";
$separator = ";";
$columns=array();

$columns['user']	=	JText::_('COM_SAXUMIPLOGGER_USER');
$columns['name']	=	JText::_('COM_SAXUMIPLOGGER_NAME');
$columns['email']	=	JText::_('COM_SAXUMIPLOGGER_EMAIL');
$columns['ip']		=	JText::_('COM_SAXUMIPLOGGER_IP');
$columns['country']	=	JText::_('COM_SAXUMIPLOGGER_COUNTRY');
$columns['date']	=	JText::_('COM_SAXUMIPLOGGER_DATE');
$columns['client']	=	JText::_('COM_SAXUMIPLOGGER_CLIENT');

$output='';
foreach($columns as $column){
	if(!empty($column)){
		$output.= '"'.str_replace('"','\\'.'"',$column).'"';
	}
	$output.= $separator;
}
echo rtrim($output,$separator).$eol;

$output='';
if(!empty($this->items)){
	foreach($this->items as $row){
		echo '"'.JText::_($row->username).'"'.$separator;
		echo '"'.JText::_($row->name).'"'.$separator;
		echo '"'.$row->email.'"'.$separator;
		echo '"'.$row->ip.'"'.$separator;
		if(empty($row->country_code)){
			echo '"'.JText::_('COM_SAXUMIPLOGGER_UNKNOWN').'"'.$separator;
		} else
		{
			echo '"'.$row->country_name.", ".$row->city.'"'.$separator; 
		}
		$d=$this->params->get('date_format');
		if (empty($d)){$d='Y-m-d H:i:s';} 
		echo '"'.JHTML::_('date',$row->visitDate, $d).'"'.$separator; 
		$clientInfo = JApplicationHelper::getClientInfo($row->client_id);
		echo '"'.$clientInfo->name.'"'.$separator; 
		echo $eol;
	}
}

$data = ob_get_clean();
header("Pragma: public");
header("Expires: 0"); // set expiration time
header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
header("Content-Type: application/force-download");
header("Content-Type: application/octet-stream");
header("Content-Type: application/download");
header("Content-Disposition: attachment; filename=iploggerexport.csv;");
header("Content-Transfer-Encoding: binary");
header('Content-Length: '.strlen($data));
echo $data;
exit;
 
?>
