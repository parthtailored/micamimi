<?php
/**
 * $Id: default.php 35 2014-02-19 21:03:56Z Szablac $
 * @Project		Saxum IPLogger Extension/Component
 * @author 		Laszlo Szabo
 * @package		Saxum IPLogger
 * @copyright	Copyright (C) 2010 Saxum 2003 Bt. All rights reserved.
 * @license 	http://www.gnu.org/licenses/old-licenses/gpl-3.0.html GNU/GPL version 3
*/

defined('_JEXEC') or die('Restricted access');
JHtml::_('behavior.tooltip');
JHtml::_('formbehavior.chosen', 'select');

$version = new JVersion;
if ($version->getShortVersion()<'3.2.2'){
	$canDo = UsersHelper::getActions();
} else {
	$canDo = JHelperContent::getActions('com_users');
}
$loggeduser = JFactory::getUser();

$canDo2 = SaxumiploggerController::getActions();
$canBlock	= $canDo2->get('core.block');
$canRefresh	= $canDo2->get('core.refresh');

$listOrder	= $this->escape($this->state->get('list.ordering'));
$listDirn	= $this->escape($this->state->get('list.direction'));
$sortFields = $this->getSortFields();
?>
<script type="text/javascript">
	Joomla.orderTable = function()
	{
		table = document.getElementById("sortTable");
		direction = document.getElementById("directionTable");
		order = table.options[table.selectedIndex].value;
		if (order != '<?php echo $listOrder; ?>')
		{
			dirn = 'asc';
		}
		else
		{
			dirn = direction.options[direction.selectedIndex].value;
		}
		Joomla.tableOrdering(order, dirn, '');
	}
</script>

<form action="<?php echo JRoute::_('index.php?option=com_saxumiplogger&view=report');?>" method="post" name="adminForm" id="adminForm">
<?php if (!empty( $this->sidebar)) : ?>
	<div id="j-sidebar-container" class="span2">
		<?php echo $this->sidebar; ?>
	</div>
	<div id="j-main-container" class="span10">
<?php else : ?>
	<div id="j-main-container">
<?php endif;?>
		<div id="filter-bar" class="btn-toolbar">
			<div class="filter-search btn-group pull-left">
				<label for="filter_search" class="element-invisible"><?php echo JText::_('COM_SAXUMIPLOGGER_FILTER_SEARCH_DESC'); ?></label>
				<input type="text" name="filter_search" id="filter_search" placeholder="<?php echo JText::_('JSEARCH_FILTER'); ?>" value="<?php echo $this->escape($this->state->get('filter.search')); ?>" class="hasTooltip" title="<?php echo JHtml::tooltipText('COM_SAXUMIPLOGGER_FILTER_SEARCH_DESC'); ?>" />
			</div>
			<div class="btn-group pull-left hidden-phone">
				<button type="submit" class="btn hasTooltip" title="<?php echo JHtml::tooltipText('JSEARCH_FILTER_SUBMIT'); ?>"><i class="icon-search"></i></button>
				<button type="button" class="btn hasTooltip" title="<?php echo JHtml::tooltipText('JSEARCH_FILTER_CLEAR'); ?>" onclick="document.id('filter_search').value='';this.form.submit();"><i class="icon-remove"></i></button>
			</div>
			<div class="btn-group pull-right hidden-phone">
				<label for="limit" class="element-invisible"><?php echo JText::_('JFIELD_PLG_SEARCH_SEARCHLIMIT_DESC'); ?></label>
				<?php echo $this->pagination->getLimitBox(); ?>
			</div>
			<div class="btn-group pull-right hidden-phone">
				<label for="directionTable" class="element-invisible"><?php echo JText::_('JFIELD_ORDERING_DESC'); ?></label>
				<select name="directionTable" id="directionTable" class="input-medium" onchange="Joomla.orderTable()">
					<option value=""><?php echo JText::_('JFIELD_ORDERING_DESC'); ?></option>
					<option value="asc" <?php if ($listDirn == 'asc') echo 'selected="selected"'; ?>><?php echo JText::_('JGLOBAL_ORDER_ASCENDING'); ?></option>
					<option value="desc" <?php if ($listDirn == 'desc') echo 'selected="selected"'; ?>><?php echo JText::_('JGLOBAL_ORDER_DESCENDING');  ?></option>
				</select>
			</div>
			<div class="btn-group pull-right">
				<label for="sortTable" class="element-invisible"><?php echo JText::_('JGLOBAL_SORT_BY'); ?></label>
				<select name="sortTable" id="sortTable" class="input-medium" onchange="Joomla.orderTable()">
					<option value=""><?php echo JText::_('JGLOBAL_SORT_BY');?></option>
					<?php echo JHtml::_('select.options', $sortFields, 'value', 'text', $listOrder); ?>
				</select>
			</div>
		</div>
		<div class="clearfix"> </div>
	<table class="table table-striped" id="reportList">
	<thead>
		<tr>
			<?php if ($canRefresh) {?>
			<th width="1%" class="hidden-phone"> 
				<?php echo JHtml::_('grid.checkall'); ?>
			</th>
			<?php }?>
			<th>
				<?php echo JHTML::_('grid.sort', JText::_('COM_SAXUMIPLOGGER_USER'), 'username', $this->sortDirection, $this->sortColumn); ?>
			</th>
			<th>
				<?php echo JHTML::_('grid.sort', JText::_('COM_SAXUMIPLOGGER_NAME'), 'name', $this->sortDirection, $this->sortColumn); ?>
			</th>
			<th>
				<?php echo JHTML::_('grid.sort', JText::_('COM_SAXUMIPLOGGER_EMAIL'), 'email', $this->sortDirection, $this->sortColumn); ?>
			</th>
			<th width="200">
				<?php echo JHTML::_('grid.sort', JText::_('COM_SAXUMIPLOGGER_IP'), 'a.ip', $this->sortDirection, $this->sortColumn); ?>
			</th>			
			<th width="200">
				<?php echo JHTML::_('grid.sort', JText::_('COM_SAXUMIPLOGGER_COUNTRY'), 'a.country_code, a.city', $this->sortDirection, $this->sortColumn); ?>
			</th>			
			<th width="120">
				<?php echo JHTML::_('grid.sort', JText::_('COM_SAXUMIPLOGGER_DATE'), 'a.visitDate', $this->sortDirection, $this->sortColumn); ?>
			</th>
			<?php
			if ($this->params->get('client_to_log')==0 ){
			echo '<th width="100">';
			echo JHTML::_('grid.sort', JText::_('COM_SAXUMIPLOGGER_CLIENT'), 'a.client_id', $this->sortDirection, $this->sortColumn); 
			echo '</th>';
			}
			?>
		</tr>			
	</thead>
	<?php
	$k = 0;
	for ($i=0, $n=count( $this->items ); $i < $n; $i++)
	{
		$row = &$this->items[$i];
		$canEdit	= $canDo->get('core.edit');
		$canChange	= $loggeduser->authorise('core.edit.state',	'com_users');
		// If this group is super admin and this user is not super admin, $canEdit is false
		if ((!$loggeduser->authorise('core.admin')) && JAccess::check($row->id, 'core.admin')) {
			$canEdit	= false;
			$canChange	= false;
		}
		?>
		<tr class="<?php echo "row$k"; ?>">
			<?php if ($canRefresh){?>
			<td>
				<?php echo JHtml::_('grid.id', $i, $row->id); ?>
			</td>
			<?php }?>
			<td>
			<?php if ($canEdit && (int)$row->userid!=0) : ?>
			<a href="<?php echo JRoute::_('index.php?option=com_users&task=user.edit&id='.(int) $row->userid); ?>">
				<?php echo JText::_($row->username); ?></a>
			<?php else : ?>
				<?php echo JText::_($row->username); ?>
			<?php endif; ?>
			</td>
			<td>
			<?php if ($canEdit && (int)$row->userid!=0) : ?>
			<a href="<?php echo JRoute::_('index.php?option=com_users&task=user.edit&id='.(int) $row->userid); ?>">
				<?php echo JText::_($row->name); ?></a>
			<?php else : ?>
				<?php echo JText::_($row->name); ?>
			<?php endif; ?>
			</td>
			<td>
				<?php echo JText::_($row->email); ?>
			</td>
			<td>
			<?php if ($canBlock) : ?>
				<a href="<?php echo JRoute::_('index.php?option=com_saxumiplogger&task=block&ip='.$row->ip); ?>">
				<?php echo JHTML::_('image', 'administrator/components/com_saxumiplogger/assets/images/icon-16-block.png', JText::_('COM_SAXUMIPLOGGER_BLOCK'));
					 ?></a>		
			<?php endif; ?>	
			<?php echo $row->ip; ?>
			</td>
			<td>
				<?php 
				if(empty($row->country_code)){
					echo JText::_('COM_SAXUMIPLOGGER_UNKNOWN');
				} else
				{
					echo JHTML::_('image', 'administrator/components/com_saxumiplogger/assets/images/flags/'.$row->country_code.'.png', $row->country_code);
					echo " ".$row->country_name; 
					echo ", ".$row->city; 
				}?>
			</td>
			<td>
				<?php
				$d=$this->params->get('date_format');
				if (empty($d)){$d='Y-m-d H:i:s';} 
				echo JHTML::_('date',$row->visitDate, $d); 
				?>
			</td>
			<?php
			if ($this->params->get('client_to_log')==0 ){
				$clientInfo = JApplicationHelper::getClientInfo($row->client_id);
				echo '<td>';
				echo $clientInfo->name;
				echo '</td>';
				}
			?>
		</tr>
		<?php
		$k = 1 - $k;
	}
	?>
	<tfoot>
			<tr>
				<td colspan="10">
					<?php echo $this->pagination->getListFooter(); 
					?>
				</td>
			</tr>
		</tfoot>
	</table>
	<input type="hidden" name="option" value="com_saxumiplogger" />
	<input type="hidden" name="task" value="" />
	<input type="hidden" name="boxchecked" value="0" />
	<input type="hidden" name="layout" value="default" />
	<input type="hidden" name="controller" value="" />
	<input type="hidden" name="filter_order" value="<?php echo $this->sortColumn; ?>" />
	<input type="hidden" name="filter_order_Dir" value="<?php echo $this->sortDirection; ?>" />
	</div>
</form>
<?php 
include_once(JPATH_COMPONENT.DIRECTORY_SEPARATOR.'footer.php'); 
?>
