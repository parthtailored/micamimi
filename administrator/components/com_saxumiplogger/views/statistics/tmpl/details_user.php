<?php
/**
 * $Id: details_user.php 10 2013-11-10 22:53:48Z Szablac $
 * @Project		Saxum IPLogger Extension/Component
 * @author 		Laszlo Szabo
 * @package		Saxum IPLogger
 * @copyright	Copyright (C) 2010 Saxum 2003 Bt. All rights reserved.
 * @license 	http://www.gnu.org/licenses/old-licenses/gpl-3.0.html GNU/GPL version 3
*/

defined('_JEXEC') or die('Restricted access');
JHtml::_('behavior.tooltip');
JHtml::_('formbehavior.chosen', 'select');

$listOrder	= $this->escape($this->state->get('list.ordering'));
$listDirn	= $this->escape($this->state->get('list.direction'));
$sortFields = $this->getSortFields();
?>
<script type="text/javascript">
	Joomla.orderTable = function()
	{
		table = document.getElementById("sortTable");
		direction = document.getElementById("directionTable");
		order = table.options[table.selectedIndex].value;
		if (order != '<?php echo $listOrder; ?>')
		{
			dirn = 'asc';
		}
		else
		{
			dirn = direction.options[direction.selectedIndex].value;
		}
		Joomla.tableOrdering(order, dirn, '');
	}
</script>

<form action="index.php?option=com_saxumiplogger&task=details&user_id=<?php echo JRequest::getCmd('user_id').'&mode='.JRequest::getCmd('mode')?>" method="post" name="adminForm" id="adminForm">
<?php if (!empty( $this->sidebar)) : ?>
	<div id="j-sidebar-container" class="span2">
		<?php echo $this->sidebar; ?>
	</div>
	<div id="j-main-container" class="span10">
<?php else : ?>
	<div id="j-main-container">
<?php endif;?>
		<div id="filter-bar" class="btn-toolbar">
			<div class="filter-search btn-group pull-left">
				<label for="filter_search" class="element-invisible"><?php echo JText::_('COM_SAXUMIPLOGGER_FILTER_SEARCH_DESC'); ?></label>
				<input type="text" name="filter_search" id="filter_search" placeholder="<?php echo JText::_('JSEARCH_FILTER'); ?>" value="<?php echo $this->escape($this->state->get('filter.search')); ?>" class="hasTooltip" title="<?php echo JHtml::tooltipText('COM_SAXUMIPLOGGER_FILTER_SEARCH_DESC'); ?>" />
			</div>
			<div class="btn-group pull-left hidden-phone">
				<button type="submit" class="btn hasTooltip" title="<?php echo JHtml::tooltipText('JSEARCH_FILTER_SUBMIT'); ?>"><i class="icon-search"></i></button>
				<button type="button" class="btn hasTooltip" title="<?php echo JHtml::tooltipText('JSEARCH_FILTER_CLEAR'); ?>" onclick="document.id('filter_search').value='';this.form.submit();"><i class="icon-remove"></i></button>
			</div>
			<div class="btn-group pull-right hidden-phone">
				<label for="limit" class="element-invisible"><?php echo JText::_('JFIELD_PLG_SEARCH_SEARCHLIMIT_DESC'); ?></label>
				<?php echo $this->pagination->getLimitBox(); ?>
			</div>
			<div class="btn-group pull-right hidden-phone">
				<label for="directionTable" class="element-invisible"><?php echo JText::_('JFIELD_ORDERING_DESC'); ?></label>
				<select name="directionTable" id="directionTable" class="input-medium" onchange="Joomla.orderTable()">
					<option value=""><?php echo JText::_('JFIELD_ORDERING_DESC'); ?></option>
					<option value="asc" <?php if ($listDirn == 'asc') echo 'selected="selected"'; ?>><?php echo JText::_('JGLOBAL_ORDER_ASCENDING'); ?></option>
					<option value="desc" <?php if ($listDirn == 'desc') echo 'selected="selected"'; ?>><?php echo JText::_('JGLOBAL_ORDER_DESCENDING');  ?></option>
				</select>
			</div>
			<div class="btn-group pull-right">
				<label for="sortTable" class="element-invisible"><?php echo JText::_('JGLOBAL_SORT_BY'); ?></label>
				<select name="sortTable" id="sortTable" class="input-medium" onchange="Joomla.orderTable()">
					<option value=""><?php echo JText::_('JGLOBAL_SORT_BY');?></option>
					<?php echo JHtml::_('select.options', $sortFields, 'value', 'text', $listOrder); ?>
				</select>
			</div>
		</div>
		<div class="clearfix"> </div>
		<table class="table table-striped" id="user_details">
		<thead>
			<tr>
				<th>
					<?php echo JHTML::_('grid.sort', JText::_('COM_SAXUMIPLOGGER_USER'), 'user', $this->sortDirection, $this->sortColumn); ?>
				</th>
				<th>
					<?php echo JHTML::_('grid.sort', JText::_('COM_SAXUMIPLOGGER_NAME'), 'name', $this->sortDirection, $this->sortColumn); ?>
				</th>
				<th width="100">
					<?php echo JHTML::_('grid.sort', JText::_('COM_SAXUMIPLOGGER_'.JRequest::getCmd('mode')), 'mode', $this->sortDirection, $this->sortColumn); ?>
				</th>			
				<th width="100">
					<?php echo JHTML::_('grid.sort', JText::_('COM_SAXUMIPLOGGER_NUM_OF_OCCURENCES'), 'num', $this->sortDirection, $this->sortColumn); ?>
				</th>	
			</tr>			
		</thead>
		<?php
		$k = 0;
		for ($i=0, $n=count( $this->items ); $i < $n; $i++)
		{
			$row = &$this->items[$i];
			?>
			<tr class="<?php echo "row$k"; ?>">
				<td>
					<?php echo  JText::_($row->username); ?>
				</td>
				<td>
					<?php echo  JText::_($row->name); ?>
				</td>
				<td align=center>
					<?php 
					if (substr($row->mode,0,8)=="TOCHANGE") {
						echo JHTML::_('image', 'administrator/components/com_saxumiplogger/assets/images/flags/'.substr(str_replace("TOCHANGE","",$row->mode),0,2).'.png', substr(str_replace("TOCHANGE","",$row->mode),0,2));
						echo " ";
						echo substr(str_replace("TOCHANGE","",$row->mode),3);
					} else {
						echo $row->mode;
					}
					?>
				</td>
				<td align=center>
					<?php echo $row->num; ?>
				</td>
			</tr>
			<?php
			$k = 1 - $k;
		}
		?>
		<tfoot>
				<tr>
					<td colspan="10">
						<?php 
						echo $this->pagination->getListFooter(); 
						?>
					</td>
				</tr>
			</tfoot>
		</table>
	</div>
<input type="hidden" name="option" value="com_saxumiplogger" />
<input type="hidden" name="task" value="details" />
<input type="hidden" name="controller" value="" />
<input type="hidden" name="filter_order" value="<?php echo $this->sortColumn; ?>" />
<input type="hidden" name="filter_order_Dir" value="<?php echo $this->sortDirection; ?>" />
</form>
<?php 
include_once(JPATH_COMPONENT.DIRECTORY_SEPARATOR.'footer.php'); 
?>
