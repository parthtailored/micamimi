<?php
/**
 * $Id: edit.php 138 2015-11-25 21:22:15Z Szablac $
 * @Project		Saxum IPLogger Extension/Component
 * @author 		Laszlo Szabo
 * @package		Saxum IPLogger
 * @copyright	Copyright (C) 2010 Saxum 2003 Bt. All rights reserved.
 * @license 	http://www.gnu.org/licenses/old-licenses/gpl-3.0.html GNU/GPL version 3
*/

defined('_JEXEC') or die('Restricted access');
JHtml::_('behavior.tooltip');
JHTML::_('behavior.formvalidation');
JHtml::_('formbehavior.chosen', 'select');
?>

<form action="<?php echo JRoute::_('index.php?option=com_saxumiplogger&layout=edit&id='.(int) $this->item->id); ?>"
      method="post" name="adminForm" id="adminForm">
	<input type="hidden" name="check" value="post"/>
	<div class="row-fluid">
		<!-- Begin Content -->
		<div class="span10 form-horizontal">
			<div class="row-fluid">
				<div>
					<div class="control-group">
						<?php echo $this->form->getLabel('id'); ?>
						<div class="controls">
							<?php echo $this->form->getInput('id'); ?>
						</div>
					</div>
					<div class="control-group">
						<?php echo $this->form->getLabel('type'); ?>
						<div class="controls">
							<?php echo $this->form->getInput('type'); ?>
						</div>
					</div>
					<div class="control-group">
						<?php echo $this->form->getLabel('description'); ?>
						<div class="controls">
							<?php echo $this->form->getInput('description'); ?>
						</div>
					</div>
					<div class="control-group">
						<?php echo $this->form->getLabel('subject'); ?>
						<div class="controls">
							<?php echo $this->form->getInput('subject'); ?>
						</div>
						
						<div class="readonly" style="float:right;">
						<?php echo JText::_( 'COM_SAXUMIPLOGGER_MESSAGE_BODY_SUBS' ); ?>:
						<ul>
						  <li>[sitename]</li>
						  <li>[userloginname]</li>
						  <li>[username]</li>
						  <li>[num_of_logins]</li>
						</ul>
						</div>
					</div>

					<div class="control-group">
						<?php echo $this->form->getLabel('body'); ?>
						<div class="controls">
							<?php echo $this->form->getInput('body'); ?>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
<div>
	<input type="hidden" name="task" value="" />
	<?php echo JHtml::_('form.token'); ?>
</div>
</form>
<?php 
include_once(JPATH_COMPONENT.DIRECTORY_SEPARATOR.'footer.php'); 
?>
