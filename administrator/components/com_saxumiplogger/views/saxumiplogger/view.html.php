<?php
/**
 * $Id: view.html.php 18 2013-12-19 12:26:36Z Szablac $
 * @Project		Saxum IPLogger Extension/Component
 * @author 		Laszlo Szabo
 * @package		Saxum IPLogger
 * @copyright	Copyright (C) 2010 Saxum 2003 Bt. All rights reserved.
 * @license 	http://www.gnu.org/licenses/old-licenses/gpl-3.0.html GNU/GPL version 3
*/

defined('_JEXEC') or die('Restricted access');

jimport( 'joomla.application.component.view' );

class SaxumiploggerViewSaxumiplogger extends JViewLegacy
{
	function display($tpl = null)
	{
		SaxumiploggerController::addSubmenu('controlpanel');
				
        $model = $this->getModel();
        
        $plugins = $model->getPlugins();
        
		$this->assignRef('plugins',$plugins);
		
		// Set the toolbar
		$this->addToolBar();
		$this->sidebar = JHtmlSidebar::render();
        
		parent::display($tpl);
	}
	
	protected function addToolBar() 
	{	
		$canDo = SaxumiploggerController::getActions();
		JHtml::stylesheet( 'administrator/components/com_saxumiplogger/saxumiplogger.css'  );
		JToolBarHelper::title(JText::_( 'COM_SAXUMIPLOGGER' ).' - '.JText::_( 'COM_SAXUMIPLOGGER_MANAGER' ), 'iplogger' );
		
		if ($canDo->get('core.admin')) 
		{
			JToolBarHelper::preferences('com_saxumiplogger', '460');
		}
	}
}
