<?php
/**
 * $Id: view.html.php 10 2013-11-10 22:53:48Z Szablac $
 * @Project		Saxum IPLogger Extension/Component
 * @author 		Laszlo Szabo
 * @package		Saxum IPLogger
 * @copyright	Copyright (C) 2010 Saxum 2003 Bt. All rights reserved.
 * @license 	http://www.gnu.org/licenses/old-licenses/gpl-3.0.html GNU/GPL version 3
*/

defined('_JEXEC') or die();

jimport( 'joomla.application.component.view' );

class SaxumiploggerViewEmails extends JViewLegacy
{
	protected $items;
	protected $pagination;
	protected $state;
	
	function display($tpl = null) 
	{
		$this->items		= $this->get('Items');
		$this->pagination	= $this->get('Pagination');
		$this->state		= $this->get('State');
		SaxumiploggerController::addSubmenu('emails');
 
		// Check for errors.
		if (count($errors = $this->get('Errors'))) {
			JError::raiseError(500, implode("\n", $errors));
			return false;
		}
		
		// Set the toolbar
		$this->addToolBar();
		$this->sidebar = JHtmlSidebar::render();
 
		// Display the template
		parent::display($tpl);
	}
	
	protected function addToolBar() 
	{
		$canDo = SaxumiploggerController::getActions();
		JHTML::stylesheet( 'administrator/components/com_saxumiplogger/saxumiplogger.css' );
		JToolBarHelper::title(JText::_( 'COM_SAXUMIPLOGGER' ).' - '.JText::_('COM_SAXUMIPLOGGER_EMAILS'),'email.png');
		if ($canDo->get('core.email.edit')) 
		{
			JToolBarHelper::editList('email.edit');
			JToolBarHelper::divider();
		}
		JToolBarHelper::cancel( 'cancel', 'Close' );
	}
	
	protected function getSortFields()
	{
		return array(
			'a.id' => JText::_('JGRID_HEADING_ID'),
			'a.type' => JText::_('COM_SAXUMIPLOGGER_TYPE'),
			'a.description' => JText::_('COM_SAXUMIPLOGGER_DESCR')
		);
	}
	
}
?>
