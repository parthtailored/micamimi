<?php
/**
 * @package     Joomla.Administrator
 * @subpackage  com_mica
 *
 * @copyright   Copyright (C) 2005 - 2017 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

$app    = JFactory::getApplication(); ?>

<script type='text/JavaScript'>
	jQuery(document).ready(function($) {
		jQuery(document).on('change', '.toggle_variables', function(event) {
			event.preventDefault();
			var name = jQuery(this).attr('id').split("_section")[0];
			if(this.checked) {
				jQuery(".attr_"+name).prop("checked", true);
			}else{
				jQuery(".attr_"+name).prop("checked", false);
			}
		});
	});
</script>
<form  name="adminForm" id="adminForm" method="post" action ="<?php echo JRoute::_('index.php?option=com_mica&view=industrylevelgrouping'); ?>" enctype="multipart/form-data">
	<?php if (!empty( $this->sidebar)) : ?>
		<div id="j-sidebar-container" class="span2">
			<?php echo $this->sidebar; ?>
		</div>
		<div id="j-main-container" class="span10">
	<?php else : ?>
		<div id="j-main-container">
	<?php endif; ?>

		<table class='table table-striped adminlist' >
			<tbody>
				<tr>
					<td width="10%" class="nowrap right" style="text-align: right;"><?php echo JText::_('Group Name'); ?></td>
					<td width="30%" class="nowrap left" style="text-align: left;">
						<input type="text" id='groupname' name="groupname" value="<?php echo $this->groupdetails['name']; ?>" />
					</td>
					<td width="10%" class="nowrap right" style="text-align: right;"><?php echo JText::_('Icon'); ?></td>
					<td width="30%" class="nowrap left" style="text-align: left;">
						<?php
						if ($this->groupdetails['icon'] != "")
						{
							?>
							<img src="<?php echo JUri::root() . "components/com_mica/images/" . $this->groupdetails['icon']; ?>" />
						<?php
						} ?>
						<input type="file" name="iconimage" />
					</td>
					<td width="10%" class="nowrap right" style="text-align: right;"><?php echo JText::_('Delete Image'); ?></td>
					<td width="10%" class="nowrap left" style="text-align: left;">
						<input type="checkbox" name="deleteimage" value="1" />
						<input type="hidden" name="oldimage" value="<?php echo $this->groupdetails['icon']; ?>"></td>
					</td>
				</tr>

				<tr>
					<td width="10%" class="nowrap right" style="text-align: right;"><?php echo JText::_('Group Description'); ?></td>
					<td width="30%" class="nowrap left" style="text-align: left;">
						<textarea name="groupDescription" cols="50" rows="5"><?php echo $this->groupdetails['groupDescription']; ?></textarea>
					</td>
				</tr>

				<tr>
					<td valign="top" width="1%" class="nowrap left">
						<input type="checkbox" name="type[]" value="Rural" <?php echo in_array('Rural', $this->groupdetails['type']) ? 'checked' : '';?>>
						Rural
						<input type="checkbox" name="type[]" value="Urban" <?php echo in_array('Urban', $this->groupdetails['type']) ? 'checked' : '';?>>
						Urban
						<input type="checkbox" name="type[]" value="Total" <?php echo in_array('Total', $this->groupdetails['type']) ? 'checked' : '';?>>All
					</td>
				</tr>
			</tbody>
		</table>

		<?php $i = 0;
		$avoid_variables  = array("OGR_FID", "india_state", "layer", "id","name", "StateName", "LocationID", "DistrictName", "india_information", "distshp", "distcensus", "first_name", "first_iso");
		$avoid_variables2 = array("state", "name_2", "censucode", "first_na_1", "UA_Name", "UA_Name", "india_city", "place_name", "country", "place", "TownName");
		$avoid_variables  = array_merge($avoid_variables, $avoid_variables2);

		foreach ($this->allfields as $field_section_title => $each_section_fields)
		{
			$selected_fields = (array_key_exists($field_section_title, $this->groupdetails['fields'])) ? $this->groupdetails['fields'][$field_section_title] : array();
			?>
			<table class='table table-striped adminlist <?php echo strtolower($field_section_title) . "_section"; ?>' >
				<thead>
					<th colspan="6" class="nowrap center">
						<?php echo $field_section_title . " Variables"; ?>
					</th>
					<th width="25px" class="nowrap right">
						<input type="checkbox" value="" id="<?php echo strtolower($field_section_title) . "_section"; ?>" class="toggle_variables"/>
					</th>
					<th width="200px" class="nowrap right">
						<?php echo JText::_('Check/Uncheck All'); ?>
					</th>
				</thead>
				<tbody>
					<?php $column = 0;
					foreach ($each_section_fields as $key => $each_field)
					{
						if (!in_array($each_field, $avoid_variables))
						{
							$checked = (in_array($each_field, $selected_fields)) ? " checked " : "";

							if ($column == 0 ) { echo "<tr>"; } ?>
							<td valign="top" width="1%" class="nowrap left">
								<input type="checkbox" id="cb<?php echo $i?>" class="attr_<?php echo strtolower($field_section_title); ?>"
									name="attr_<?php echo $field_section_title; ?>[]" value="<?php echo $each_field ?>" <?php echo $checked; ?> />
							</td>
							<td valign="top" width="18%" class="nowrap left">
								<?php echo $each_field; ?>
							</td>
							<?php if ($column == 3 ) { echo "<tr>"; }
							$column = ($column == 3) ? 0 : ($column + 1);
							$i++;
						}
					}

					$extra_colspans = 8;
					if ($column == 0) { echo "<tr><td colspan='".($extra_colspans - ($column * 2))."'></td></tr>"; }
					if ($column > 0 & $column < 4 ) { echo "<td  colspan='".($extra_colspans - ($column * 2))."'></td></tr>"; }
					?>
				</tbody>
			</table>
		<?php
		} ?>

		<input type='hidden' name='task' value='' />
		<input type='hidden' name='id' value='<?php echo $this->groupdetails['id']; ?>' />
		<?php echo JHtml::_('form.token'); ?>
		</div>
</form>
