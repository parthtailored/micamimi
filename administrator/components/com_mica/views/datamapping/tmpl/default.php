<?php
/**
 * @package     Joomla.Administrator
 * @subpackage  com_mica
 *
 * @copyright   Copyright (C) 2005 - 2017 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die; ?>

<style type="text/css">
	#facebook{
		margin-top:30px;
		float:left;
	}
	.facebook_block{
		background-color:#9FC0FF;
		border:2px solid #3B5998;
		float:left;
		height:30px;
		margin-left:5px;
		width:8px;
	    opacity:0.1;
		-webkit-transform:scale(0.7);
		-webkit-animation-name: facebook;
	 	-webkit-animation-duration: 1s;
	 	-webkit-animation-iteration-count: infinite;
	 	-webkit-animation-direction: linear;
	}
	#block_1{
	 	-webkit-animation-delay: .1s;
	}
	#block_2{
	 	-webkit-animation-delay: .2s;
	}
	#block_3{
	 	-webkit-animation-delay: .3s;
	}
	#block_4{
	 	-webkit-animation-delay: .4s;
	}
	#block_5{
	 	-webkit-animation-delay: .5s;
	}
	#block_6{
	 	-webkit-animation-delay: .6s;
	}
	#loadingDiv{
	  position:fixed;
	  top:0px;
	  right:0px;
	  width:100%;
	  height:100%;
	  background-color:#666;
	  background-image:url('components/com_mica/images/2.gif');
	  background-repeat:no-repeat;
	  background-position:center;
	  z-index:10000000;
	  opacity: 0.4;
	  filter: alpha(opacity=40); /* For IE8 and earlier */
	  display: none;
	}

	.float-container {
    
	}

	.float-child {
	    width: 45%;
	    float: left;
	    padding: 20px;
	    border: 2px solid red;
	}  


	@keyframes spin {
	  0% { transform: rotate(0deg); }
	  100% { transform: rotate(360deg); }
	}
	@-webkit-keyframes facebook{
		0%{-webkit-transform: scale(1.2);opacity:1;}
		100%{-webkit-transform: scale(0.7);opacity:0.1;}
	}
</style>

<?php $app = JFactory::getApplication(); ?>
<fieldset></fieldset>
<?php if (!empty( $this->sidebar)) : ?>
	<div id="j-sidebar-container" class="span2">
		<?php echo $this->sidebar; ?>
	</div>
	<div id="j-main-container" class="span10">
<?php else : ?>
	<div id="j-main-container">
<?php endif; ?>
		<h1>Data Mapping</h1>
		<div class="default_div" style="display: none;">
			<div class="data-div">
				<input type="text" placeholder="Enter key Word" >
				<select  name="variable_select[]" class="default_select" multiple>
					<option value="">Select Variable</option>
				</select>
				<button type="button" class="btn btn-sm btn-danger remove">Remove</button>
			</div>
		</div>
		<div class="container">
			<div class="data-div">
				<input type="text" placeholder="Enter key Word" >
				<select  name="variable_select[]" class="select select_first" multiple>
					<option value="">Select Variable</option>
				</select> 
				<button type="button" class="btn btn-sm btn-success addmore">Add More</button>
			</div>
		</div>
		<div class="container addmore_content">
			
		</div>
		<div class="container">
			<button type="button" class="btn btn-sm btn-success submit">Submit</button>
		</div>	
	</div>
<script type="text/javascript">
	$= jQuery;
	var getUrlParameter = function getUrlParameter(sParam) {
	    var sPageURL = window.location.search.substring(1),
	        sURLVariables = sPageURL.split('&'),
	        sParameterName,
	        i;

	    for (i = 0; i < sURLVariables.length; i++) {
	        sParameterName = sURLVariables[i].split('=');

	        if (sParameterName[0] === sParam) {
	            return sParameterName[1] === undefined ? true : decodeURIComponent(sParameterName[1]);
	        }
	    }
	};
	$(document).ready(function($) {
		sheet = getUrlParameter('sheet');
		//console.log(getUrlParameter('sheet'));
		jQuery.ajax({
			url  : 'index.php?option=com_mica&ajaxprocess=1&task=import.getvariablelist&view=import&sheet='+sheet,
			type : 'GET'
		}).done(function(dataResp) {
			$('.select_first').empty();
			$('.default_select').empty();
			$('.select_first').append(dataResp);
			$('.default_select').append(dataResp);
		});
		$(document).on('click','.addmore',function () {
			var html = $('.default_div	 .data-div').clone().css('display','block');
			$('.addmore_content').append(html);
		});
		$(document).on('click','.remove',function () {
			$(this).parents('.data-div').remove()
		});
		$(document).on('click','.submit',function () {
			var data1 = {};
			$('.container .data-div').each(function(index, el) {
				var variable = $(el).find('input[type="text"]').val();
				var selected_variable = $(el).find('select').val();
				data1[variable] = selected_variable;
			});
			console.log(JSON.stringify(data1),data1,data1.toString());
			jQuery.ajax({
			url  : 'index.php?option=com_mica&ajaxprocess=1&task=import.savemapping&view=import&sheet='+sheet,
			type : 'POST',
			data : {
				'mapping' : JSON.stringify(data1)
			},
			success : {
				alert('Data Mapping Successfully Added');
			}
			})

		});
	});
</script>

