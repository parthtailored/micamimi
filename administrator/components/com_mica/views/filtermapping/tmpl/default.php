<?php
/**
 * @package     Joomla.Administrator
 * @subpackage  com_mica
 *
 * @copyright   Copyright (C) 2005 - 2017 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die; ?>

<style type="text/css">
	#facebook{
		margin-top:30px;
		float:left;
	}
	.facebook_block{
		background-color:#9FC0FF;
		border:2px solid #3B5998;
		float:left;
		height:30px;
		margin-left:5px;
		width:8px;
	    opacity:0.1;
		-webkit-transform:scale(0.7);
		-webkit-animation-name: facebook;
	 	-webkit-animation-duration: 1s;
	 	-webkit-animation-iteration-count: infinite;
	 	-webkit-animation-direction: linear;
	}
	#block_1{
	 	-webkit-animation-delay: .1s;
	}
	#block_2{
	 	-webkit-animation-delay: .2s;
	}
	#block_3{
	 	-webkit-animation-delay: .3s;
	}
	#block_4{
	 	-webkit-animation-delay: .4s;
	}
	#block_5{
	 	-webkit-animation-delay: .5s;
	}
	#block_6{
	 	-webkit-animation-delay: .6s;
	}
	#loadingDiv{
	  position:fixed;
	  top:0px;
	  right:0px;
	  width:100%;
	  height:100%;
	  background-color:#666;
	  background-image:url('components/com_mica/images/2.gif');
	  background-repeat:no-repeat;
	  background-position:center;
	  z-index:10000000;
	  opacity: 0.4;
	  filter: alpha(opacity=40); /* For IE8 and earlier */
	  display: none;
	}

	.float-container {
    
	}

	.float-child {
	    width: 45%;
	    float: left;
	    padding: 20px;
	    border: 2px solid red;
	}  


	@keyframes spin {
	  0% { transform: rotate(0deg); }
	  100% { transform: rotate(360deg); }
	}
	@-webkit-keyframes facebook{
		0%{-webkit-transform: scale(1.2);opacity:1;}
		100%{-webkit-transform: scale(0.7);opacity:0.1;}
	}
</style>

<?php $app = JFactory::getApplication(); ?>
<fieldset></fieldset>
<?php if (!empty( $this->sidebar)) : ?>
	<div id="j-sidebar-container" class="span2">
		<?php echo $this->sidebar; ?>
	</div>
	<div id="j-main-container" class="span10">
<?php else : ?>
	<div id="j-main-container">
<?php endif; ?>
		<h1>Filter Mapping</h1>
		<div class="container main-div">
			
		</div>	
	</div>
<script type="text/javascript">
	$= jQuery;
	var getUrlParameter = function getUrlParameter(sParam) {
	    var sPageURL = window.location.search.substring(1),
	        sURLVariables = sPageURL.split('&'),
	        sParameterName,
	        i;

	    for (i = 0; i < sURLVariables.length; i++) {
	        sParameterName = sURLVariables[i].split('=');

	        if (sParameterName[0] === sParam) {
	            return sParameterName[1] === undefined ? true : decodeURIComponent(sParameterName[1]);
	        }
	    }
	};
	$(document).ready(function($) {
		sheet = getUrlParameter('sheet');
		jQuery.ajax({
			url  : 'index.php?option=com_mica&ajaxprocess=1&task=import.getheadervariable&view=import&sheet='+sheet,
			type : 'GET'
		}).done(function(dataResp) {
			console.log(dataResp,'ujash');
			$('.main-div').empty();
			$('.main-div').append(dataResp);
		});

		$(document).on('click', '.submit', function() {
			var data = {};
			$('input:radio:checked').each(function(index, el) {
				if(typeof data[$(el).data('type')] == 'undefined'){
					data[$(el).data('type')] = [];	
				}
				data[$(el).data('type')].push([$(el).attr('name'),$(el).val()]);
			});
				

			jQuery.ajax({
				url  : 'index.php?option=com_mica&ajaxprocess=1&task=import.savefiltervariable&view=import&sheet='+sheet,
				type : 'POST',
				data : {
					'data' : JSON.stringify(data)
				}
			}).done(function(dataResp) {
				alert('Filter Data Saved');
				console.log(dataResp,'ujash');
				
			});	
		});

		
		
	});
</script>

