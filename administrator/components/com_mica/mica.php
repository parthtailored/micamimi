<?php
/**
 * @package     Joomla.Administrator
 * @subpackage  com_mica
 *
 * @copyright   Copyright (C) 2005 - 2017 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;
JHtml::_('behavior.tabstate');

if (!JFactory::getUser()->authorise('core.manage', 'com_mica'))
{
	throw new JAccessExceptionNotallowed(JText::_('JERROR_ALERTNOAUTHOR'), 403);
}

JLoader::register('MicaHelper', __DIR__ . '/helpers/mica.php');

$controller = JControllerLegacy::getInstance('Mica');
JFactory::getApplication()->input->get('task');

$controller->execute(JFactory::getApplication()->input->get('task'));
$controller->redirect();
