<?php
/**
 * @package     Joomla.Administrator
 * @subpackage  com_mica
 *
 * @copyright   Copyright (C) 2005 - 2017 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

/**
 * MICA Configuration model.
 *
 * @since  1.6
 */
class MicaModelConfiguration extends JModelLegacy
{
	/**
	 *
	 */
	function getData(){
		$db = JFactory::getDBO();
		$db->setQuery("SELECT * FROM ".$db->quoteName('#__mica_configuration'));
		return $db->loadAssoc();
	}

	/**
	 *
	 */
	function save(){
		$tomcaturl = JFactory::getApplication()->input->post->get('tomcaturl','', 'raw');
		$db        = JFactory::getDBO();

		/*  echo $query="select count(*) from #__mica_configuration where tomcatpath like '".$tomcaturl."'";exit;
			$db->setQuery($query);
			$db->query();
			$fieldexist=$db->loadResult();
			if(!$fieldexist){
				$query ="insert into #__mica_configuration (tomcatpath) values ('".$tomcaturl."')";
				$db->setQuery($query);
				$db->query();
				$msg   =JText::_( 'CONFIGURATION_SAVED' );
			}else{
		*/
				$query="UPDATE ".$db->quoteName('#__mica_configuration')." SET ".$db->quoteName('tomcatpath')." = ".$db->quote($tomcaturl);
				$db->setQuery($query);
				$db->execute();
				$msg = JText::_( 'CONFIGURATION_UPDATED' );
		/*	} 	*/
		return $msg;
	}

}
