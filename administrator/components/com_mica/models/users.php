<?php
/**
 * @package     Joomla.Administrator
 * @subpackage  com_mica
 *
 * @copyright   Copyright (C) 2005 - 2017 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

use Joomla\Utilities\ArrayHelper;

/**
 * Methods supporting a list of MICA Users records.
 *
 * @since  1.6
 */
class MicaModelUsers extends JModelList
{

	/**
	 * Constructor.
	 *
	 * @param   array  $config  An optional associative array of configuration settings.
	 *
	 * @see     JController
	 * @since   1.6
	 */
	public function __construct($config = array())
	{
		parent::__construct($config);
	}

	/**
	 * Gets the list of users and adds expensive joins to the result set.
	 *
	 * @return  mixed  An array of data items on success, false on failure.
	 *
	 * @since   1.6
	 */
	public function getItems()
	{
		$items = parent::getItems();
		if (empty($items)){
			return array();
		}
		return $items;
	}

	/**
	 * Build an SQL query to load the list data.
	 *
	 * @return  JDatabaseQuery
	 *
	 * @since   1.6
	 */
	protected function getListQuery(){
		// Create a new query object.
		$db    = $this->getDbo();
		$query = $db->getQuery(true);

		// Select the required fields from the table.
		$query->select('a.id, a.title');
		$query->from($db->quoteName('#__osmembership_plans') . ' AS a ');
		//$query->where($db->quoteName('a.published')." = ".$db->quote('1'));
		$query->order($db->quoteName('a.ordering').' ASC ');

		return $query;
	}

	/**
	 *
	 */
	public function getUpdateAttrData(){
		$db     = $this->getDbo();
		$app    = JFactory::getApplication();

		$plan   = $app->input->get->get('plan', 0, 'string');
		$planid = $app->input->get->get('planid', 0, 'string');

		//$query="select * from #__mica_group_field allfields JOIN #__mica_group grp ON grp.id=allfields.groupid GROUP BY allfields.groupid";
		$query  = "SELECT *
			FROM ".$db->quoteName('#__mica_user_attribute')."
			WHERE ".$db->quoteName('aec_plan_id')." = ".$db->quote($planid)."
			GROUP BY ".$db->quoteName('dbtable');
		$db->setQuery($query);
		return array(
			'userattribute' => $db->loadResult(),
			'selecteddata'  => $db->loadObjectList()
		);
	}

	/**
	 *
	 */
	public function getAllAttributes($db_table){
		$db  = $this->getDbo();
		$app = JFactory::getApplication();

		$allattributes = array();
		foreach($db_table as $eachtable){
			$query = "SELECT * FROM ".$db->quoteName('#__mica_group_field')." AS allfields
				JOIN ".$db->quoteName('#__mica_group')." AS grp ON ".$db->quoteName('grp.id')." = ".$db->quoteName('allfields.groupid')."
				WHERE ".$db->quoteName('allfields.table')." LIKE ".$db->quote($eachtable);
			$db->setQuery($query);
			$allattributes[$eachtable] = $db->loadObjectList();
		}
		return $allattributes;
	}

	/**
	 * Collects Types data and returns to view.
	 */
	public function gettypesData(){
		$db  = $this->getDbo();
		$app = JFactory::getApplication();

		$selQuery = "SELECT type FROM ".$db->quoteName('#__mica_user_attribute_type')."
			WHERE ".$db->quoteName('aec_plan_id')." = ".$db->quote($app->input->get->get('planid', 0, 'string'));
		$db->setQuery($selQuery);
		return $db->loadResult();
	}

}
