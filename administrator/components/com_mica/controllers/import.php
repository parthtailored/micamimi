<?php
/**
 * @package     Joomla.Administrator
 * @subpackage  com_mica
 *
 * @copyright   Copyright (C) 2005 - 2017 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

jimport('joomla.filesystem.file');
jimport('joomla.filesystem.folder');

/**
 * Import controller class.
 *
 * @since  1.6
 */
class MicaControllerImport extends JControllerLegacy
{
	public function importjson()
	{
		$com_media_params = JComponentHelper::getParams('com_media');
		$folder = 'imported_xls';
		$img = $com_media_params->get('file_path', 'images');
		$filename =JPATH_ROOT."/".$img."/".$folder."/gj.geojson";
		$jsondata = file_get_contents($filename);


		$i=0;
		foreach ($variable as $key => $value) {

			echo "<pre>";
			print_r($value);
			echo "</pre>";
			$i++;
			if($i>10)
				break;
		}
		print_r($jsondata);
		return;
	}

	/**
	 * Upload File from which data needs to be imported.
	 *
	 * @return  boolean
	 *
	 * @since   1.5
	 */
	public function upload(){
		// Check for request forgeries
		JSession::checkToken('request') or jexit(JText::_('JINVALID_TOKEN'));

		$com_media_params = JComponentHelper::getParams('com_media');
		define('COM_MEDIA_BASE', JPATH_ROOT . '/' . $com_media_params->get('file_path', 'images'));

		// Get some data from the request
		$file   = $this->input->files->get('toupload', array(), 'array');
		$folder = 'imported_xls';

		// If there are no files to upload - then bail
		if (empty($file)){
			return false;
		}

		// Instantiate the media helper
		$mediaHelper = new JHelperMedia;
		$upload_path = COM_MEDIA_BASE.DIRECTORY_SEPARATOR.$folder;
		if (!is_dir($upload_path)){
			if (JFolder::create($upload_path)){
				$data = "<html>\n<body bgcolor=\"#FFFFFF\">\n</body>\n</html>";
				JFile::write($upload_path.'/index.html', $data);
			}
		}
		if (!is_dir($upload_path)){
			JError::raiseWarning(100, JText::_('Upload Destination Folder is not exist: '.$upload_path));
			return false;
		}

		// Perform basic checks on file info before attempting anything
		$file['name']     = JFile::makeSafe($file['name']);
		$file['name']     = str_replace(' ', '-', $file['name']);
		$file['filepath'] = JPath::clean(implode(DIRECTORY_SEPARATOR, array($upload_path, $file['name'])));

		if ($file['error'] == 1){
			// File size exceed either 'upload_max_filesize' or 'upload_maxsize'.
			$this->setRedirect('index.php?option=com_mica&view=import', JText::_('Error, Uploaded file contains error!'), 'error');
			return false;
		}

		if (JFile::exists($file['filepath'])){
			unlink($file['filepath']);
		}

		if (!isset($file['name'])){
			// No filename (after the name was cleaned by JFile::makeSafe)
			$this->setRedirect('index.php?option=com_mica&view=import', JText::_('Invalid Request'), 'error');
			return false;
		}

		$options['format']    = '{DATE}\t{TIME}\t{LEVEL}\t{CODE}\t{MESSAGE}';
		$options['text_file'] = 'upload.error.php';
		JLog::addLogger($options, JLog::INFO, array('error'));

		// Load the helper class
		$format = $file['type'];
		$err    = null;// The request is valid
		if (!$mediaHelper->canUpload($file, 'com_media')){
			if ($format == 'application/vnd.ms-excel' || $format == 'application/wps-office.xls') {
				JLog::add('Invalid: '.$file['filepath'].': Unsupported Media Type', JLog::INFO, 'error');
				header('HTTP/1.0 415 Unsupported Media Type');
				$this->setRedirect('index.php?option=com_mica&view=import', JText::_('Error. Unsupported Media Type!'), 'error');
				return false;
			}else{
				// Error in upload
				$this->setRedirect('index.php?option=com_mica&view=import', JText::_($err), 'error');
				return false;
			}
		}

		if (JFile::exists($file['filepath'])) {
			if ($format == 'application/vnd.ms-excel' || $format == 'application/wps-office.xls') {
				JLog::add('File already exists: '.$file['filepath'], JLog::INFO, 'error');
				header('HTTP/1.0 409 Conflict');
				$this->setRedirect('index.php?option=com_mica&view=import', JText::_('Error. File already exists'), 'error');
				return false;
			} else {
				$this->setRedirect('index.php?option=com_mica&view=import', JText::_($err), 'error');
				return false;
			}
		}

		// Trigger the onContentBeforeSave event.
		$object_file = new JObject($file);
		if (!JFile::upload($object_file->tmp_name, $object_file->filepath) ){
			if ($format == 'application/vnd.ms-excel' || $format == 'application/wps-office.xls') {
				JLog::add('Cannot upload: '.$filepath, JLog::INFO, 'error');
				header('HTTP/1.0 400 Bad Request');
				$this->setRedirect('index.php?option=com_mica&view=import', JText::_('Error. Unable to upload file'), 'error');
				return false;
			} else {
				JError::raiseWarning(100, JText::_('Error. Unable to upload file'));
				return false;
			}
		}else{
			if ($format == 'application/vnd.ms-excel' || $format == 'application/wps-office.xls') {
				JLog::add($object_file->name, JLog::INFO, 'success');
			}
			$this->setRedirect('index.php?option=com_mica&view=import&ajaxprocess=1&filename='.$object_file->name, JText::_('Upload complete.')."<br />".JText::_('Your file in process...'), 'success');
			return false;
		}
	}

	/**
	 *
	 */
	public function insertToDatabasePreview(){
		$com_media_params = JComponentHelper::getParams('com_media');
		define('COM_MEDIA_BASE', JPATH_ROOT.'/'.$com_media_params->get('file_path', 'images'));
		$folder      = 'imported_xls';
		$upload_path = COM_MEDIA_BASE.DIRECTORY_SEPARATOR.$folder;

		$filename    = $this->input->get('filename', '', 'string');
		$operation   = $this->input->get('operation', '', 'string');
		$sheet       = $this->input->get('sheet', '', 'string');
		$import_type = $this->input->get('import_type', '', 'string');
		$maxcoloumn = 10;
		$maxrows = 10;
		require_once JPATH_ADMINISTRATOR.'/components/com_mica/controllers/Excel/reader.php';

		$data = new Spreadsheet_Excel_Reader();
		$data->setOutputEncoding('CP1251');
		$data->read($upload_path.DIRECTORY_SEPARATOR.$filename);
		/*echo "<pre>";
		print_r($data->sheets[0]['cells']);
		die();*/
		error_reporting(E_ALL ^ E_NOTICE);
		$toprint = array();

		
		if($import_type == 'default'){
			echo "</br></br><b>Select Operation</b></br>";
			echo '<input type="radio" name="operation" value="overwrite" /> Overwrite (Select this option if you add new variable and Data)<br /><input type="radio" name="operation" value="new" /> New (Select this option if you want start from begin Note :: Automatically Remove all existing variable)</br>';
			echo "</br></br><b>Where to Insert?</b>";
			echo '</br><input type="radio" name="table" value="state" /> State Data<br />
				<input type="radio" name="table" value="district" /> District Data</br>
				<input type="radio" name="table" value="Urban" /> Urban  Data</br>
				<input type="radio" name="table" value="Town" /> Town  Data</br></br>';
		} elseif($import_type == 'social_economic') {
			$db = JFactory::getDbo();
			$data = new stdClass();
			$data->file_path = $upload_path.DIRECTORY_SEPARATOR.$filename;
			$data->name=$filename;
			$data->status=0;
			$data->first_uploaded_date=date('Y-m-d h:i:s');
			// Insert the object into the user data table.
			$result = $db->insertObject('dataset_master', $data);

			echo "<br><br><b>Select The Source Formate</b><br><br>";
			echo "<table><thead></thead><tbody>";
			echo '<tr><td>Coloumn Levels :</td><td><select  id="coloumn_level" ><option value="">Select Coloumn Levels</option>';
			for ($i = 1; $i <= $maxcoloumn; $i++) {
				echo "<option value='".$i."'>".$i."</option>";
			}
			echo '</select></td></tr>';
			echo '<tr><td>Rows Levels :</td><td><select  id="rows_level" ><option value="">Row Coloumn Levels</option>';
			for ($i = 1; $i <= $maxrows; $i++) {
				echo "<option value='".$i."'>".$i."</option>";
			}
			echo "</select><div style='display:none;' id='file_content' data-file_content=".json_encode($data->sheets[0]['cells'])."></div></td></tr>";
			echo "</tbody></table>";
			echo '<input type ="button" name="start" id="start" value="Next" class="btn 	btn-small btn-success" /></br></br></br>';
			/*echo 'Coloumn Levels :<select  id="coloumn_level" >';
			for ($i = 1; $i <= $maxcoloumn; $i++) {
				echo "<option value='".$i."'>".$i."</option>";
			}
			echo '</select><br><br>';
			echo 'Rows Levels :<select  id="rows_level" >';
			for ($i = 1; $i <= $maxrows; $i++) {
				echo "<option value='".$i."'>".$i."</option>";
			}*/
			/*echo "</select>";*/
			echo "<div style='display:none;' id='file_content' data-file_content=".json_encode($data->sheets[0]['cells'])."></div><br><br>";
		}
		
		//echo '<input type ="button" name="Step3" id="step3" value="Insert to Database" class="btn btn-small btn-success" /></br></br></br>';
		echo "<div id='preview'><table class='table table-striped' >";

		for ($i = 1; $i <= $data->sheets[$sheet]['numRows']; $i++) {
			if($i == 1){
				echo "<thead><tr bgcolor='gray' >";
			}else{
				echo "<tr>";
			}

			for ($j = 1; $j <= $data->sheets[$sheet]['numCols']; $j++) {
				$toprint[$i][$data->sheets[$sheet]['cells'][1][$j]] = $data->sheets[$sheet]['cells'][$i][$j];
				echo "<td>".$data->sheets[$sheet]['cells'][$i][$j]."</td>";
			}

			if($i == 1){
				echo "</tr></thead>";
			}else{
				echo "</tr>";
			}

			if($i == 3){
				echo "</table>";break;
			}
		}
		echo "</table></div>";exit;
	}

	public function RowLevel(){
		$filename             = $this->input->get('filename', '', 'string');
		$operation            = $this->input->get('operation', '', 'string');
		$sheet                = $this->input->get('sheet', '', 'string');
		$import_type          = $this->input->get('import_type', '', 'string');
		$rows_level           = $this->input->get('rows_level', '', 'string');
		$rows_level_selected  = $this->input->get('rows_level_selected', '', 'string');
		$child_level_selected = $this->input->get('child_level_selected', '', 'string');
		$name = $this->input->get('name', '', 'string');
		$level                = $this->input->get('level', '', 'integer');
		$nextlevel = 0;
		if(is_numeric($nextlevel)){
			$nextlevel = $level+1;
		}
		/*echo "<pre>";
				print_r($name);
				die('1321');*/
		$com_media_params = JComponentHelper::getParams('com_media');
		define('COM_MEDIA_BASE', JPATH_ROOT.'/'.$com_media_params->get('file_path', 'images'));
		$folder      = 'imported_xls';
		$upload_path = COM_MEDIA_BASE.DIRECTORY_SEPARATOR.$folder;
		require_once JPATH_ADMINISTRATOR.'/components/com_mica/controllers/Excel/reader.php';

		$data = new Spreadsheet_Excel_Reader();
		$data->setOutputEncoding('CP1251');
		$data->read($upload_path.DIRECTORY_SEPARATOR.$filename);
		/*echo "<pre>";
		print_r($data->sheets[0]['cells']);
		die();*/
		$rowfilterdata = array();
		$filterdata = array();
		foreach ($data->sheets[0]['cells'] as $key => $value) {
			if($key <= $rows_level){
				$rowfilterdata[] = $value;
			}
		}
		$result = call_user_func_array('array_merge', $rowfilterdata);
		$filterdata = array_values($result);
		error_reporting(E_ALL ^ E_NOTICE);
		$toprint = array();
			if(!$rows_level_selected && empty($child_level_selected)){
				echo "<br><br><b>Select Identifiers and Variable</b><br><br>";
				echo "<table border=0><tbody>";
				foreach ($filterdata as $k=> $data) {
						/*if($k % 2 == 0){ */
					        echo"<tr>
								<td>".$data." is ?</td>
								<td><input type='radio' name='__".$data."' class='identifiers_variable' value='identifiers'>Identifier</td>
								<td><input type='radio' name='__".$data."' class='identifiers_variable' value='variable' checked>Variable</td>
								</tr>";
					    /*} */
						
					
				}
				echo "</tbody></table>";
				/*echo "<table border=0><tbody>";
				foreach ($filterdata as $k=> $data) {
						if($k % 2 != 0){ 
					        echo"<tr>
								<td>".$data." is ?</td>
								<td><input type='radio' name='__".$data."' class='identifiers_variable' value='identifiers'>Identifier</td>
								<td><input type='radio' name='__".$data."' class='identifiers_variable' value='variable' checked>Variable</td>
								</tr>";
					    } 
						
					
				}
				echo "</tbody></table>";*/
				echo "<br><br><b>Select The Root Formate</b><br><br>";
				echo 'Root Levels Coloumn :<select multiple class="social_economic_select"  id="root_level"  name="root" style="width: 25%;">';
				$i = 0;
				foreach ($filterdata as $data) {
					echo "<option value='".$data."' data-int ='".$i."'  data-level=0>".$data."</option>";
					$i++;
				}
				echo '</select><br><br>';
				echo '<input type ="button" name="next" id="next" value="next" class="btn 	btn-small btn-success" data-level=0 /></br></br></br>';	
			} if($rows_level_selected && $level+1 == 1) {
				$rows_level_selected_array = explode(',', $rows_level_selected);
				
				$filterchilddata = array_diff($filterdata,$rows_level_selected_array);
				echo "<br><br><b>Select Child Formate</b><br><br>";
				echo "<table border=0><tbody>";
				foreach ($rows_level_selected_array as $root) {
							
					$new_name = $name.'['.$root.']';
							echo"<tr>
								<td>".$root." have child ?</td>
								<td><input type='radio' name='".$root."' class='child' value='yes'>Yes</td>
								<td><input type='radio' name='".$root."' class='child' value='no'>No</td>
								</tr>";
							echo "<tr class='child_select_tr' style='display:none;'><td><b>Select Child</b><select multiple id='".$root."' class='social_economic_select child_level_select child_select' data-parent='".$root."' name='".$new_name."' >";
								foreach ($filterchilddata as $data) {
									echo "<option value='".$data."' data-level='".$nextlevel."'>".$data."</option>";
								}
							echo "</select></td></tr>";
							
					//echo $root." have child ? &nbsp;&nbsp;&nbsp;<input type='radio' name='".$root."' class='child_select' value='yes'>Yes &nbsp;&nbsp;&nbsp;<input type='radio' name='".$root."' class='child_select' value='no'>No<br>";
					
				}
				echo "</tbody></table>";
				

				echo '<input type ="button" name="next" id="next" value="next" class="btn 	btn-small btn-success" data-level=1/></br></br></br>';	
				
			} 
			
			if(!empty($child_level_selected) && $child_level_selected != 'undefined' ) {
				$child_level_selected_array = explode(',', $child_level_selected);
				$rows_level_selected_array = explode(',', $rows_level_selected);
				$filterchilddata = array_diff($filterdata,$child_level_selected_array);
				$filterchilddata = array_diff($filterchilddata,$rows_level_selected_array);
				echo "<br><br><b>Select Child Formate</b><br><br>";
				echo "<table border=0><tbody>";
				foreach ($child_level_selected_array as $root) {
							$new_name = $name.'['.$root.']';
							echo"<tr>
								<td>".$root." have child ?</td>
								<td><input type='radio' name='".$root."' class='child' value='yes'>Yes</td>
								<td><input type='radio' name='".$root."' class='child' value='no'>No</td>
								</tr>";
							echo "<tr class='child_select_tr' style='display:none;'><td><b>Select Child</b><select multiple  class='social_economic_select child_level_select child_select' data-parent='".$root."' name='".$new_name."'>";
								foreach ($filterchilddata as $data) {
									echo "<option value='".$data."'  data-level='".$nextlevel."'>".$data."</option>";
								}
							echo "</select></td></tr>";
						
					//echo $root." have child ? &nbsp;&nbsp;&nbsp;<input type='radio' name='".$root."' class='child_select' value='yes'>Yes &nbsp;&nbsp;&nbsp;<input type='radio' name='".$root."' class='child_select' value='no'>No<br>";
					
				}
				echo "</tbody></table>";
				
				if($rows_level-1 > $nextlevel){
					
					echo '<input type ="button" name="next" id="nextchild" value="next" class="btn 	btn-small btn-success" data-level="'.$nextlevel.'" /></br></br></br>';
				} else {
					echo '<input type ="button" name="finish" id="finish" value="finish" class="btn btn-small btn-success" data-level="'.$nextlevel.'" /></br></br></br>';
				}
			}
			die();
		
	}

	public function finalTreeInsert()
	{
		ini_set('memory_limit', '-1');
        ini_set('max_execution_time', 0);
		$identifiers_variable_key_pair = $selected_array = array();
		$tree                 = $this->input->get('tree', '', 'string');
		$identifiers_variable = $this->input->get('identifiers_variable', '', 'string');
		$column_level         = $this->input->get('column_level', '', 'integer');
		$rows_level           = $this->input->get('rows_level', '', 'integer');
		$filename             = $this->input->get('filename', '', 'string');

		foreach (json_decode($identifiers_variable) as $key => $value) {
			$identifiers_variable_key_pair[$value->name] = $value->value;
			
		}
		
		$com_media_params = JComponentHelper::getParams('com_media');
		define('COM_MEDIA_BASE', JPATH_ROOT.'/'.$com_media_params->get('file_path', 'images'));
		$folder      = 'imported_xls';
		$upload_path = COM_MEDIA_BASE.DIRECTORY_SEPARATOR.$folder;
		require_once JPATH_ADMINISTRATOR.'/components/com_mica/controllers/Excel/reader.php';

		$data = new Spreadsheet_Excel_Reader();
		$data->setOutputEncoding('CP1251');
		$data->read($upload_path.DIRECTORY_SEPARATOR.$filename);

		

		$key_array = [];
		$temp = [];
		foreach (json_decode($tree,true) as $key => $value) {
			if(!array_key_exists('children', $value)){
				$key_array[] = $value['id'];
			} else {
				$a = $this->getid($value['children'], $temp);
				foreach ($a as $k => $v) {
					$key_array[] = $v;
				}
			}
			
		}
		$temp = $final_array = [];
		foreach ($data->sheets[0]['cells'] as $key => $value) {			
			if($key <= $rows_level){
				unset($data->sheets[0]['cells'][$key]);
			} else {
				foreach ($value as $k => $v) {
					$temp[$key_array[$k-1]] = $v;					
				}
				$final_array[] = $temp;
			}
		}
		$variable_columns = $column_level_array=[];
		foreach ($final_array as $value) {
			$i =1;
			foreach ($value as $k => $v) {
			
				if($i <= $column_level){
					$column_level_array[] = [$k=>$v];
					$variable_columns[] = $k; 
					$i++;
				} 
			}
		}
		$variable_columns = array_unique($variable_columns);
		$variable_row_array = [];
		$temp = [];
		$db = JFactory::getDbo();
		$query = "SELECT * FROM `dataset_master` WHERE `name` LIKE '%".$filename."%' AND `status` = 0";
		$result = $db->setQuery($query);
		$result = $db->loadObjectList();
		foreach (json_decode($tree,true) as $key => $value) {
			if(!array_key_exists('children', $value)){
				$variable_row_array[] = ['variable' => $value['id']];
			} else {
				if (array_key_exists('parent_id', $value)) {
					$variable_row_array[] = ['variable' => $value['id'], 'parent_id' => $value['parent_id']];
				} else {
					$variable_row_array[] = ['variable' => $value['id']];
				}
				$a = $this->getparent($value['children'], $temp);
				foreach ($a as $k => $v) {
					$variable_row_array[] = ['variable' => $v['variable'], 'parent_id' => $v['parent_id']];
				}
			}
			
		}
		
		foreach ($variable_row_array as $key => $value) {
			$data = new stdClass();
			$data->dataset_master_id = $result[0]->id;
			$data->variable_name = $value['variable'];
			if(isset($value['parent_id'])){
				$query = "SELECT * FROM `variable_rows` WHERE `variable_name` = '".$value['parent_id']."' AND `dataset_master_id` = '".$result[0]->id."'";
				$db->setQuery($query);
				$parent = $db->loadObjectList();
				if($parent){
					$data->variable_parent = $parent[0]->id;
				}
			}
			
			$data->variable_type=$identifiers_variable_key_pair[$value['variable']];
			$data->has_child=0;
			$db->insertObject('variable_rows', $data);
		
		}
		
		$column_level_array = array_map("unserialize", array_unique(array_map("serialize", $column_level_array)));

		
		
		foreach ($column_level_array as $key => $value) {
			foreach ($value as $k => $v) {
				$data = new stdClass();
				$data->dataset_master_id = $result[0]->id;
				$data->variable_name = $v;
				$data->variable_group_title = $k;
				if(isset($k)){
					$query = "SELECT * FROM `variable_rows` WHERE `variable_name` = '".$k."' AND `dataset_master_id` = '".$result[0]->id."'";
					$db->setQuery($query);
					$group = $db->loadObjectList();
					if($group){
						$data->group_id = $group[0]->id;
					}
				}
				$db->insertObject('variable_columns', $data);
			}
		}
		$column = '';
		foreach ($variable_columns as $key => $value) {
			$column .='`'.$value.'` int(11) DEFAULT NULL,';
		}
		
		$create_table = "CREATE TABLE IF NOT EXISTS `#__mica_data_table_".$result[0]->id."` (`id` int(11) NOT NULL,".$column." `variable_row_id` int(11) DEFAULT NULL,`value` varchar(255) DEFAULT NULL ,PRIMARY KEY (id)) ENGINE=InnoDB DEFAULT CHARSET=latin1";
		
		try{
			$db->setQuery($create_table);
			$db->query();			
		}catch(Exception $e){
			$msg = $e->getMessage(); 
    		$code = $e->getCode(); 
    		JFactory::getApplication()->enqueueMessage($msg, 'error');
		}
		
		$set_primary = " ALTER TABLE `#__mica_data_table_".$result[0]->id."` CHANGE `id` `id` INT(11) NOT NULL AUTO_INCREMENT";		
		
		try{
			$db->setQuery($set_primary);
			$db->query();			
		}catch(Exception $e){
			$msg = $e->getMessage(); 
    		$code = $e->getCode(); 
    		JFactory::getApplication()->enqueueMessage($msg, 'error');
		}
		$query = "SELECT `id`,`variable_group_title`,`variable_name` FROM `variable_columns` WHERE `dataset_master_id` = '".$result[0]->id."'";
		$db->setQuery($query);
		$variable_columns_data =  $db->loadObjectList();
		foreach ($final_array as $key => $value) {			
			foreach ($variable_columns_data as $v1) {
				if(array_key_exists($v1->variable_group_title, $value) && $value[$v1->variable_group_title] == $v1->variable_name ){
					$final_array[$key][$v1->variable_group_title] = $v1->id; 
				}
			}
		}

		foreach ($final_array as $key => $value) {
			foreach ($value as $k => $v) {
				if (!in_array($k, $variable_columns)) {
					$query = "SELECT `id` FROM `variable_rows` WHERE `dataset_master_id` = '".$result[0]->id."' AND `variable_name`='".$k."'";
					$db->setQuery($query);
					$variable_rows_data =  $db->loadObjectList();
						
					$insert = new stdClass();
					foreach ($variable_columns as $variable_column) {
						$insert->{$variable_column} = $value[$variable_column];
					}
					$insert->variable_row_id = $variable_rows_data[0]->id;
					$insert->value = $v;
					$db->insertObject("#__mica_data_table_".$result[0]->id, $insert);
				}
			}
		}
		
	}


	public function getid($ary, $temp = [])
	{
		foreach ($ary as $key => $value) {
			if(!array_key_exists('children', $value)){
				$temp[] = $value['id'];
			} else {
				foreach ($this->getid($value['children']) as $k => $v) {
					$temp[] = $v;
				}
			}
		}
	 	
		return $temp;
	}
	public function getparent($ary,$temp=[])
	{
		foreach ($ary as $key => $value) {
			if(!array_key_exists('children', $value)){
				if (array_key_exists('parent_id', $value)) {
					$temp[] = ['variable' => $value['id'], 'parent_id' => $value['parent_id']];
				} else {
					$temp[] = ['variable' => $value['id']];
				}
			} else {
				if (array_key_exists('parent_id', $value)) {
					$temp[] = ['variable' => $value['id'], 'parent_id' => $value['parent_id']];
				} else {
					$temp[] = ['variable' => $value['id']];
				}
				foreach ($this->getparent($value['children']) as $k => $v) {
					$temp[] = ['variable' => $v['variable'], 'parent_id' => $v['parent_id']];
				}
			}
		}
	 	
		return $temp;
	}
	/**
	 *
	 */
	public function insertToDatabase(){
		$com_media_params = JComponentHelper::getParams('com_media');
		define('COM_MEDIA_BASE', JPATH_ROOT.'/'.$com_media_params->get('file_path', 'images'));
		$folder      = 'imported_xls';
		$upload_path = COM_MEDIA_BASE.DIRECTORY_SEPARATOR.$folder;

		$filename  = $this->input->get('filename', '', 'string');
		$operation = $this->input->get('operation', '', 'string');
		$sheet     = $this->input->get('sheet', '', 'string');
		$table     = $this->input->get('table', '', 'string');

		require_once JPATH_ADMINISTRATOR.'/components/com_mica/controllers/Excel/reader.php';
		$data = new Spreadsheet_Excel_Reader();
		$data->setOutputEncoding('CP1251');
		$data->read($upload_path.DIRECTORY_SEPARATOR.$filename);

		error_reporting(E_ALL ^ E_NOTICE);
		$toprint = array();
		for ($i = 1; $i <= $data->sheets[$sheet]['numRows']; $i++) {
			for ($j = 1; $j <= $data->sheets[$sheet]['numCols']; $j++) {
				$toprint[$i][$data->sheets[$sheet]['cells'][1][$j]] = $data->sheets[$sheet]['cells'][$i][$j];
			}
		}

		if($operation==""){
			return "Please Select Operation";
		}

		if($operation == "new"){
			$resu = $this->operationAdd($table, $toprint[1], $toprint);
		}

		if($operation == "overwrite"){
			$resu = $this->operationUpdate($table, $toprint[1], $toprint);
			$resu .= $this->addLanguageVariables($table, $toprint[1], $toprint);
		}

		echo "<pre>";print_r($resu);exit;
	}

	/**
	 *
	 */
	public function selectSheet(){
		$com_media_params = JComponentHelper::getParams('com_media');
		define('COM_MEDIA_BASE', JPATH_ROOT.'/'.$com_media_params->get('file_path', 'images'));
		$folder      = 'imported_xls';
		$upload_path = COM_MEDIA_BASE.DIRECTORY_SEPARATOR.$folder;

		$filename = $this->input->get('filename', '', 'string');

		require_once JPATH_ADMINISTRATOR.'/components/com_mica/controllers/Excel/reader.php';

		$data = new Spreadsheet_Excel_Reader();
		$data->setOutputEncoding('CP1251');
		$data->read($upload_path.DIRECTORY_SEPARATOR.$filename);

		error_reporting(E_ALL ^ E_NOTICE);
		echo 'Select Sheet :<select  id="sheet" >';
		for ($i = 0; $i < count($data->boundsheets); $i++) {
			echo "<option value='".$i."'>".$data->boundsheets[$i]['name']."</option>";
		}
		echo '</select><br>';
		echo "<label>Select Import Sheet Type :</label>";
		echo '<input type="radio" id="default" name="import_type" value="default"><label for="default">Default</label><br><input type="radio" id="social_economic" name="import_type" value="social_economic"><label for="social_economic">Social Economic</label><br>';
		echo '</br><input type="button" name="Step2" id="step2" value="Preview" class="btn btn-small btn-success" />';
		exit;
	}

	/**
	 *
	 */
	private function operationAdd($table,$columnstoinsert,$toprint){
		if($table == "state"){
			$db_table = "rail_state";
			$dndArray = array('OGR_FID','india_state','name','id','StateName','LocationID' );
		}else if($table == "district"){
			$db_table = "india_information";
			$dndArray = array('OGR_FID','india_information','distshp','state');
		}else if($table == "Urban"){
			$db_table = "jos_mica_urban_agglomeration";
			$dndArray = array('OGR_FID','india_city','place_name','state','country');
		}else if($table == "Town"){
			$db_table = "villages";
			$dndArray = array('OGR_FID','state','state_name','district','district_name','place_name');
		}else if($table == "villagesummery"){
			$db_table = "villagesummery";
			$dndArray = array('OGR_FID','villagesummery','distshp','state');
		}else if($table == "language"){
			return $this->addLanguageVariables($db_table, $columnstoinsert, $toprint);
		}

		$db    = JFactory::getDBO();
		$query = 'DESCRIBE '.$db_table;
		$db->setQuery($query);
		$data  = $db->loadObjectList();

		foreach($data as $eachdata){
			if(!in_array($eachdata->Field, $dndArray)){
				$field_array[] = $eachdata->Field;
				$query = "ALTER TABLE ".$db_table." DROP ".$eachdata->Field;
				$db->setQuery($query);
				$db->execute();
			}
		}
		return $this->operationUpdate($table,$columnstoinsert,$toprint);
	}

	/**
	 *
	 */
	private function addLanguageVariables($table,$columnstoinsert,$toprint){

		if($table == "state"){
			$db_table = "rail_state";
		}else if($table == "district"){
			$db_table = "india_information";
		}else if($table == "Urban"){
			$db_table = "jos_mica_urban_agglomeration";
		}else if($table == "Town"){
			$db_table = "villages";
		}
		else if($table == "villagesummery"){
			$db_table = "villagesummery";
		}
		$db    = JFactory::getDBO();
		$query = 'DESCRIBE '.$db_table;
		$db->setQuery($query);
		$data  = $db->loadObjectList();

		$str   = "";
		echo "<pre>";

		foreach($toprint as $field){
			foreach($field as $key => $eachfield){
				//if(trim($key) == "Variable"){
					$eachfield = trim($eachfield);
					$eachfield = str_replace(" ","_",$eachfield);
					$eachfield = str_replace(")","",$eachfield);
					$eachfield = str_replace("(","",$eachfield);
					$eachfield = str_replace("-","_",$eachfield);
					$eachfield = str_replace("/","_",$eachfield);
					$eachfield = str_replace("'","_",$eachfield);
					$eachfield = str_replace("`","_",$eachfield);
					$eachfield = str_replace(",","_",$eachfield);
					$eachfield = str_replace("|","_",$eachfield);

					$eachfieldkey = str_replace("\\","_",$eachfield);
          if(strlen($eachfieldkey) > 63)
          {
            $k="";
            $eachfieldwords = preg_split("/[\s,_-]+/", $eachfieldkey);
            foreach ($eachfieldwords as $w) {
              $k .= $w[0].$w[1];
            }
            $eachfieldkey=$k;
          }
					if($eachfieldkey!=""){
						$str .= strtoupper($eachfieldkey)."='".$key."' \n";
					}
				// }else{
				// 	if($eachfieldkey!=""){
				// 		$str .= $eachfield."' \n";
				// 	}
				// }
			}
      break;
		}

		$str1 .= "Please Copy and Pest Below code in '/language/en-GB/en-GB.com_mica.ini' \n <pre>".$str."</pre>";
		return ($str1);
	}

	/**
	 *
	 */
	private function operationUpdate($table,$columnstoinsert,$toprint){
		if($table == "state"){
			$db_table = "rail_state";
		}else if($table == "district"){
			$db_table = "india_information";
		}else if($table == "Urban"){
			$db_table = "jos_mica_urban_agglomeration";
		}else if($table == "Town"){
			$db_table = "villages";
		}
		else if($table == "villagesummery"){
			$db_table = "villagesummery";
		}

		$db    = JFactory::getDBO();
		$query = 'DESCRIBE '.$db_table;
		$db->setQuery($query);
		$data  = $db->loadObjectList();

		foreach($data as $eachdata) {
			$field_array[] = $eachdata->Field;
		}

		foreach($columnstoinsert as $eachfield){
			if (!in_array($eachfield, $field_array)){
				if(stristr($eachfield,"name")){
					$datatype = " varchar(100)";
				}else{
					$datatype = " TEXT";
				}

				if($eachfield == "Rating"){
					$datatype = " INT(100)";
				}

				$eachfield = trim($eachfield);
				$eachfield = str_replace(" ","_",$eachfield);
				$eachfield = str_replace(")","",$eachfield);
				$eachfield = str_replace("\"","",$eachfield);
				$eachfield = str_replace('"',"",$eachfield);
				$eachfield = str_replace("'","",$eachfield);
				$eachfield = str_replace("(","",$eachfield);
				$eachfield = str_replace("-","",$eachfield);
				$eachfield = str_replace("*","",$eachfield);
				$eachfield = str_replace("/","",$eachfield);
				$eachfield = str_replace("'","",$eachfield);
				$eachfield = str_replace("`","",$eachfield);
				$eachfield = str_replace(",","",$eachfield);
				$eachfield = str_replace("|","",$eachfield);
				$eachfield = str_replace(":","",$eachfield);
				$eachfield = str_replace(".","",$eachfield);
				$eachfield = str_replace("?","",$eachfield);
				$eachfield = str_replace("&","",$eachfield);
				$eachfield = str_replace("\\","",$eachfield);
				$eachfield = str_replace("__","_",$eachfield);

				// $eachfield = trim($eachfield);
				// $eachfield = str_replace(" ","_",$eachfield);
				// $eachfield = str_replace(")","",$eachfield);
				// $eachfield = str_replace("(","",$eachfield);
				// $eachfield = str_replace("-","",$eachfield);
				// $eachfield = str_replace("/","",$eachfield);
				// $eachfield = str_replace("'","",$eachfield);
				// $eachfield = str_replace("`","",$eachfield);
				// $eachfield = str_replace(",","",$eachfield);
				// $eachfield = str_replace("|","",$eachfield);

				// $eachfield = str_replace("\\","",$eachfield);
				// $eachfield = str_replace("__","_",$eachfield);
				if(strlen($eachfield) > 63)
		        {
		          $k="";
		          $eachfieldwords = preg_split("/[\s,_-]+/", $eachfield);
		          foreach ($eachfieldwords as $w) {
		            $k .= $w[0].$w[1];
		          }
		          $eachfield=$k;
		        }
				$query = 'ALTER TABLE '.$db_table.' ADD '. $eachfield.$datatype;
				
				$db->setQuery($query);
				try {
					$db->execute();
				} catch (Exception $e) {

				}
				//$result[] = $query;
				//$result[] = 'ALTER TABLE '.$db_table.' ADD '. $eachfield.' INT(100)';
			}
		}
		

		$options['format']    = '{DATE}\t{TIME}\t{LEVEL}\t{CODE}\t{MESSAGE}';
		$options['text_file'] = 'error.php';
		JLog::addLogger($options, JLog::INFO, array('error'));

		$toprint    = array_reverse($toprint);
		$queryarray = array();
		foreach($toprint as $key => $val){
			$queryarray[$db_table][] = $val;
		}

		foreach($queryarray as $table=>$values){
			foreach($values as $val){
				$qvalues = array();
				foreach($val as $k => $v){

					if($v!=null){
						$eachfield = trim($k);
						$eachfield = str_replace("-","",$eachfield);
						$eachfield = str_replace(" ","_",$eachfield);
						$eachfield = str_replace('"','',$eachfield);
						$eachfield = str_replace("'","",$eachfield);
						$eachfield = str_replace("\"","",$eachfield);
						$eachfield = str_replace('&quot;',"",$eachfield);
						$eachfield = str_replace('"',"",$eachfield);
						$eachfield = str_replace(")","",$eachfield);
						$eachfield = str_replace("(","",$eachfield);
						$eachfield = str_replace("/","",$eachfield);
						$eachfield = str_replace("'","",$eachfield);
						$eachfield = str_replace("`","",$eachfield);
						$eachfield = str_replace(",","",$eachfield);
						$eachfield = str_replace("*","",$eachfield);
						$eachfield = str_replace("|","",$eachfield);
						$eachfield = str_replace(".","",$eachfield);
						$eachfield = str_replace("?","",$eachfield);
						$eachfield = str_replace("&","",$eachfield);
						$eachfield = str_replace(":","",$eachfield);


						// $eachfield = str_replace("-","",$eachfield);
						// $eachfield = str_replace(" ","_",$eachfield);
						// $eachfield = str_replace(")","",$eachfield);
						// $eachfield = str_replace("(","",$eachfield);
						// $eachfield = str_replace("/","",$eachfield);
						// $eachfield = str_replace("'","",$eachfield);
						// $eachfield = str_replace("`","",$eachfield);
						// $eachfield = str_replace(",","",$eachfield);
						// $eachfield = str_replace("|","",$eachfield);

						$k = str_replace("\\","",$eachfield);
						$k = str_replace("__","_",$eachfield);
						if($k == "Rating"){
							$v = intval($v);
						}
			if(strlen($k) > 63)
            {
              $ks="";
              $eachfieldwords = preg_split("/[\s,_-]+/", $k);
              foreach ($eachfieldwords as $w) {
                $ks .= $w[0].$w[1];
              }
              $k=$ks;
            }
						$qvalues[] = "`".$k."` = \"".ucwords(strtolower($v))."\" ";
					}
				}

				$qvaluess = implode(",",$qvalues);
				
				$state ='';
				if(isset($val['State']))
					$state = $val['State'];
				else if(isset($val['state']))
					$state = $val['state'];


				
				if($state != ""){
					if($table == "india_information"){
						/*$dtableb->setQuery('UPDATE  `'.$table.'` SET  '.$qvaluess.' WHERE  `OGR_FID`  = '.$val['OGR_FID']);
						echo "select OGR_FID from india_information where state like '".$val['State']."' and distshp like '".$val['District']."'";exit;
						*/
						$db->setQuery("select OGR_FID from india_information where state like '".$state."' and distshp like '".$val['District']."'");
						//echo "select OGR_FID from india_information where state like '".$val['State']."' and distshp like '".$val['District']."'";
						//echo "select OGR_FID from india_information where state like '".$val['State']."' and distshp like '".$val['District']."'";
						$val['OGR_FID'] = $ogr_fid=$db->loadResult();



						if($ogr_fid!=""){
							
							echo $ogr_fid ." found for ".$state . ' -- '.$val['District']."<BR>";
							$db->setQuery('UPDATE  `'.$table.'` SET  '.$qvaluess.' WHERE `state` = "'.ucwords(strtolower($state)).'" and `distshp` = "'.ucwords(strtolower($val['District'])).'"');
							$q = $ogr_fid.'->'.'UPDATE  `'.$table.'` SET  '.$qvaluess.' WHERE `state` = "'.ucwords(strtolower($state)).'" and `distshp` = "'.ucwords(strtolower($val['District'])).'"';
						}
						else {
							echo "NOT-FOUND for ".$state . ' -- '.$val['District']."<BR>";
							//$db->setQuery('INSERT into  `'.$table.'` set  '.$qvaluess);
						}

						/*}else{
							$q=$ogr_fid.'->'.ucwords(trim($val['District'])).'"=>"'.ucwords(trim($val['State']));
						}*/
					}
				}
				elseif($state != ""){
					if($table == "villagesummery"){

						/*$db->setQuery('UPDATE  `'.$table.'` SET  '.$qvaluess.' WHERE  `OGR_FID`  = '.$val['OGR_FID']);
						echo "select OGR_FID from india_information where state like '".$val['State']."' and distshp like '".$val['District']."'";exit;
						$db->setQuery("select OGR_FID from india_information where state like '".$val['State']."' and distshp like '".$val['District']."'");
						echo "select OGR_FID from india_information where state like '".$val['State']."' and distshp like '".$val['District']."'";
						$ogr_fid=$db->loadResult();
						if($ogr_fid!=""){*/

							$db->setQuery("select OGR_FID from villagesummery where OGR_FID = " . $val['OGR_FID']);
							$ogr_fid=$db->loadResult();

							if ($ogr_fid!="") {
								$db->setQuery('UPDATE  `'.$table.'` SET  '.$qvaluess.' WHERE `state` = "'.ucwords(strtolower($state)).'" and `distshp` = "'.ucwords(strtolower($val['District'])).'"');
								$q = $ogr_fid.'->'.'UPDATE  `'.$table.'` SET  '.$qvaluess.' WHERE `state` = "'.ucwords(strtolower($state)).'" and `distshp` = "'.ucwords(strtolower($val['District'])).'"';
							}
							else {
								$db->setQuery('INSERT into  `'.$table.'` set  '.$qvaluess);
							}

						/*}else{
							$q=$ogr_fid.'->'.ucwords(trim($val['District'])).'"=>"'.ucwords(trim($val['State']));
						}*/
					}
				}
				elseif($table == "villages"){
					$db->setQuery("select OGR_FID from villages where OGR_FID = ".$val['OGR_FID']);
					$ogr_fid=$db->loadResult();

					if($ogr_fid!=""){
						$db->setQuery('UPDATE  `'.$table.'` SET  '.$qvaluess.' WHERE `OGR_FID` = '.$val['OGR_FID']);
					}
					else {
						$db->setQuery('INSERT into  `'.$table.'` set  '.$qvaluess);
					}
				}
				if ($val['OGR_FID'] != 0) {
					$res          = $db->execute();
					$affectedRows = (int) $db->getAffectedRows($res);

					if(!$res || $affectedRows == 0){
						JLog::add($q, JLog::INFO, 'error');
					}
				}
			}
		}
		echo implode("", $ar);
		return "<b>Done!!</b>";
	}

	public function getvariablelist()
	{
		$id = $this->input->get('sheet', '', 'string');
		$db = JFactory::getDbo();
		$query = "SELECT * FROM `variable_rows` WHERE `dataset_master_id` = '".$id."'";
		$db->setQuery($query);
		$variable = $db->loadObjectList();
		$html = '';
		$html .= '<option value="">Select Variable</option>';
		foreach ($variable as $key => $value) {
			$html .= '<option value="'.$value->id.'">'.$value->variable_name.'</option>'; 
		}
		echo $html;
		exit();
	}
	public function getidentifierlist()
	{
		$id = $this->input->get('sheet', '', 'string');
		$db = JFactory::getDbo();
		$query = "SELECT * FROM `variable_rows` WHERE `dataset_master_id` = '".$id."' AND `variable_type` = 'identifiers'";
		$db->setQuery($query);
		$variable = $db->loadObjectList();

		$query = "SELECT * FROM `filter_managment` WHERE `dataset_master_id` = '".$id."'" ;
		$db->setQuery($query);
		$data_mapping = $db->loadObjectList();
		$array = [];
		foreach ($data_mapping as $key => $value) {
			$array[$value->name] = $value->variable_row_id;
		}
		/*echo "<pre>";
		print_r($data_mapping);
		print_r($array);
		die();*/
		$html = '';
		$html .= '<div class="data-div">';
		$html .= '<input type="text" placeholder="Enter key Word" value="State" readonly>&nbsp;<select  name="state" id="state" class="select"><option value="">Select State Variable</option>';
		foreach ($variable as $key => $value) {
			if(isset($array['state']) && $array['state'] == $value->id){
				$html .= '<option value="'.$value->id.'" selected>'.$value->variable_name.'</option>';	
			} else {

			$html .= '<option value="'.$value->id.'">'.$value->variable_name.'</option>';
			}
		}	
		$html .= '</select></div>';
		$html .= '<div class="data-div">';
		$html .= '<input type="text" placeholder="Enter key Word" value="District" readonly>&nbsp;<select  name="district" id="district" class="select"><option value="">Select District Variable</option>';
		foreach ($variable as $key => $value) {
			if(isset($array['district']) &&  $array['district'] == $value->id){
				$html .= '<option value="'.$value->id.'" selected>'.$value->variable_name.'</option>';	
			} else {

				$html .= '<option value="'.$value->id.'">'.$value->variable_name.'</option>';
			}
		}
		$html .='</select></div>';
		$html .= '<div class="data-div">';
		$html .= '<input type="text" placeholder="Enter key Word" value="Sub District" readonly>&nbsp;<select  name="sub_district" id="sub_district" class="select"><option value="">Select Sub District Variable</option>';
		foreach ($variable as $key => $value) {
			if(isset($array['sub district']) &&  $array['sub district'] == $value->id){
				$html .= '<option value="'.$value->id.'" selected>'.$value->variable_name.'</option>';	
			} else {

				$html .= '<option value="'.$value->id.'">'.$value->variable_name.'</option>';
			}
		}
		$html .= '</select></div>';
		$html .= '<div class="data-div">';
		$html .= '<input type="text" placeholder="Enter key Word" value="Village" readonly>&nbsp;<select  name="village" id="village" class="select"><option value="">Select Village Variable</option>';
		foreach ($variable as $key => $value) {
			if(isset($array['village']) && $array['village'] == $value->id){
				$html .= '<option value="'.$value->id.'" selected>'.$value->variable_name.'</option>';	
			} else {
				$html .= '<option value="'.$value->id.'">'.$value->variable_name.'</option>';
			}
		}
		$html .= '</select></div>';
				



		/*$html .= '<option value="">Select Variable</option>';
		foreach ($variable as $key => $value) {
			$html .= '<option value="'.$value->id.'">'.$value->variable_name.'</option>'; 
		}*/
		echo $html;
		exit();
	}

	public function savemapping() {
		$id = $this->input->get('sheet', '', 'string');
		$data = $this->input->get('mapping', '', 'array');
		$db = JFactory::getDbo();
		foreach ($data as $key => $value) {
			foreach (json_decode($value) as $k => $v) {
				foreach ($v as $row_id) {
					$data = new stdClass();
					$data->dataset_master_id = $id;
					$data->name=$k;
					$data->variable_row_id=$row_id;
					// Insert the object into the user data table.
					$result = $db->insertObject('data_mapping', $data);
				}
			}
		}
		die('ujash');
	}

	public function getheadervariable() {
		$id = $this->input->get('sheet', '', 'string');
		$mapping_value = $mapping_variable = [];
		$db = JFactory::getDbo();
		$query= "SELECT * FROM variable_rows WHERE `dataset_master_id`=".$id;
		$result = $db->setQuery($query);
		$result = $db->loadObjectList();

		$mapping = "SELECT * FROM data_mapping WHERE `dataset_master_id`=".$id;
		$mapping_data = $db->setQuery($mapping);
		$mapping_data = $db->loadObjectList();		
		foreach ($mapping_data as $key => $value) {
			array_push($mapping_value, $value->name);
			array_push($mapping_variable, $value->variable_row_id);			
		}

		$mapping_variable = array_unique($mapping_variable);
		$mapping_value = array_unique($mapping_value);
		$html = '';
		$html .= '<table border=0 class="table"><tbody>';
		foreach ($result as $key => $value) {
			if(!in_array($value->id, $mapping_variable)){
				$html .= '<tr><td>'.$value->variable_name.' is Filterable ? </td>';
				if($value->is_filterable == 1){
					$html .= '<td><input type="radio" name="'.$value->id.'" data-type="default" value="yes" checked>Yes</td>';	
				} else {
					$html .= '<td><input type="radio" name="'.$value->id.'" data-type="default" value="yes" >Yes</td>';	
				}
				if($value->is_filterable == 0){
					$html .= '<td><input type="radio" name="'.$value->id.'" data-type="default" value="no" checked>No</td>';	
				} else {
					$html .= '<td><input type="radio" name="'.$value->id.'" data-type="default" value="no">No</td>';
				}
				
				
				$html .= '</tr>';
			}
		}
		foreach ($mapping_value as $key => $value) {
			$html .= '<tr><td>'.$value.' is Filterable ?</td>';
			$html .= '<td><input type="radio" name="'.$value.'" data-type="mapping" value="yes" checked>Yes</td>';
			$html .= '<td><input type="radio" name="'.$value.'" data-type="mapping" value="no">No</td>';
			$html .= '</tr>';
		}
		$html .= '</tbody></table>';
		$html .= '<button class="btn btn-sm btn-success submit">Submit</button>'; 
		echo $html;
		exit();

	}
	public function savefiltervariable()
	{
		$data = $this->input->get('data', '', 'array');
		$id = $this->input->get('sheet', '', 'string');		
		$db = JFactory::getDbo();
		foreach (json_decode($data[0]) as $key => $value) {
			
			foreach ($value as $k => $v) {
				if($key == 'default'){
					$is_filterable = $v[1] == 'yes' ? 1 :0;
					$query = 'UPDATE `variable_rows` SET `is_filterable`= "'.$is_filterable.'" WHERE `id` = "'.$v[0].'" ';
					$db->setQuery($query);
					$db->query();	
				}
				if($key == 'mapping'){
				
					$getMapping = 'SELECT * FROM data_mapping WHERE dataset_master_id = '.$id.' AND name = "'.$v[0].'"';
					$result = $db->setQuery($getMapping);
					$result = $db->loadObjectList();
					foreach ($result as $key1 => $value1) {
						$is_filterable = $v[1] == 'yes' ? 1 :0;
						$query = 'UPDATE `variable_rows` SET `is_filterable`= "'.$is_filterable.'" WHERE `id` = "'.$value1->variable_row_id.'" ';
						
						$db->setQuery($query);
						$db->query();
					}
				}		
			}
		}
						die();
	}

	public function savevariablemanagment() {
		$id = $this->input->get('sheet', '', 'string');
		$data = $this->input->get('data', '', 'array');		
		$db = JFactory::getDbo();

		foreach (json_decode($data[0]) as $key => $value) {
			if($key && $value){
				$obj = new stdClass();
				$obj->dataset_master_id = $id;
				$obj->name=$key;
				$obj->variable_row_id=$value;
				// Insert the object into the user data table.
				$result = $db->insertObject('filter_managment', $obj);	
			}
		}
	}

	public function importsheet()
	{
		$db = JFactory::getDbo();
		$query = "SELECT * FROM dataset_master WHERE `status` = 1";	
		$result = $db->setQuery($query);
		$result = $db->loadObjectList();

		$html = '';
		$html = '<table class="table"><tbody>';
		$html .= '<tr><td>NO.</td><td>Sheet Name</td><td>Actions</td></tr>';
		$i = 1;
		foreach ($result as $key => $value) {
			$html .='<tr>'; 
			$html .= '<td>'.$i.'</td>';
			$html .= '<td>'.$value->name.'</td>';
			$html .= '<td><a href="index.php?option=com_mica&view=datamapping&sheet='.$value->id.'"><button class="btn btn-sm btn-success">Data Mapping</button></a>&nbsp;<a href="index.php?option=com_mica&view=filtermapping&sheet='.$value->id.'"><button class="btn btn-sm btn-success">Filter Mapping</button></a>&nbsp;<a href="index.php?option=com_mica&view=variablemanagment&sheet='.$value->id.'"><button class="btn btn-sm btn-success">Variable Managment</button></a></td>';
			$html .='</tr>'; 
			$i++;			
		}
		$html.='</tbody></table>';

		echo $html ;
		exit();
	}
}
