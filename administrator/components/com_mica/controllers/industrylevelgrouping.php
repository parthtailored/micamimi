<?php
/**
 * @package     Joomla.Administrator
 * @subpackage  com_mica
 *
 * @copyright   Copyright (C) 2005 - 2017 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

/**
 * MICA Variable Grouping controller class.
 *
 * @since  1.6
 */
class MicaControllerIndustryLevelGrouping extends JControllerLegacy
{
	/**
	 * Proxy for getModel.
	 *
	 * @param   string  $name    The model name. Optional.
	 * @param   string  $prefix  The class prefix. Optional.
	 * @param   array   $config  Configuration array for model. Optional.
	 *
	 * @return  object  The model.
	 *
	 * @since   1.6
	 */
	public function getModel($name = 'IndustryLevelGrouping', $prefix = 'MicaModel', $config = array('ignore_request' => true))
	{
		return parent::getModel($name, $prefix, $config);
	}

	/**
	 * A task to render a form to add new a group.
	 *
	 * @return [type] [<description>]
	 */
	public function add()
	{
		$view = $this->getView('IndustryLevelGrouping', 'html', 'MicaView');
		$view->setModel($this->getModel(), true);
		$view->setLayout('add');

		return $view->add();
	}

	/**
	 * Function to save posted data for group.
	 *
	 * @return  void
	 */
	public function save()
	{
		$groupname        = $this->input->post->get('groupname', '', 'raw');
		$groupDescription = $this->input->post->get('groupDescription', '', 'raw');
		$deleteimage      = $this->input->post->get('deleteimage', 0, 'int');
		$id               = $this->input->post->get('id');
		$type             = implode(',', $this->input->post->get('type'));
		$checkedattr      = array("State" => array(), "District" => array(), "Urban" => array(), "Town" => array() );

		foreach ($checkedattr as $field_type => &$each_field)
		{
			$each_field = $this->input->post->get("attr_" . $field_type, array(), 'array');
		}

		$insertedicon = $this->uploadIcon();
		$db           = JFactory::getDBO();

		if ($id > 0 )
		{
			if ($insertedicon != "")
			{
				$icon_update = ", " . $db->quoteName('icon') . " = " . $db->quote($insertedicon);
			}

			if ( $deleteimage == 1)
			{
				$icon_update = ", " . $db->quoteName('icon') . " = " . $db->quote('');
			}

			$query = "UPDATE  " . $db->quoteName('#__mica_industry_level_group') . "
				SET " . $db->quoteName('group') . " = " . $db->quote($groupname) . $icon_update . ", " . $db->quoteName('type') . " = " . $db->quote($type) .", " .$db->quoteName('groupDescription') . " = " . $db->quote($groupDescription). "
				WHERE " . $db->quoteName('id') . " = " . $db->quote($id);
			$db->setQuery($query);
			$db->execute();
		}
		else
		{
			$query = "INSERT INTO " . $db->quoteName('#__mica_industry_level_group') . "
				(" . $db->quoteName('group') . ", " . $db->quoteName('groupDescription') . "," . $db->quoteName('icon') . "," . $db->quoteName('publish') . "," . $db->quoteName('type') . ") VALUES (" . $db->quote($groupname) . ", " . $db->quote($groupDescription) . ", " . $db->quote($insertedicon) . ", " . $db->quote(1) . ", " . $db->quote($type) . ")";
			$db->setQuery($query);
			$db->execute();
			$id = $db->insertid();
		}

		if ($id > 0)
		{
			$query = "DELETE FROM  " . $db->quoteName('#__mica_industry_level_group_field') . " WHERE  " . $db->quoteName('groupid') . " = " . $db->quote($id);
			$db->setQuery($query);
			$db->execute();
		}

		$VALUES = array();

		if (count($checkedattr) > 0)
		{
			foreach ($checkedattr as $field_type => $fields)
			{
				if (count($fields) > 0)
				{
					foreach ($fields as $key => $each_field)
					{
						$VALUES[] = " (" . $db->quote($id) . ", " . $db->quote($each_field) . ", " . $db->quote($field_type) . ") ";
					}
				}
			}
		}

		if (count($VALUES) > 0)
		{
			$query = "INSERT INTO  " . $db->quoteName('#__mica_industry_level_group_field') . "
				(" . $db->quoteName('groupid') . ", " . $db->quoteName('field') . ", " . $db->quoteName('table') . ")
				VALUES " . implode(", ", $VALUES);
			$db->setQuery($query);
			$db->execute();
		}

		$msg = JText::_('GROUP_UPDATED');

		if ($id > 0)
		{
			if ($insertedicon === 0 && $deleteimage != 1)
			{
				$msg = JText::_('GROUP_INSERTED_WITH_IMAGE_ERROR');
			}
		}
		else
		{
			if ($insertedicon === 0 )
			{
				$msg = JText::_('GROUP_INSERTED_WITH_IMAGE_ERROR');
			}
		}

		if ($deleteimage == 1)
		{
			$oldimage = $this->input->post->get('oldimage', '', 'raw');
			unlink(JPATH_SITE . "/components/com_mica/images/" . $oldimage);
		}

		$this->setRedirect("index.php?option=com_mica&view=industrylevelgrouping", $msg, 'success');
	}

	/**
	 * Uploads Icon for groups.
	 *
	 * @return  boolean
	 */
	private function uploadIcon()
	{
		$file = $this->input->files->get('iconimage', array(), 'array');

		if (count($file) > 0)
		{
			list($width, $height, $type, $attr) = getimagesize($file['tmp_name']);

			$validimage = array("image/jpeg","image/png","image/pjpeg","image/gif","image/jpg");

			/*if ($width > 33 && $height > 33 && !in_array($validimage, $file['type']))
			{
				return false;
			}*/

			if (move_uploaded_file($file['tmp_name'], JPATH_SITE . "/components/com_mica/images/" . $file['name']))
			{
				return $file['name'];
			}
			else
			{
				return false;
			}
		}
		else
		{
			return false;
		}
	}

	/**
	 * A redirection task called to calcel the group form.
	 *
	 * @return  void
	 */
	public function cancel_add()
	{
		$this->setRedirect("index.php?option=com_mica&view=industrylevelgrouping", JText::_('Operation Cancelled'), 'warning');
	}

	/**
	 * Publish Groups.
	 *
	 * @return  void
	 */
	public function publish()
	{
		$cid = $this->input->post->get('cid', array(), 'array');

		if (count($cid) > 0)
		{
			$cid = implode(",", $cid);
			$db = JFactory::getDBO();
			$STATE_SQL = " UPDATE " . $db->quoteName('#__mica_industry_level_group') . "
				SET " . $db->quoteName('publish') . " = " . $db->quote(1) . "
				WHERE " . $db->quoteName('id') . " IN ('" . $cid . "')";
			$db->setQuery($STATE_SQL);
			$db->execute();
			$this->setRedirect("index.php?option=com_mica&view=industrylevelgrouping", JText::_('GROUP_PUBLISHED'), 'success');
		}
		else
		{
			$this->setRedirect("index.php?option=com_mica&view=industrylevelgrouping", JText::_('Kindly select group first.'), 'error');
		}
	}

	/**
	 * Unpublish Groups.
	 *
	 * @return  void
	 */
	public function unpublish()
	{
		$cid = $this->input->post->get('cid', array(), 'array');

		if (count($cid) > 0)
		{
			$cid       = implode(",", $cid);
			$db        = JFactory::getDBO();
			$STATE_SQL = " UPDATE " . $db->quoteName('#__mica_industry_level_group') . "
				SET " . $db->quoteName('publish') . " = " . $db->quote(0) . "
				WHERE " . $db->quoteName('id') . " IN ('" . $cid . "')";
			$db->setQuery($STATE_SQL);
			$db->execute();
			$this->setRedirect("index.php?option=com_mica&view=industrylevelgrouping", JText::_('GROUP_UNPUBLISHED'), 'success');
		}
		else
		{
			$this->setRedirect("index.php?option=com_mica&view=industrylevelgrouping", JText::_('Kindly select group first.'), 'error');
		}
	}

	/**
	 * A task to be called to delete groups.
	 *
	 * @return  void
	 */
	public function delete()
	{
		$cid = $this->input->post->get('cid', array(), 'array');

		if (count($cid) > 0)
		{
			$cid = implode(",", $cid);
			$db = JFactory::getDBO();
			$DELETE_SQL = "DELETE FROM " . $db->quoteName('#__mica_industry_level_group') . " WHERE " . $db->quoteName('id') . " IN ('" . $cid . "')";
			$db->setQuery($DELETE_SQL);
			$db->execute();

			$DELETE_SQL = "DELETE FROM " . $db->quoteName('#__mica_industry_level_group_field') . " WHERE " . $db->quoteName('groupid') . " IN ('" . $cid . "')";
			$db->setQuery($DELETE_SQL);
			$db->execute();

			$msg = (count($cid) > 1) ? JText::_('GROUPS_DELETED') : JText::_('GROUPS_DELETED');
			$this->setRedirect("index.php?option=com_mica&view=industrylevelgrouping", $msg, 'success');
		}
		else
		{
			$this->setRedirect("index.php?option=com_mica&view=industrylevelgrouping", JText::_('Kindly select group first.'), 'error');
		}
	}
}
