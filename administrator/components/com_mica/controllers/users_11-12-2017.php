<?php
/**
 * @package     Joomla.Administrator
 * @subpackage  com_mica
 *
 * @copyright   Copyright (C) 2005 - 2017 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

/**
 * MICA Users controller class.
 *
 * @since  1.6
 */
class MicaControllerUsers extends JControllerAdmin
{

	/**
	 * Proxy for getModel.
	 *
	 * @param   string  $name    The model name. Optional.
	 * @param   string  $prefix  The class prefix. Optional.
	 * @param   array   $config  Configuration array for model. Optional.
	 *
	 * @return  object  The model.
	 *
	 * @since   1.6
	 */
	public function getModel($name = 'Users', $prefix = 'MicaModel', $config = array('ignore_request' => true))
	{
		return parent::getModel($name, $prefix, $config);
	}

	/**
	 * A task to render a form to UPDATE ATTRIBUTES of plans.
	 */
	public function updateAttr(){
		$view = $this->getView('Users', 'html', 'MicaView');
		$view->setModel($this->getModel(), true);
		$view->setLayout('updateAttr');
		return $view->updateAttr();

		$plan   = $this->input->get->get('plan', 0, 'string');
		$planid = $this->input->get->get('planid', 0, 'string');

		//$limitattr=$this->limitAttrByPlan($planid);
		$db_table = array("State","District","Town","Urban");
		//echo $this->addToolbar();

		$db    = JFactory::getDBO();
		$query = "SELECT *
			FROM ".$db->quoteName('#__mica_user_attribute')."
			WHERE ".$db->quoteName('aec_plan_id')." = ".$db->quote($planid)."
			GROUP BY ".$db->quoteName('dbtable');
		//$query="select * from #__mica_group_field allfields JOIN #__mica_group grp ON grp.id=allfields.groupid GROUP BY allfields.groupid";
		$db->setQuery($query);
		$userattribute = $db->loadResult();
		$selecteddata  = $db->loadObjectList();

		$selectedtable = "";
		foreach($selecteddata as $eachplan){
			$selectedtable[$eachplan->dbtable] = $eachplan->attribute;
		}
		//echo "<pre>";print_r($selecteddata);exit;
		$str ='
			<script type="text/javascript">
				function submitbutton(event){
					if(event=="save"){
						$("#myform").submit();
						//ajaxsave();
					}else if(event=="cancel"){
						window.location="index.php?option=com_mica&view=users";
					}
				}

				function ajaxsave(){
					var tablearray = ["rail_state","india_information"];
					var counton    = [];

					for(var i=0;i<tablearray.length;i++)
					{
						jQuery("input."+tablearray[i]+"_attributes:checkbox").each(function () {
							if(this.checked)
							{
								counton.push(jQuery(this).val());
							}
						});
						if(counton.length > 0)
						{
							save(counton,tablearray[i]);
							counton=[];
						}
					}
					alert("Saved");
				}

				function save(counton,myclass){
					var myplan= "'.$planid.'";
					if(myplan=="")
					{
						alert("select Plan First");
					}

					jQuery.ajax({
						type    :"POST",
						url     :"index.php?option=com_mica&view=users&task=users.saveAttr&planid='.$planid.'&table="+myclass,
						data    :"attr="+counton,
						success : function(data){

						}
					});
				}
			</script>';

		$checked    =false;
		$str .="<form name='myform' id='myform' action='' method='post'>";
		$j          = 0;
		$k          = 0;
		$javascript = "";

		$myarray = array();
		foreach($db_table as $eachtable){
			//$result = mysql_query('DESCRIBE '.$eachtable);
			//$query  ='DESCRIBE '.$eachtable;
			$query = "SELECT * FROM ".$db->quoteName('#__mica_group_field')." AS allfields
				JOIN ".$db->quoteName('#__mica_group')." AS grp ON ".$db->quoteName('grp.id')." = ".$db->quoteName('allfields.groupid')."
				WHERE ".$db->quoteName('allfields.table')." LIKE ".$db->quote($eachtable);
			$db->setQuery($query);
			$allattributes = $db->loadObjectList();

			// india_information
			$values = array();
			if($eachtable == "State"){
				$label = "State Field";
				$j     = 1;
			}else if($eachtable == "District"){
				$label = "District Field";
				$j     = 2;
			}else if($eachtable == "Town"){
				$label = "Town Field";
				$j     = 3;
			}else if($eachtable == "Urban"){
				$label = "Urban Field";
				$j     = 4;
			}

			$javascript .='
				function toggleChecked'.$j.'(status){
					jQuery(".checkbox'.$j.'").each( function() {
						jQuery(this).attr("checked",status);
					});
				}';

			$str .="<table width='50%' style='float:left;width:25%;' class='table table-striped adminlist' >
						<thead><tr width='50%'> <th colspan='2'>".$label."<th><tr>
						<tr ><td><a href='#' onClick='toggleChecked$j(true)'>Check All</a> / <a href='#'  onClick='toggleChecked$j(false)'>Uncheck All</a></td><td>Select Attribute</td></tr></thead>";

    		for ($i = 0; $i < count($allattributes); $i++){
				$myarray[$k][$allattributes[$i]->table][$allattributes[$i]->group][] = $allattributes[$i]->field;
			}

			if (count($myarray) > 0 && array_key_exists($k, $myarray) && array_key_exists($eachtable, $myarray[$k])) {
				foreach($myarray[$k][$eachtable] as $key => $val){
					$str .= '<tr><th colspan="2">'.$key.'</th></tr>';
					for($i = 0; $i < count($val); $i++){
						if(@strstr($selectedtable[$eachtable],$val[$i])){
							$str .='<tr >
								<td align="left" valign="top"><input type="checkbox" name="'.$eachtable.'_attributes[]" class="'.$eachtable.'_attributes checkbox'.$j.'" value="'.$val[$i].'" checked></td>
								<td align="left" valign="top">'.$val[$i].'</td>
							</tr>';
						}else{
							$str .='<tr>
								<td align="left" valign="top"><input type="checkbox" name="'.$eachtable.'_attributes[]" class="'.$eachtable.'_attributes checkbox'.$j.'" value="'.$val[$i].'"></td>
								<td align="left" valign="top">'.$val[$i].'</td>
							</tr>';
						}
					}
				}
			}

			$k++;
		}

		$str .= "</table>
			<input type='hidden' name='option' value='com_mica' />
			<input type='hidden' name='view' value='users' />
			<input type='hidden' name='task' value='users.saveAttr' />
			<input type='hidden' name='tables' value='".implode(',',$db_table)."' />
			<input type='hidden' name='planid' value='".$planid."' />
			</form>";
		echo '<script type="text/javascript">'.$javascript.'</script>';
		echo $str;
		//parent::display($tpl);
	}

	/**
	 * A task to be called after submission of Update Attributes form for specific plan.
	 */
	public function saveAttr(){
		$userid = $this->input->get("userid");
		//$attr   = $this->input->post->get("attr");
		$planid = $this->input->post->get("planid");
		$tables = $this->input->post->get("tables", "", 'raw');

		$table  = explode(",",$tables);
		for($i = 0; $i < count($table); $i++){
			$attributes[$i] = $this->input->post->get($table[$i]."_attributes", array(), "array");

			if(!is_array($attributes[$i])  && $attributes[$i] != ""){
				$attributes[$i] = implode(",",$attributes[$i]);
				unset($tables[$i]);
			}else{
				$attributes[$i] = implode(",",$attributes[$i]);
			}
		}

		$db = JFactory::getDBO();
		for($i = 0; $i < count($table); $i++){
			$query = "SELECT COUNT(*) FROM ".$db->quoteName('#__mica_user_attribute')."
				WHERE ".$db->quoteName('aec_plan_id')." = ".$db->quote($planid)."
					AND ".$db->quoteName('dbtable')." LIKE ".$db->quote($table[$i]);
			$db->setQuery($query);
			$count = $db->loadResult();

			if($count == 0){
				$this->insertNew($userid, $attributes[$i], $table[$i], $planid);
				$msg = JText::_( 'VARIABLE_INSERTED' );
			}else{
				$this->Update($userid, $attributes[$i], $table[$i], $planid);
				$msg = JText::_( 'VARIABLE_UPDATED' );
			}
		}
		$this->setRedirect("index.php?option=com_mica&view=users",$msg);
		return;
	}

	/**
	 * A private function to be called from saveAttr() function to add new data.
	 */
	private function insertNew($userid, $attr, $table, $planid){
		$db = JFactory::getDBO();
		/* $query = "insert into #__mica_user_profile (user_id,id) values (".$userid.",null);";
		$db->setQuery($query);
		$db->query();
		$lastInsertedId = $db->insertid();*/
		$query = "INSERT INTO ".$db->quoteName('#__mica_user_attribute')."
			(".$db->quoteName('attribute').", ".$db->quoteName('dbtable').", ".$db->quoteName('aec_plan_id').")
			VALUES ( ".$db->quote($attr).", ".$db->quote($table).", ".$db->quote($planid).") ";
		$db->setQuery($query);
		$db->execute();
		return $db->insertid();
	}

	/**
	 * A private function to be called from saveAttr() function to update existing data.
	 */
	private function Update($userid, $attr, $table, $planid){
		$db = JFactory::getDBO();
		/*$query = "select id from #__mica_user_profile where user_id = ".$userid.";";
		$db->setQuery($query);
		$row = $db->loadRow();*/
		$query = "SELECT COUNT(*) FROM ".$db->quoteName('#__mica_user_attribute')."
			WHERE ".$db->quoteName('aec_plan_id')." = ".$db->quote($planid)."
				AND ".$db->quoteName('dbtable')." LIKE ".$db->quote($table);
		$db->setQuery($query);
		$userattrexist = $db->loadResult();

		if($userattrexist == 0){
			$query = "INSERT INTO ".$db->quoteName('#__mica_user_attribute')."
				(attribute, dbtable, aec_plan_id) VALUES ('".$attr."','".$table."' , '".$planid."') ";
		}else{
			$query = " UPDATE ".$db->quoteName('#__mica_user_attribute')."
				SET ".$db->quoteName('attribute')." = ".$db->quote($attr)."
				WHERE ".$db->quoteName('dbtable')." LIKE ".$db->quote($table)."
					AND ".$db->quoteName('aec_plan_id')." = ".$db->quote($planid);
		}
		$db->setQuery($query);
		$result = $db->execute();
		return true;
	}

	public function cancel_updateattr(){
		$this->setRedirect("index.php?option=com_mica&view=users", JText::_('Operation Cancelled.'), 'warning');
		return;
	}

	public function cancel_updatetype(){
		$this->setRedirect("index.php?option=com_mica&view=users", JText::_('Operation Cancelled.'), 'warning');
		return;
	}


	/**
	 * A task to be called to update types for plan.
	 */
	public function updateType() {
		$view = $this->getView('Users', 'html', 'MicaView');
		$view->setModel($this->getModel(), true);
		$view->setLayout('updateType');
		return $view->updateType();


		$userid = $this->input->get->get("userid");
		$planid = $this->input->get->get("planid");

		$db = JFactory::getDBO();
		$selQuery = "SELECT type FROM ".$db->quoteName('#__mica_user_attribute_type')."
			WHERE ".$db->quoteName('aec_plan_id')." = ".$db->quote($planid);
		$db->setQuery($selQuery);
		$typesData = $db->loadResult();

		echo $this->addToolbar();

		$str = '
		<script type="text/javascript">
			function submitbutton(event){
				if(event=="save"){
					$("#MyFormType").submit();
					//ajaxsave();
				}else if(event=="cancel"){
					window.location="index.php?option=com_mica&view=users";
				}
			}
		</script>';
		$typesDataArray = explode(',',$typesData);
		$str .="<form name='adminForm' id='MyFormType' action='".JURI::base()."index.php?option=com_mica&view=users&task=users.save' method='post'>
				<table width='50%' style='float:left;width:25%;' class='adminlist' >
					<tr width='50%'><th colspan='2'>Select Type</th></tr>";

		if(in_array('Rural',$typesDataArray)) { $chk1 = " checked "; } else { $chk1 = ""; }
		if(in_array('Urban',$typesDataArray)) { $chk2 = " checked "; } else { $chk2 = ""; }

		$str .="<tr>
					<td>
						<input type='checkbox' name='types[]' id='types[]' value='Rural' ".$chk1." /><span style='line-height: 20px;vertical-align: top;'>Rural</span>
					</td>
				</tr>
				<tr>
					<td>
						<input type='checkbox' name='types[]' id='types[]' value='Urban' ".$chk2." /><span style='line-height: 20px;vertical-align: top;'>Urban</span>
					</td>
				</tr>
			</table>
			<input type='hidden' name='task' value='users.save' />
			<input type='hidden' name='planid' value='".$planid."' />
		</form>";

		echo $str;
		//parent::display($tpl);
	}

	/**
	 * A task to be called after submission of Update Types form.
	 */
	public function save(){
		$db = JFactory::getDBO();

		//$userid  =  jRequest::getVar("userid");
		$planid = $this->input->post->get("planid", 0 , "int");

		$query = "SELECT COUNT(*)
			FROM ".$db->quoteName('#__mica_user_attribute_type')."
			WHERE ".$db->quoteName('aec_plan_id')." = ".$db->quote($planid);
		$db->setQuery($query);
		$count = $db->loadResult();
		if($count == 0){
			//$this->insertNewType($userid,$planid);
			$this->insertNewType();
			$msg = JText::_( 'VARIABLE_TYPE_INSERTED' );
		}else{
			$this->UpdateTypeData();
			$msg = JText::_( 'VARIABLE_TYPE_UPDATED' );
		}
		$this->setRedirect("index.php?option=com_mica&view=users", $msg);
		return;
	}

	/**
	 * A private function to be called from save() function to add new data.
	 */
	private function insertNewType(){
		$db     = JFactory::getDBO();
		$types  = implode(',', $this->input->post->get('types', array(), 'array'));
		$planid = $this->input->post->get("planid", 0, "int");

		$query = "INSERT INTO ".$db->quoteName('#__mica_user_attribute_type')."
			(".$db->quoteName('type').", ".$db->quoteName('aec_plan_id').") VALUES ( ".$db->quote($types).", ".$db->quote($planid).") ";
		$db->setQuery($query);echo "<pre />";print_r($query);
		$db->execute();
		return $db->insertid();
	}

	/**
	 * A private function to be called from save() function to update existing data.
	 */
	private function UpdateTypeData(){
		$db     = JFactory::getDBO();
		$types  = implode(',', $this->input->post->get('types', array(), 'array'));
		$planid = $this->input->post->get("planid", 0, "int");

		$query = "UPDATE ".$db->quoteName('#__mica_user_attribute_type')."
				SET ".$db->quoteName('type')." = ".$db->quote($types)."
			WHERE ".$db->quoteName('aec_plan_id')." = ".$db->quote($planid);
		$db->setQuery($query);
		$db->execute();
		return true;
	}

	/**
	 * NON Used function, detected during joomla migration.
	 * private function called from updateAttr().
	 */
	private function limitAttrByPlan($plan){
		if($plan == 4){
			return 3;
		}else if($plan == 1){
			return 5;
		}else if($plan == 2){
			return 10;
		}else if($plan == 3){
			return 15;
		}
	}

}
