<?php
/**
 * @package        Joomla
 * @subpackage     Membership Pro
 * @author         Tuan Pham Ngoc
 * @copyright      Copyright (C) 2012 - 2017 Ossolution Team
 * @license        GNU/GPL, see LICENSE.php
 */

defined('_JEXEC') or die;

class plgOSMembershipDOcuments extends JPlugin
{
	public function __construct(& $subject, $config)
	{
		parent::__construct($subject, $config);
		JTable::addIncludePath(JPATH_ADMINISTRATOR . '/components/com_osmembership/table');
	}

	/**
	 * Render setting form
	 *
	 * @param PlanOSMembership $row
	 *
	 * @return array
	 */
	public function onEditSubscriptionPlan($row)
	{
		ob_start();
		$this->drawSettingForm($row);
		$form = ob_get_clean();

		return array('title' => JText::_('OSM_DOWNLOADS_MANAGER'),
		             'form'  => $form,
		);
	}

	/**
	 * Store setting into database, in this case, use params field of plans table
	 *
	 * @param OSMembershipTablePlan $row
	 * @param bool                  $isNew true if create new plan, false if edit
	 */
	public function onAfterSaveSubscriptionPlan($context, $row, $data, $isNew)
	{
		jimport('joomla.filesystem.folder');

		$db    = JFactory::getDbo();
		$query = $db->getQuery(true);

		//remove old date before save
		if (!$isNew)
		{
			$query->delete('#__osmembership_documents')
				->where('plan_id = ' . (int) $row->id);
			$db->setQuery($query);
			$db->execute();
		}

		//save new data
		if (isset($data['title']))
		{
			$titleDocuments       = $data['document_title'];
			$attachments          = $_FILES['document_attachment'];
			$availableAttachments = $data['document_available_attachment'];
			$orderings            = $data['document_ordering'];
			$pathUpload           = JPATH_ROOT . '/media/com_osmembership/documents/';

			if (!JFolder::exists($pathUpload))
			{
				JFolder::create($pathUpload, 777);
			}

			for ($i = 0; $n = count($titleDocuments), $i < $n; $i++)
			{
				$attachmentsFileName = '';

				if (is_uploaded_file($attachments['tmp_name'][$i]))
				{
					$attachmentsFileName = JFile::makeSafe($attachments['name'][$i]);
					JFile::upload($attachments['tmp_name'][$i], $pathUpload . $attachmentsFileName, false, true);
				}

				$titleDocument = $db->quote($titleDocuments[$i]);
				$ordering      = (int) $orderings[$i];

				if (!$attachmentsFileName)
				{
					$attachmentsFileName = $availableAttachments[$i];
				}

				$attachmentsFileName = $db->quote($attachmentsFileName);

				$query->clear()
					->insert('#__osmembership_documents')
					->columns('plan_id, ordering, title, attachment')
					->values("$row->id,$ordering,$titleDocument,$attachmentsFileName");
				$db->setQuery($query);
				$db->execute();
			}
		}
	}

	/**
	 * Render setting form
	 *
	 * @param JTable $row
	 *
	 * @return array
	 */
	public function onProfileDisplay($row)
	{
		ob_start();
		$this->drawDocuments($row);
		$form = ob_get_contents();
		ob_end_clean();

		return array('title' => JText::_('OSM_MY_DOWNLOADS'),
		             'form'  => $form,
		);
	}

	/**
	 * Display list of files which users can choose for event attachment
	 *
	 * @return array
	 */
	private static function getAttachmentList()
	{
		jimport('joomla.filesystem.folder');

		$path      = JPATH_ROOT . '/media/com_osmembership/documents';
		$files     = JFolder::files($path);
		$options   = array();
		$options[] = JHtml::_('select.option', '', JText::_('OSM_SELECT_DOCUMENT'));

		for ($i = 0, $n = count($files); $i < $n; $i++)
		{
			$file      = $files[$i];
			$options[] = JHtml::_('select.option', $file, $file);
		}

		return $options;
	}

	/**
	 * Display form allows users to change settings on subscription plan add/edit screen
	 *
	 * @param object $row
	 */
	private function drawSettingForm($row)
	{
		$db    = JFactory::getDbo();
		$query = $db->getQuery(true);
		$query->select('*')
			->from('#__osmembership_documents')
			->order('ordering')
			->where('plan_id=' . (int) $row->id)
			->order('ordering');
		$db->setQuery($query);
		$documents = $db->loadObjectList();
		?>
		<div class="row-fluid">
			<table class="adminlist table table-striped" id="adminForm">
				<thead>
				<tr>
					<th class="nowrap center"><?php echo JText::_('OSM_TITLE'); ?></th>
					<th class="nowrap center"><?php echo JText::_('OSM_ORDERING'); ?></th>
					<th class="nowrap center"><?php echo JText::_('OSM_DOCUMENT'); ?></th>
					<th class="nowrap center"><?php echo JText::_('OSM_REMOVE'); ?></th>
				</tr>
				</thead>
				<tbody id="additional_documents">
				<?php
				$options = static::getAttachmentList();
				for ($i = 0; $i < count($documents); $i++)
				{
					$document = $documents[$i];
					?>
					<tr id="document_<?php echo $i; ?>">
						<td><input type="text" class="input-xlarge" name="document_title[]"
						           value="<?php echo $document->title; ?>"/></td>
						<td><input type="text" class="input-mini" name="document_ordering[]"
						           value="<?php echo $document->ordering; ?>"/></td>
						<td><input type="file" name="document_attachment[]"
						           value=""><?php echo JHtml::_('select.genericlist', $options, 'document_available_attachment[]', 'class="input-xlarge"', 'value', 'text', $document->attachment); ?>
						</td>
						<td>
							<button type="button" class="btn btn-danger"
							        onclick="removeDocument(<?php echo $i; ?>)"><i
									class="icon-remove"></i><?php echo JText::_('OSM_REMOVE'); ?></button>
						</td>
					</tr>
					<?php
				}
				?>
				</tbody>
			</table>
			<button type="button" class="btn btn-success" onclick="adddocument()"><i
					class="icon-new icon-white"></i><?php echo JText::_('OSM_ADD'); ?></button>
		</div>
		<script language="JavaScript">
			(function ($) {
				removeDocument = (function (id) {
					if (confirm('<?php echo JText::_('OSM_REMOVE_ITEM_CONFIRM'); ?>')) {
						$('#document_' + id).remove();
					}
				})
				var countDocument = <?php echo count($documents) ?>;
				adddocument = (function () {
					var html = '<tr id="document_' + countDocument + '">'
					html += '<td><input type="text" class="input-medium" name="document_title[]" value="" /></td>';
					html += '<td><input type="text" class="input-mini" name="document_ordering[]" value="" /></td>';
					html += '<td><input type="file" name="document_attachment[]" value=""><?php echo preg_replace(array('/\r/', '/\n/'), '', JHtml::_('select.genericlist', $options, 'document_available_attachment[]', 'class="input-xlarge"', 'value', 'text', '')); ?></td>';
					html += '<td><button type="button" class="btn btn-danger" onclick="removeDocument(' + countDocument + ')"><i class="icon-remove"></i><?php echo JText::_('OSM_REMOVE'); ?></button></td>';
					html += '</tr>';
					$('#additional_documents').append(html);
					countDocument++;
				})
			})(jQuery)
		</script>
		<?php
	}

	/**
	 * Display Display List of Documents which the current subscriber can download from his subscription
	 *
	 * @param object $row
	 */
	private function drawDocuments($row)
	{
		$db            = JFactory::getDbo();
		$query         = $db->getQuery(true);
		$activePlanIds = OSMembershipHelper::getActiveMembershipPlans();
		$query->select('*')
			->from('#__osmembership_documents')
			->order('ordering')
			->where('plan_id IN (' . implode(',', $activePlanIds) . ')')
			->order('plan_id')
			->order('ordering');
		$db->setQuery($query);
		$documents = $db->loadObjectList();

		if (empty($documents))
		{
			return;
		}

		$Itemid = JFactory::getApplication()->input->getInt('Itemid');
		$path   = JPATH_ROOT . '/media/com_osmembership/documents/';
		?>
		<table class="adminlist table table-striped" id="adminForm">
			<thead>
			<tr>
				<th class="title"><?php echo JText::_('OSM_TITLE'); ?></th>
				<th class="title"><?php echo JText::_('OSM_DOCUMENT'); ?></th>
				<th class="center"><?php echo JText::_('OSM_SIZE'); ?></th>
				<th class="center"><?php echo JText::_('OSM_DOWNLOAD'); ?></th>
			</tr>
			</thead>
			<tbody>
			<?php
			for ($i = 0; $i < count($documents); $i++)
			{
				$document     = $documents[$i];
				$downloadLink = JRoute::_('index.php?option=com_osmembership&task=download_document&id=' . $document->id . '&Itemid=' . $Itemid);
				?>
				<tr>
					<td><a href="<?php echo $downloadLink ?>"><?php echo $document->title; ?></a></td>
					<td><?php echo $document->attachment; ?></td>
					<td class="center"><?php echo OSMembershipHelperHtml::getFormattedFilezize($path . $document->attachment); ?></td>
					<td class="center">
						<a href="<?php echo $downloadLink; ?>"><i class="icon-download"></i></a>
					</td>
				</tr>
				<?php
			}
			?>
			</tbody>
		</table>
		<?php
	}
}
