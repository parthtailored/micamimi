<?php
/**
 * $Id: saxumiplogger.php 204 2016-08-09 13:35:25Z Szablac $
 * @Project		Saxum IPLogger Extension/Component
 * @author 		Laszlo Szabo
 * @package		Saxum IPLogger
 * @copyright	Copyright (C) 2010 Saxum 2003 Bt. All rights reserved.
 * @license 	http://www.gnu.org/licenses/old-licenses/gpl-3.0.html GNU/GPL version 3
*/

defined('_JEXEC') or die( 'Restricted access' );

jimport('joomla.plugin.plugin');

class plgUserSaxumiplogger extends JPlugin {

	function onUserLogin($user, $options = array())
	{
		$mainframe = JFactory::getApplication();
		
		$lang = JFactory::getLanguage();
		$lang->load('com_saxumiplogger', JPATH_ADMINISTRATOR);

		$session = JFactory::getSession();
		$db = JFactory::getDBO();
        $usr_id = intval(JUserHelper::getUserId($user['username']));    //fixed for php safe mode by Chris Coleman of espacenetworks.com

        jimport('joomla.application.component.helper');
        $params	= JComponentHelper::getParams('com_saxumiplogger');        
        jimport( 'joomla.access.access' );
        $groups = JAccess::getGroupsByUser($usr_id,false);
		$usergroup_not_to_log=$params->get('usergroup_not_to_log',array());
		$intersect=array_intersect($groups,$usergroup_not_to_log);
        if (!empty($intersect)) return true;
 
 /*       $server_time = gmmktime();
		$date = JFactory::getDate($server_time,JFactory::getConfig()->getValue('offset'));*/
		$date = JFactory::getDate();

		require_once JPATH_ADMINISTRATOR.DIRECTORY_SEPARATOR.'components'.DIRECTORY_SEPARATOR.'com_saxumiplogger'.DIRECTORY_SEPARATOR.'controller.php';
		$ip=SaxumiploggerController::getip();		
		
		$db = JFactory::getDBO();
		$sql = $db->getQuery(true)
			->select('ip')
			->from('#__saxum_iplogger_excp');
		$db->setQuery($sql);
		$iptable = $db->loadColumn();
		$no=SaxumiploggerController::ipinlist($iptable,$ip);
			
		if($no!=1)
		{
			$rowid = $session->get('saxumipid');
			if (!isset($rowid)) {
			    if ($params->get('client_to_log')!=2 and $mainframe->getClientId()==1){
		            $db->setQuery("insert into #__saxum_iplogger (user_id, ip, visitDate, client_id) values (".$usr_id.",'".$ip."',".$db->Quote($date->toSql()).",".$mainframe->getClientId().")");
		            $db->query();
		        }
		        if ($params->get('client_to_log')!=1 and $mainframe->getClientId()==0){
		            $db->setQuery("insert into #__saxum_iplogger (user_id, ip, visitDate, client_id) values (".$usr_id.",'".$ip."',".$db->Quote($date->toSql()).",".$mainframe->getClientId().")");
		            $db->query();
		        }
		        $rowid=$db->insertid();
		        $session->set('saxumipid', $rowid);
			} else {
				
				$db->setQuery("update #__saxum_iplogger set user_id=".$usr_id." where id=".$rowid);
		        $db->query();
			}
			
			if ($params->get('data_collection')==1){
				SaxumiploggerController::location_update( );
			}

			$usergroup_not_to_restrict=$params->get('usergroup_not_to_restrict',array());
			$intersect2=array_intersect($groups,$usergroup_not_to_restrict);
			if (!empty($intersect2)) return true;
			
			JLoader::import('joomla.application.component.model');  
			JLoader::import( 'report', JPATH_ADMINISTRATOR . DIRECTORY_SEPARATOR . 'components' . DIRECTORY_SEPARATOR . 'com_saxumiplogger' . DIRECTORY_SEPARATOR . 'models' );
			$model = JModelLegacy::getInstance( 'report', 'SaxumiploggerModel' );
	    	require_once (JPATH_ADMINISTRATOR.DIRECTORY_SEPARATOR.'components'.DIRECTORY_SEPARATOR.'com_saxumiplogger'.DIRECTORY_SEPARATOR.'controller.php');
			$options['user']=$usr_id;
			
			//Check abuse - daily logins
			$multiple_login=$params->get('multiple_login');
			$mail2user_login=$params->get('mail2user_login');
			$mail2admin_login=$params->get('mail2admin_login');
			$blockuser_login=$params->get('blockuser_login'); 
			if ($multiple_login!=0) {
				$m=$model->getDailyLogins($usr_id);
				$options['num_of_logins']=$m;
				//JFactory::getApplication()->enqueueMessage('$usr_id: '.$usr_id);
				if ($mail2user_login!=0 and $mail2user_login<=$m and $m<$blockuser_login){
					$options['type']='user_login';
					SaxumiploggerController::sendmail($options);
				} 
				if ($mail2admin_login!=0 and $mail2admin_login<=$m){
					$options['type']='admin_login';
					SaxumiploggerController::sendmail($options);
				} 
				if ($blockuser_login!=0 and $blockuser_login<=$m){
					$model->blockUser($usr_id);
					JFactory::getApplication()->enqueueMessage(JText::_('COM_SAXUMIPLOGGER_BLOCKED_MESSAGE'),'error');
					$options['type']='block_login';
					SaxumiploggerController::sendmail($options);
				} 
			}
			//Check abuse - daily IPs
			$multiple_ip=$params->get('multiple_ip');
			$mail2user_ip=$params->get('mail2user_ip');
			$mail2admin_ip=$params->get('mail2admin_ip');
			$blockuser_ip=$params->get('blockuser_ip'); 
			if ($multiple_ip!=0) {
				$m=$model->getDailyIps($usr_id);
				$options['num_of_logins']=$m;
				if ($mail2user_ip!=0 and $mail2user_ip<=$m and $m<$blockuser_ip){
					$options['type']='user_ip';
					SaxumiploggerController::sendmail($options);
				} 
				if ($mail2admin_ip!=0 and $mail2admin_ip<=$m){
					$options['type']='admin_ip';
					SaxumiploggerController::sendmail($options);
				} 
				if ($blockuser_ip!=0 and $blockuser_ip<=$m){
					$model->blockUser($usr_id);
					JFactory::getApplication()->enqueueMessage(JText::_('COM_SAXUMIPLOGGER_BLOCKED_MESSAGE'),'error');
					$options['type']='block_ip';
					SaxumiploggerController::sendmail($options);
				} 
			}
			//Check abuse - daily locations
			$multiple_loc=$params->get('multiple_loc');
			$mail2user_loc=$params->get('mail2user_loc');
			$mail2admin_loc=$params->get('mail2admin_loc');
			$blockuser_loc=$params->get('blockuser_loc'); 
			if ($multiple_loc!=0) {
				$m=$model->getDailyLocs($usr_id);
				$options['num_of_logins']=$m;
				if ($mail2user_loc!=0 and $mail2user_loc<=$m and $m<$blockuser_loc){
					$options['type']='user_location';
					SaxumiploggerController::sendmail($options);
				} 
				if ($mail2admin_loc!=0 and $mail2admin_loc<=$m){
					$options['type']='admin_location';
					SaxumiploggerController::sendmail($options);
				} 
				if ($blockuser_loc!=0 and $blockuser_loc<=$m){
					$model->blockUser($usr_id);
					JFactory::getApplication()->enqueueMessage(JText::_('COM_SAXUMIPLOGGER_BLOCKED_MESSAGE'),'error');
					$options['type']='block_location';
					SaxumiploggerController::sendmail($options);
				} 
			}
		}
				
		return true;
	}

}
